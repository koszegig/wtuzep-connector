<?php

use Illuminate\Database\Seeder;
use App\Role;
use Ramsey\Uuid\Uuid;
use App\User;
use Carbon\Carbon;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::disableQueryLog();

        $developer = ['usergroup','role','permission'];
        $objects = ['products','orders','user','usergroup','role','permission'];
        $actions = ['create','edit','delete','show','menu','list'];
        $roles = ['admin' => ['create','edit','delete','show','menu','list'],'editor' => ['create','edit','show','list'], 'user' => ['show','list']];
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        $permissionarray = [];
        $user = User::where('system',true)->first();
        foreach( $objects as $object){
            foreach( $actions as $action){
                $uuid4 = Uuid::uuid4();
                $time = Carbon::now();
                $permission = [
                    'id' => $uuid4->toString(),
                    'guard_name' => 'web',
                    'name' => "{$action}_{$object}",
                    'slug' => "{$action}.{$object}",
                    'created_at' => $time->toDateTimeString(),
                    'updated_at' => $time->toDateTimeString(),
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                ];
                $permissionarray[] = $permission;
            }
        }

        \DB::table('permissions')->insert($permissionarray);

        foreach( $objects as $object){
            foreach( $roles as $key => $value){
                $dev = in_array($object,$developer);
                $role = Role::create(['name' => "{$object}_{$key}",'slug' => "{$object}.{$key}",'developer' => $dev]);
                foreach($value as $perm)
                    $role->syncPermissions("{$perm}_{$object}");

            }
        }
    }
}
