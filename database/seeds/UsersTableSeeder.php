<?php

use App\User;
use App\UserGroup;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$faker = Faker::create('hu_HU');
        $groups = UserGroup::all();
        foreach ($groups as $group) {
            if ($group->name === 'developer') {
                // Create admin user
                $admin = User::create([
                    'name' => 'Super',
                    'firstname' => 'Super',
                    'lastname' => ' Admin',
                    'username' => 'developer',
                    'password' => md5('hX2s6MGsRwP7zfv4'),
                ]);
                $admin->assignGroup($group->name);
            }
            if ($group->name === 'user') {
                // Create admin user
                $admin = User::create([
                    'name' => 'user',
                    'firstname' => 'user',
                    'lastname' => ' user',
                    'username' => 'user',
                    'password' => md5('teszt'),
                ]);
                $admin->assignGroup($group->name);
            }
        }
    }
}
