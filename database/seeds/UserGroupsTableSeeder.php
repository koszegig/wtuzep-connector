<?php

use Illuminate\Database\Seeder;
use App\UserGroup;
use App\Role;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();
        $groups = ['superadmin','user','support','developer'];
        foreach($groups as $group){

		  $_group = UserGroup::create(['name' => $group]);
          if($group == 'developer'){
            foreach ($roles as $role) {
                $_group->assignRole($role);
            }
          }

          if($group == 'superadmin'){
            foreach ($roles as $role) {
                if($role->developer) continue;
                $_group->assignRole($role);
            }
          }
        }

    }
}
?>
