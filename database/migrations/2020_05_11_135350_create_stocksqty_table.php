<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksqtyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stocksqty');
        Schema::create('stocksqty', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ProductId', 80)->index();
            $table->string('StockId', 80)->nullable();
            $table->string('StockName', 80)->nullable();
            $table->string('FreeStock', 80)->nullable();
            $table->string('ReservedStock', 80)->nullable();
            $table->string('TotalStock', 80)->nullable();
            $table->string('IncommingQuantity', 80)->nullable();
            $table->string('IncommingStockDate', 80)->nullable();
            $table->string('magento_id', 80)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocksqty');
    }
}
