<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('erp_id', 80)->nullable()->index();
            $table->string('magento_id', 80)->nullable();
            $table->string('product_sku', 80)->index();
            $table->string('attribute_id', 80)->index();
            $table->string('name', 255)->nullable();
            $table->string('code', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->index(['erp_id','product_sku']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
