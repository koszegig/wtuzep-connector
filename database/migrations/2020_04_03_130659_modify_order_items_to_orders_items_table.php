<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyOrderItemsToOrdersItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('order_items');
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id', 191)->index();
            $table->string('product_sku', 191)->index();
            $table->text('quantity')->nullable();
           $table->text('item_id')->nullable();
           $table->text('parent_item_id',)->nullable();
           $table->text('quote_item_id',)->nullable();
           $table->text('store_id',)->nullable();
           $table->text('product_id',)->nullable();
           $table->text('product_type',)->nullable();
           $table->text('product_options',)->nullable();
           $table->text('weight',)->nullable();
           $table->text('is_virtual',)->nullable();
           $table->text('sku',)->nullable();
           $table->text('name',)->nullable();
           $table->text('description',)->nullable();
           $table->text('applied_rule_ids',)->nullable();
           $table->text('additional_data',)->nullable();
           $table->text('is_qty_decimal',)->nullable();
           $table->text('no_discount',)->nullable();
           $table->text('qty_backordered',)->nullable();
           $table->text('qty_canceled',)->nullable();
           $table->text('qty_invoiced',)->nullable();
           $table->text('qty_ordered',)->nullable();
           $table->text('qty_refunded',)->nullable();
           $table->text('qty_shipped',)->nullable();
           $table->text('base_cost',)->nullable();
           $table->text('price',)->nullable();
           $table->text('base_price',)->nullable();
           $table->text('original_price',)->nullable();
           $table->text('base_original_price',)->nullable();
           $table->text('tax_percent',)->nullable();
           $table->text('tax_amount',)->nullable();
           $table->text('base_tax_amount',)->nullable();
           $table->text('tax_invoiced',)->nullable();
           $table->text('base_tax_invoiced',)->nullable();
           $table->text('discount_percent',)->nullable();
           $table->text('discount_amount',)->nullable();
           $table->text('base_discount_amount',)->nullable();
           $table->text('discount_invoiced',)->nullable();
           $table->text('base_discount_invoiced',)->nullable();
           $table->text('amount_refunded',)->nullable();
           $table->text('base_amount_refunded',)->nullable();
           $table->text('row_total',)->nullable();
           $table->text('base_row_total',)->nullable();
           $table->text('row_invoiced',)->nullable();
           $table->text('base_row_invoiced',)->nullable();
           $table->text('row_weight',)->nullable();
           $table->text('base_tax_before_discount',)->nullable();
           $table->text('tax_before_discount',)->nullable();
           $table->text('ext_order_item_id',)->nullable();
           $table->text('locked_do_invoice',)->nullable();
           $table->text('locked_do_ship',)->nullable();
           $table->text('price_incl_tax',)->nullable();
           $table->text('base_price_incl_tax',)->nullable();
           $table->text('row_total_incl_tax',)->nullable();
           $table->text('base_row_total_incl_tax',)->nullable();
           $table->text('discount_tax_compensation_amount',)->nullable();
           $table->text('base_discount_tax_compensation_amount',)->nullable();
           $table->text('discount_tax_compensation_invoiced',)->nullable();
           $table->text('base_discount_tax_compensation_invoiced',)->nullable();
           $table->text('discount_tax_compensation_refunded',)->nullable();
           $table->text('base_discount_tax_compensation_refunded',)->nullable();
           $table->text('tax_canceled',)->nullable();
           $table->text('discount_tax_compensation_canceled',)->nullable();
           $table->text('tax_refunded',)->nullable();
           $table->text('base_tax_refunded',)->nullable();
           $table->text('discount_refunded',)->nullable();
           $table->text('base_discount_refunded',)->nullable();
           $table->text('free_shipping',)->nullable();
           $table->text('gift_message_id',)->nullable();
           $table->text('gift_message_available',)->nullable();
           $table->text('weee_tax_applied',)->nullable();
           $table->text('weee_tax_applied_amount',)->nullable();
           $table->text('weee_tax_applied_row_amount',)->nullable();
           $table->text('weee_tax_disposition',)->nullable();
           $table->text('weee_tax_row_disposition',)->nullable();
           $table->text('base_weee_tax_applied_amount',)->nullable();
           $table->text('base_weee_tax_applied_row_amnt',)->nullable();
           $table->text('base_weee_tax_disposition',)->nullable();
           $table->text('base_weee_tax_row_disposition',)->nullable();

            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
            $table->string('compliance',)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
