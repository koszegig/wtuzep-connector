<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('partner_id', 191)->index();
            $table->string('magento_id', 191)->index();
            $table->string('vat_number', 80)->nullable()->index();
            $table->string('name', 80)->nullable()->index();
            $table->text('zip_code', 30)->nullable();
            $table->text('email', 255)->nullable();
            $table->string('city', 80)->nullable();
            $table->string('street', 200)->nullable();
            $table->string('number', 80)->nullable();
            $table->text('region', 255)->nullable();
            $table->text('country', 255)->nullable();
            $table->text('payment_type', 15)->nullable();
            $table->integer('vedett')->default(0);
            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
            //$table->index(['partner_id','product_sku','category_magento_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
