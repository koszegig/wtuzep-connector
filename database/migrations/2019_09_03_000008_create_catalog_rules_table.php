<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('erp_id',15)->index();
            $table->string('partner_id', 191)->index();
            $table->string('product_sku', 80)->index();
            $table->string('erpcat_code', 80)->nullable();
            $table->text('special_price', 10)->nullable();
            $table->string('discount_percent', 80)->nullable();
            $table->string('special_from_date', 191)->nullable();
            $table->string('special_to_date', 191)->nullable();
            $table->text('quantity_unit', 15)->nullable();
            $table->string('quantity', 10)->nullable();
            $table->string('label', 80)->nullable();
            $table->string('type', 10)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_rules');
    }
}
