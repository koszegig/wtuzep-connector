<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductgroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productgroups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('magento_id')->default(0);
            $table->string('cikkcsoportkod', 191)->nullable();//-	Cikkcsoport kódja
            $table->text('cikkcsopormegnevezes', 191)->nullable();//-	Cikkcsoport megnevezése
            $table->string('cikkcsoportszulokod', 191)->nullable();//Cikkcsoport szülőcsoport kódja
            $table->string('cikkcsoportszintje', 191)->nullable();//CCikkcsoport szintje
            $table->integer('parent_id')->default(0);
            $table->integer('active')->default(1);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->integer('syncedorder')->default(-1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productgroups');
    }
}
