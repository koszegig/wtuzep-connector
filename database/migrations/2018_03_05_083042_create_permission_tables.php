<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('permissions', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('guard_name');
            $table->string('slug');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->uuid('created_by')->index()->nullable();
            $table->uuid('updated_by')->index()->nullable();
            $table->primary('id');
           // $table->foreign('created_by')->references('id')->on('users');
           // $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('guard_name');
            $table->string('slug');
            $table->boolean('active')->default(true);
            $table->boolean('developer')->default(false);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->uuid('created_by')->index()->nullable();
            $table->uuid('updated_by')->index()->nullable();
            $table->primary('id');
            //$table->foreign('created_by')->references('id')->on('users');
            ///$table->foreign('updated_by')->references('id')->on('users');
        });
        Schema::create('model_has_permissions', function (Blueprint $table) {
            $table->uuid('permission_id')->index();
            $table->uuid('model_id');
            $table->string('model_type');


            $table->primary(['permission_id', 'model_id', 'model_type']);
        });

        Schema::create('model_has_roles', function (Blueprint $table) {
            $table->uuid('role_id')->index();
            $table->uuid('model_id');
            $table->string('model_type');


            $table->primary(['role_id', 'model_id', 'model_type']);
        });

        Schema::create('role_has_permissions', function (Blueprint $table) {
            $table->uuid('permission_id')->index();
            $table->uuid('role_id')->index();

            $table->primary(['permission_id', 'role_id']);

            app('cache')->forget('spatie.permission.cache');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_has_permissions');
        Schema::dropIfExists('model_has_roles');
        Schema::dropIfExists('model_has_permissions');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('permissions');

    }
}
