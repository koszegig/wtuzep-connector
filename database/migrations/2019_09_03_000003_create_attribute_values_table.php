<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attribute_id', 191)->index();
            $table->string('type')->nullable();
            $table->string('label', 255)->nullable();
            $table->string('value', 255)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_values');
    }
}
