<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableArticlenumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articlenumbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cikkszam', 50)->nullable();//-	Cikkszám
            $table->string('mertekegyseg', 191)->nullable();//-	Mértékegység kódja
            $table->integer('valtoszam')->default(0);//Váltószám a cikk nyilvántartási egységéhez képest
            $table->integer('enabled')->default(1);
            $table->integer('active')->default(1);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->integer('syncedorder')->default(-1);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articlenumbers');
    }
}
