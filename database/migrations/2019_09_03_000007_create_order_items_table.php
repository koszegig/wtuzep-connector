<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id', 191)->index();
            $table->string('product_sku', 80)->index();
            $table->text('price', 15)->nullable();
            $table->text('quantity', 15)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
            $table->string('compliance', 80)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
