<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_type', 80)->index();
            $table->string('start', 191)->nullable();
            $table->string('lastTick', 191)->nullable();
            $table->string('stop', 191)->nullable();
            $table->integer('new')->default(0);
            $table->integer('refresh')->default(0);
            $table->integer('PID')->default(0);
            $table->integer('statusCode')->default(0);
            $table->integer('stopme')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
}
