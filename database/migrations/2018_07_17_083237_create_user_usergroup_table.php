<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserUsergroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaName ='01_sys';
        Schema::create('user_usergroup', function (Blueprint $table) {
            $table->uuid('user_id')->index();
            $table->uuid('group_id')->index();
            $table->primary(['user_id', 'group_id']);
         //   $table->foreign('user_id')->references('id')->on('users');
         //   $table->foreign('group_id')->references('id')->on('user_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_usergroup');
    }
}
