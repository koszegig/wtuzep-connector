<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMagentotoken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magentotoken', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 80)->index();
            $table->text('rp_token', 10)->nullable();
            $table->string('until', 191)->nullable();
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magentotoken');
    }
}
