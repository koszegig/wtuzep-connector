<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_head', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id', 191)->index();
            $table->string('partner_id', 30)->nullable()->index();
            $table->text('order_status', 15)->nullable();
            $table->string('ordered_at', 150)->nullable();
            $table->string('shipping_date', 150)->nullable();
            $table->string('tracking_code',25)->nullable();
            $table->text('compliance',65355)->nullable();
            $table->integer('privacy_policy')->default(0);
            $table->integer('newsletter')->default(0);
            $table->integer('terms')->default(0);
            $table->integer('synced')->default(0);
            $table->string('created_at', 30)->default(0);
            $table->string('updated_at', 30)->default(0);
            //$table->index(['partner_id','order_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_head');
    }
}
