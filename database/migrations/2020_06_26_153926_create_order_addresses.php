<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('order_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id')->unsigned();
            $table->string('address_type', 191)->index();
            $table->string('city', 191)->nullable();
            $table->string('company', 191)->nullable();
            $table->string('country_id', 80)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('entity_id', 80)->nullable();
            $table->string('firstname', 80)->nullable();
            $table->string('lastname', 80)->nullable();
            $table->string('middlename', 80)->nullable();
            $table->string('parent_id', 80)->nullable();
            $table->string('postcode', 80)->nullable();
            $table->string('prefix', 80)->nullable();
            $table->string('street', 80)->nullable();
            $table->string('region', 80)->nullable();
            $table->string('region_code', 80)->nullable();
            $table->string('region_id', 80)->nullable();
            $table->string('suffix', 80)->nullable();
            $table->string('telephone', 80)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addresses');
    }
}
