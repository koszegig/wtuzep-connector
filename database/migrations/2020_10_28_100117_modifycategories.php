<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Modifycategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('categories');
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('erp_id', 191)->nullable();
            $table->string('erp_partent_id')->nullable();
            $table->string('category_magento_id', 80)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('position', 255)->nullable();
            $table->integer('level')->default(0);
            $table->integer('parent_id')->default(0);
            $table->integer('status')->default(0);
            $table->integer('synced')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            //$table->index(['erp_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
