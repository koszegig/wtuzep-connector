<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ProductId')->nullable();
            $table->integer('erp_id')->nullable();
            $table->string('WebShopId', 80)->nullable();
            $table->string('ProductCode', 80)->nullable();
            $table->string('ItemNumber', 80)->index()->nullable();
            $table->string('Name', 80)->nullable()->nullable();
            $table->string('QuantityUnits', 80)->nullable();
            $table->string('DefaultQuantityUnit', 80)->nullable();
            $table->string('VatCode', 80)->nullable();
            $table->string('VatRate', 80)->nullable();
            $table->string('UnitPrice', 80)->nullable();
            $table->string('DiscountPrice', 80)->nullable();
            $table->text('Comment', 65535)->nullable();
            $table->string('Category', 80)->nullable();
            $table->string('Service', 80)->nullable();
            $table->string('GuaranteeDays', 80)->nullable();
            $table->string('MinimumQuantity', 80)->nullable();
            $table->string('MaximumQuantity', 80)->nullable();
            $table->string('OptimalQuantity', 80)->nullable();
            $table->string('Barcode', 80)->nullable();
            $table->string('QuantityDigits', 80)->nullable();
            $table->string('Recent', 80)->nullable();
            $table->string('WebMinOrderQuantity', 80)->nullable();
            $table->string('WebMaxOrderQuantity', 80)->nullable();
            $table->string('IncommingStockDate', 80)->nullable();
            $table->string('IncommingQuantity', 80)->nullable();
            $table->string('WebProductUrl', 80)->nullable();
            $table->string('LastPurchasePrice', 80)->nullable();
            $table->integer('total_qty')->default(0);
            $table->integer('store_view')->default(0);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
			//$table->index(['erp_id','ItemNumber']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
