<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExportShipmentMagento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id', 191)->index();
            $table->string('order_id')->nullable();
            $table->string('product_qty')->nullable();
            $table->string('delivery_number')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('created_at')->nullable();
            $table->string('synced')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment');
    }
}
