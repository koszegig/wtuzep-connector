<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cikkszam', 50)->nullable();//-	Cikkszám
            $table->string('cikknev', 191)->nullable();//-	Cikknév
            $table->string('mertekegyseg', 191)->nullable();//-	Nyilvántartási mennyiségi egység kódja
            $table->string('beszmertekegyseg', 191)->nullable();//-	Beszerzési mennyiségi egység kódja
            $table->string('afa', 191)->nullable();//-	Eladási Áfa százalék
            $table->integer('bonthato')->default(0);//Bonthatóság
            $table->string('kiszereles', 80)->nullable();//Kiszerelés
            $table->text('megjegyzes')->nullable();//Megjegyzés
            $table->integer('enabled')->default(1);
            $table->integer('active')->default(1);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->integer('syncedorder')->default(-1);
        });
    }






    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
