<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentShippingAndCommentToOrderHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_head', function (Blueprint $table) {
            //
            $table->string("payment_method",200)->nullable();
            $table->string("shipping_method",200)->nullable();

            $table->text("delivery_instructions")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_head', function (Blueprint $table) {
            //

        });
    }
}
