<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyOrdersHeadToOrdersHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders_head');
        Schema::create('orders_head', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id', 191)->index();
            $table->string('partner_id', 30)->nullable()->index();
            $table->string('state',25)->nullable();
            $table->string('status',25)->nullable();
            $table->string('coupon_code',80)->nullable();
            $table->string('protect_code',80)->nullable();
            $table->string('shipping_description',80)->nullable();
            $table->integer('is_virtual')->default(0);
            $table->integer('store_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('base_discount_amount',80)->nullable();
            $table->string('base_discount_canceled',80)->nullable();
            $table->string('base_discount_invoiced',80)->nullable();
            $table->string('base_discount_refunded',80)->nullable();
            $table->string('base_grand_total',80)->nullable();
            $table->string('base_shipping_amount',80)->nullable();
            $table->string('base_shipping_canceled',80)->nullable();
            $table->string('base_shipping_invoiced',80)->nullable();
            $table->string('base_shipping_refunded',80)->nullable();
            $table->string('base_shipping_tax_amount',80)->nullable();
            $table->string('base_shipping_tax_refunded',80)->nullable();
            $table->string('base_subtotal',80)->nullable();
            $table->string('base_subtotal_canceled',80)->nullable();
            $table->string('base_subtotal_invoiced',80)->nullable();
            $table->string('base_subtotal_refunded',80)->nullable();
            $table->string('base_tax_amount',80)->nullable();
            $table->string('base_tax_canceled',80)->nullable();
            $table->string('base_tax_invoiced',80)->nullable();
            $table->string('base_tax_refunded',80)->nullable();
            $table->string('base_to_global_rate',80)->nullable();
            $table->string('base_to_order_rate',80)->nullable();
            $table->string('base_total_canceled',80)->nullable();
            $table->string('base_total_invoiced',80)->nullable();
            $table->string('base_total_invoiced_cost',80)->nullable();
            $table->string('base_total_offline_refunded',80)->nullable();
            $table->string('base_total_online_refunded',80)->nullable();
            $table->string('base_total_paid',80)->nullable();
            $table->string('base_total_qty_ordered',80)->nullable();
            $table->string('base_total_refunded',80)->nullable();
            $table->string('discount_amount',80)->nullable();
            $table->string('discount_canceled',80)->nullable();
            $table->string('discount_invoiced',80)->nullable();
            $table->string('discount_refunded',80)->nullable();
            $table->string('grand_total',80)->nullable();
            $table->string('shipping_amount',80)->nullable();
            $table->string('shipping_canceled',80)->nullable();
            $table->string('shipping_invoiced',80)->nullable();
            $table->string('shipping_refunded',80)->nullable();
            $table->string('shipping_tax_amount',80)->nullable();
            $table->string('shipping_tax_refunded',80)->nullable();
            $table->string('store_to_base_rate',80)->nullable();
            $table->string('store_to_order_rate',80)->nullable();
            $table->string('subtotal',80)->nullable();
            $table->string('subtotal_canceled',80)->nullable();
            $table->string('subtotal_invoiced',80)->nullable();
            $table->string('subtotal_refunded',80)->nullable();
            $table->string('tax_amount',80)->nullable();
            $table->string('tax_canceled',80)->nullable();
            $table->string('tax_invoiced',80)->nullable();
            $table->string('tax_refunded',80)->nullable();
            $table->string('total_canceled',80)->nullable();
            $table->string('total_invoiced',80)->nullable();
            $table->string('total_offline_refunded',80)->nullable();
            $table->string('total_online_refunded',80)->nullable();
            $table->string('total_paid',80)->nullable();
            $table->string('total_qty_ordered',80)->nullable();
            $table->string('total_refunded',80)->nullable();
            $table->string('can_ship_partially',80)->nullable();
            $table->string('can_ship_partially_item',80)->nullable();
            $table->string('customer_is_guest',80)->nullable();
            $table->string('customer_note_notify',80)->nullable();
            $table->string('billing_address_id',80)->nullable();
            $table->string('customer_group_id',80)->nullable();
            $table->string('edit_increment',80)->nullable();
            $table->string('email_sent',80)->nullable();
            $table->string('send_email',80)->nullable();
            $table->string('forced_shipment_with_invoice',80)->nullable();
            $table->string('payment_auth_expiration',80)->nullable();
            $table->string('quote_address_id',80)->nullable();
            $table->string('quote_id',80)->nullable();
            $table->string('shipping_address_id',80)->nullable();
            $table->string('adjustment_negative',80)->nullable();
            $table->string('adjustment_positive',80)->nullable();
            $table->string('base_adjustment_negative',80)->nullable();
            $table->string('base_adjustment_positive',80)->nullable();
            $table->string('base_shipping_discount_amount',80)->nullable();
            $table->string('base_subtotal_incl_tax',80)->nullable();
            $table->string('base_total_due',80)->nullable();
            $table->string('payment_authorization_amount',80)->nullable();
            $table->string('shipping_discount_amount',80)->nullable();
            $table->string('subtotal_incl_tax',80)->nullable();
            $table->string('total_due',80)->nullable();
            $table->string('weight',80)->nullable();
            $table->string('customer_dob',80)->nullable();
            $table->string('increment_id',80)->nullable();
            $table->string('applied_rule_ids',80)->nullable();
            $table->string('base_currency_code',80)->nullable();
            $table->string('customer_email',80)->nullable();
            $table->string('customer_firstname',80)->nullable();
            $table->string('customer_lastname',80)->nullable();
            $table->string('customer_middlename',80)->nullable();
            $table->string('customer_prefix',80)->nullable();
            $table->string('customer_suffix',80)->nullable();
            $table->string('customer_taxvat',80)->nullable();
            $table->string('discount_description',80)->nullable();
            $table->string('ext_customer_id',80)->nullable();
            $table->string('ext_order_id',80)->nullable();
            $table->string('global_currency_code',80)->nullable();
            $table->string('hold_before_state',80)->nullable();
            $table->string('hold_before_status',80)->nullable();
            $table->string('order_currency_code',80)->nullable();
            $table->string('original_increment_id',80)->nullable();
            $table->string('relation_child_id',80)->nullable();
            $table->string('relation_child_real_id',80)->nullable();
            $table->string('relation_parent_id',80)->nullable();
            $table->string('relation_parent_real_id',80)->nullable();
            $table->string('remote_ip',80)->nullable();
            $table->string('shipping_method',80)->nullable();
            $table->string('store_currency_code',80)->nullable();
            $table->string('x_forwarded_for',80)->nullable();
            $table->string('customer_note',80)->nullable();
            $table->string('total_item_count',80)->nullable();
            $table->string('customer_gender',80)->nullable();
            $table->string('discount_tax_compensation_amount',80)->nullable();
            $table->string('base_discount_tax_compensation_amount',80)->nullable();
            $table->string('shipping_discount_tax_compensation_amount',80)->nullable();
            $table->string('base_shipping_discount_tax_compensation_amnt',80)->nullable();
            $table->string('discount_tax_compensation_invoiced',80)->nullable();
            $table->string('base_discount_tax_compensation_invoiced',80)->nullable();
            $table->string('discount_tax_compensation_refunded',80)->nullable();
            $table->string('base_discount_tax_compensation_refunded',80)->nullable();
            $table->string('shipping_incl_tax',80)->nullable();
            $table->string('base_shipping_incl_tax',80)->nullable();
            $table->string('coupon_rule_name',80)->nullable();
            $table->string('paypal_ipn_customer_notified',80)->nullable();
            $table->string('gift_message_id',80)->nullable();
            $table->string('status_label',80)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 30)->default(0);
            $table->string('updated_at', 30)->default(0);
            //$table->index(['partner_id','order_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_head');
    }
}
