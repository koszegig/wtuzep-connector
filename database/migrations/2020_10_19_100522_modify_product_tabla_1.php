<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyProductTabla1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cikkszam', 50)->nullable();//-	Cikkszám
            $table->string('cikknev', 191)->nullable();//-	Cikknév
            $table->integer('mertekegyseg')->nullable();//-	Nyilvántartási mennyiségi egység kódja
            $table->integer('beszmertekegyseg')->nullable();//-	Beszerzési mennyiségi egység kódja
            $table->float('afa')->nullable();//-	Eladási Áfa százalék
            $table->string('bonthato', 191)->nullable();//Bonthatóság
            $table->float('kiszereles')->nullable();//Kiszerelés
            $table->text('megjegyzes')->nullable();//Megjegyzés
            $table->integer('enabled')->default(1);
            $table->integer('active')->default(1);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->integer('syncedorder')->default(-1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
