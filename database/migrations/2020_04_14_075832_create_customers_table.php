<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('magento_id', 191)->index();
            $table->string('vat_number', 80)->nullable()->index();
            $table->string('name', 80)->nullable()->index();
            $table->text('billing_zip_code', 30)->nullable();
            $table->text('email', 255)->nullable();
            $table->string('billing_city', 80)->nullable();
            $table->string('billing_street', 200)->nullable();
            $table->string('billing_number', 80)->nullable();
            $table->text('shipping_zip_code', 30)->nullable();
            $table->string('shipping_city', 80)->nullable();
            $table->string('shipping_street', 200)->nullable();
            $table->string('shipping_number', 80)->nullable();
            $table->integer('synced')->default(0);
            $table->string('created_at', 40)->default(0);
            $table->string('updated_at', 40)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
