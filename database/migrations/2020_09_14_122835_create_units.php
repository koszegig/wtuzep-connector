<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('magento_id')->default(0);
            $table->string('erp_id', 191)->nullable();//-	Mértékegység kódja
            $table->text('megnevezes', 191)->nullable();//-	Mértékegység megnevezése
            $table->string('rovidnev', 191)->nullable();//Mértékegység rövid neve
            $table->integer('active')->default(1);
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
            $table->integer('syncedorder')->default(-1);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
