<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTimestampColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        foreach ([
            'partners',
            'attribute_values',
            'attributes',
            'catalog_rules',
            'categiries',
            'emails',
            'order_items',
            'order_head',
            'phones',
            'product_attributes',
            'products',
            'shipment'
            ] as $name) {
            echo "$name \n";
            try {
                Schema::table($name, function (Blueprint $table) {
                    $table->dropColumn('created_at');
                });
            } catch (Exception $e) {
                echo "Exception created_at\n";
                continue;
            }

            try {
                Schema::table($name, function (Blueprint $table) {
                    $table->dropColumn('updated_at');
                });
            } catch (Exception $e) {
                echo "Exception updated_at\n";
                continue;
            }

            Schema::table($name, function (Blueprint $table) {
                //
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            //
        });
    }
}
