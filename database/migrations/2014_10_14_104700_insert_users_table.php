<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         \DB::table('users')->insert(
            ['id' => \MyHash::genUUID(),
             'system' => true,
             'firstname' => 'System',
             'lastname' => ' System',
             'username' => 'System',
             'created_at' => \MyDate::now()->toDateTimeString(),
             'updated_at' =>  \MyDate::now()->toDateTimeString()]);
    }
}
