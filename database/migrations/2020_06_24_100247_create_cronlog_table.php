<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCronlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronlog', function (Blueprint $table) {
                $table->increments('id');
                $table->string('task_type', 80)->index();
                $table->text('message')->nullable();
                $table->string('channel')->nullable();
                $table->integer('level')->default(0);
                $table->string('level_name', 20);
                $table->integer('unix_time');
                $table->string('datetime')->nullable();
                $table->longText('context')->nullable();
                $table->text('extra')->nullable();
                $table->string('created_at', 191)->nullable();
                $table->string('updated_at', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronlog');
    }
}
