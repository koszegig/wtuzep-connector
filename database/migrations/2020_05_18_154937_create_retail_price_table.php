<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetailPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retail_price', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id', 80)->index();
            $table->string('sku', 80)->nullable();
            $table->string('magento_id', 80)->nullable();
            $table->string('erp_id', 80);
            $table->string('tier_price_website', 80)->nullable();
            $table->string('tier_price_customer_group', 80)->nullable();
            $table->string('tier_price_qty', 80)->nullable();
            $table->string('tier_price', 80)->nullable();
            $table->string('tier_price_value_type', 80)->nullable();
            $table->string('created_at', 191)->nullable();
            $table->string('updated_at', 191)->nullable();
            $table->integer('synced')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retail_price');
    }
}
