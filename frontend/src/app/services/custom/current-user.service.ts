import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class CurrentUserService {

  @Output() currentUser: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.reload();
  }

  reload() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser.emit(currentUser);
  }

  getEmittedValue() {
    return this.currentUser;
  }

}
