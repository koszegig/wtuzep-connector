import { Injectable } from '@angular/core';
import { MainrestService} from '../../services/base/mainrest.service';
@Injectable({
  providedIn: 'root'
})
export class MenuparameterekService  extends MainrestService {
  protected controller:string  = 'rendszer';
  loading = false;
  public rows = [];
  public   constructor() {
    super();
   }

  public getMenuparameterek(menu_id){
    return this.get('menuparameterek',[menu_id],false);
  }
  public getParameter(par_megnevezes: string): string {
    // private perform translation
    let parameter = '';
    this.rows.forEach(function (item, index) {
      if (item.menuprm_name.toString() == par_megnevezes.toString()) {

        parameter = item.menuprm_value;

      }
    });

   /* if (this._translations[this.currentLang] && this._translations[this.currentLang][modul] && this._translations[this.currentLang][modul][key]) {
        return this._translations[this.currentLang][modul][key];
    }
*/
    return parameter;
}

}
