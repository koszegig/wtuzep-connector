import { Injectable } from '@angular/core';
import { LogHelper } from '../../utility/log_helper';
//import { AclService } from 'ng2-acl/dist';
import 'rxjs/add/operator/map';
import { MainrestService} from '../base/mainrest.service';

@Injectable()
export class AuthenticationService extends MainrestService  {

    protected controller: string  = 'auth';

    constructor(
        /*private aclService: AclService*/)
    {
        super(); }

    login(username: string, password: string) {
        const logindata = JSON.stringify({ username: username, password: password });
        return this.post('login', [], logindata, false, 'storeUser');
                // login successful if there's a jwt token in the response
    }

    storeUser(response){
        const data = response.json();
        const user = data.data.user;
        user.token = data.data.token;
        user.menu = data.data.menus; 
        user.felhasznalo = data.data.felhasznalo;
        user.cegadatok = data.data.cegadatok;
        user.parameters = data.data.parameters;
        user.parametervalues = data.data.parametervalues;
        user.raktar = data.data.raktar;
        user.blokk = data.data.blokk;
        user.cegcimek = data.data.cegcimek;
        user.cegbankszamlaszamok = data.data.cegbankszamlaszamok;
        user.kassza = data.data.kassza;
        const abilities = data.data.abilities;
        const userRole = data.data.userRole;
        LogHelper.logging(user, 'user', 'storeUser');
   /*     this.aclService.setAbilities(abilities);
        this.attachrole(userRole);*/
        if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            //localStorage.setItem('abilities', JSON.stringify(abilities));
            //localStorage.setItem('userRole', JSON.stringify(userRole));
        }

        return user;
    }


  /*  attachrole(roles){
        this.aclService.flushRoles();
        roles.forEach(item => {this.aclService.attachRole(item);
        })
    }*/

    logout() {
       // this.aclService.flushRoles();
        localStorage.removeItem('currentUser');
    }
}
