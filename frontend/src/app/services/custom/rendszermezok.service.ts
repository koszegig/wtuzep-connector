import { Injectable } from '@angular/core';
import { MainrestService} from '../../services/base/mainrest.service';
@Injectable({
  providedIn: 'root'
})
export class RendszermezokService extends MainrestService {
  protected controller = 'rendszer';
  protected mezoLeirasok;
  private _rows;
  constructor() {
    super();
  }
  private getMezoLeirasok(p_table_named) {
    return this.get('mezoleirasok', [p_table_named], false);
  }
 


  public instant(p_table_named: string, _rows : any) {
    //this.logging(p_table_named,'p_table_named','instant','35');
    if (!p_table_named) return [];
    this.getMezoLeirasok(p_table_named)
      .subscribe(
        data => {
          this._rows = data.data;
          this.logging(this.rows.get_json_getfields, 'this.rows', 'instant');
          return this.rows;
        },
        error => {
          //  this.alertService.error(error);
        });

  }
  /**
     * Getter row
     * @return {Row}
  */
  public get rows():any {
      return this._rows;
  }
  public getCaption(fieldname: string): string {
    let translation = fieldname;
    this.rows.forEach(function (item, index) {
      if (item.datafield == fieldname) {
        translation = item.caption;
      }
    });
    return translation;
  }
}
