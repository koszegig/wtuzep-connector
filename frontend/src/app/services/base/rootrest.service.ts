import { Injectable } from '@angular/core';
import { Router} from '@angular/router';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {RootClass} from '../../models/RootClass';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RootrestService extends RootClass{
  protected controller: string;
  protected cu: any;
  protected router: Router;
  protected http: Http;

  constructor() {
    super();
    try {
      this.router = this.injector.get(Router);
      this.http = this.injector.get(Http);
    } catch (e) {
    }
  }

  get(action = null, params = [], auth = true, withoutmap = false, options: any = null) {
    const url = this.generateurl(action, params);
    const __response = this.http.get(url, this.setheaders(auth, options));
    if (withoutmap) return __response;
    return this.mapResponse(__response);
  }

  post(action = null, params = [], data: any, auth = true, callback = 'convertResponse') {
    this.logging('lefutittis', 'testszoveg', 'rootrest.post');
    const url = this.generateurl(action, params);
    this.logging(url, 'url', 'rootrest.post');
    this.logging(data, 'data', 'rootrest.post');
    this.logging(params, 'params', 'rootrest.post');
    const __response = this.http.post(url, data, this.setheaders(auth));
    return this.mapResponse(__response, callback);
  }

  put(action = null, params = [], data: any, auth = true) {
    const url = this.generateurl(action, params);
    const __response = this.http.put(url, data, this.setheaders(auth));
    return this.mapResponse(__response);
  }

  delete(action = null, params = [], auth = true) {
    const url = this.generateurl(action, params);
    const __response = this.http.delete(url, this.setheaders(auth));
    return this.mapResponse(__response);
  }

  mapResponse(__response, callback = 'convertResponse'){
    return __response.map((response: Response) => this[callback](response)).catch(this.handleError.bind(this));
  }

  protected handleError(error: Response) {
    const data = error.json();
    if (this.constructor.name != 'AuthenticationService')
        switch (data.data) {
            case 'Token is Invalid':
            case 'Token is expired':
            case 'Something is wrong': {
                localStorage.removeItem('currentUser');
                const url = this.router.url;
                this.router.navigate(['/login'], { queryParams: { returnUrl: url }});
                break;
            }
        }
    const errors = this._.isArray(data.data) ? this._.values(data.data) : [data.data];
    return Observable.throw(errors || 'Server error');
  }

  protected convertResponse(response){
      const data = response.json();
      return data;
  }

  protected generateurl(action, params){
    let url = this.environment.apiUrl + this.controller;
    url += (this._.isNull(action)) ? '/' : '/' + action + '/';
    if (!this._.isEmpty(params)){
      url += params.join('/');
      url += '/';
    }
    url = this.__.trimEnd(url, '/');
    return url;

  }

  protected setheaders(auth = true, params: any = null){
    const headerobj = { 'Content-Type': 'application/json'};
    if (auth){
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser && this.currentUser.token) {
          headerobj['Authorization'] =  'Bearer ' + this.currentUser.token;
        }
    }
    const headers = new Headers(headerobj);
    const options = { headers: headers };
    if (!this._.isEmpty(params))
      options['params'] = params.generateParams(params);
    const reqopt = new RequestOptions(options);
    return reqopt;

  }
}
