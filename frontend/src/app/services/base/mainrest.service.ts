
import { RootrestService } from './rootrest.service';

export class MainrestService extends RootrestService{
    constructor() {
          super();
    }

    activate(id: string) {
        return this.get('activate', [id]);
    }

    inactivate(id: string) {
        return this.get('inactivate', [id]);
    }

    list(params = null) {
        //this.logging(params.apiparams, 'params', 'mainrest.list');
        let vals = [];
        let apiparams : {
             size: number,
             total: number,
             totalPages: number,
             pageNumber: number,
             from: number,
             to: number,
             offset : number
        } ;
        if (! this._.isNull(params)) {
          vals = [params.apiparams.page.size];
          apiparams = params.apiparams.page;
          apiparams.offset = params.apiparams.page.size*params.apiparams.page.pageNumber;
          apiparams.to = params.apiparams.page.to;
        }
        return this.post('list', [], {filters : params.columns.filters,api : apiparams});
    }

    listDn(data: any = {}) {
      return this.post('list/dropdown', [], data, true);
    }

    getAll() {
      return this.get([], [], true, false);
    }

    getById(id: string) {
        return this.get(null, [id]);
    }

    create(data: any) {
        return this.post(null, [], data);
    }

    update(data: any) {
        return this.put(null, [data.id], data);
    }

    destroy(id: string) {
        return this.delete(null, [id]);
    }
}
