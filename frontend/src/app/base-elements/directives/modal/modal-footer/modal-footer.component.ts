﻿import { Component} from '@angular/core';
import { AdminRootComponent } from '../../../../admin/admin-base/admin-root/admin-root.component';

@Component({
  selector: 'modal-footer',
  templateUrl: './modal-footer.component.html',
})

export class ModalFooterComponent extends AdminRootComponent {

  constructor() {
    super();
  }
}
