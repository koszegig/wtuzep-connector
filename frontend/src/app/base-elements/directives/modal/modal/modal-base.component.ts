﻿import { Component, Input, ContentChild, TemplateRef, ElementRef, EventEmitter, Output, ChangeDetectorRef} from '@angular/core';
import { AdminRootComponent } from '../../../../admin/admin-base/admin-root/admin-root.component';
import { ModalService} from '../../../services/modal.service';

@Component({
  selector: 'base-modal',
  templateUrl: './modal-base.component.html',
  styleUrls: ['./modal-base.component.css'],
})

export class ModalBaseComponent extends AdminRootComponent {
  protected modalService: ModalService;
  display = false;
  public domid = null;
  modalcheck = false;
  public innerHeight: any;
  contentMaxHeight: any = 'auto';
  modalData: any;
  innerDivMaxHeight: any = 'auto';
  @Input('csswidth') csswidth: number = null;

  @Input('isLoading') loading: boolean = false;
  @Input('title') title: any;
  @Input('params') params: any;
  @Input('maxHeight') maxHeight: boolean = false;
  //@ContentChild('modal') modal: TemplateRef<any>;//koszegi
  @ContentChild('modal', { static: false } ) modal: TemplateRef<any>;
  @Output() CallModalCBEmmitter = new EventEmitter<any>();
  @Output() CallModalOpenCBEmmitter = new EventEmitter<any>();


  constructor(private el: ElementRef, protected cdRef: ChangeDetectorRef) {
    super();
    try {
      this.modalService = this.injector.get(ModalService);
    } catch (e) {
    }
    this.domid = this.uuid();
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
  }


  ngAfterViewChecked() {
      if (this.modalcheck){
        this.modalcheck = false;
        this.openModalCb();
        this.modalData = this.getModalBodyOffsetHeight();
        this.setClass();
        this.contentMaxHeight = this.setMaxHeight();
        this.setInfopanelMaxHeight();
      }
      this.detectChanges();
  }

  openModal(){
    this.logging(this.modalService, 'this.modalService', 'ModalBaseComponent-openModal');
    this.display = true;
    this.modalService.open(this.domid);
    this.modalcheck = true;
  }

  closeModal(){
      this.modalService.close(this.domid);
      this.display = false;
      this.closeModalCb();
  }

  closeModalCb(){
    if (!this._.isUndefined(this.CallModalCBEmmitter))
      this.CallModalCBEmmitter.emit();
  }

  openModalCb(){
    if (!this._.isUndefined(this.CallModalOpenCBEmmitter)){
      this.CallModalOpenCBEmmitter.emit();
    }
  }


  getModalBodyOffsetHeight(){
    const element = this.modalService.findModal(this.domid);
    return {
      contentHeight : element.getElement().find('.content').height(),
      maxBoxesHeight : this.setMaxBoxesHeight(element),
      contentHeaderHeight : element.getElement().find('.content-header').height(),
      contentTopPadding : parseInt(element.getElement().find('.content').css('paddingTop')),
      contentBottomPadding : parseInt(element.getElement().find('.content').css('paddingBottom')),
      modaltop : element.getElement().find('.modal').position(),
      modalHeaderHeight : element.getElement().find('.modal-header').height(),
      modalHeaderTopMargin : parseInt(element.getElement().find('.modal-header').css('marginTop')),
      bodyTopPadding : parseInt(element.getElement().find('.modal-body').css('paddingTop')),
      bodyBottomPadding : parseInt(element.getElement().find('.modal-body').css('paddingBottom'))
    };
  }

  setMaxHeight(){
    const innerHeight = this.innerHeight - (this.__.get(this.modalData.modaltop, 'top', 0) * 2) - this.modalData.bodyBottomPadding - this.modalData.bodyTopPadding - this.modalData.modalHeaderHeight - this.modalData.modalHeaderTopMargin - this.modalData.contentHeaderHeight;
    this.logging(this.maxHeight, 'this.maxHeight', 'setMaxHeight');
    if (this.maxHeight) {
      return this.innerHeight;
    }
    const contentHeight = this.modalData.contentBottomPadding + this.modalData.contentTopPadding + Math.max.apply(null, [this.modalData.contentHeight, this.modalData.maxBoxesHeight]);
    this.logging(contentHeight, 'contentHeight', 'setMaxHeight');
    return Math.min(innerHeight, contentHeight);
  }

  setMaxBoxesHeight(element){
    const self = this;
    return Math.max.apply(null,  element.getElement().find('.boxes').map(function ()
    {
      self.logging($(this), '$(this)', 'setMaxBoxesHeight');
      self.logging($(this).height(), '$(this).height()', 'setMaxBoxesHeight');
      return $(this).height();
    }).get());
  }

  setClass(){
    const element = this.modalService.findModal(this.domid);
    if ( element.getElement().find('.displayInlineBlock').length ) {
        element.getElement().find('#modalContent').addClass('displayInlineBlock col-xs-12 pull_none');
    }
  }

  setInfopanelMaxHeight(){
    const element = this.modalService.findModal(this.domid);
    if ( element.getElement().find('#infopanel').length ) {
      const css = {
        'max-height' : this.contentMaxHeight + 'px',
        'overflow-y' : 'auto'
      };
      this.logging(css, 'css', 'setInfopanelMaxHeight');
      element.getElement().find('#infopanel').css(css);
    }
  }
}
