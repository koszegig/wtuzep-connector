﻿import { Component, Input} from '@angular/core';
import { AdminRootComponent } from '../../../../admin/admin-base/admin-root/admin-root.component';

@Component({
  selector: 'modal-header',
  templateUrl: './modal-header.component.html',
})

export class ModalHeaderComponent extends AdminRootComponent {

  @Input('title') title: any;

  constructor() {
    super();
  }
}
