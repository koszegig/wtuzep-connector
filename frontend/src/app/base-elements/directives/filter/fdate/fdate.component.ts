import { Component, Input, Output, forwardRef, EventEmitter} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
  selector: 'app-filter-date',
  templateUrl: './fdate.component.html',
  styleUrls: ['./fdate.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FdateComponent),
      multi: true
    }
  ]
})
export class FdateComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  @Input('value') _value = false;
  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    super();
   }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  change(event){
    this.search.next(event);
  }
}
