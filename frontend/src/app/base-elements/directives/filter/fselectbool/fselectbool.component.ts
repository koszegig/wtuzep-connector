import { Component, Input, Output, forwardRef, EventEmitter} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
  selector: 'app-filter-select-boolean',
  templateUrl: './fselectbool.component.html',
  styleUrls: ['./fselectbool.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FselectboolComponent),
      multi: true
    }
  ]
})
export class FselectboolComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  blabel = 'display_name';
  bvalue = 'name';
  items = [{name: true, display_name: 'Igen'}, {name: false, display_name: 'Nem'}];
  @Input('value') _value = false;
  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    super();
   }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  change(event){
    this.search.next(event);
  }
}
