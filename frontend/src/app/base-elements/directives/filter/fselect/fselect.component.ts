import { Component, Input, Output, forwardRef, EventEmitter} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
  selector: 'app-filter-select',
  templateUrl: './fselect.component.html',
  styleUrls: ['./fselect.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FselectComponent),
      multi: true
    }
  ]
})
export class FselectComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  blabel = 'name';
  bvalue = 'id';
  protected promisAction;
  items = [];
  @Input('extdata')
    set _extdata(extdata){
      this.blabel = this.__.get(extdata, 'blabel', 'name');
      this.bvalue = this.__.get(extdata, 'bvalue', 'id');
      this.items = this.__.get(extdata, 'items', []);
      this.promisAction = this.__.get(extdata, 'promisAction', null);
      if (!this._.isNull(this.promisAction)) this.getItem();
    }
  @Input('value') _value = false;
  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    super();
   }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  change(event){
    this.search.next(event);
  }

  getItem(){
    return this.promiseAsync(
      this.promisAction,
      'items'
    );
  }
}
