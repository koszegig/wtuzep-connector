﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'box-base',
    templateUrl: './box-base.component.html',
})

export class BoxBaseComponent extends AdminSuperRootComponent{
    @Input('title') title: String;
    constructor() {
      super();
    }
}
