﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
@Component({
    moduleId: module.id.toString(),
    selector: 'box-header',
    templateUrl: './box-header.component.html',
})

export class BoxHeaderComponent extends AdminSuperRootComponent{
    @Input('title') title: String;
    constructor() {
      super();
    }

    ngOnInit() {
    }




}
