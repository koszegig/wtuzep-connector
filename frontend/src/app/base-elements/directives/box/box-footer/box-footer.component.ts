﻿import { Component} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    selector: 'box-footer',
    templateUrl: './box-footer.component.html',
})

export class BoxFooterComponent extends AdminSuperRootComponent{

    constructor() {
      super();
    }
}
