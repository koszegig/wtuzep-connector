﻿import { Component, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';

import { ModalService } from '../services/modal.service';
import $ from 'jquery';
//import { element } from '@angular/core/src/render3/instructions';//Koszegi---!!
@Component({
    moduleId: module.id.toString(),
    selector: 'modal',
    template: '<ng-content></ng-content>'
    //templateUrl: 'modal/modal.component.html'
})

export class ModalComponent implements OnInit, OnDestroy {
    @Input() id: string;
    private element: any;

    constructor(private modalService: ModalService, private el: ElementRef) {
        this.element = $(el.nativeElement);
    }

    ngOnInit(): void {
        const modal = this;
        // ensure id attribute exists
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        // move element to bottom of page (just before </body>) so it can be displayed above everything else
        this.element.appendTo('body');

        // add self (this modal instance) to the modal service so it's accessible from controllers
        this.modalService.add(this);
    }

    // remove self from modal service when directive is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    // open modal
    open(){
        console.log(this.element);
        this.element.show();
        $('body').addClass('modal-open');
        return this.element;
    }

    // close modal
    close(){
        this.element.hide();
        $('body').removeClass('modal-open');
        return this.element;
    }

    getElement(){
      return this.element;
    }
}
