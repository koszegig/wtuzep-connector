﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AdminSuperRootComponent } from '../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'lw-head',
    templateUrl: './lwhead.component.html',
})

export class LwheadComponent extends AdminSuperRootComponent{
    @Input('params') params: any;
    @Output() _inneradd: EventEmitter<string> = new EventEmitter<string>();
    @Output() _search: EventEmitter<any> = new EventEmitter<any>();

    public sizes = this.environment.tablesize.data;

    public size;

    constructor() {
      super();
    }

    ngOnInit(): void {
      this.size = this.__.get(this.params, 'params.page.size');
    }

    inneradd(id: string) {
      this._inneradd.emit(id); //emmiting the event.
    }

    search(event, size: string) {
      this._search.emit({event: event, mode: size}); //emmiting the event.
    }
}
