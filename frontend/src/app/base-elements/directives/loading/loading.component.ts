﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-loading',
    templateUrl: './loading.component.html',
})

export class LoadingComponent extends AdminSuperRootComponent{

  constructor() {
      super();
    }
}
