﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-kpi-field',
    templateUrl: './kpi-field.component.html',
    styleUrls: ['./kpi-field.component.css']
})

export class KpiFieldComponent extends AdminSuperRootComponent{
    _data: any = null;
    @Input('data')
    set name(data: string) {
      this._data = data;
    }

    constructor() {
      super();
    }

    ngOnInit(){


    }
}
