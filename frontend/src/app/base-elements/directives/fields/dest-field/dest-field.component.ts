﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
import {StringHelper} from '../../../../utility/string_helper';

@Component({
    moduleId: module.id.toString(),
    selector: 'dest-field',
    templateUrl: './dest-field.component.html',
    styleUrls: ['./dest-field.component.css']
})

export class DestFieldComponent extends AdminSuperRootComponent{
    shorttext: String = '';
    data: String = '';
    options: { 'tooltip-class': string; 'content-type': string; 'theme': string; 'placement': string; 'show-delay': number; 'max-width': string; };
    @Input('data')
    set _data(data: any) {
      this.data = data;
      this.shorttext = StringHelper.truncate(this.data, 4);
    }

    constructor() {
      super();
      this.setOption();
    }

    setOption(){
      this.options = {
        'tooltip-class' : 'templatecontents',
        'content-type' : 'template',
        'theme': 'light',
        'placement' : 'top',
        'show-delay': 500,
        'max-width': '300 px'
      };
    }
}
