﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'progress-field',
    templateUrl: './progress-field.component.html',
    styleUrls: ['./progress-field.component.css']
})

export class ProgressFieldComponent extends AdminSuperRootComponent{
    @Input('type') type: any='ready';
    @Input('data') percent: any;
    classes :any;
    constructor() {
      super()
      this.initialClasses();
    }
    initialClasses(){
      this.classes = {
        ready: "progress-bar-success",
        active : "progress-bar-info",
        check : "progress-bar-warning",
      }
    }
}
