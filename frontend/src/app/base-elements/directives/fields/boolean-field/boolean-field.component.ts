﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'boolean-field',
    templateUrl: './boolean-field.component.html',
})

export class BooleanFieldComponent extends AdminSuperRootComponent{

    classes :any;
    @Input('data') data: any;

    constructor() {
      super();
      this.initialClasses();
    }

    initialClasses(){
      this.classes = {
        true : "greenColor fas fa-check",
        false : "redColor fas fa-times",
      }
    }


}
