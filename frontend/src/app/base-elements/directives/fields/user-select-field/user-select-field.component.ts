import { Component, Input, Output, forwardRef, EventEmitter  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
  selector: 'app-user-select-field',
  templateUrl: './user-select-field.component.html',
  styleUrls: ['./user-select-field.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSelectFieldComponent),
      multi: true
    }
  ]
})
export class UserSelectFieldComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  @Input('items') items = [];
  @Input('value') _value = null;

  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };
  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    super();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  change(event){
    this.search.next(event);
  }
}
