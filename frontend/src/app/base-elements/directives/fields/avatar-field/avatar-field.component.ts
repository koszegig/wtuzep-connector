﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'avatar-field',
    templateUrl: './avatar-field.component.html',
    styleUrls: ['./avatar-field.component.css']
})

export class AvatarFieldComponent extends AdminSuperRootComponent{
    addAvatarsNr = 0;
    avatars: any = [];
    _data: any = [];
    @Input('maxNr') maxNr: any;
    @Input('data')
    set name(data: string) {
      this._data = data;
      this.initialAvatar();
    }

    constructor() {
      super();
    }

    ngOnInit(){


    }

    initialAvatar(){
      if (this._.isEmpty(this._data)){ this.avatars = []; return; }
      if (!this._.isArray(this._data)) this._data = [{avatar: this._data}];
      if (this._.isUndefined(this.maxNr)) this.maxNr = this.environment.maxavatarDefault;
      this.splitData();
    }

    splitData(){
      const datasize = this._.size(this._data) * 1;
      this.maxNr = this.maxNr * 1;
      if (this.maxNr == -1 || datasize < this.maxNr){ this.avatars = this._data; return; }
      this.avatars = this._.first(this._data, this.maxNr);
      this.addAvatarsNr = (datasize) - (this.maxNr);

    }


}
