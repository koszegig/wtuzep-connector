﻿import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { SettlemenService } from '../../../services/settlemen.service';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
import { FormBuilder, Validators,  FormGroup } from '@angular/forms';

@Component({
    moduleId: module.id.toString(),
    selector: 'address-field',
    templateUrl: './address-field.component.html',
    styleUrls: ['./address-field.component.css']
})

export class AddressFieldComponent extends AdminSuperRootComponent implements OnInit{
    AddressForm: FormGroup;
    isSubbmit: boolean = false;
    settlemenlist: any = [];
    @Input('address')
    set _address(address: any) {
      this.logging(address, 'address', '_address');
      this.fillForm(address);
    }

    @Input('isSubbmit')
    set _isSubbmit(isSubbmit: boolean) {
      this.isSubbmit = isSubbmit;
    }
    @Output() setAddress = new EventEmitter<any>();
    constructor(public settlemenService: SettlemenService,
    fb: FormBuilder) {
      super();
      this.AddressForm = fb.group({
          id: '',
          zip: ['', Validators.compose([Validators.required])],
          settlemen: ['', Validators.compose([Validators.required])],
          address: ['', Validators.compose([Validators.required])],
          type: ['', Validators.compose([Validators.required])],
      });
    }

    ngOnInit() {
      this.listSettlemen();
    }

    fillForm(address){
      this.logging(address, 'address', 'fillForm');
      this.AddressForm.patchValue({
        id: address.id,
        zip: address.zip,
        settlemen: address.settlemen,
        address: address.address,
        type: address.type,
      });
    }

    listSettlemen() {
      this.promise(
        this.settlemenService.listDn(),
        'settlemenlist',
        () => {}
      );
    }

    changeZip(){
      const settlemen = this.findZip(this.AddressForm.get('zip').value);
      this.AddressForm.patchValue({
        settlemen: settlemen,
      });
    }

    findZip(zip){
      if (zip == '' || this._.isNull(zip)) return '';
      const settlemen = this._.find(this.settlemenlist, function(settl){
        return settl.zip + '' == zip + '';
      });
      if (this._.isUndefined(settlemen)) return '';
      return settlemen.name;
    }

    displayCssFor(field: string|Array<string>) {
      const _class = (this.isSubbmit && this.AddressForm.get(field).invalid) ? 'has-error' : '';
      return _class;
    }

    getForm(){
      return this.AddressForm;
    }

}
