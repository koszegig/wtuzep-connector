﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'status-field',
    templateUrl: './status-field.component.html',
    styleUrls: ['./status-field.component.css']
})

export class StatusFieldComponent extends AdminSuperRootComponent{
    class = 'label';
    @Input('data') data: any;
    @Input('class')
    set _class( _class){
      this.class += ' ' + _class;
    }

    constructor() {
      super();
    }
}
