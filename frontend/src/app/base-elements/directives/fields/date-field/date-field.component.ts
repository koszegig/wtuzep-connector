﻿import { Component, Input, Output, forwardRef, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap';

@Component({
  moduleId: module.id.toString(),
  selector: 'app-date-field',
  templateUrl: './date-field.component.html',
  styleUrls: ['./date-field.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateFieldComponent),
      multi: true
    }
  ]
})

export class DateFieldComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  protected optRange = this.environment.date.optional_range;
  protected _value: any;
  @Input('value')
  set __value(value){
    this._value = value;
    this.markForCheck();
  }
  @Input('minDate') _minDate: string = null;
  @Input('maxDate') _maxDate: string = null;
  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };

  get value() {
    this.logging(this._value, 'this._value', 'value');
    return this._value;
  }

  set value(val: any) {
    this._value = this.initialVal(val);
    this.markForCheck();
    this.onChange(val);
    this.onTouched();
  }

  constructor(
    protected cdRef: ChangeDetectorRef) {
    super();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  initialVal(val){
    this.logging(val, 'val', 'initialÍVal');
    const _val = this.initialDate(val).toObject();
    const year = this.getYear(_val);
    const month = this.getMonth(_val);
    const day = this.getDay(_val);
    return new NgbDate(year, month, day);
  }

  convertVal(){

  }

  getVal(val){
    return this.initialDate(val).toString();
  }

  getYear(val){
    return this.__.get(val, 'years', this.__.get(val, 'year', null) );
  }

  getMonth(val){
    let month = this.__.get(val, 'months', null);
    if (!this._.isNull(month)){
      month = month + 1;
      return month;
    }
    return this.__.get(val, 'month', null);
  }

  getDay(val){
    return this.__.get(val, 'date', this.__.get(val, 'day', null) );
  }

  getOptRange(type){
    if (type == 'min') return this.getOptRangeMin();
    return this.getOptRangeMax();
  }

  getOptRangeMin(){
    let date = null;
    if (this._.isNull(this._minDate) || this._.isUndefined(this._minDate)){
      date = this.initialDate(this.optRange.from);
      date.subtract(this.optRange.range.past.value, this.optRange.range.past.unit);
    }
    else date = this.initialDate(this._minDate);
    const _date = date.toNgbDateObject();
    return _date;
  }

  getOptRangeMax(){
    let date = null;
    if (this._.isNull(this._maxDate) || this._.isUndefined(this._maxDate)){
      date = this.initialDate(this.optRange.from);
      date.add(this.optRange.range.future.value, this.optRange.range.future.unit);
    }
    else date = this.initialDate(this._maxDate);
    const _date = date.toNgbDateObject();
    return _date;
  }

  change(event){
    this.markForCheck();
    this.search.next(event);
  }
}
