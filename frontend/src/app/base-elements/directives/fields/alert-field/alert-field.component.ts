﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-alert-field',
    templateUrl: './alert-field.component.html',
})

export class AlertFieldComponent extends AdminSuperRootComponent{

    classes: any;
    @Input('data') data: any;

    constructor() {
      super();
      this.initialClasses();
    }

    initialClasses(){
      this.classes = {
        true : 'redColor fas fa-exclamation',
      };
    }
}
