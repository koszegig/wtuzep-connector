﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
//import {Comment} from '../../../../comments/models/comment';
@Component({
    moduleId: module.id.toString(),
    selector: 'app-comment-field',
    templateUrl: './comment-field.component.html',
    styleUrls: ['./comment-field.component.css']
})

export class CommentFieldComponent extends AdminSuperRootComponent{
    @Input('rows') rows: Array<Comment>;

    constructor() {
      super();
    }

    getRows(){
      if (this._.isEmpty(this.rows)){
        return ['common.no_conten'];
      }

      return this.rows;
    }
}
