﻿import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'scalable-field',
    templateUrl: './scalable-field.component.html',
    styleUrls: ['./scalable-field.component.css']
})

export class ScalableFieldComponent extends AdminSuperRootComponent implements OnInit{
  value: number = null;
  min: number = null;
  max: number = null;
  @Input('value')
  set _value(value){
    this.value = value;
    this.resetValue();
  }
  @Input('min')
  set _min(min){
    this.min = min;
    this.resetValue();
  }
  @Input('max')
  set _max(max){
    this.max = max;
    this.resetValue();
  }
  @Output() onChangeEmmitter = new EventEmitter<any>();
  constructor() {
    super();
  }

  onChange(){
    const data = {value: this.value};
    this.onChangeEmmitter.emit(data);
  }

  resetValue(){
    if (!this._.isNull(this.value)){
      if (!this._.isNull(this.min)){
        this.value = Math.max(this.value, this.min);
      }
      if (!this._.isNull(this.max)){
        this.value = Math.min(this.value, this.max);
      }
    }
  }
}
