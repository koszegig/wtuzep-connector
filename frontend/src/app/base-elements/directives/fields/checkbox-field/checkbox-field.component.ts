﻿import { Component, Input, Output, EventEmitter} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'checkbox-field',
    templateUrl: './checkbox-field.component.html',
    styleUrls: ['./checkbox-field.component.css']
})

export class CheckboxFieldComponent extends AdminSuperRootComponent{

    @Input('data') data: any;
    @Input('id') id: any;
    @Input('resetable') resetable: boolean = false;
    @Output() onChangeCHeckedEmmitter = new EventEmitter<any>();
    constructor() {
      super();
    }

    onChange(checked){
      if (this._.isNull(checked)) this.data = null;
      const data = {checked: checked, id: this.id};
      this.onChangeCHeckedEmmitter.emit(data);
    }
}
