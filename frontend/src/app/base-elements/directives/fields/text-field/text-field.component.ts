﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'text-field',
    templateUrl: './text-field.component.html',
    styleUrls: ['./text-field.component.css']
})

export class TextFieldComponent extends AdminSuperRootComponent{
    @Input('data') data: any;
    @Input('prefix') prefix: string;
    @Input('subfix') subfix: string;

    constructor() {
      super();
    }
}
