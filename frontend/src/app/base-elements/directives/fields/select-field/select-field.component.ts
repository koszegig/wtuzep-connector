import { Component, Input, Output, forwardRef, EventEmitter  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
  selector: 'app-select-field',
  templateUrl: './select-field.component.html',
  styleUrls: ['./select-field.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectFieldComponent),
      multi: true
    }
  ]
})
export class SelectFieldComponent extends AdminSuperRootComponent implements ControlValueAccessor {
  protected readonlyvalue = '';
  protected ngcls = '';
  @Input('blabel') blabel = 'name';
  @Input('bvalue') bvalue = 'id';
  @Input('items') items = [];
  @Input('value') _value = null;
  @Input('readonly') readonly = false;
  @Input('ngcls')
  set _ngcls(ngcls){
   this.ngcls = ngcls;
  }

  @Output() search = new EventEmitter<string>();
  onChange: any = () => { };
  onTouched: any = () => { };
  get value() {
    if (this.readonly) return  this.findValue();
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    super();
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  findValue(){
    //if (this._.isNull(this._value) || this._value == '') return this.translate('common', 'no_data');
    if (this._.isNull(this._value) || this._value == '') return 'no_data';
    const props = {};
    props[this.bvalue] = this._value;
    const item = this._.findWhere(this.items, props);
    return item[this.blabel];
  }

  change(event){
    this.search.next(event);
  }
}
