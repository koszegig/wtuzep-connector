﻿import { Component, Input, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { AdminFormBaseComponent } from '../../../../admin/admin-base/admin-base-form/admin-form-base.component';
import { FormBuilder, Validators,  FormGroup } from '@angular/forms';

@Component({
    moduleId: module.id.toString(),
    selector: 'name-field',
    templateUrl: './name-field.component.html',
    styleUrls: ['./name-field.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class NameFieldComponent extends AdminFormBaseComponent implements OnInit{

    @Input('name')
    set _name(name: any) {
      this.fillForm(name);
    }

    @Input('isSubbmit')
    set _isSubbmit(isSubbmit: boolean) {
      this.isSubbmit = isSubbmit;
    }

    constructor(fb: FormBuilder) {
      super();
      this.ModeForm = fb.group({
          firstname: ['', Validators.compose([Validators.required])],
          middlename: '',
          lastname: ['', Validators.compose([Validators.required])],
      });
    }

    ngOnInit() {
    }

    fillForm(name){
      this.ModeForm.patchValue({
        firstname: name.firstname,
        middlename: name.middlename,
        lastname: name.lastname,
      });
    }

    getForm(){
      return this.ModeForm;
    }

    setName(name){
      this.logging(name,   'name ' , 'setName');
      this.fillForm(name);
    }

}
