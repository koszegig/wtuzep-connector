﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'contact-field',
    templateUrl: './contact-field.component.html',
    styleUrls: ['./contact-field.component.css']
})

export class ContactFieldComponent extends AdminSuperRootComponent{
    @Input('data') data: any;

    constructor() {
      super();
    }
}
