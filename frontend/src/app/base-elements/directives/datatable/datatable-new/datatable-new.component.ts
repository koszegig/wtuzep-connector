﻿import { Component, Input, Output, EventEmitter, ViewChild, TemplateRef, ChangeDetectionStrategy  } from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
import param from '../param.json';
import * as __ from 'lodash';
@Component({
    moduleId: module.id.toString(),
    selector: 'c-datatable-new',
    templateUrl: './datatable-new.component.html',
    styleUrls: ['./datatable-new.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DatatableNewComponent extends AdminSuperRootComponent {
    rows: any[] = [];
    dexData: any =  null;
    //@ViewChild('myTable', { read: false }) mtable: any;//koszegi
    @ViewChild('myTable', { static: false } ) mtable: any;
    @Input('params') params: any;
    @Input('detail') detail: boolean = false;
    @Input('rows')
    set _rows(rows: any) {
      this.rows = rows;
      //this.logging(this.rows, 'this.rows', '_rows');
    }
    @Input('dexData')
    set _dexData(dexData: any) {
      this.dexData = dexData;
      //this.logging(this.rows, 'this.rows', '_rows');
    }
    @Input('columns') columns: any;
    @Input('scrollbarH') scrollbarH: boolean = false;
    @Input('templateRef') templateRef: TemplateRef<any>;
    @Output() _click = new EventEmitter<any>();
    @Output() _search = new EventEmitter<any>();
    @Output() _setPage = new EventEmitter<any>();
    @Output() _onChangeCheckbox = new EventEmitter<any>();
    isAction = false;
    expanded: any = {};
    timeout: any;
    public table = param;
    public loadingIndicator: boolean = false;

    constructor() {
      super();
    }

    ngOnInit(): void {
      this.table.scrollbarH = this.scrollbarH;
      this.initialIsAction();
    }


    clickevent(params) {
      this._click.emit(params);
    }

    search(event){
      //this.logging(event, 'event', 'search');
      this._search.emit(event);
    }

    setPage(event){
      this._setPage.emit(event);
    }

    changeLoadingIndicator(status: boolean){
      this.loadingIndicator = status;
    }

    initialIsAction(){
      const self = this;
      //this.logging(this.params.buttons, 'this.params.buttons', 'initialIsAction', true);
      this._.each(this.params.buttons, function(btn){
        const display = self.isButtonDisplay(btn, self.params);
        self.isAction = self.isAction || display;
      });
    }

    onChangeChecked(data){
      this._onChangeCheckbox.emit(data);
    }

    toggleExpandRow(row) {
      console.log('Toggled Expand Row!', row);
      row.dexData = this.dexData;
      row.params = this.params;
      this.mtable.rowDetail.toggleExpandRow(row);
    }

    onDetailToggle(event) {
      console.log('Detail Toggled', event);
    }

    headerClass = (row, column, value) => {
      return this.__.get(this.params, 'params.classes.datatable.actcolumn', '');
    }
}
