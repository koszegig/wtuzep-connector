﻿import { Component, Input, ViewChild, Output, EventEmitter} from '@angular/core';
import { AdminSuperRootComponent } from '../../../../admin/admin-base/admin-super-root/admin-super-root.component';
import param from '../param.json';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
    moduleId: module.id.toString(),
    selector: 'c-button',
    templateUrl: './buttons.component.html',
    styleUrls: ['./buttons.component.css'],
})

export class ButtonsComponent extends AdminSuperRootComponent {

    @Input('button') button: any;
    @Input('type') type: any = 'submit';
    @Input('label') label: any;
    @Input('outline') outline: boolean;
    @Input('tloc') tloc: any;
    @Input('tclass') tclass: string = '';
    @Input('row') row: any;
    @Input('swalopt') swal: any;
    @ViewChild('confirmSwal', {static: false}) private confirmSwal: SwalComponent;
    @Output('_click') click = new EventEmitter<any>();
    public buttons: any = null;
    public current: any = null;

    constructor() {
      super();
      this.buttons = param;
    }

    ngOnInit(): void {
      //this.logging(this.buttons, 'this.buttons', 'ngOnInit');
      //this.logging(this.button, 'this.button', 'ngOnInit');
      this.current = this.buttons[this.button];
      //this.logging(this.current, 'this.current', 'ngOnInit');
      if (!this._.isUndefined(this.label) && this.label !== '') {
        this.current.label = this.label;
      }
      if (!this._.isUndefined(this.type) && this.type !== '') {
        this.current.type = this.type;
      }
      if (!this._.isUndefined(this.tloc) && this.tloc !== '') {
        this.current.tloc = this.tloc;
      }
      if (!this._.isUndefined(this.label) && this.label !== '') {
        this.current.label = this.label;
      }
      if (!this._.isUndefined(this.outline)) {
        this.current.outline = this.outline;
      }
    }

    getbtnclass(): string {
      let bclass = 'btn-';
      if (this.current.outline == true)
        bclass += 'outline-';
      bclass += this.getPropVal(this.current, 'bclass');
      return bclass;
    }

    getTitle(title){
      if (!this.isSwal()) return '';
      if (title.indexOf('{{') !== -1 && title.indexOf('}}') !== -1){
        title = title.replace('{{', '');
        title = title.replace('}}', '');
        return this.getPropVal(this, title);
      }
      return title;
    }

    isSwal(){
      return !this._.isEmpty(this.swal);
    }
    fireSwal($event): void{
      if (this.isSwal()){
        $event.preventDefault();
        this.confirmSwal.fire();
      }
      else{
        this.confirm();
      }
    }

    confirm(){
      this.click.next();
    }
}
