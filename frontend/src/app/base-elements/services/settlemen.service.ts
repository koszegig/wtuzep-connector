import { Injectable } from '@angular/core';
import { Settlemen } from '../../models/index';
import { MainrestService} from '../../services/base/mainrest.service';

@Injectable({
  providedIn: 'root'
})
export class SettlemenService extends MainrestService {
	protected controller: string  = 'settlemen';

  constructor() {
    super();
   }

	create(settlemen: Settlemen) {
	    return super.create(settlemen);
	}

	update(settlemen: Settlemen) {
	    return super.update(settlemen);
	}
}
