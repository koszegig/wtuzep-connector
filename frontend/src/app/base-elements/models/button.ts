export class Button {
  public btn_class: string;
  public type: string;
  public i_class: string;
  public title: any;
  constructor(bclass: string = '', iclass: string = '', title: any = '', type= 'submit') {
    this.btn_class = bclass;
    this.i_class = iclass;
    this.title = title;
    this.type = type;
  }

  public getBtn_class(): string {
      return this.btn_class;
  }

  public setBtn_class(btn_class: string): void {
      this.btn_class = btn_class;
  }

  public getI_class(): string {
      return this.i_class;
  }

  public setI_class(i_class: string): void {
      this.i_class = i_class;
  }

  public getTitle(): any {
      return this.title;
  }

  public setTitle(title: any): void {
      this.title = title;
  }

  public getType(): string {
    return this.type;
  }

  public setType(type: string): void {
    this.type = type;
  }

}
