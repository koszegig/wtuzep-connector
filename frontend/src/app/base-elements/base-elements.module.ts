
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TooltipModule} from 'ng2-tooltip-directive';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {MultiTranslateHttpLoader} from 'ngx-translate-multi-http-loader';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//
import { SwitchComponent,  LoadingComponent, LwheadComponent, AlertComponent, ModalComponent} from './directives/index';
//content start
import { AvatarContentComponent, InfoboxContentComponent} from './../content/partial/index';
import { PopupInfoboxComponent} from './../content/index';
//content end

//datatable start
import { DatatableNewComponent} from './directives/datatable/index';
//datatable/end

//buttons start
import { ButtonsComponent, CloseButtonsComponent} from './directives/buttons/index';
//buttons end

//modal start
import { ModalBaseComponent, ModalFooterComponent, ModalHeaderComponent } from './directives/modal/index';
//modal end

//box start
import { BoxBaseComponent, BoxFooterComponent, BoxHeaderComponent } from './directives/box/index';
//box end

//fields start
import { AvatarFieldComponent, BooleanFieldComponent, StatusFieldComponent, TextFieldComponent} from './directives/fields/index';
import { CheckboxFieldComponent, RadioFieldComponent, ProgressFieldComponent, DestFieldComponent, AddressFieldComponent} from './directives/fields/index';
import { NameFieldComponent, ContactFieldComponent, ScalableFieldComponent, SelectFieldComponent} from './directives/fields/index';
import { DateFieldComponent, UserSelectFieldComponent, AlertFieldComponent, CommentFieldComponent} from './directives//fields/index';
//fields end

//filters start
import { FtextComponent, FselectComponent, FselectboolComponent, FdateComponent} from './directives/filter/index';
import param from './param.json';
//filters end
export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, param.lang_files);
}
@NgModule({
	declarations: [
    SwitchComponent,
    LoadingComponent,
    LwheadComponent,
    AlertComponent,
    ModalComponent,
    AvatarContentComponent,
    InfoboxContentComponent,
    PopupInfoboxComponent,
    DatatableNewComponent,
    ButtonsComponent,
    CloseButtonsComponent,
    ModalBaseComponent,
    ModalFooterComponent,
    ModalHeaderComponent,
    BoxBaseComponent,
    BoxFooterComponent,
    BoxHeaderComponent,
    AvatarFieldComponent,
    BooleanFieldComponent,
    StatusFieldComponent,
    TextFieldComponent,
    CheckboxFieldComponent,
    RadioFieldComponent,
    ProgressFieldComponent,
    DestFieldComponent,
    AddressFieldComponent,
    NameFieldComponent,
    ContactFieldComponent,
    ScalableFieldComponent,
    SelectFieldComponent,
    UserSelectFieldComponent,
    DateFieldComponent,
    AlertFieldComponent,
    CommentFieldComponent,
    FtextComponent,
    FselectComponent,
    FdateComponent,
    FselectboolComponent,
  ],
	imports: [
    NgbModule,
    NgxPrettyCheckboxModule,
    NgxDatatableModule,
    TooltipModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
    RouterModule,
    SweetAlert2Module,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
    HttpClientModule,
  ],
	exports: [
    SwitchComponent,
    LoadingComponent,
    LwheadComponent,
    AlertComponent,
    ModalComponent,
    AvatarContentComponent,
    InfoboxContentComponent,
    PopupInfoboxComponent,
    DatatableNewComponent,
    ButtonsComponent,
    CloseButtonsComponent,
    ModalBaseComponent,
    ModalFooterComponent,
    ModalHeaderComponent,
    BoxBaseComponent,
    BoxFooterComponent,
    BoxHeaderComponent,
    AvatarFieldComponent,
    BooleanFieldComponent,
    StatusFieldComponent,
    TextFieldComponent,
    CheckboxFieldComponent,
    ProgressFieldComponent,
    DestFieldComponent,
    AddressFieldComponent,
    NameFieldComponent,
    ContactFieldComponent,
    ScalableFieldComponent,
    SelectFieldComponent,
    UserSelectFieldComponent,
    DateFieldComponent,
    AlertFieldComponent,
    CommentFieldComponent,
    FtextComponent,
    FselectComponent,
    FdateComponent,
    FselectboolComponent,
    TranslateModule
  ]
})

export class BaseElementsModule { }
