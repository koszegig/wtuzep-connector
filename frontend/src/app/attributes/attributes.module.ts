import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AttributesService } from './services/attributes.service';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminAttributesFromComponent } from './admin-attributes-from/admin-attributes-from.component';
import { AdminAttributesListComponent } from './admin-attributes-list/admin-attributes-list.component';
import { AdminAttributesModalComponent } from './admin-attributes-modal/admin-attributes-modal.component';

//import { AdminImageUploadAndCropComponent } from '../admin/admin-base/admin-image-upload-and-crop/admin-image-upload-and-crop.component';

@NgModule({
    declarations: [
        AdminAttributesFromComponent,
        AdminAttributesListComponent,
        AdminAttributesModalComponent//,
        //AdminImageUploadAndCropComponent
    ],
    imports: [
        BaseElementsModule,
        TooltipModule,
        NgSelectModule,
        NgxPrettyCheckboxModule,
        ImageCropperModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        NgxEchartsModule,
        NgMultiSelectDropDownModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgxDatatableModule,
    ],
    providers: [
        AttributesService,
        AlertService,
        ModalService,
        {
            provide: NG_SELECT_DEFAULT_CONFIG,
            useValue: {
                notFoundText: 'Book not found'
            }
        }
    ],
    exports: [AdminAttributesFromComponent, AdminAttributesListComponent, AdminAttributesModalComponent]
})

export class AttributesModule { }
