import { Injectable } from '@angular/core';
import { Attribute } from '../models/Attributes';
import { MainrestService} from '../../services/base/mainrest.service';
import {AdminAttributesModalComponent} from '../admin-attributes-modal/admin-attributes-modal.component';

export class AttributesService  extends MainrestService {

  protected controller: string  = 'attributes';

  constructor() {
    super();
   }

  create(attribute: Attribute) {
      return super.create(attribute);
  }

  update(attribute: Attribute) {
      return super.update(attribute);
  }

}


