﻿
import {AdminAttributesListComponent } from '../attributes/admin-attributes-list/admin-attributes-list.component';

export const ATTRIBUTES_ROUTING = [
  { path: 'attributes-list',
    component: AdminAttributesListComponent
  }
];
