import {Params} from '../../models/listviews/params/params';
import param from '../param/param.json';
import columns from '../param/columns.json';
export class attparams extends Params {

  constructor() {
    super(param, columns);
  }
}
