import { Component, OnInit } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-attributes-modal',
  templateUrl: './admin-attributes-modal.component.html',
  styleUrls: ['./admin-attributes-modal.component.css']
})
export class AdminAttributesModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
