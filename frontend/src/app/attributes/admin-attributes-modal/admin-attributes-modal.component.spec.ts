import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttributesModalComponent } from './admin-attributes-modal.component';

describe('AdminAttributesModalComponent', () => {
  let component: AdminAttributesModalComponent;
  let fixture: ComponentFixture<AdminAttributesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAttributesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttributesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
