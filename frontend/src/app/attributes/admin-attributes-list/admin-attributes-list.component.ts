import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { attparams } from '../models/attparams';
import {  AttributesService } from '../services/attributes.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminAttributesModalComponent } from '../admin-attributes-modal/admin-attributes-modal.component';
import additionalcolumns from '../param/additionalcolumns.json';

@Component({
  selector: 'admin-attributes-list',
  templateUrl: './admin-attributes-list.component.html',
  styleUrls: ['./admin-attributes-list.component.css']
})
export class AdminAttributesListComponent   extends AdminBaseListComponent {

  @ViewChild(AdminAttributesModalComponent, {static: false}) FormComponent: AdminAttributesModalComponent;
  @Input('subtitle') subtitle: string;
  public buttons: any;
  //public optionsService: OptionsService;
  constructor(public Service: AttributesService,
              protected cdRef: ChangeDetectorRef) {
    super();
    try {
      //this.Service = this.injector.get(CustomerService);
      //  this.optionsService = this.injector.get(OptionsService);
    } catch (e) {
    }
    this.params = new attparams();
  }


}
