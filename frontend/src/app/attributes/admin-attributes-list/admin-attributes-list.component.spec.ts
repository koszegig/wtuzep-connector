import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttributesListComponent } from './admin-attributes-list.component';

describe('AdminAttributesListComponent', () => {
  let component: AdminAttributesListComponent;
  let fixture: ComponentFixture<AdminAttributesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAttributesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttributesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
