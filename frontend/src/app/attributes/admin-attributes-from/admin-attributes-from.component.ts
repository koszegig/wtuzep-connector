import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { AttributesService } from '../services/attributes.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';
//import { OptionsService } from '../../options/services/options.service';
import { NameFieldComponent} from '../../base-elements/directives/fields/name-field/name-field.component';
import {ProductsService} from '../../products/services/products.service';

@Component({
  selector: 'admin-attributes-from',
  templateUrl: './admin-attributes-from.component.html',
  styleUrls: ['./admin-attributes-from.component.css']
})
export class AdminAttributesFromComponent extends AdminBaseFormComponent{
  public itemes: any;
  public modies: any;
  @ViewChild(NameFieldComponent, {static: false}) namefield: NameFieldComponent;
  constructor(
      public Service: AttributesService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }

  initialform(){
    this.ModeForm = this.fb.group({
      synced: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      synced: this.currentItem.synced,

    });
  }

}

