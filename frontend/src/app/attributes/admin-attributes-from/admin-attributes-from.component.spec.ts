import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAttributesFromComponent } from './admin-attributes-from.component';

describe('AdminAttributesFromComponent', () => {
  let component: AdminAttributesFromComponent;
  let fixture: ComponentFixture<AdminAttributesFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAttributesFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAttributesFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
