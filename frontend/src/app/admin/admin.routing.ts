import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { IndexComponent } from './index/index.component';
import { IotDashboardComponent } from './iot-dashboard/iot-dashboard.component';
import { AuthGuard } from './../authentication/guards/auth.guard';
import { REGISTRATION_ROUTES } from '../registrations/registrations.routing';
import { TASKS_ROUTING } from '../task/tasks.routing';
import { LOGS_ROUTING } from '../Logs/logs.routing';
import { PRODUCT_ROUTING } from '../products/products.routing';
import { STOCKSQTYS_ROUTING } from '../stocksqtys/stocksqtys.routing';
import { ATTRIBUTES_ROUTING } from '../attributes/attributes.routing';
import { CUSTOMERS_ROUTING } from '../customers/customers.routing';
import { CATEGORIES_ROUTING } from '../categories/categories.routing';
import { ORDERS_ROUTING } from '../orders/orders.routing';
import { USERS_ROUTING } from '../users/users.routing';
import { APPLICATIONS_ROUTING } from '../applications/applications.routing';
const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            {
                path: 'dashboard',
                children: [
                    { path: '', redirectTo: 'index', pathMatch: 'full' },
                    { path: 'index', component: IndexComponent, data: { title: ':: Lucid Angular :: Dashboard :: Analytical ::' } },
                    { path: 'iot', component: IotDashboardComponent, data: { title: ':: Lucid Angular :: Dashboard :: IoT ::' } },
                ]
            },
            { path: 'app', children: APPLICATIONS_ROUTING},
            { path: 'users', children: USERS_ROUTING},
            { path: 'tasks', children: TASKS_ROUTING},
            { path: 'logs', children: LOGS_ROUTING},
            { path: 'products', children: PRODUCT_ROUTING},
            { path: 'stocksqtys', children: STOCKSQTYS_ROUTING},
            { path: 'registrations', children: REGISTRATION_ROUTES},
            { path: 'attributes', children: ATTRIBUTES_ROUTING},
            { path: 'categories', children: CATEGORIES_ROUTING},
            { path: 'orders', children: ORDERS_ROUTING},
            { path: 'customers', children: CUSTOMERS_ROUTING}
        ]
    },
];
export const routing = RouterModule.forChild(routes);
