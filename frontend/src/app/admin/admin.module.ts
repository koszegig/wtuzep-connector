import { NgModule, ApplicationModule, Injector  } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppInjector } from './../services/custom/app.injector.service';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { routing } from './admin.routing';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FullCalendarModule } from 'ng-fullcalendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { AdminComponent } from './admin/admin.component';
import { DetailTilesComponent } from './detail-tiles/detail-tiles.component';
import { CardActionsComponent } from './card-actions/card-actions.component';
import { TimelinePostComponent } from './timeline-post/timeline-post.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ChatComponent } from './chat/chat.component';
import { GeneralFeedComponent } from './general-feed/general-feed.component';
import { TwitterFeedComponent } from './twitter-feed/twitter-feed.component';
import { MemberInfoComponent } from './member-info/member-info.component';
import { ManagedDataComponent } from './managed-data/managed-data.component';
import { TopProductsComponent } from './top-products/top-products.component';
import { ReferralsComponent } from './referrals/referrals.component';
import { TotalRevenueComponent } from './total-revenue/total-revenue.component';
import { ApplicationsModule } from '../applications/applications.module';
import { CommonElementsModule } from '../common-elements/common-elements.module';
import { PagesModule } from '../pages/pages.module';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../layout/layout.module';
import { UsersModule } from '../users/users.module';
import { RegistrationsModule } from '../registrations/registrations.module';
import { UserGroupsModule } from '../usergroups/usergroups.module';
import { RolesModule } from '../roles/roles.module';
import { PermissionsModule } from '../permissions/permissions.module';
import { IotDashboardComponent } from './iot-dashboard/iot-dashboard.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AuthGuard } from '../authentication/guards/index';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { TasksModule } from '../task/tasks.module.js';
import { ProductsModule } from '../products/products.module';
import { StocksqtysModule } from '../stocksqtys/stocksqtys.module';
import { AttributesModule } from '../attributes/attributes.module';
import { LogsModule } from '../Logs/logs.module';
import { CategoriesModule } from '../categories/categories.module';
import { OrdersModule } from '../orders/orders.module';
import { CustomersModule } from '../customers/customers.module';

import { AdminSuperRootComponent} from './admin-base/admin-super-root/admin-super-root.component';
import { AdminRootComponent} from './admin-base/admin-root/admin-root.component';
import { AdminBaseComponent} from './admin-base/admin-base/admin-base.component';
import { AdminBaseFormComponent} from './admin-base/admin-base-form/admin-base-form.component';
import { AdminListBaseComponent} from './admin-base/admin-base-list/admin-list-base.component';
import { AdminBaseListComponent} from './admin-base/admin-base-list/admin-base-list.component';
import { AdminBaseListDatatableComponent} from './admin-base/admin-base-list/admin-base-list-datatable.component';
import { AdminBaseModalComponent} from './admin-base/admin-base-modal/admin-base-modal.component';
@NgModule({
	imports: [
		HttpModule,
		NgSelectModule,
		NgxPrettyCheckboxModule,
		ImageCropperModule,
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		routing,
		NgxEchartsModule,
		NgxDatatableModule,
		LayoutModule,
		RichTextEditorAllModule,
		NgbModule,
		FullCalendarModule,
		ApplicationsModule,
		CommonElementsModule,
		PagesModule,
		RouterModule,
		UsersModule,

		TasksModule,
		ProductsModule,
		StocksqtysModule,
		AttributesModule,
		LogsModule,
		CategoriesModule,
		OrdersModule,
		CustomersModule,

		RegistrationsModule,
		UserGroupsModule,
		RolesModule,
		PermissionsModule,
		SweetAlert2Module.forRoot(),
	],
	declarations: [
		AdminComponent,
		IndexComponent,
		DetailTilesComponent,
		CardActionsComponent,
		TimelinePostComponent,
		ActivitiesComponent,
		ChatComponent,
		GeneralFeedComponent,
		TwitterFeedComponent,
		MemberInfoComponent,
		ManagedDataComponent,
		TopProductsComponent,
		ReferralsComponent,
		TotalRevenueComponent,
		IotDashboardComponent,
		AdminSuperRootComponent,
		AdminRootComponent,
		AdminBaseComponent,
		AdminBaseFormComponent,
		AdminBaseListComponent,
		AdminBaseListDatatableComponent,
		AdminListBaseComponent,
		AdminBaseModalComponent
  	],
  providers: [
	AuthGuard,
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Custom not found'
      }
  	}
  ]
})
export class AdminModule {
	constructor(injector: Injector){
		AppInjector.setInjector(injector);
	}
}
