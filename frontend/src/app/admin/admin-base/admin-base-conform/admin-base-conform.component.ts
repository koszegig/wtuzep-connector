import { Component, ViewChild } from '@angular/core';
//import { AdminBaseListComponent } from '../admin-base-list/admin-base-list.component';
import { AdminBaseListComponent } from '../admin-base-list/admin-base-list.component';

@Component({
  template: ''
})
export class AdminBaseConformComponent extends AdminBaseListComponent {
  constructor() {
    super();
  }
  ngOnInit() {
    super.ngOnInit();
    this.params.setText('headerTitle',this.menu_title);
  }

}
