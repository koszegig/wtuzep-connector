import { Component, ViewChild, ViewChildren, Input} from '@angular/core';
import { AdminRootComponent } from '../admin-root/admin-root.component';

@Component({
  template: '',
})

export class AdminBaseModalComponent extends AdminRootComponent {

  public params = null;

  public content = null;

  protected clickeventparams = null;
  @ViewChild('modalchild', {static: false}) modalchild: any;
  @ViewChildren('modalcontent') modalcontent: any;

  @Input('params')
    set _params(params: any) {
  }

  constructor() {
    super();

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    const self = this;
    self.logging(this.modalcontent, 'this.modalcontent', 'ngAfterViewInit');
    this.modalcontent.changes.subscribe(() => {
      self.logging('lefutacucc', 'lefutacucc', 'ngAfterViewInit');
      self.callClickEvent();
    });
  }

  openModal(){
    //this.logging('OpenModal', 'OpenModal', 'OpenModal');
 //   this.logging(this.modalchild, 'this.modalchild', 'OpenModal');
    this.modalchild.openModal();
  }

  getContentMaxHeight(){
    return this.modalchild.contentMaxHeight;
  }

  closeModal(){
    this.modalchild.closeModal();
  }

  CloseModalCB(){
   // this.clickeventparams = {method: 'callBackCloseModal', params: []};
    this.content = null;
    this.callClickEvent();
  }

  OpenModalCB(){
  //  this.logging('OpenModalCB', 'OpenModalCB', 'OpenModalCB');
    this.clickeventparams = {method: 'callBackOpenModal', params: []};
    this.callClickEvent();
    //this.content = null;
  }

  add(id?, extData?){

    this.content = 'form';
    this.clickeventparams = {method: 'add', params: [id, extData]};
  }

  edit(id?, extData?){
    //this.logging(this.content, 'this.content', 'edit');
    this.content = 'form';
    //this.logging(this.content, 'this.content', 'edit');
    this.clickeventparams = {method: 'edit', params: [id, extData]};
  }

  list(id?, extData?){
    this.modalcontent.clickevent({method: 'list', params: [id, extData]});
  }

  listSub(params){
    this.content = 'listsub';
    this.clickeventparams = {method: 'subList', params: params};
  }

  callClickEvent(){
    this.logging(this.modalcontent, 'this.modalcontent', 'callClickEvent');
    if (this._.isNull(this.clickeventparams)) {
      this.logging(this.clickeventparams, 'this.clickeventparams is null', 'callClickEvent');
      return;
    }
    if (this._.isUndefined(this.modalcontent.first)){
      this.logging(this.modalcontent.first, 'this.modalcontent.first undefine', 'callClickEvent');
        return;
    }
    //this.logging(this.clickeventparams, 'this.clickeventparams', 'callClickEvent');

    this.modalcontent.first.clickevent(this.clickeventparams);
  }


}
