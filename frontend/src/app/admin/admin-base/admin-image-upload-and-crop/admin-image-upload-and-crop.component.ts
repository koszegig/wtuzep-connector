import { Component, Output, EventEmitter} from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AdminRootComponent } from '../admin-root/admin-root.component';

@Component({
  selector: 'app-admin-image-upload-and-crop',
  templateUrl: './admin-image-upload-and-crop.component.html',
  styleUrls: ['./admin-image-upload-and-crop.component.css']
})
export class AdminImageUploadAndCropComponent extends AdminRootComponent{
  imageChangedEvent: any = '';
  croppedImage: any = '';

  @Output() saveAvatarEmmitter = new EventEmitter<any>();
  constructor() {
    super();
  }

  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  save(){
    this.saveAvatarEmmitter.emit(this.croppedImage);
    this.closeModal();
  }

  uploadAvatar(){
    this.openModal();
  }

}
