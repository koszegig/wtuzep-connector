import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminImageUploadAndCropComponent } from './admin-image-upload-and-crop.component';

describe('AdminImageUploadAndCropComponent', () => {
  let component: AdminImageUploadAndCropComponent;
  let fixture: ComponentFixture<AdminImageUploadAndCropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminImageUploadAndCropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminImageUploadAndCropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
