import { AdminBaseComponent } from '../admin-base/admin-base.component';
import { FormBuilder, FormArray, Validators,  FormGroup } from '@angular/forms';

export class AdminFormBaseComponent extends AdminBaseComponent {
  public fb: FormBuilder;
  public Validators: any = Validators;

  public ModeForm: FormGroup;
  protected isSubbmit = false;

  constructor() {
    super();
    try {
      this.fb = this.injector.get(FormBuilder);
    } catch (e) {
    }
  }

  ngOnInit() {
    super.ngOnInit();
  }

  addValidators(control, validator){
    this.ModeForm.controls[control].setValidators([validator]);
    this.ModeForm.controls[control].updateValueAndValidity();
  }

  addRequired(control){
    this.addValidators(control, this.Validators.required);
  }

  removeValidators(control) {
    this.ModeForm.get(control).clearValidators();
    this.ModeForm.get(control).updateValueAndValidity();
  }

  onSubmit(){
//    this.logging(this.isSubbmit, 'this.isSubbmit', 'onSubmit');
    this.isSubbmit = true;
  }

  displayCssFor(field: string|Array<string>) {
    const _class = (this.isInvalid(field)) ? 'has-error' : '';
    return _class;
  }

  displayCssForFa(field: string|Array<string>, subControl, index) {
    const _class = (this.isInvalidFa(field, subControl, index)) ? 'has-error' : '';
    return _class;
  }

  isInvalid(field: string|Array<string>){
    return (this.isSubbmit && this.ModeForm.get(field).invalid);
  }

  isInvalidFa(field: string|Array<string>, subControl, index){
    const _control = this.getFormArrayControl(field, index);
    return (this.isSubbmit && _control.get(subControl).invalid);
  }

  hasError(control, rule){
    const _control = this.ModeForm.get(control);
    return (this.isSubbmit &&  _control.hasError(rule));
  }

  hasErrorFa(control, subControl, rule, index){
    const _control = this.getFormArrayControl(control, index);
    return _control.get(subControl).hasError(rule);
  }

  getFormArrayControl(control, index){
    const _control = this.ModeForm.get(control) as FormArray;
    return _control.at(index);
  }

  getRequiredError(modul, key){
    let message = this.getErrorMessagePrefix(modul, key);
    message += this.translate('common.field') + ' ';
    message += this.translate('common.specifying') + ' ';
    message += this.translate('common.required');
    return message;
  }

  getMaxValueError(modul, key, value){
    let message = this.getErrorMessagePrefix(modul, key);
    message += this.translate('common.ownMaxValue') + ' ';
    message += ' : ';
    message += value;
    return message;
  }

  getFormatError(modul, key){
    let message = this.getErrorMessagePrefix(modul, key);
    message += this.translate('common.its_format') + ' ';
    message += this.translate('common.invalid');
    return message;
  }

  getErrorMessagePrefix(modul, key){
    let message = this.translateWParams(modul, key) + ' ';
    message += this.translate('common.field') + ' ';
    return message;
  }


}
