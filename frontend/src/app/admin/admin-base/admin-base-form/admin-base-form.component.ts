import { Component} from '@angular/core';
import { AdminFormBaseComponent } from './admin-form-base.component';
import { FormControl, FormBuilder, FormArray, Validators,  FormGroup } from '@angular/forms';

@Component({
  template: ''
})

export class AdminBaseFormComponent extends AdminFormBaseComponent {
   public FormControl: any = FormControl;
  loading = false;
  action = null;
  mode = null;

  columns = [];
  subscription: any;
  protected Service: any;
  protected currentItem: any;
  protected current: any;
  protected currentId: string;
  protected extData: any;
  protected fields: Array<string>;

  constructor() {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
  }

  processor(){
    if (this.mode == 'add') {
        this.create();
    } else{
        this.update();
    }
  }
  update() {
    this.loading = true;
    this.current = this.ModeForm.value;
    if (this.callbackapipre() == false) return;
    this._update();
  }

  _update(){
    this.promise(
      this.Service.update(this.current),
      null,
      (val) => {this.callbackapipost('Success', val); },
      'data'
    );
  }

  create() {
    this.loading = true;
    this.current = this.ModeForm.value;
    if (this.callbackapipre() == false) return;
    this._create();
  }

  _create(){
    this.promise(
      this.Service.create(this.current),
      null,
      (val) => {this.logging(val, 'val', '_create'); this.callbackapipost('Success', val); },
      'data'
    );
  }

  callbackapipost(message: string, row: any){
      this.closeModal();
      this.loading = false;
      this.setAlertService({status: 'success', message: message});
      if (!this._.isNull(row) && !this._.isEmpty(row))
        this.refreshTableRowEmitter();
  }

  callbackapipre(){
    if (this.mode == 'edit')
      this.current.id = this.currentItem.id;
    return true;
  }

  edit(id: string = null, extData?){
    if (!this._.isUndefined(extData)) this.extData = extData;
    this.mode = 'edit';
    this.action = 'update';
    this.currentId = id;
    this.callbackform();
  }

  _edit(){
    this.promise(
      this.Service.getById(this.currentId),
      'currentItem',
      () => {this.initial(); },
      'data'
    );
  }

  initial(){
    this.initialform();
    if (this.mode == 'edit')
      this.fillForm();
    this.extInitialForm();
    this.openModal();
    this.loadingIndicatorEmitter(false);
  }

  add(id?, extData?){
    if (!this._.isUndefined(extData)) this.extData = extData;
    if (!this._.isUndefined(id)) this.currentId = id;
    this.mode = 'add';
    this.action = 'save';
    this.callbackform();
  }

  _add(){

  }

  callbackform(){
    //this.logging(this.mode,  'this.mode' , 'callbackform');
    if (this.mode == 'add')
      this.initial();
    else
      this._edit();
  }

  initialform(){}

  extInitialForm(){}

  fillForm(){}


  buildCheckboxItem(controllname){
    const items = this.initialItems();
    items.forEach(item => {
          this.onChange(controllname, item, true);
      });
  }

  buildRadioItem(controllname){
    const items = this.initialItems();
    items.forEach(item => {
          this.onChange(controllname, item, true);
      });
  }

  onChangeChecked(controlname: string, item: any, checked){
    let formArray = this.getFormArray(controlname);
    formArray = this.onchangeUpdate(formArray, item, checked);
  }

  onChangeCheckedSimple(controlname: string, checked){
    const patch = {};
    patch[controlname] = checked;
    this.ModeForm.patchValue(patch);
  }

  onChange(controlname: string, item: any, isFill = false, onlyOne = false) {
    let formArray = this.getFormArray(controlname);

    if (!isFill)
      formArray = this.removeFormArray(formArray, item);
    else if (onlyOne){
      //
    }
    formArray.push(this.generateArrayItem(item));
  }


  onchangeUpdate(formArray: any, item: any, checked){
    item.checked = checked;
    item = this.updateArrayPre(item);
    const index = this.getArrayIndex(formArray, item);
    formArray = this.updateFormArray(formArray, index, item);
    return formArray;
  }

  checked(array: any, item: any){
    if (this._.isObject(array[0])){
      return !this._.isUndefined(this._.findWhere(array, item));
    }
    return this._.indexOf(array, item) !== -1;
  }

  removeFormArray(formArray: any, item){
    const index = formArray.controls.findIndex(x => x.value.name == item.value.name);
    formArray.removeAt(index);
    return formArray;
  }

  uncheckedAllArray(formArray: any, item){
    const index = formArray.controls.findIndex(x => x.value.name == item.value.name);
    formArray.removeAt(index);
    return formArray;
  }


  getArrayIndex(formArray: any, item: any, prop: string = 'name', propitem: string = 'name'){
    return formArray.controls.findIndex(x => x.value[prop] == item.value[propitem]);
  }

  generateArrayItem(item: any){
    return null;
  }

  initialItems(){
    return null;
  }

  updateFormArray(formArray: any, index, item, all?){
    return null;
  }

  updateArrayPre(item: any){
    return item;
  }

  getFormArray(controlname: string){
      return this.ModeForm.get(controlname) as FormArray;
      //return <FormArray>this.ModeForm.controls[controlname];
  }

  isLoading(){
    this.logging(this.ModeForm, 'this.ModeForm', 'isLoading');
    return this._.isUndefined(this.ModeForm);
  }

  isDisplayed(field: string){
    return (this._.indexOf(this.fields, field) != -1);
  }
}
