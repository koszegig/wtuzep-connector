import { Component, Output, EventEmitter, ViewChild} from '@angular/core';
import { AdminRootComponent } from '../../../admin/admin-base/admin-root/admin-root.component';
import { Router, ActivatedRoute} from '@angular/router';
import { ModalService} from '../../../services/custom/modal.service';
//import {Column} from '../../../models/column';

@Component({
  template: ''
})

export class AdminBaseComponent extends AdminRootComponent {
  @ViewChild('datatable', {static: true}) datatable: any;
  @ViewChild('modalchild', {static: false}) modalchild: any;

  protected route: ActivatedRoute;
  protected router: Router;
  protected modalService: ModalService;
  protected Service: any;
  //protected Column: any = Column;

  public domid = null;

  public menu_id;
  public menu_title;

  public FormComponent: any;

  protected sub: any;
  public loadingGIF;
  public loading = false;
  protected isModal = true;
  display = false;
  protected clickeventparams = null;
  @Output() refreshTableRowEmmitter = new EventEmitter<any>();
  @Output() loadingIndicatorEmmitter = new EventEmitter<boolean>();
  @Output() openModalEmmitter = new EventEmitter<any>();



  constructor() {
    super();
    try {
      // injector is undefined at this point
      this.route = this.injector.get(ActivatedRoute);
      this.router = this.injector.get(Router);
      this.modalService = this.injector.get(ModalService);
      const segments: Array<string> = this.router.url.split('/');
      this.menu_id = segments[3];//----Kőszegi
      this.menu_title= decodeURIComponent(segments[4]); //----Kőszegi
    } catch (e) {
    }
    //this.cdRef = ChangeDetectorRef;
  }

  ngOnInit() {
    super.ngOnInit();
  }

  callBackOpenModal(){
    this.detectChanges();
  }

  callBackCloseModal(){
  }

  getParams(){
    return this.__.get(this, 'params', null);
  }
}
