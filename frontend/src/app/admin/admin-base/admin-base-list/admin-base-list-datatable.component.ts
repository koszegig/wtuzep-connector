import { Component, Input } from '@angular/core';
import { AdminListBaseComponent } from './admin-list-base.component';

export class AdminBaseListDatatableComponent extends  AdminListBaseComponent{

    rows = [];

    public params: any;

    ngOnInit() {
        super.ngOnInit();
        this.setPage({ page: 1 });
    }

    setPagePost(data){
        this.params.apiparams.setPage(data);
        this.rows = data.data;
    }

    setPage(pageInfo){
      this.params.apiparams.page.pageNumber = pageInfo.page - 1;
      this.list();
    }

    listPost(data){
        this.setPagePost(data.data);
        this.changeLoadingIndicator(false);
        super.listPost(data);
    }

    listPostError(error){
        super.listPost(error);
        this.changeLoadingIndicator(false);
    }

    listPre(){
        this.changeLoadingIndicator(true);
    }

    changeLoadingIndicator(status: boolean){
        if (this._.isUndefined(this.datatable)) return;
        this.datatable.changeLoadingIndicator(status);
    }

    refreshTableRow(){
        this.list();
    }
    choose(id) {
      const Selectrow = this._.filter(this.rows,function(obj:any) {
        return obj.id === id;
      });
     this.params.choose_row = Selectrow[0];
     this.logging(this.params.choose_row, 'AdminBaseListDatatableComponent -> this.params._choose_row', 'choose(id)');
     this.selectedEmmitter.emit(Selectrow[0]);
     this.closeModal();
    }
}
