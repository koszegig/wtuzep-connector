import { Component } from '@angular/core';
import { AdminBaseComponent } from '../admin-base/admin-base.component';

export class AdminListBaseComponent extends  AdminBaseComponent{

  public params: any;

  list() {
    this.listPre();
    const list = this._list();
    if (list === null){
      return;
    }
    list.subscribe(
      data => {
          this.listPost(data);
      },
      error => {
          this.listPostError(error);
      });
  }

  _list(){
   // this.logging(this.params, 'this.params', '_list'); //return this.Service.list(this.params.params);
    return this.Service.list(this.params);
  }

  listPre(){}

  listPost(data){
    this.markForCheck();
  }

  listPostError(error){
    this.alertService.error(error);
  }

}
