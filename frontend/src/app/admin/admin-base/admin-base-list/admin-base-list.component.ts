import { Component, Input} from '@angular/core';
import { AdminBaseListDatatableComponent } from './admin-base-list-datatable.component';
//import {Column} from '../../../models/column';

@Component({
  template: ''
})

export class AdminBaseListComponent extends  AdminBaseListDatatableComponent{

  FormComponent: any;

  mode: string = 'list';

  action: string;

  currentItem: any;

  @Input('isModal') isModal: boolean = false;

  setPage(pageInfo){
    if (!this.isModal){
      super.setPage(pageInfo);
    }
  }

  listPost(data){
    super.listPost(data);
    this.openModal();
  }

  listPre(){
    super.listPre();
    this.openModal();
  }

  activate(id) {
    this.changeLoadingIndicator(true);
    this.Service.activate(id).subscribe(
            data => {
              this.refreshTableRow();
                this.changeLoadingIndicator(false);
            },
            error => {
                this.alertService.error(error);
                this.changeLoadingIndicator(false);
            });
  }
  inactivate(id) {
    this.changeLoadingIndicator(true);
    this.Service.inactivate(id).subscribe(
            data => {
                this.refreshTableRow();
                this.changeLoadingIndicator(false);

            },
            error => {
                this.alertService.error(error);
                this.changeLoadingIndicator(false);
            });
  }

  inneradd(extData){
    this.changeLoadingIndicator(true);
    this.FormComponent.clickevent({method: 'add', params: [extData]});
  }

  inneredit(id, extData?){
    this.changeLoadingIndicator(true);
    this.logging(this.FormComponent,'inneredit----FormComponent');
    this.FormComponent.edit(id, extData);
  }

  search(event: any) {
    if (!this.isValidSearchEvent(event.mode)){
      return;
    }
    let list = true;
    switch (event.mode){
      case 'size':
            this.params.apiparams.page.size = event.event.value;
            break;
      case 'filter':
            list = (event.event.key === 'Enter');
            break;
    }
    if (list)
      this.list();
  }

  isValidSearchEvent(mode){
    const validMode = ['size', 'filter', 'filterDn'];
    return this.ExtUnderscore.inArray(validMode, mode);
  }
}
