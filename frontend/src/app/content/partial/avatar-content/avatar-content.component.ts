﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-avatar-content',
    templateUrl: './avatar-content.component.html',
    styleUrls: ['./avatar-content.component.css']
})

export class AvatarContentComponent extends AdminSuperRootComponent{
    public options;
    @Input('avatar') avatar: any = null;
    @Input('mode') mode: string = 'simple';
    @Input('imgClass') imgClass: string = '';
    @Input('placement') placement: string = 'right';
    @Input('maxWidth') maxWidth: number = 400;

    constructor() {
      super();
      this.setOption();
    }

    setOption(){
      this.options = {
        'tooltip-class' : 'templatecontents',
        'content-type' : 'template',
        'theme': 'light',
        'placement' : this.placement,
        'show-delay': 500,
        'max-width': this.maxWidth + 'px'
      };
    }

    isMode(mode){
      this.logging(this.mode, 'this.tooltip', 'isMode');
      this.logging(mode, 'mode', 'isMode');
      this.logging(this.avatar, 'this.avatar', 'isMode');
      return this.mode == mode;
    }
}
