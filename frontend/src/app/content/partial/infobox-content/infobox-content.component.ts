﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-infobox-content',
    templateUrl: './infobox-content.component.html',
    styleUrls: ['./infobox-content.component.css']
})

export class InfoboxContentComponent extends AdminSuperRootComponent{
    @Input('data') data: any = {};

    constructor() {
      super();
    }

    getName(){
      const name = this.getPropVal(this.data, 'fullname', null);
      if  ( !this._.isNull(name)) return name;
      return this.getPropVal(this.data, 'name', '');
    }

    getPosition(){
      const email = this.getPropVal(this.data, 'positions.name', null);
      if  ( !this._.isNull(email)) return email;
      return this.getPropVal(this.data, 'position', '');
    }

    getPhone(){
      const phone = this.getPropVal(this.data, 'primary_phone', null);
      if  ( !this._.isNull(phone)) return phone;
      return this.getPropVal(this.data, 'phone', '');

    }

    getEmail(){
      const email = this.getPropVal(this.data, 'primary_email', null);
      if  ( !this._.isNull(email)) return email;
      return this.getPropVal(this.data, 'email', '');
    }
}
