﻿import { Component, Input} from '@angular/core';
import { AdminSuperRootComponent } from '../../admin/admin-base/admin-super-root/admin-super-root.component';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-popup-infobox',
    templateUrl: './popup-infobox.component.html',
    styleUrls: ['./popup-infobox.component.css']
})

export class PopupInfoboxComponent extends AdminSuperRootComponent{
  @Input('data') data: any = null;

  constructor() {
    super();
  }
}
