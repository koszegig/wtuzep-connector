import { Component,  Output , EventEmitter } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-users-modal',
  templateUrl: './admin-users-modal.component.html',
  styleUrls: ['./admin-users-modal.component.css']
})
export class AdminUsersModalComponent  extends AdminBaseModalComponent {

  @Output() updateUserEmmitter = new EventEmitter<any>();
  @Output() saveAvatarEmmitter = new EventEmitter<any>();
  constructor() {
    super();
   }

  selection(userdata,  project_id = null){
    this.content   = 'selection';
    this.clickeventparams = {method: 'selectlist',  params : [userdata, project_id]};
  }

  uploadAvatar(){
    this.content   = 'uploadAvatar';
    this.clickeventparams = {method: 'uploadAvatar',  params : []};
  }

  updateUser(users:  any){
    this.logging('users');
    this.logging(users);
    this.updateUserEmmitter.emit(users);
  }

  saveAvatar(avatar:  any){
    this.saveAvatarEmmitter.emit(avatar);
  }

}
