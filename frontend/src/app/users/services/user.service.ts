import { User } from '../models/user';
import { MainrestService} from '../../services/base/mainrest.service';
import { Observable } from 'rxjs/Rx';

export class UserService extends MainrestService {

    protected controller: string  = 'user';

  	constructor() {
      super();
	   }

    create(user: User) {
        return super.create(user);
    }

    update(user: User) {
        return super.update(user);
    }

    registration(user: User) {
        return this.post('registration', [], user, false);
    }

    password_reset(password: string, token: string) {
        return this.post('password_reset', [], {password: password, token: token}, false);
    }

    password_reset_request(id: string) {
        return this.get('password_reset_request', [id]);
    }

    forgot_username(id: string) {
        return this.get('forgott_username', [id]);
    }

    getMe() {
        return this.post('me', [], false);
    }

    profile() {
      return this.get('profile', []);
    }
    updateMe(user: User) {
        return this.put('me', [], user);
    }

    refreshToken() {
        return this.get('refresh_token', [], true, true);
    }

    update_token(){
        const self = this;
        Observable.interval(30000 * 10)
        .flatMap(() => this.refreshToken())
        .map((response: Response) => this.convertResponse(response)).catch(this.handleError.bind(this)).subscribe(data => {
            self.updateToken(data.data);
        });
    }

    updateToken(token: string){
        const user: any = JSON.parse(localStorage.getItem('currentUser'));
        user.token = token;
        localStorage.setItem('currentUser', JSON.stringify(user));
    }
}
