import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { UParams } from '../models/uparams';
import { UserService } from '../services/user.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminUsersModalComponent } from '../admin-users-modal/admin-users-modal.component';
/*import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';
import { DtTextDn } from '../../models/datatable/columns.ts/dtTextDn';
import { DtAvatarNone } from '../../models/datatable/columns.ts/dtAvatarNone';*/
@Component({
  selector: 'admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminUsersListComponent extends AdminBaseListComponent {

  @ViewChild(AdminUsersModalComponent, {static: false}) FormComponent: AdminUsersModalComponent;
  constructor(public Service: UserService,
    protected cdRef: ChangeDetectorRef) {
    super();
    this.initialData();
    this.params = new UParams();
  }

  initialData(){
  }
}
