
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UserService } from './services/user.service';
import { TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminImageUploadAndCropComponent } from '../admin/admin-base/admin-image-upload-and-crop/admin-image-upload-and-crop.component';
import { AdminUsersFormComponent } from './admin-users-form/admin-users-form.component';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { AdminUsersModalComponent } from './admin-users-modal/admin-users-modal.component';
import { AdminUsersSelectionComponent } from './admin-users-selection/admin-users-selection.component';

@NgModule({
	declarations: [
    AdminUsersFormComponent,
    AdminUsersListComponent,
    AdminUsersModalComponent,
    AdminUsersSelectionComponent,
    AdminImageUploadAndCropComponent
  ],
	imports: [
		BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    ImageCropperModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ],
  providers: [
    UserService,
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Custom not found'
      }
  	}
  ],
	exports: [AdminUsersFormComponent, AdminUsersListComponent, AdminUsersModalComponent, AdminUsersSelectionComponent]
})
export class UsersModule { }
