import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { UserService} from '../../users/services/user.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';
import { emailValidator} from '../../validators/validators';
import { Usergroupitem } from '../../models/index';
import { NameFieldComponent} from '../../base-elements/directives/fields/name-field/name-field.component';
@Component({
  selector: 'admin-users-form',
  templateUrl: './admin-users-form.component.html',
  styleUrls: ['./admin-users-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminUsersFormComponent extends AdminBaseFormComponent{

  userGroups = [];

  groups: any;
  stages: any;
  @ViewChild(NameFieldComponent, {static: false}) namefield: NameFieldComponent;

  constructor(
      public Service: UserService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }


  initialform(){
		this.ModeForm = this.fb.group({
        username: ['', this.Validators.required],
        status: ['active', this.Validators.required],
        birth_date: ['', this.Validators.required],
        emails: this.fb.array([]),
        phones: this.fb.array([]),
    });
    this.detectChanges();
    this.logging('lefut', 'lefut', 'initialform');
  }

  fillForm(){
    this.ModeForm.patchValue({
        username: this.currentItem.username,
        status: this.currentItem.status,
        birth_date: this.currentItem.birth_date,
    });
    this.markForCheck();
    this.detectChanges();
  }


  callBackOpenModal(){
    super.callBackOpenModal();
    this.namefield.setName(this.getName());
  }

  callbackapipre(){
    const name = this.namefield.getForm();
    if (!name.valid){ this.loading = false; return false; }
    this.setName(name.value);
    super.callbackapipre();
    this.current.birth_date = this.initialDate(this.current.birth_date).toString();
    this.current.groups = this._.pluck(this.ExtUnderscore.filterchecked(this.current.groups), 'name');
    return true;
  }

    listGroups(){

    }



    addEmail(){
      const emails = this.getFormArray('emails');
      emails.push(this.createEmail());
    }

    addEmails(){
      const emails = this.getFormArray('emails');
      if (this.mode == 'edit'){
        if (this._.isEmpty(this.currentItem.emails)){
          emails.push(this.createEmail());
        }
        else {
          const self = this;
          this._.each(this.currentItem.emails, function(email){
              self.logging(email, 'email', 'addEmails');
              emails.push(self.createEmail(email.id, email.email, email.primary));
          });
        }
      }
      else{

        emails.push(this.createEmail());
      }
    }

    createEmail(id = null, email= '', primary= true){
      return this.fb.group({
        id: id,
        email: [email, this.Validators.compose([this.Validators.required,  emailValidator])],
        primary: new this.FormControl(primary) //email.primary
    });
    }
    addPhone(){
      const phones = this.getFormArray('phones');
      phones.push(this.createPhone());
    }
    addPhones(){
      const phones = this.getFormArray('phones');
      if (this.mode == 'edit'){
        if (this._.isEmpty(this.currentItem.phones)){
          phones.push(this.createPhone());
        }
        else {
          const self = this;
          this._.each(this.currentItem.phones, function(item){
            phones.push(self.createPhone(item.id, item.phone, item.primary));
          });
        }
      }
      else{
        phones.push(this.createPhone());
      }

    }
    createPhone(id = null, phone= '', primary= true){
      return this.fb.group({
        id: id,
        phone: [phone, this.Validators.compose([this.Validators.required  ])],
        primary: new this.FormControl(primary) //phone.primary
    });
    }
    generateArrayItem(item: any){
      return this.fb.group({
        name: item.name,
        display_name: item.display_name,
        checked: item.checked
      });
    }

    updateFormArray(formArray: any, index, item){
      formArray.at(index).patchValue({
        checked: item.checked
      });
      return formArray;
    }

    getName(){
      return {
        firstname: this.__.get(this.currentItem, 'firstname', ''),
        middlename: this.__.get(this.currentItem, 'middlename', ''),
        lastname: this.__.get(this.currentItem, 'lastname', ''),
      };
    }

    setName(name){
      this.current.firstname = name.firstname;
      this.current.middlename = name.middlename;
      this.current.lastname = name.lastname;
    }

}
