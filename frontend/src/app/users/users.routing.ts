﻿
import { AdminUsersListComponent} from '../users/admin-users-list/admin-users-list.component';

export const USERS_ROUTING = [
  { path: 'user-list', component: AdminUsersListComponent},
];
