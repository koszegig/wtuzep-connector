export class User {
    id: string;
    username: string;
    email: string;
    password: string;
    password_confirmation: string;
    firstname: string;
    lastname: string;
}