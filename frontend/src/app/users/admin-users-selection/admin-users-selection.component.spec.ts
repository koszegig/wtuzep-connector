import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsersSelectionComponent } from './admin-users-selection.component';

describe('AdminUsersSelectionComponent', () => {
  let component: AdminUsersSelectionComponent;
  let fixture: ComponentFixture<AdminUsersSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUsersSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsersSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
