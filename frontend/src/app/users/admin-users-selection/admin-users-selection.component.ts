import { Component, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy  } from '@angular/core';
import { UParams } from '../models/uparams';
import { UserService } from '../services/user.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
/*import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';
import { DtTextDn } from '../../models/datatable/columns.ts/dtTextDn';
import { DtAvatarNone } from '../../models/datatable/columns.ts/dtAvatarNone';
import { DtCheckboxNone} from '../../models/datatable/columns.ts/dtCheckboxNone';*/

@Component({
  selector: 'admin-users-selection',
  templateUrl: './admin-users-selection.component.html',
  styleUrls: ['./admin-users-selection.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminUsersSelectionComponent extends AdminBaseListComponent {

  userlist: any;
  positions: any;
  sections: any;
  stages: any;
  @Output() updateUserEmmitter = new EventEmitter<any>();

  constructor(public Service: UserService,
    protected cdRef: ChangeDetectorRef) {
    super();
    this.getlists();
    this.params = new UParams();
    this.isModal = true;
  }

  selectlist(userdata, project_id = null){
    if (!this._.isNull(project_id)) this.params.params.filter['project_id'] = project_id;
    this.logging(userdata, 'userdata', 'selectlist');
    this.userlist = userdata;
  //  this.openModal();
    this.setPage({ page: 1 });
  }

  listPost(){
    this.rows = this.genUsersData();
  }

  /*_list(){
    if (this.__.get(this.params, 'params.filter.project_id', false) === false) return super._list();
    return this.Service.listProject(this.params.params);
  }*/

  genUsersData(){
    const self = this;
    return this._.map(this.rows, function(item){
      const found = self._.findWhere(self.userlist, {id: item.id});
      item.checked = !self._.isUndefined(found);
      return item;
    });
  }

  save(){
   /* let users = this.ExtUnderscore.filterchecked(this.rows);
    if (this._.isObject(users[0].avatar)){
      users = this._.pluck(users, 'avatar');
      users = this._.map(users, function(user){ return user[0]; });
    }
    this.logging(users, 'users', 'save');*/
    this.logging(this.userlist, 'this.userlist', 'save');
    this.updateUserEmmitter.emit(this.userlist);
    this.closeModal();
  }

  onChangeCheckbox(data){
    this.logging(this.rows, 'this.rows', 'onChangeCheckbox');
    const row = this._.filter(this.rows, function(obj: any) {
      return obj.id === data.id;
    });
    this.rows = this._.filter(this.rows, function(obj: any) {
      return obj.id !== data.id;
    });
    this.logging(row, 'row', 'onChangeCheckbox');
    this.userlist = this._.filter(this.userlist, function(obj: any) {
      return obj.id !== data.id;
    });
    this.logging(this.userlist, 'this.userlist', 'onChangeCheckbox');
    if (data.checked){
      if (this._.isString(row[0].avatar))
        this.userlist.push(row[0]);
      else
        this.userlist.push(row[0].avatar[0]);
    }
    this.logging(this.userlist, 'this.userlist', 'onChangeCheckbox');
    row[0].checked = data.checked;
    this.rows.push(row[0]);
    this.rows = this._.sortBy(this.rows, 'fullname');
  }

  getlists(){
    this.initialData();
  }

  initialData(){
    /*this.ncolumns = [ new DtAvatarNone(false, 'avatar', 'common.avatar', '', 'avatar'),
                      new DtNormalCol(true, 'fullname', 'common.fullname', '', 'fullname'),
                      new DtCheckboxNone(false, 'checked', 'common.select', '', 'checked'),
                    ];*/
  }
}

