﻿
import {AdminLogsListComponent } from '../Logs/admin-logs-list/admin-logs-list.component';

export const LOGS_ROUTING = [
  { path: 'logs-list',
    component: AdminLogsListComponent
  }
];
