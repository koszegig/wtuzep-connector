import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { lgparams } from '../models/logparams';
import {  LogService } from '../services/log.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminLogsModalComponent } from '../admin-logs-modal/admin-logs-modal.component';
import additionalcolumns from '../param/additionalcolumns.json';
import {attparams} from '../../attributes/models/attparams';


@Component({
  selector: 'admin-logs-list',
  templateUrl: './admin-logs-list.component.html',
  styleUrls: ['./admin-logs-list.component.css']
})
export class AdminLogsListComponent   extends AdminBaseListComponent {

  @ViewChild(AdminLogsModalComponent, {static: false}) FormComponent: AdminLogsModalComponent;
  @Input('subtitle') subtitle: string;
  public buttons: any;
  //public optionsService: OptionsService;
  constructor(public Service: LogService,
              protected cdRef: ChangeDetectorRef) {
    super();
    try {
      //this.Service = this.injector.get(CustomerService);
      //  this.optionsService = this.injector.get(OptionsService);
    } catch (e) {
    }
    this.params = new lgparams();
  }


}
