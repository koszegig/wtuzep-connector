import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLogsFromComponent } from './admin-logs-from.component';

describe('AdminLogsFromComponent', () => {
  let component: AdminLogsFromComponent;
  let fixture: ComponentFixture<AdminLogsFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLogsFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLogsFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
