import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {AdminBaseFormComponent} from '../../admin/admin-base/admin-base-form/admin-base-form.component';
import {NameFieldComponent} from '../../base-elements/directives/fields';
import {LogService} from '../../Logs/services/log.service';

@Component({
  selector: 'admin-logs-from',
  templateUrl: './admin-logs-from.component.html',
  styleUrls: ['./admin-logs-from.component.css']
})
export class AdminLogsFromComponent extends AdminBaseFormComponent{
  public itemes: any;
  public modies: any;
  @ViewChild(NameFieldComponent, {static: false}) namefield: NameFieldComponent;
  constructor(
      public Service: LogService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }

  initialform(){
    this.ModeForm = this.fb.group({
      extra: ['', this.Validators.required],
      message: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      extra: JSON.stringify(this.currentItem.extra),
      message: this.currentItem.message,

    });
  }

}

