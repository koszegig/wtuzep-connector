import { Injectable } from '@angular/core';
import { Log } from '../models/Log';
import { MainrestService} from '../../services/base/mainrest.service';
import {AdminLogsModalComponent} from '../admin-logs-modal/admin-logs-modal.component';

export class LogService  extends MainrestService {

  protected controller: string  = 'log';

  constructor() {
    super();
   }

  create(log: Log) {
      return super.create(log);
  }

  update(log: Log) {
      return super.update(log);
  }

}


