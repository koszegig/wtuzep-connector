import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLogsModalComponent } from './admin-logs-modal.component';

describe('AdminLogsModalComponent', () => {
  let component: AdminLogsModalComponent;
  let fixture: ComponentFixture<AdminLogsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLogsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLogsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
