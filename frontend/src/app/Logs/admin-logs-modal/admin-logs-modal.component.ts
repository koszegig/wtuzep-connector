import { Component, OnInit } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-logs-modal',
  templateUrl: './admin-logs-modal.component.html',
  styleUrls: ['./admin-logs-modal.component.css']
})
export class AdminLogsModalComponent   extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
