import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPermissionsListComponent } from './admin-permissions-list.component';

describe('AdminPermissionsListComponent', () => {
  let component: AdminPermissionsListComponent;
  let fixture: ComponentFixture<AdminPermissionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPermissionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPermissionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
