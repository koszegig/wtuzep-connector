import { Component, ViewChild } from '@angular/core';
import { PeParams } from '../models/peparams';
import { PermissionService } from '../services/permission.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminPermissionsModalComponent } from '../admin-permissions-modal/admin-permissions-modal.component';
//import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';

@Component({
  selector: 'admin-permissions-list',
  templateUrl: './admin-permissions-list.component.html',
  styleUrls: ['./admin-permissions-list.component.css']
})
export class AdminPermissionsListComponent extends AdminBaseListComponent{

  @ViewChild(AdminPermissionsModalComponent, {static: false}) FormComponent: AdminPermissionsModalComponent;
  constructor(public Service: PermissionService) {
    super();
    this.initialData();
    this.params = new PeParams();
  }
  initialData(){
  /*  this.ncolumns = [ new DtNormalCol(true, 'name', 'name.name', '', 'name'),
                      new DtNormalCol(true, 'slug', 'name.slug', '', 'slug'),
                    ];*/
  }

}
