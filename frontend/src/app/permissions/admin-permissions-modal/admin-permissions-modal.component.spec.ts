import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPermissionsModalComponent } from './admin-permissions-modal.component';

describe('AdminPermissionsModalComponent', () => {
  let component: AdminPermissionsModalComponent;
  let fixture: ComponentFixture<AdminPermissionsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPermissionsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPermissionsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
