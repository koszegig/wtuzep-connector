import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-permissions-modal',
  templateUrl: './admin-permissions-modal.component.html',
  styleUrls: ['./admin-permissions-modal.component.css']
})
export class AdminPermissionsModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
   }


}
