import { Permission } from '../models/permission';
import { MainrestService} from '../../services/base/mainrest.service';

export class PermissionService extends MainrestService {

    protected controller: string  = 'permission';

		constructor() {
      super();
	   }

   	create(permission: Permission) {
        return this.post(null, [], permission);
    }
}
