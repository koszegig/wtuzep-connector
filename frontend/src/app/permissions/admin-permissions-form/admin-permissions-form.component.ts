import { Component } from '@angular/core';
import { PermissionService } from '../services/permission.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';

@Component({
  selector: 'admin-permissions-form',
  templateUrl: './admin-permissions-form.component.html',
  styleUrls: ['./admin-permissions-form.component.css']
})
export class AdminPermissionsFormComponent extends AdminBaseFormComponent {

  constructor(
    public Service: PermissionService,
  ) {super();  }

  initialform(){
    this.ModeForm = this.fb.group({
      name: ['', this.Validators.required],
			slug: ['', this.Validators.required],
    });
  }

  fillForm(){

    this.ModeForm.patchValue({
      name: this.currentItem.name,
      slug: this.currentItem.slug,
    });
  }
}
