export class Permission {
    id: string;
    name: string;
    slug: string;
    description: string;
    model: string;
}
