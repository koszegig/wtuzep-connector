
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TooltipModule} from 'ng2-tooltip-directive';

import { AdminPermissionsFormComponent } from './admin-permissions-form/admin-permissions-form.component';
import { AdminPermissionsListComponent } from './admin-permissions-list/admin-permissions-list.component';
import { AdminPermissionsModalComponent} from './admin-permissions-modal/admin-permissions-modal.component';

@NgModule({
	declarations: [
    AdminPermissionsFormComponent,
    AdminPermissionsListComponent,
    AdminPermissionsModalComponent
  ],
	imports: [
    BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
	],
	exports: [AdminPermissionsFormComponent, AdminPermissionsListComponent]
})
export class PermissionsModule { }
