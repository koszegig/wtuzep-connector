import { Injectable } from '@angular/core';
import { Role } from '../models/role';
import { MainrestService} from '../../services/base/mainrest.service';

@Injectable()
export class RoleService extends MainrestService {

    protected controller: string  = 'role';

		constructor() {
      super();
	   }

    create(role: Role) {
        return super.create(role);
    }

    update(role: Role) {
        return super.update(role);
    }

    setPermission(permissions, id) {
        return this.put('permissions', [id], permissions);
    }

}
