
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AdminRolesFormComponent } from './admin-roles-form/admin-roles-form.component';
import { AdminRolesListComponent } from './admin-roles-list/admin-roles-list.component';
import { AdminRolesModalComponent} from './admin-roles-modal/admin-roles-modal.component';
import { AdminRolePermissionsFormComponent} from './admin-role-permissions-form/admin-role-permissions-form.component';

@NgModule({
	declarations: [
    AdminRolesFormComponent,
    AdminRolesListComponent,
    AdminRolesModalComponent,
    AdminRolePermissionsFormComponent
  ],
	imports: [
    BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
	],
	exports: [AdminRolesFormComponent, AdminRolesListComponent]
})
export class RolesModule { }
