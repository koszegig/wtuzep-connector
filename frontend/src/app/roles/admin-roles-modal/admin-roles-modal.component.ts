import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-roles-modal',
  templateUrl: './admin-roles-modal.component.html',
  styleUrls: ['./admin-roles-modal.component.css']
})
export class AdminRolesModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
   }

}
