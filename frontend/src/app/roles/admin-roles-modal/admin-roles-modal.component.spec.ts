import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRolesModalComponent } from './admin-roles-modal.component';

describe('AdminRolesModalComponent', () => {
  let component: AdminRolesModalComponent;
  let fixture: ComponentFixture<AdminRolesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRolesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRolesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
