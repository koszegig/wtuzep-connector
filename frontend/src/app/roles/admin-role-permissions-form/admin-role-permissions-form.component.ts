import { Component} from '@angular/core';
import { RoleService} from '../services/role.service';
import { PermissionService } from '../../permissions/services/permission.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';
import { Rolepermissionitem } from '../../models/item/rolepermissionitem';

@Component({
  selector: 'admin-role-permissions-form',
  templateUrl: './admin-role-permissions-form.component.html',
  styleUrls: ['./admin-role-permissions-form.component.css']
})
export class AdminRolePermissionsFormComponent extends AdminBaseFormComponent {

  permissionlist: any;

  currentRole: any;

  rolePermissions: any;

  constructor(
    public Service: RoleService,
    public PermissionService: PermissionService,
  ) {super();  }

  callbackform(){
    this.promise(
      this.PermissionService.listDn(),
      'permissionlist',
      () => {this.getRolePermissions(); }
    );
  }

  getRolePermissions() {
    this.promise(
      this.Service.getById(this.currentId),
      'rolePermissions',
      () => {super.initial(); },
      'data.permissions'
    );
  }

  initialform(){
  this.ModeForm = this.fb.group({
      permissions: this.fb.array([])
    });
  }

  fillForm(){
  this.buildCheckboxItem('permissions');
  }

  initialItems() {
    const self = this;
    return this._.map(this.permissionlist, function(item){
      const currentitem = self._.findWhere(self.rolePermissions, {name: item.name});
      const checked = !self._.isUndefined(currentitem);
      const permission = new Rolepermissionitem(item.name, checked, item.display_name);
      return permission;
    });
  }

  callbackapipre() {
    const self = this;
    this.current.permissions = this._.pluck(this.ExtUnderscore.filterchecked(this.current.permissions), 'name');
    return super.callbackapipre();
  }

  _update(){
    this.promise(
      this.Service.setPermission(this.current, this.currentId),
      null,
      (val) => {this.callbackapipost('Success', val); },
      'data'
    );
  }

  generateArrayItem(item: any){
    return this.fb.group({
      name: item.name,
      display_name: item.display_name,
      checked: item.checked
    });
  }

  updateFormArray(formArray: any, index, item){
    formArray.at(index).patchValue({
      checked: item.checked
    });
    return formArray;
  }
}
