import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRolePermissionsFormComponent } from './admin-role-permissions-form.component';

describe('AdminRolePermissionsFormComponent', () => {
  let component: AdminRolePermissionsFormComponent;
  let fixture: ComponentFixture<AdminRolePermissionsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRolePermissionsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRolePermissionsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
