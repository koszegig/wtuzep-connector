import { Component} from '@angular/core';
import { RoleService } from '../services/role.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';

@Component({
  selector: 'admin-roles-form',
  templateUrl: './admin-roles-form.component.html',
  styleUrls: ['./admin-roles-form.component.css']
})
export class AdminRolesFormComponent extends AdminBaseFormComponent {

  constructor(
    public Service: RoleService,
  ) {super();  }

  initialform(){
    this.ModeForm = this.fb.group({
      name: ['', this.Validators.required],
			slug: ['', this.Validators.required],
    });
  }

  fillForm(){

    this.ModeForm.patchValue({
      name: this.currentItem.name,
      slug: this.currentItem.slug,
    });
  }

}
