import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { RoParams } from '../models/roparams';
import { RoleService } from '../services/role.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminRolesModalComponent } from '../admin-roles-modal/admin-roles-modal.component';
import { AdminRolePermissionsFormComponent } from '../admin-role-permissions-form/admin-role-permissions-form.component';
//import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';

@Component({
  selector: 'admin-roles-list',
  templateUrl: './admin-roles-list.component.html',
  styleUrls: ['./admin-roles-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRolesListComponent extends AdminBaseListComponent{

  @ViewChild(AdminRolesModalComponent, {static: false}) FormComponent: AdminRolesModalComponent;
  @ViewChild(AdminRolePermissionsFormComponent, {static: false}) PermissionFormComponent: AdminRolePermissionsFormComponent;
  constructor(public Service: RoleService) {
    super();
    this.initialData();
    this.params = new RoParams();

  }
  initialData(){
  /*  this.ncolumns = [ new DtNormalCol(true, 'name', 'name.name', '', 'name'),
                      new DtNormalCol(true, 'slug', 'name.slug', '', 'slug'),
                    ];*/
  }

  relatePermission(id: string){
    this.changeLoadingIndicator(true);
    this.PermissionFormComponent.edit(id);
  }
}
