export class Role {
    id: string;
    name: string;
    slug: string;
    description: string;
    level: number;
}
