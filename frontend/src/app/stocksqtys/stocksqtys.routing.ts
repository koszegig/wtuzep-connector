﻿
import {AdminStocksqtysListComponent } from '../stocksqtys/admin-stocksqtys-list/admin-stocksqtys-list.component';

export const STOCKSQTYS_ROUTING = [
  { path: 'stocksqtys-list',
    component: AdminStocksqtysListComponent
  }
];
