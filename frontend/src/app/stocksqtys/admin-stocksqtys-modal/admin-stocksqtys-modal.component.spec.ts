import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStocksqtysModalComponent } from './admin-stocksqtys-modal.component';

describe('AdminStocksqtysModalComponent', () => {
  let component: AdminStocksqtysModalComponent;
  let fixture: ComponentFixture<AdminStocksqtysModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStocksqtysModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStocksqtysModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
