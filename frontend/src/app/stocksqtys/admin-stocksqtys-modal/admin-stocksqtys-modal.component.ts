import { Component, OnInit } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-stocksqtys-modal',
  templateUrl: './admin-stocksqtys-modal.component.html',
  styleUrls: ['./admin-stocksqtys-modal.component.css']
})
export class AdminStocksqtysModalComponent extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}