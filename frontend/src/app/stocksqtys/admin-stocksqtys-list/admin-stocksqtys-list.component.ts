import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { stqparams } from '../models/stqparams';
import { StocksqtyService } from '../services/stocksqty.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminStocksqtysModalComponent } from '../admin-stocksqtys-modal/admin-stocksqtys-modal.component';
import additionalcolumns from '../param/additionalcolumns.json';
import hosts from '../param/hosts.json';
/*import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';
import { DtTextDate } from '../../models/datatable/columns.ts/dtTextDate';
import { DtBoolBool } from '../../models/datatable/columns.ts/dtBoolBool';*/

@Component({
  selector: 'admin-stocksqtys-list',
  templateUrl: './admin-stocksqtys-list.component.html',
  styleUrls: ['./admin-stocksqtys-list.component.css']
})
export class AdminStocksqtysListComponent  extends AdminBaseListComponent {

  @ViewChild(AdminStocksqtysModalComponent, {static: false}) FormComponent: AdminStocksqtysModalComponent;
  @Input('subtitle') subtitle: string;
  public buttons: any;
  //public optionsService: OptionsService;
  constructor(public Service: StocksqtyService,
              protected cdRef: ChangeDetectorRef) {
    super();
    try {
      //this.Service = this.injector.get(CustomerService);
      //  this.optionsService = this.injector.get(OptionsService);
    } catch (e) {
    }
    this.params = new stqparams();
  }


}