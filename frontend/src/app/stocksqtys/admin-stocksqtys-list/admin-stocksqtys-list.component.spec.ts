import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStocksqtysListComponent } from './admin-stocksqtys-list.component';

describe('AdminStocksqtysListComponent', () => {
  let component: AdminStocksqtysListComponent;
  let fixture: ComponentFixture<AdminStocksqtysListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStocksqtysListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStocksqtysListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
