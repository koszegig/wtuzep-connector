import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStocksqtysFromComponent } from './admin-stocksqtys-from.component';

describe('AdminStocksqtysFromComponent', () => {
  let component: AdminStocksqtysFromComponent;
  let fixture: ComponentFixture<AdminStocksqtysFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStocksqtysFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStocksqtysFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
