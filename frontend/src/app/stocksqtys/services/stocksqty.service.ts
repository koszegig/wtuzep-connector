import { Injectable } from '@angular/core';
import { Stocksqty } from '../models/Stocksqty';
import { MainrestService} from '../../services/base/mainrest.service';
import {AdminStocksqtysModalComponent} from '../admin-stocksqtys-modal/admin-stocksqtys-modal.component';

export class StocksqtyService  extends MainrestService {

  protected controller: string  = 'stocksqty';

  constructor() {
    super();
   }

  create(stocksqty: Stocksqty) {
      return super.create(stocksqty);
  }

  update(stocksqty: Stocksqty) {
      return super.update(stocksqty);
  }

}


