﻿
import {AdminProductsListComponent } from '../products/admin-products-list/admin-products-list.component';

export const PRODUCT_ROUTING = [
  { path: 'products-list',
    component: AdminProductsListComponent
  }
];
