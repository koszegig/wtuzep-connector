import {Params} from '../../models/listviews/params/params';
import param from '../param/param.json';
import columns from '../param/columns.json';
export class PParams extends Params {

  constructor() {
    super(param, columns);
  }
}
