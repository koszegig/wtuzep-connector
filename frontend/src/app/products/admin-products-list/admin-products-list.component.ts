import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { PParams } from '../models/pparams';
import { ProductsService } from '../services/products.service';
//import { OptionsService } from '../../options/services/options.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminProductsModalComponent } from '../admin-products-modal/admin-products-modal.component';
import additionalcolumns from '../param/additionalcolumns.json';

@Component({
  selector: 'admin-products-list',
  templateUrl: './admin-products-list.component.html',
  styleUrls: ['./admin-products-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminProductsListComponent  extends AdminBaseListComponent {

  @ViewChild(AdminProductsModalComponent, {static: false}) FormComponent: AdminProductsModalComponent;
  @Input('subtitle') subtitle: string;
  public buttons: any;
  //public optionsService: OptionsService;
  public statusfilter: string;
  public statusop: string;
  host: any;
  constructor(public Service: ProductsService,
              protected cdRef: ChangeDetectorRef) {
    super();
    try {
      //this.Service = this.injector.get(CustomerService);
      //  this.optionsService = this.injector.get(OptionsService);
    } catch (e) {
    }
    this.params = new PParams();
    //this.host = hosts[this._.last(this.router.url.split('/'))];
    //this.host = hosts[this._.last(this.router.url.split('-'))];

    //this.params.mods.AddActivateButton = false;

    //this.params.mods.InactivateButton = false;
  }

  public pushStatusFilter(){
    const segments: Array<string> = this.router.url.split('/');
    //this.host =  hosts[segments[3]];
    additionalcolumns[0].filter.value = this.statusfilter;
    additionalcolumns[0].filter.field[0].op = this.statusop;
    additionalcolumns[1].filter.value = this.host;
    this.params.pushColumns(additionalcolumns);
  }

  listPre(){
    this.pushStatusFilter();
    this.listButtons();
    this.listButtons();
    super.listPre();
  }

  listButtons(){
    /*this.promiseAsync(
        this.optionsService.listDn({language: 'hungary', parent: 'statuses_order'}),
        'buttons',
        'data'
    );*/
  }

  setStatus(status, id){
    this.changeLoadingIndicator(true);
    this.Service.setStatus(status, id).subscribe(
        data => {
          this.refreshTableRow();
          this.changeLoadingIndicator(false);
        },
        error => {
          this.alertService.error(error);
          this.changeLoadingIndicator(false);
        }
    );
  }

  OrderOpen(id){
    this.setStatus('open', id);
  }

  OrderPaid(id){
    this.setStatus('paid', id);
  }

  OrderPosted(id){
    this.setStatus('posted', id);
  }
  Orderclosed(id){
    this.setStatus('closed', id);
  }
  setUpdateAllSynced(status, id){
    this.changeLoadingIndicator(true);
    this.Service.setUpdateAllSynced(status, id).subscribe(
        data => {
          this.refreshTableRow();
          this.changeLoadingIndicator(false);
        },
        error => {
          this.alertService.error(error);
          this.changeLoadingIndicator(false);
        }
    );
  }
}
