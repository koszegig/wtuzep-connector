import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProductsModalComponent } from './admin-products-modal.component';

describe('AdminProductsModalComponent', () => {
  let component: AdminProductsModalComponent;
  let fixture: ComponentFixture<AdminProductsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProductsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProductsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
