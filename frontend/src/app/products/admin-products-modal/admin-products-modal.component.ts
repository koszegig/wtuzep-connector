import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-products-modal',
  templateUrl: './admin-products-modal.component.html',
  styleUrls: ['./admin-products-modal.component.css']
})
export class AdminProductsModalComponent extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}