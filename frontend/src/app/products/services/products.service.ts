import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { MainrestService} from '../../services/base/mainrest.service';

export class ProductsService  extends MainrestService {

  protected controller: string  = 'product';

  constructor() {
    super();
   }

  create(product: Product) {
      return super.create(product);
  }

  update(product: Product) {
      return super.update(product);
  }
  setStatus(status: string, ID: string){
    return this.post('status', [ID], {flag: status});
  }
  setUpdateAllSynced(status: string, ID: string){
        return this.post('allsynced', [ID], {flag: status});
  }
}


