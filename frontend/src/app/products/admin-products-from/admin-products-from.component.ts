import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { ProductsService } from '../services/products.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';
//import { OptionsService } from '../../options/services/options.service';
import { NameFieldComponent} from '../../base-elements/directives/fields/name-field/name-field.component';


@Component({
  selector: 'admin-products-from',
  templateUrl: './admin-products-from.component.html',
  styleUrls: ['./admin-products-from.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminProductsFromComponent extends AdminBaseFormComponent{
  public itemes: any;
  public modies: any;
  @ViewChild(NameFieldComponent, {static: false}) namefield: NameFieldComponent;
  constructor(
      public Service: ProductsService,
      //public OptionsService: OptionsService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }

  initialform(){
    this.ModeForm = this.fb.group({
      synced: ['', this.Validators.required],
      syncedorder: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      synced: this.currentItem.synced,
      syncedorder: this.currentItem.syncedorder,

    });
  }

}

