import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProductsFromComponent } from './admin-products-from.component';

describe('AdminProductsFromComponent', () => {
  let component: AdminProductsFromComponent;
  let fixture: ComponentFixture<AdminProductsFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProductsFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProductsFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
