import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProductsService } from './services/products.service';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminProductsFromComponent } from './admin-products-from/admin-products-from.component';
import { AdminProductsListComponent } from './admin-products-list/admin-products-list.component';
import { AdminProductsModalComponent } from './admin-products-modal/admin-products-modal.component';

//import { AdminImageUploadAndCropComponent } from '../admin/admin-base/admin-image-upload-and-crop/admin-image-upload-and-crop.component';

@NgModule({
	declarations: [
        AdminProductsFromComponent,
        AdminProductsListComponent,
        AdminProductsModalComponent//,
    //AdminImageUploadAndCropComponent
  ],
	imports: [
		BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    ImageCropperModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ],
  providers: [
    ProductsService,
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Book not found'
      }
  	}
  ],
	exports: [AdminProductsFromComponent, AdminProductsListComponent, AdminProductsModalComponent]
})

export class ProductsModule { }
