﻿
import {AdminOrdersListComponent } from '../orders/admin-orders-list/admin-orders-list.component';

export const ORDERS_ROUTING = [
  { path: 'orders-list',
    component: AdminOrdersListComponent
  }
];
