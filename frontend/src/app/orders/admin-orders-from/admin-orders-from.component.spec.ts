import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOrdersFromComponent } from './admin-orders-from.component';

describe('AdminOrdersFromComponent', () => {
  let component: AdminOrdersFromComponent;
  let fixture: ComponentFixture<AdminOrdersFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOrdersFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrdersFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
