import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { OrdersService } from '../services/orders.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';

@Component({
  selector: 'admin-orders-from',
  templateUrl: './admin-orders-from.component.html',
  styleUrls: ['./admin-orders-from.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminOrdersFromComponent  extends AdminBaseFormComponent{
  constructor(
      public Service: OrdersService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }
  ngOnInit() {
  }
  initialform(){
    this.ModeForm = this.fb.group({
      synced: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      synced: this.currentItem.synced,

    });
  }
}
