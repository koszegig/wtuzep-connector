import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { OrdersParams } from '../models/ordersparams';
import { OrdersService } from '../services/orders.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminOrdersModalComponent } from '../admin-orders-modal/admin-orders-modal.component';

@Component({
  selector: 'admin-orders-list',
  templateUrl: './admin-orders-list.component.html',
  styleUrls: ['./admin-orders-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminOrdersListComponent extends AdminBaseListComponent {
@ViewChild(AdminOrdersModalComponent, {static: false}) FormComponent: AdminOrdersModalComponent;

  constructor(public Service: OrdersService,
      protected cdRef: ChangeDetectorRef) {
    super();
    this.params = new OrdersParams();

  }
  ngOnInit() {
    super.ngOnInit();
    this.logging(this.FormComponent,'ngOnInit----FormdComponent');
  }

}
