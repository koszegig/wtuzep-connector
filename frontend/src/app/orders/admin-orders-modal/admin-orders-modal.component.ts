import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-orders-modal',
  templateUrl: './admin-orders-modal.component.html',
  styleUrls: ['./admin-orders-modal.component.css']
})
export class AdminOrdersModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
