import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOrdersModalComponent } from './admin-orders-modal.component';

describe('AdminOrdersModalComponent', () => {
  let component: AdminOrdersModalComponent;
  let fixture: ComponentFixture<AdminOrdersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOrdersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrdersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
