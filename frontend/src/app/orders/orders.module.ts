import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OrdersService } from './services/orders.service';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminOrdersModalComponent } from './admin-orders-modal/admin-orders-modal.component';
import { AdminOrdersFromComponent } from './admin-orders-from/admin-orders-from.component';
import { AdminOrdersListComponent } from './admin-orders-list/admin-orders-list.component';
import {AppComponent} from '../app.component';

//import { AdminImageUploadAndCropComponent } from '../admin/admin-base/admin-image-upload-and-crop/admin-image-upload-and-crop.component';

@NgModule({
	declarations: [
        AdminOrdersModalComponent,
        AdminOrdersFromComponent,
        AdminOrdersListComponent//,
    //AdminImageUploadAndCropComponent
  ],
	imports: [
		BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    ImageCropperModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ],
  providers: [
      OrdersService,
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Book not found'
      }
  	}
  ],
	exports: [AdminOrdersFromComponent, AdminOrdersListComponent, AdminOrdersModalComponent]
})

export class OrdersModule { }
