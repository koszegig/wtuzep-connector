import { Injectable } from '@angular/core';
import { Order } from '../models/order';
import { MainrestService} from '../../services/base/mainrest.service';

export class OrdersService  extends MainrestService {

  protected controller: string  = 'order';

  constructor() {
    super();
   }

  create(order: Order) {
      return super.create(Order);
  }

  update(order: Order) {
      return super.update(Order);
  }
  setStatus(status: string, ID: string){
    return this.post('status', [ID], {flag: status});
  }
}


