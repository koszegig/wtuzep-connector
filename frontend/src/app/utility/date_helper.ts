import moment from 'moment';
import {LogHelper} from './log_helper';
import * as _ from 'underscore';
import * as __ from 'lodash';
import 'moment/locale/hu';

export class DateHelper{

  private date: any = null;

  constructor(date = null, format = 'YYYY-MM-DD'){
      this.setDate(date, format);
  }

  setDate(date, format){
    if (_.isEmpty(date)) return;
    this.date = this.initial(date, format);
  }

  initial(date, format){
    if  (_.isObject(date)) return this.setMoment(date);
    if (date == 'now') return moment();
    return moment(date, format);
  }

  setMoment(date){
    const _date = {
      year : this.getYear(date),
      month : this.getMonth(date),
      date : this.getDay(date),
    };
    return moment().set(_date);
  }

  getYear(val){
    return __.get(val, 'year', null);
  }

  getMonth(val){
    let month = __.get(val, 'month', null);
    month = month - 1;
    return month;
  }

  getDay(val){
    return __.get(val, 'day', null);
  }

  toString(){
    if (_.isNull(this.date)) return '';
    return this.date.format('YYYY-MM-DD');
  }

  toObject(){
    if (_.isNull(this.date)) return {};
    return this.date.toObject();
  }

  toNgbDateObject(){
    const object = this.toObject();
    return {
      year: __.get(object, 'years', null),
      month: __.get(object, 'months', null) + 1,
      day: __.get(object, 'date', null),
      };
  }

  add(number, unit){
    LogHelper.logging(this.date, 'this.date', 'add');
    LogHelper.logging(number, 'number', 'add');
    LogHelper.logging(unit, 'unit', 'add');
    this.date.add(number, unit);
  }

  subtract(number, unit){
    LogHelper.logging(this.date, 'this.date', 'subtract');
    LogHelper.logging(number, 'number', 'subtract');
    LogHelper.logging(unit, 'unit', 'subtract');
    this.date.subtract(number, unit);
  }
}
