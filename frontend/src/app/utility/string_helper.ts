export class StringHelper {

    static truncate(str, no_words, maxletter = 35) {
        if ( str == '' || str == null) return '';
        const spliced =  str.split(' ').splice(0, no_words).join(' ');
        let substr = spliced.substring(0, maxletter);
        if (str.length > maxletter)
          substr = substr + '...';
        return substr;
    }
}
