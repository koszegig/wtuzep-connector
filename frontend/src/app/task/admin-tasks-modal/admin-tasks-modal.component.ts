import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-tasks-modal',
  templateUrl: './admin-tasks-modal.component.html',
  styleUrls: ['./admin-tasks-modal.component.css']
})
export class AdminTasksModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
