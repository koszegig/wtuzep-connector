import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTasksModalComponent } from './admin-tasks-modal.component';

describe('AdminTasksModalComponent', () => {
  let component: AdminTasksModalComponent;
  let fixture: ComponentFixture<AdminTasksModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTasksModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTasksModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
