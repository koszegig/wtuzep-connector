import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTasksFormComponent } from './admin-tasks-form.component';

describe('AdminTasksFormComponent', () => {
  let component: AdminTasksFormComponent;
  let fixture: ComponentFixture<AdminTasksFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTasksFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTasksFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
