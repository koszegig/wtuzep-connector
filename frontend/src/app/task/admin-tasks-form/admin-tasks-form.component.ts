import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';


@Component({
  selector: 'admin-tasks-form',
  templateUrl: './admin-tasks-form.component.html',
  styleUrls: ['./admin-tasks-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminTasksFormComponent extends AdminBaseFormComponent{
  constructor(
      public Service: TasksService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }
  ngOnInit() {
  }
  initialform(){
    this.ModeForm = this.fb.group({
      stopme: ['', this.Validators.required],
      cron: ['', this.Validators.required],
      limit: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      stopme: this.currentItem.stopme,
      cron: this.currentItem.cron,
      limit: this.currentItem.limit,

    });
  }
}

