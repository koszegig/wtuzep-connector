﻿
import { AdminTasksListComponent} from '../task/admin-tasks-list/admin-tasks-list.component';

export const TASKS_ROUTING = [
  { path: 'task-list',
    component: AdminTasksListComponent
  }
];
