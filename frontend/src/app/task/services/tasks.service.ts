import { Injectable } from '@angular/core';
import { Task } from '../models/task';
import { MainrestService} from '../../services/base/mainrest.service';

export class TasksService  extends MainrestService {

  protected controller: string  = 'task';

  constructor() {
    super();
   }

  create(task: Task) {
      return super.create(task);
  }

  update(task: Task) {
      return super.update(task);
  }
  setStatus(status: string, ID: string){
    return this.post('status', [ID], {flag: status});
  }
}


