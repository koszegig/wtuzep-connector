import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { TaskParams } from '../models/taskparams';
import { TasksService } from '../services/tasks.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminTasksModalComponent } from '../admin-tasks-modal/admin-tasks-modal.component';

@Component({
  selector: 'admin-tasks-list',
  templateUrl: './admin-tasks-list.component.html',
  styleUrls: ['./admin-tasks-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminTasksListComponent  extends AdminBaseListComponent {
  @ViewChild(AdminTasksModalComponent, {static: false}) FormComponent: AdminTasksModalComponent;

  constructor(public Service: TasksService,
              protected cdRef: ChangeDetectorRef) {
    super();
    this.params = new TaskParams();

  }
  ngOnInit() {
    super.ngOnInit();
    this.logging(this.FormComponent,'ngOnInit----FormdComponent');
  }

}
