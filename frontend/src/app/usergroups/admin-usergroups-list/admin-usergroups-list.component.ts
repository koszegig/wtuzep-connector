import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UGParams } from '../models/ugparams';
import { UserGroupService } from '../services/user-group.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminUsergroupsModalComponent } from '../admin-usergroups-modal/admin-usergroups-modal.component';
import { AdminUsergroupsRolesFormComponent } from '../admin-usergroups-roles-form/admin-usergroups-roles-form.component';
/*import { DtNormalCol } from '../../models/datatable/columns.ts/dtNormalCol';*/

@Component({
  selector: 'admin-usergroups-list',
  templateUrl: './admin-usergroups-list.component.html',
  styleUrls: ['./admin-usergroups-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminUsergroupsListComponent extends AdminBaseListComponent {

  @ViewChild(AdminUsergroupsModalComponent, {static: false}) FormComponent: AdminUsergroupsModalComponent;
  @ViewChild(AdminUsergroupsRolesFormComponent, {static: false}) RoleFormComponent: AdminUsergroupsRolesFormComponent;

  constructor(public Service: UserGroupService,
    protected cdRef: ChangeDetectorRef) {
    super();
    this.initialData();
    this.params = new UGParams();
    this.params.mods.ActivateButton = false;
    this.params.mods.InactivateButton = false;
  }

  initialData(){
  /*  this.ncolumns = [ new DtNormalCol(true, 'usergroup_name', 'common.usergroup_name', '', 'usergroup_name'),
                    ];*/
  }



  relateRole(id: string){
    this.changeLoadingIndicator(true);
   // this.RoleFormComponent.edit(id);
  }
}
