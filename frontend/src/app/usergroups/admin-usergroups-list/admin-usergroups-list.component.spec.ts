import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsergroupsListComponent } from './admin-usergroups-list.component';

describe('AdminUsergroupListComponent', () => {
  let component: AdminUsergroupsListComponent;
  let fixture: ComponentFixture<AdminUsergroupsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUsergroupsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsergroupsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
