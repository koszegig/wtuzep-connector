import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsergroupsFormComponent } from './admin-usergroups-form.component';

describe('AdminUsergroupFormComponent', () => {
  let component: AdminUsergroupsFormComponent;
  let fixture: ComponentFixture<AdminUsergroupsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUsergroupsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsergroupsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
