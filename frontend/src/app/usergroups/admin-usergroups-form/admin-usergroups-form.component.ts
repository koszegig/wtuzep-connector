import { Component} from '@angular/core';
import { UserGroupService } from '../services/user-group.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';


@Component({
  selector: 'admin-usergroups-form',
  templateUrl: './admin-usergroups-form.component.html',
  styleUrls: ['./admin-usergroups-form.component.css']
})
export class AdminUsergroupsFormComponent extends AdminBaseFormComponent{

  constructor(public Service: UserGroupService) {
    super();
  }

  initialform(){
		this.ModeForm = this.fb.group({
			name: ['', this.Validators.required],
		});
	}

  	fillForm(){
    	this.ModeForm.patchValue({
        	name: this.currentItem.name,
    	});
  	}

}
