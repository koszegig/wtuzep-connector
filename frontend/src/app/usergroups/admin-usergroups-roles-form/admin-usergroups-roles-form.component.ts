import { Component} from '@angular/core';
import { RoleService } from '../../roles/services/role.service';
import { UserGroupService } from '../services/user-group.service';
import { AdminBaseFormComponent } from './../../admin/admin-base/admin-base-form/admin-base-form.component';
import { Rolepermissionitem } from '../../models/item/rolepermissionitem';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'admin-usergroups-roles-form',
  templateUrl: './admin-usergroups-roles-form.component.html',
  styleUrls: ['./admin-usergroups-roles-form.component.css']
})
export class AdminUsergroupsRolesFormComponent extends AdminBaseFormComponent {

  rolelist: any;

  currentRole: any;

  userGroupRoles: any;

  constructor(
      public Service: UserGroupService,
      public roleService: RoleService,
    ) {super();
  }
  getUserGroupRoles() {
    this.promise(
      this.Service.getById(this.currentId),
      'userGroupRoles',
      () => {super.initial(); },
      'data.roles'
    );
  }

callbackform() {
  this.promise(
    this.roleService.listDn(),
    'rolelist',
    () => {this.getUserGroupRoles(); }
  );
}

initialform() {
this.ModeForm = this.fb.group({
    roles: this.fb.array([])
  });
}

fillForm() {
this.buildCheckboxItem('roles');
}

initialItems() {
  const self = this;
  return this._.map(this.rolelist, function(item){
    const currentitem = self._.findWhere(self.userGroupRoles, {name: item.name});
    const checked = !self._.isUndefined(currentitem);
    const role = new Rolepermissionitem(item.name, checked, item.display_name);
    return role;
  });
}

callbackapipre() {
  this.current.roles = this._.pluck(this.ExtUnderscore.filterchecked(this.current.roles), 'name');
  return super.callbackapipre();
}

_update(){
  this.promise(
    this.Service.setroles(this.current, this.currentId),
    null,
    (val) => {this.callbackapipost('Success', val); },
    'data'
  );
}

generateArrayItem(item: any) {
  return this.fb.group({
    name: item.name,
    display_name: item.display_name,
    checked: item.checked
  });
}

updateFormArray(formArray: any, index, item) {
  formArray.at(index).patchValue({
    checked: item.checked
  });
  return formArray;
}

}
