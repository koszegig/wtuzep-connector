import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsergroupsRolesFormComponent } from './admin-usergroups-roles-form.component';

describe('AdminUsergroupsRolesFormComponent', () => {
  let component: AdminUsergroupsRolesFormComponent;
  let fixture: ComponentFixture<AdminUsergroupsRolesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUsergroupsRolesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsergroupsRolesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
