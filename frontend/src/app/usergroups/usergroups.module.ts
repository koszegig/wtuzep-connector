
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AdminUsergroupsFormComponent } from './admin-usergroups-form/admin-usergroups-form.component';
import { AdminUsergroupsListComponent } from './admin-usergroups-list/admin-usergroups-list.component';
import { AdminUsergroupsModalComponent } from './admin-usergroups-modal/admin-usergroups-modal.component';
import { AdminUsergroupsRolesFormComponent } from './admin-usergroups-roles-form/admin-usergroups-roles-form.component';

@NgModule({
	declarations: [
    AdminUsergroupsFormComponent,
    AdminUsergroupsListComponent,
    AdminUsergroupsModalComponent,
    AdminUsergroupsRolesFormComponent
  ],
	imports: [
    BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
	],
	exports: [AdminUsergroupsFormComponent, AdminUsergroupsListComponent]
})
export class UserGroupsModule { }
