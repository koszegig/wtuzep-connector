import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUsergroupsModalComponent } from './admin-usergroups-modal.component';

describe('AdminUsergroupsModalComponent', () => {
  let component: AdminUsergroupsModalComponent;
  let fixture: ComponentFixture<AdminUsergroupsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUsergroupsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUsergroupsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
