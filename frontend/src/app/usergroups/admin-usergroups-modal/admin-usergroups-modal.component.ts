import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-usergroups-modal',
  templateUrl: './admin-usergroups-modal.component.html',
  styleUrls: ['./admin-usergroups-modal.component.css']
})
export class AdminUsergroupsModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
   }

}
