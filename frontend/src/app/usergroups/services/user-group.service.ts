import { Injectable } from '@angular/core';
import { UserGroup } from '../models/user-group';
import { MainrestService} from '../../services/base/mainrest.service';

@Injectable()
export class UserGroupService extends MainrestService {

    protected controller: string  = 'usergroup';

    constructor() {
      super();
	   }

    create(userGroup: UserGroup) {
        return super.create(userGroup);
    }

    update(userGroup: UserGroup) {
        return super.update(userGroup);
    }
    setroles(permissions, id) {
        return this.put('roles', [id], permissions);
    }

}

