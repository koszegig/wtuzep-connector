﻿import { AdminRegistrationsListComponent} from './admin-registrations-listview/admin-registrations-list/admin-registrations-list.component';

export const REGISTRATION_ROUTES = [
  { path: 'registrations-list-hu',
    component: AdminRegistrationsListComponent
  },
  { path: 'registrations-list-sk',
    component: AdminRegistrationsListComponent,
  },
  { path: 'registrations-list-en',
    component: AdminRegistrationsListComponent
  }
];
