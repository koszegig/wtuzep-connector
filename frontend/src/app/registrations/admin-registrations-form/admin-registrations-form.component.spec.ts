import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationsFormComponent } from './admin-registrations-form.component';

describe('AdminRegistrationsFormComponent', () => {
  let component: AdminRegistrationsFormComponent;
  let fixture: ComponentFixture<AdminRegistrationsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
