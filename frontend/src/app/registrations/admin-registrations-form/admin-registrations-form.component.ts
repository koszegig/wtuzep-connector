import { Component, ChangeDetectorRef, ViewChild, OnInit } from '@angular/core'; ///Koszegi
import { CustomerService } from '../../registrations/services/customer.service'; //koszegig
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';
//import { OptionsService } from '../../options/services/options.service';
import { UserService} from '../../users/services/user.service';
import { NameFieldComponent} from '../../base-elements/directives/fields/name-field/name-field.component';
import { CustomerGenderItem } from '../../models/item/customergenderitem';

@Component({
  selector: 'admin-registrations-form',
  templateUrl: './admin-registrations-form.component.html',
  styleUrls: ['./admin-registrations-form.component.css']
})
export class AdminRegistrationsFormComponent extends AdminBaseFormComponent {

	public nationalities: any;
	public genders: any = [];
	public gendericon = {'male': 'fa fa-mars', 'female': 'fa fa-venus'};
	@ViewChild(NameFieldComponent, {static: false}) namefield: NameFieldComponent;
		constructor(
			public Service: CustomerService,
			public userService: UserService,
			//public optionsService: OptionsService,
			protected cdRef: ChangeDetectorRef) {
		super();
		}

  	initialform(){
		this.ModeForm = this.fb.group({
			name: ['', this.Validators.required],
			email: ['', this.Validators.required],
			phone: ['', this.Validators.required],
			nationality: ['', this.Validators.required],
			gender: ['', this.Validators.required],
		});
	}

  	fillForm(){
    	this.ModeForm.patchValue({
        	name: this.currentItem.name,
        	email: this.currentItem.email,
        	phone: this.currentItem.phone,
        	nationality: this.currentItem.nationality_id,
        	gender: this.currentItem.gender_id,
      });
    }

	  callBackOpenModal(){
		super.callBackOpenModal();
		this.namefield.setName(this.getName());
    }

	  getName(){
      return {
        firstname: this.__.get(this.currentItem, 'firstname', ''),
        middlename: this.__.get(this.currentItem, 'middlename', ''),
        lastname: this.__.get(this.currentItem, 'lastname', ''),
      };
	  }

	  setName(name) {
		this.current.firstname = name.firstname;
		this.current.middlename = name.middlename;
		this.current.lastname = name.lastname;
    }

	callbackform() {
      this.listOptions('gender', 'genders');
      this.listOptions('nationality', 'nationalities');
      super.callbackform();
    }

	listOptions(type, field) {
      /*this.promiseAsync(
        ///this.optionsService.listDn({language: 'hungary', parent: type}),
        field
      );*/
    }
}
