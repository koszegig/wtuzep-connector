import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminRegistrationsFormComponent } from './admin-registrations-form/admin-registrations-form.component';
import { AdminRegistrationsListComponent } from './admin-registrations-listview/admin-registrations-list/admin-registrations-list.component';
import { AdminRegistrationsListBaseComponent } from './admin-registrations-listview/admin-registration-list-base/admin-registrations-list-base.component';
import { AdminRegistrationsListMaybeLaterComponent } from './admin-registrations-listview/admin-registration-maybe-later-list/admin-registrations-list-maybe-later.component';
import { AdminRegistrationsListRegisteredComponent } from './admin-registrations-listview/admin-registration-registered-list/admin-registrations-list-registered.component';
import { AdminRegistrationsModalComponent } from './admin-registrations-modal/admin-registrations-modal.component';
import { AdminRegistrationsSelectionComponent } from './admin-registrations-selection/admin-registrations-selection.component';

@NgModule({
	declarations: [
    AdminRegistrationsFormComponent,
    AdminRegistrationsListComponent,
    AdminRegistrationsListBaseComponent,
    AdminRegistrationsModalComponent,
    AdminRegistrationsSelectionComponent,
    AdminRegistrationsListMaybeLaterComponent,
    AdminRegistrationsListRegisteredComponent
  ],
	imports: [
    BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    ImageCropperModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ],
  providers: [
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Custom not found'
      }
  	}
  ],
	exports: [AdminRegistrationsFormComponent, AdminRegistrationsListComponent, AdminRegistrationsModalComponent, AdminRegistrationsSelectionComponent]
})

export class RegistrationsModule { }
