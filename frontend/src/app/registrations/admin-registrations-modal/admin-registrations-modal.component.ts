import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-registrations-modal',
  templateUrl: './admin-registrations-modal.component.html',
  styleUrls: ['./admin-registrations-modal.component.css']
})
export class AdminRegistrationsModalComponent   extends AdminBaseModalComponent {

  constructor() {
    super();
   }

}
