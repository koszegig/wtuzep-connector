import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationsModalComponent } from './admin-registrations-modal.component';

describe('AdminRegistrationsModalComponent', () => {
  let component: AdminRegistrationsModalComponent;
  let fixture: ComponentFixture<AdminRegistrationsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
