import { Injectable } from '@angular/core';
import { Customer } from '../models/customer';
import { MainrestService} from '../../services/base/mainrest.service';

export class CustomerService  extends MainrestService {

  protected controller: string  = 'customer';

  constructor() {
    super();
   }

  create(customer: Customer) {
      return super.create(customer);
  }

  update(customer: Customer) {
    return super.update(customer);
  }


  uploadFPrint(id, data: any) {
    return this.post('fprint/upload', [id], data);
  }

  setStatus(status: string, customerID: string){
    return this.post('status', [customerID], {status: status});
  }

  setFlag(flag: string, customerID: string){
    return this.post('flag', [customerID], {flag: flag});
  }

  listComments(id){
    return this.get('comment/list', [id]);
  }

  listReadingDates(){
    return this.get('reading-date');
  }

  listReadingCalendar(fetchInfo){
    return this.post('reading-calendar', [], fetchInfo);
  }

  addComment(data, id){
    return this.put('comment/add', [id], data);
  }
}


