import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { AdminRegistrationsListBaseComponent } from '../admin-registration-list-base/admin-registrations-list-base.component';

@Component({
  selector: 'app-admin-registrations-list-maybe-later',
  templateUrl: '../admin-registration-list-base/admin-registrations-list-base.component.html',
  styleUrls: ['../admin-registration-list-base/admin-registrations-list-base.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRegistrationsListMaybeLaterComponent  extends AdminRegistrationsListBaseComponent {

  constructor(protected cdRef: ChangeDetectorRef) {
    super();
    this.statusfilter = 'maybe_later';
    this.params.domelem.unset('Breadcumps');
  }
}
