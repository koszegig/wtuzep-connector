import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationsListComponent } from './admin-registrations-list.component';

describe('AdminRegistrationsListComponent', () => {
  let component: AdminRegistrationsListComponent;
  let fixture: ComponentFixture<AdminRegistrationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
