import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AdminBaseComponent } from '../../../admin/admin-base/admin-base/admin-base.component';


@Component({
  selector: 'app-admin-registrations-list',
  templateUrl: './admin-registrations-list.component.html',
  styleUrls: ['./admin-registrations-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRegistrationsListComponent  extends AdminBaseComponent {
  constructor() {
    super();
  }
}
