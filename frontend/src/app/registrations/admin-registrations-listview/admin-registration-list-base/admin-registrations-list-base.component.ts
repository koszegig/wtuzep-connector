import { ViewChild, Input} from '@angular/core';
import { ReParams } from '../../models/reparams';
import { CustomerService } from '../../services/customer.service'; //koszegig
//import { OptionsService } from '../../../options/services/options.service';
import { AdminBaseListComponent } from '../../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminRegistrationsModalComponent } from '../../admin-registrations-modal/admin-registrations-modal.component';
import additionalcolumns from '../../param/additionalcolumns.json';
import hosts from '../../param/hosts.json';


export class AdminRegistrationsListBaseComponent  extends AdminBaseListComponent {

  @ViewChild(AdminRegistrationsModalComponent, {static: false}) FormComponent: AdminRegistrationsModalComponent;
  public buttons: any;
  //public optionsService: OptionsService;
  public statusfilter: string;
  @Input('subtitle') subtitle: string;
  host: any;
  constructor() {
    super();
    try {
      this.Service = this.injector.get(CustomerService);
      ///this.optionsService = this.injector.get(OptionsService);
    } catch (e) {
    }
    this.params = new ReParams();
    this.host = hosts[this._.last(this.router.url.split('/'))];
  }

  public pushStatusFilter(){
    additionalcolumns[0].filter.value = this.statusfilter;
    additionalcolumns[1].filter.value = this.host;
    this.params.pushColumns(additionalcolumns);
  }

  listPre(){
    this.pushStatusFilter();
    super.listPre();
    this.listButtons();
  }

  listButtons(){
    this.promiseAsync(
      //this.optionsService.listDn({language: 'hungary', parent: 'flag'}),
      'buttons',
      'data'
    );
  }

  setStatus(status, id){
    this.changeLoadingIndicator(true);
    this.Service.setStatus(status, id).subscribe(
      data => {
        this.refreshTableRow();
        this.changeLoadingIndicator(false);
      },
      error => {
        this.alertService.error(error);
        this.changeLoadingIndicator(false);
      }
    );
  }

  statusReading(id){
    this.inneredit(id);
  }

  statusMaybeLater(id){
    this.setStatus('maybe_later', id);
  }

  statusDontWant(id){
    this.setStatus('don_t_want', id);
  }

  setFlag(flag, id){
    this.changeLoadingIndicator(true);
    this.Service.setFlag(flag, id).subscribe(
      data => {
        this.refreshTableRow();
        this.changeLoadingIndicator(false);
      },
      error => {
        this.alertService.error(error);
        this.changeLoadingIndicator(false);
      }
    );
  }

  flagNotPickedUp(id){
    this.setFlag('not_picked_up', id);
  }

  flagCallInThisMonth(id){
    this.setFlag('call_in_this_month', id);
  }

  flagCallInThisWeek(id){
    this.setFlag('call_in_this_week', id);
  }

  flagWrongNumber(id){
    this.setFlag('wrong_number', id);
  }

  flagRegistrationInProgress(id){
    this.setFlag('registration_in_progress', id);
  }
}
