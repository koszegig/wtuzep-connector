import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationsListBaseComponent } from './admin-registrations-list-base.component';

describe('AdminRegistrationsListBaseComponent', () => {
  let component: AdminRegistrationsListBaseComponent;
  let fixture: ComponentFixture<AdminRegistrationsListBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationsListBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationsListBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
