import {Params} from '../../models/listviews/params/params';
import param from '../param/param.json';
import columns from '../param/columns.json';
import buttons from '../param/buttons.json';
export class CParams extends Params {

  constructor() {
    super(param, columns);
    this.addButtons(buttons);
  }
}
