export class Customer {
        id: string;
        reg_number: string;
        firstname: string;
        middlename: string;
        lastname: string;
        gender: string;
        birth_date: string;
        nationality: string;
        reading_language: string;
        contact_person: string;
        translator: string;
        palmleaf_reader: string;
        reading_room_id: string;
        internet: string;
        skype: string;
        thumbprint_ful: string;l
        thumbprint_small: string;
        registered_at: string;
        reading_at: string;
        thumbprintsent_at: string;
        bundlesarrived_at: string;
        newsletter: string;
        status: string;
        flag : string;
        active: string;
}