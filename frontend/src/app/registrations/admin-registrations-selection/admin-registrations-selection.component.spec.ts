import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegistrationsSelectionComponent } from './admin-registrations-selection.component';

describe('AdminRegistrationsSelectionComponent', () => {
  let component: AdminRegistrationsSelectionComponent;
  let fixture: ComponentFixture<AdminRegistrationsSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegistrationsSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegistrationsSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
