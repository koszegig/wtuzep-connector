import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import { AuthenticationService } from '../../services/custom/authentication.service';
import { AlertService} from '../../base-elements/services/alert.service';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'app-page-login',
	templateUrl: './page-login.component.html',
	styleUrls: ['./page-login.component.css']
})
export class PageLoginComponent implements OnInit {
	model: any = {};
    loading = false;
    returnUrl: string;
    loadingGIF = environment.loadingGIF;
    hasReg = environment.hasRegistration;
  	sub: any;
    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private alertService: AlertService,
      private sanitizer: DomSanitizer) {}

	  ngOnInit() {
        // reset login status
        this.sub = this.route.data.subscribe(v => {
          if (v.autologin && environment.autologin.state){
            this.model.username = environment.autologin.data.username;
            this.model.password = environment.autologin.data.password;
            this.login();
          }
        });
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/admin';
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    //this.userService.update_token();
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                  //console.log('error');
                  //console.log(error);
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
    sanitize(url: string) {
      //return url;
      return this.sanitizer.bypassSecurityTrustUrl(url);
    }
}
