import { NgModule, Injector } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppInjector } from './../services/custom/app.injector.service';
import { CommonModule } from '@angular/common';
import { PageLoginComponent } from './page-login/page-login.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { routing } from './authentication.routing';
import { PageRegisterComponent } from './page-register/page-register.component';
import { PageLockscreenComponent } from './page-lockscreen/page-lockscreen.component';
import { PageForgotPasswordComponent } from './page-forgot-password/page-forgot-password.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageForbiddonErrorComponent } from './page-forbiddon-error/page-forbiddon-error.component';
import { PageIsErrorComponent } from './page-is-error/page-is-error.component';
import { PageTryLaterComponent } from './page-try-later/page-try-later.component';
import { AuthenticationService} from '../services/custom/authentication.service';
import { PagesModule } from '../pages/pages.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './guards/index';
@NgModule({
	declarations: [PageLoginComponent, AuthenticationComponent, PageRegisterComponent, PageLockscreenComponent, PageForgotPasswordComponent, PageNotFoundComponent, PageForbiddonErrorComponent, PageIsErrorComponent, PageTryLaterComponent],
	imports: [
		HttpModule,
		BaseElementsModule,
		CommonModule,
		routing,
		PagesModule,
        RouterModule,
        FormsModule
	],
	providers: [
		AuthGuard,
		AuthenticationService,
	]
})
export class AuthenticationModule {
	constructor(injector: Injector){
		AppInjector.setInjector(injector);
	}
}
