import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCategoriesFromComponent } from './admin-categories-from.component';

describe('AdminCategoriesFromComponent', () => {
  let component: AdminCategoriesFromComponent;
  let fixture: ComponentFixture<AdminCategoriesFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCategoriesFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCategoriesFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
