import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { CategoriesService } from '../services/categories.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';

@Component({
  selector: 'admin-categories-from',
  templateUrl: './admin-categories-from.component.html',
  styleUrls: ['./admin-categories-from.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCategoriesFromComponent extends AdminBaseFormComponent{
  constructor(
      public Service: CategoriesService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }
  ngOnInit() {
  }
  initialform(){
    this.ModeForm = this.fb.group({
      synced: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      synced: this.currentItem.synced,

    });
  }
}
