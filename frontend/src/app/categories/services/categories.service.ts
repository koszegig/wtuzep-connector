import { Injectable } from '@angular/core';
import { Categorie } from '../models/categorie';
import { MainrestService} from '../../services/base/mainrest.service';

export class CategoriesService  extends MainrestService {

  protected controller: string  = 'categorie';

  constructor() {
    super();
   }

  create(categorie: Categorie) {
      return super.create(categorie);
  }

  update(categorie: Categorie) {
      return super.update(categorie);
  }
  setStatus(status: string, ID: string){
    return this.post('status', [ID], {flag: status});
  }
}


