import { Component, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { CategoriesParams } from '../models/categoriesparams';
import { CategoriesService } from '../services/categories.service';
import { AdminBaseListComponent } from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import { AdminCategoriesModalComponent } from '../admin-categories-modal/admin-categories-modal.component';

@Component({
  selector: 'admin-categories-list',
  templateUrl: './admin-categories-list.component.html',
  styleUrls: ['./admin-categories-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCategoriesListComponent  extends AdminBaseListComponent {
  @ViewChild(AdminCategoriesModalComponent, {static: false}) FormComponent: AdminCategoriesModalComponent;

  constructor(public Service: CategoriesService,
              protected cdRef: ChangeDetectorRef) {
    super();
    this.params = new CategoriesParams();

  }
  ngOnInit() {
    super.ngOnInit();
    this.logging(this.FormComponent,'ngOnInit----FormdComponent');
  }

}

