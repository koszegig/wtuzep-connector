import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG} from '@ng-select/ng-select';
import { NgxPrettyCheckboxModule } from 'ngx-pretty-checkbox';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CategoriesService } from './services/categories.service';
import {TooltipModule} from 'ng2-tooltip-directive';
import { AlertService} from '../base-elements/services/alert.service';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { AdminCategoriesFromComponent } from './admin-categories-from/admin-categories-from.component';
import { AdminCategoriesListComponent } from './admin-categories-list/admin-categories-list.component';
import { AdminCategoriesModalComponent } from './admin-categories-modal/admin-categories-modal.component';

//import { AdminImageUploadAndCropComponent } from '../admin/admin-base/admin-image-upload-and-crop/admin-image-upload-and-crop.component';

@NgModule({
	declarations: [
        AdminCategoriesFromComponent,
        AdminCategoriesListComponent,
        AdminCategoriesModalComponent//,
    //AdminImageUploadAndCropComponent
  ],
	imports: [
		BaseElementsModule,
    TooltipModule,
    NgSelectModule,
    NgxPrettyCheckboxModule,
    ImageCropperModule,
    FormsModule,
    ReactiveFormsModule,
		CommonModule,
		RouterModule,
		NgxEchartsModule,
		NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
  ],
  providers: [
      CategoriesService,
    AlertService,
    ModalService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
          notFoundText: 'Book not found'
      }
  	}
  ],
	exports: [AdminCategoriesFromComponent, AdminCategoriesListComponent, AdminCategoriesModalComponent]
})

export class CategoriesModule { }
