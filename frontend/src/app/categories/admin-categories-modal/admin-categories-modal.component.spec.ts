import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCategoriesModalComponent } from './admin-categories-modal.component';

describe('AdminCategoriesModalComponent', () => {
  let component: AdminCategoriesModalComponent;
  let fixture: ComponentFixture<AdminCategoriesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCategoriesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCategoriesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
