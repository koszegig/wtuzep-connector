import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'admin-categories-modal',
  templateUrl: './admin-categories-modal.component.html',
  styleUrls: ['./admin-categories-modal.component.css']
})
export class AdminCategoriesModalComponent  extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
