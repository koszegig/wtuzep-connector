﻿
import {AdminCategoriesListComponent } from '../categories/admin-categories-list/admin-categories-list.component';

export const CATEGORIES_ROUTING = [
  { path: 'categories-list',
    component: AdminCategoriesListComponent
  }
];
