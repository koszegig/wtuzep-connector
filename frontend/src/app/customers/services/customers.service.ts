import { Injectable } from '@angular/core';
import { Customer } from '../models/customer';
import { MainrestService} from '../../services/base/mainrest.service';

export class CustomersService  extends MainrestService {

  protected controller: string  = 'customer';

  constructor() {
    super();
   }

  create(customer: Customer) {
      return super.create(customer);
  }

  update(customer: Customer) {
      return super.update(customer);
  }
  setStatus(status: string, ID: string){
    return this.post('status', [ID], {flag: status});
  }
}


