import { Component } from '@angular/core';
import {AdminBaseModalComponent} from '../../admin/admin-base/admin-base-modal/admin-base-modal.component';

@Component({
  selector: 'app-admin-customers-modal',
  templateUrl: './admin-customers-modal.component.html',
  styleUrls: ['./admin-customers-modal.component.css']
})
export class AdminCustomersModalComponent extends AdminBaseModalComponent {

  constructor() {
    super();
  }

}
