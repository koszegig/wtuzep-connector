import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomersModalComponent } from './admin-customers-modal.component';

describe('AdminCustomersModalComponent', () => {
  let component: AdminCustomersModalComponent;
  let fixture: ComponentFixture<AdminCustomersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
