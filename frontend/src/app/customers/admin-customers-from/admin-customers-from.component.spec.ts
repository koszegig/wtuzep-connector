import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustomersFromComponent } from './admin-customers-from.component';

describe('AdminCustomersFromComponent', () => {
  let component: AdminCustomersFromComponent;
  let fixture: ComponentFixture<AdminCustomersFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustomersFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustomersFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
