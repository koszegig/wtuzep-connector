import { Component, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { CustomersService } from '../services/customers.service';
import { AdminBaseFormComponent } from '../../admin/admin-base/admin-base-form/admin-base-form.component';

@Component({
  selector: 'admin-customers-from',
  templateUrl: './admin-customers-from.component.html',
  styleUrls: ['./admin-customers-from.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCustomersFromComponent  extends AdminBaseFormComponent{
  constructor(
      public Service: CustomersService,
      protected cdRef: ChangeDetectorRef) {
    super();
  }
  ngOnInit() {
  }
  initialform(){
    this.ModeForm = this.fb.group({
      synced: ['', this.Validators.required],


    });
  }

  fillForm(){
    this.ModeForm.patchValue({
      synced: this.currentItem.synced,

    });
  }
}

