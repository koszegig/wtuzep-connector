import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {AdminBaseListComponent} from '../../admin/admin-base/admin-base-list/admin-base-list.component';
import {AdminCustomersModalComponent} from '../../customers/admin-customers-modal/admin-customers-modal.component';
import {CustomersService} from '../../customers/services/customers.service';
import {Customersparams} from '../../customers/models/customersparams';

@Component({
  selector: 'app-admin-customers-list',
  templateUrl: './admin-customers-list.component.html',
  styleUrls: ['./admin-customers-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCustomersListComponent extends AdminBaseListComponent {
  @ViewChild(AdminCustomersModalComponent, {static: false}) FormComponent: AdminCustomersModalComponent;

  constructor(public Service: CustomersService,
              protected cdRef: ChangeDetectorRef) {
    super();
    this.params = new Customersparams();

  }
  ngOnInit() {
    super.ngOnInit();
    this.logging(this.FormComponent,'ngOnInit----FormdComponent');
  }

}
