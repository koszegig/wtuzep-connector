﻿
import {AdminCustomersListComponent } from '../customers/admin-customers-list/admin-customers-list.component';

export const CUSTOMERS_ROUTING = [
  { path: 'customers-list',
    component: AdminCustomersListComponent
  }
];
