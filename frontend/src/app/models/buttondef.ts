import {RootClass} from './RootClass';

export class Buttondef extends RootClass{
  private _button: string;
  private _action: string;
  private _modprop: string;
  private _rowprop: string;
  private _rowval: string;
  private _swal: any;
  private _label: string;
  private _tloc: string;
  private _isDisplayCb: any;

   /**
   * Create a Buttondef
   * @param {string} button
   * @param {string} action
   * @param {string} modprop
   * @param {string} rowprop
   * @param {any} rowval
   * @param {any} rowval
   * @param {string} label
   * @param {string} tloc
   */

  constructor(
      button: string = '',
      action: string = '',
      modprop: string = '',
      rowprop: string = '',
      rowval: any = null,
      label: string = '',
      tloc: string = 'none',
      swal: any,
    ) {
    super();
    this._button = button;
    this._action = action;
    this._modprop = modprop;
    this._rowprop = rowprop;
    this._rowval = rowval;
    this._label = label;
    this._tloc = tloc;
    this._swal = swal;
    this.isDisplayCb = (row) => true;
  }

  /**
   * Getter swal
   * @return {any}
   */
	public get swal(): any {
		return this._swal;
	}

  /**
   * Setter button
   * @param {any} value
   */
	public set swal(value: any) {
		this._swal = value;
	}

  /**
   * Getter button
   * @return {string}
   */
	public get button(): string {
		return this._button;
	}

  /**
   * Setter button
   * @param {string} value
   */
	public set button(value: string) {
		this._button = value;
	}

  /**
   * Getter modprop
   * @return {string}
   */
	public get modprop(): string {
		return this._modprop;
	}

  /**
   * Setter modprop
   * @param {string} value
   */
	public set modprop(value: string) {
		this._modprop = value;
	}

  /**
   * Getter action
   * @return {string}
   */
	public get action(): string {
		return this._action;
	}

  /**
   * Setter action
   * @param {string} value
   */
	public set action(value: string) {
		this._action = value;
	}

  /**
   * Getter rowprop
   * @return {string}
   */
	public get rowprop(): string {
		return this._rowprop;
	}

  /**
   * Setter rowprop
   * @param {string} value
   */
	public set rowprop(value: string) {
		this._rowprop = value;
	}

  /**
   * Getter rowval
   * @return {string}
   */
	public get rowval(): string {
		return this._rowval;
	}

  /**
   * Setter rowval
   * @param {string} value
   */
	public set rowval(value: string) {
		this._rowval = value;
	}

  /**
   * Getter label
   * @return {string}
   */
	public get label(): string {
		return this._label;
	}

  /**
   * Setter label
   * @param {string} value
   */
	public set label(value: string) {
		this._label = value;
	}

  /**
   * Getter tloc
   * @return {string}
   */
	public get tloc(): string {
		return this._tloc;
	}

  /**
   * Setter tloc
   * @param {string} value
   */
	public set tloc(value: string) {
		this._tloc = value;
	}

  /**
   * Getter $isDisplayCb
   * @return {any}
   */
	public get isDisplayCb(): any {
		return this._isDisplayCb;
	}

  /**
   * @param {any} value
   */
	public set isDisplayCb(value: any) {
		this._isDisplayCb = value;
	}

  /**
   * @param {any} row
   */
  public isDisplay(row: any){
    let display =  this._.isEmpty(row) || this._.isNull(this.rowprop) || this.isRowprop(row);
    display = display && this.isDisplayCb(row);
    return display;
  }

  protected isRowprop(row){
   // this.logging(this.rowval, 'this.rowval', 'buttondef.isRowprop');
    if (this._.isArray(this.rowval))
      return this.ExtUnderscore.inArray(this.rowval, row[this.rowprop]);
    return (row[this.rowprop] == this.rowval);

  }

}
