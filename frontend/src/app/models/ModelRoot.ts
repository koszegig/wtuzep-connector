
import {RootClass} from './RootClass';

export class ModelRoot extends RootClass{
  public id: string;
  constructor(id: string = null) {
    super();
    this.id = (this._.isNull(id)) ? this.uuid() : id;
  }

  public getId(): string {
      return this.id;
  }

  public setId(id: string): void {
      this.id = id;
  }

  public update(data: any): this {
    const copydata = this.__.cloneDeep(data);
    delete copydata.id;
    this.updateprop(copydata);
    return this;
  }

  public create(data: any): this {
    this.updateprop(data);
    return this;
  }

  updateprop(data: any){
    const self = this;
    this._.map(data, function(val, key){
      self[key] = val;
    });
  }

}
