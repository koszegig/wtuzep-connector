import {Header} from './header';
import {Row} from './row';
import {Filter} from './filter';
import {RootClass} from '../RootClass';

export class Column extends RootClass{
  private _header: Header;
  private _row: Row;
  private _filter: Filter;
  private _virtual: boolean;

  constructor(
    _header: Header,
    _row: Row,
    _filter: Filter,
    _virtual: boolean) {
    super();
    this._header = (_header == null) ? null : new Header(_header.filterable, _header.label, _header.type);
    this._row = (_row == null) ? null : new Row(_row.field, _row.extdata, _row.type, _row.prefix, _row.subfix);
    this._filter = (_filter == null) ? null : new Filter(_filter.field, _filter.fieldtype, _filter.op, _filter.lop, _filter.value);
    this._virtual = _virtual;
  }

    /**
     * Getter header
     * @return {Header}
     */
	public get header(): Header {
		return this._header;
	}

    /**
     * Getter row
     * @return {Row}
     */
	public get row(): Row {
		return this._row;
	}

    /**
     * Getter filter
     * @return {Filter}
     */
	public get filter(): Filter {
		return this._filter;
	}

  /**
   * Getter virtual
   * @return {boolean}
   */
  public get virtual(): boolean {
    return this._virtual;
  }

    /**
     * Setter header
     * @param {Header} value
     */
	public set header(value: Header) {
		this._header = value;
	}

    /**
     * Setter row
     * @param {Row} value
     */
	public set row(value: Row) {
		this._row = value;
	}

    /**
     * Setter filter
     * @param {Filter} value
     */
	public set filter(value: Filter) {
		this._filter = value;
	}

  /**
   * Setter virtual
   * @param {Filter} value
   */
  public set virtual(value: boolean) {
    this._virtual = value;
  }

}
