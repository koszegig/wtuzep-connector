import {Column} from './column';
import {RootClass} from '../RootClass';

export class Columns extends RootClass{

  private _columns: Array<Column>;

  constructor(columns: Array<Column>) {
    super();
    this._columns = [];
    this.pushColumns(columns);
  }

    /**
     * Getter columns
     * @return {Array<column>}
     */
	public get columns(): Array<Column> {
		return this._columns;
	}

    /**
     * Setter columns
     * @param {Array<column>} value
     */
	public set columns(value: Array<Column>) {
    this.pushColumns(value);
  }

  public get filters() {
    const self = this;
    let columns: Array<Column> = this._.filter(this._columns, function(col){ return  self.getPropVal(col, 'header.filterable', false) && self.getPropVal(col, 'filter.value', '') !== ''; });
    columns = this._.pluck(columns, 'filter');
    columns = this._.map(columns, function(column){
      return column.getProperties();
    });
    return columns;
  }

  /**
   * Getter columns
   * @return {Array<column>}
   */

  public get dtcolumns(): Array<Column>{
    const self = this;
    return this._.filter(this._columns, function(col){ return  !self.getPropVal(col, 'virtual', false); });
  }

  public pushColumns(columns: Array<Column>){
    columns.forEach(col => {
      this._columns.push(new Column(col.header, col.row, col.filter, col.virtual)) ;
    });  }
}
