import {Field} from './field';
import {RootClass} from '../RootClass';

export class Filter extends RootClass {
  private _field: Array<Field>;
  private _fieldtype: String;
  private _op: String;
  private _lop: String;
  private _value: String;


	constructor(
    _field: Array<Field>,
    _fieldtype: String,
    _op: String,
    _lop: String,
    _value: String) {
    super();
    this.setField(_field);
    this._fieldtype = _fieldtype;
    this._op = _op;
    this._lop = _lop;
    this._value = _value;
  }

  private setField(_field: Array<Field>){
    if (this._.isArray(_field)){
      this._field = [];
      _field.forEach(field => {
        this._field.push(new Field(field.field, field.op, field.lop));
      });
    }

  }

    /**
     * Getter field
     * @return {Array<Field>}
     */
	public get field(): Array<Field> {
		return this._field;
	}

    /**
     * Getter fieldtype
     * @return {String}
     */
	public get fieldtype(): String {
		return this._fieldtype;
	}

    /**
     * Getter op
     * @return {String}
     */
	public get op(): String {
		return this._op;
	}

    /**
     * Getter lop
     * @return {String}
     */
	public get lop(): String {
		return this._lop;
	}

    /**
     * Getter value
     * @return {String}
     */
	public get value(): String {
		return this._value;
	}

    /**
     * Setter field
     * @param {Array<Field>} value
     */
	public set field(value: Array<Field>) {
		this._field = value;
	}

    /**
     * Setter fieldtype
     * @param {String} value
     */
	public set fieldtype(value: String) {
		this._fieldtype = value;
	}

    /**
     * Setter op
     * @param {String} value
     */
	public set op(value: String) {
		this._op = value;
	}

    /**
     * Setter lop
     * @param {String} value
     */
	public set lop(value: String) {
		this._lop = value;
	}

    /**
     * Setter value
     * @param {String} value
     */
	public set value(value: String) {
		this._value = value;
	}

}
