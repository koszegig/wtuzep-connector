
import {RootClass} from '../RootClass';
export class Field extends RootClass{
  private _field: String;
  private _op: String;
  private _lop: String;

  constructor(_field: String, _op: String, _lop: String) {
     super();
    this._field = _field;
    this._op = _op;
    this._lop = _lop;
  }

    /**
     * Getter field
     * @return {String}
     */
	public get field(): String {
		return this._field;
	}

    /**
     * Getter op
     * @return {String}
     */
	public get op(): String {
		return this._op;
	}

    /**
     * Getter lop
     * @return {String}
     */
	public get lop(): String {
		return this._lop;
	}

    /**
     * Setter field
     * @param {String} value
     */
	public set field(value: String) {
		this._field = value;
	}

    /**
     * Setter op
     * @param {String} value
     */
	public set op(value: String) {
		this._op = value;
	}

    /**
     * Setter lop
     * @param {String} value
     */
	public set lop(value: String) {
		this._lop = value;
	}
}
