
import {RootClass} from '../RootClass';
export class Header extends RootClass{
  private _filterable: boolean;
  private _label: String;
  private _type: String;

  constructor(
    _filterable: boolean,
    _label: String,
    _type: String
  ) {
    super();
    this._filterable = _filterable;
    this._label = _label;
    this._type = _type;
  }

    /**
     * Getter filterable
     * @return {boolean}
     */
	public get filterable(): boolean {
		return this._filterable;
	}

    /**
     * Getter label
     * @return {String}
     */
	public get label(): String {
		return this._label;
	}

    /**
     * Getter type
     * @return {String}
     */
	public get type(): String {
		return this._type;
	}

    /**
     * Setter filterable
     * @param {boolean} value
     */
	public set filterable(value: boolean) {
		this._filterable = value;
	}

    /**
     * Setter label
     * @param {String} value
     */
	public set label(value: String) {
		this._label = value;
	}

    /**
     * Setter type
     * @param {String} value
     */
	public set type(value: String) {
		this._type = value;
	}

}
