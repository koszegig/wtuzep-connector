
import {RootClass} from '../RootClass';
export class Row extends RootClass{
  private _field: String;
  private _extdata: any;
  private _type: String;
  private _prefix: String;
  private _subfix: String;
  constructor(
    _field: String,
    _extdata: any,
    _type: String,
    _prefix: String,
    _subfix: String
  ) {
     super();
    this._field = _field;
    this._extdata = _extdata;
    this._type = _type;
    this._prefix = _prefix;
    this._subfix = _subfix;
  }

  /**
     * Getter field
     * @return {String}
     */
	public get field(): String {
		return this._field;
	}

    /**
     * Getter extdata
     * @return {any}
     */
	public get extdata(): any {
		return this._extdata;
	}

    /**
     * Getter type
     * @return {String}
     */
	public get type(): String {
		return this._type;
	}

  /**
   * Getter prefix
   * @return {String}
   */
  public get prefix(): String {
    return this._prefix;
  }

  /**
   * Getter subfix
   * @return {String}
   */
  public get subfix(): String {
    return this._subfix;
  }

    /**
     * Setter field
     * @param {String} value
     */
	public set field(value: String) {
		this._field = value;
	}

  /**
   * Setter prefix
   * @param {String} value
   */
  public set prefix(value: String) {
    this._prefix = value;
  }

  /**
   * Setter subfix
   * @param {String} value
   */
  public set subfix(value: String) {
  this._subfix = value;
  }

    /**
     * Setter extdata
     * @param {any} value
     */
	public set extdata(value: any) {
		this._extdata = value;
	}

    /**
     * Setter type
     * @param {String} value
     */
	public set type(value: String) {
		this._type = value;
	}

}
