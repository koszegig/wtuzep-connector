import { environment } from '../../environments/environment';
export class Page {
  //The number of elements in the page
  public size: number;
  //The total number of elements
  public totalElements: number;
  //The total number of pages
  public totalPages: number;
  //The current page number
  public pageNumber: number;
  //The current page number
  public from: number;
  //The current page number
  public to: number;

  constructor(
    size: number = environment.tablesize.default,
    totalElements: number = 0,
    totalPages: number = 0,
    pageNumber: number = 0,
    from: number = 0,
    to: number = 0) {
    this.size = size;
    this.totalElements = totalElements;
    this.totalPages = totalPages;
    this.pageNumber = pageNumber;
    this.from = from;
    this.to = to;
  }

  public getSize(): number {
      return this.size;
  }

  public setSize(size: number): Page {
      this.size = size;
      return this;
  }

  public getTotalElements(): number {
      return this.totalElements;
  }

  public setTotalElements(totalElements: number): Page {
      this.totalElements = totalElements;
      return this;
  }

  public getTotalPages(): number {
      return this.totalPages;
  }

  public setTotalPages(totalPages: number): Page {
      this.totalPages = totalPages;
      return this;
  }

  public getPageNumber(): number {
      return this.pageNumber;
  }

  public setPageNumber(pageNumber: number): Page {
      this.pageNumber = pageNumber;
      return this;
  }

  public getFrom(): number {
      return this.from;
  }

  public setFrom(from: number): Page {
      this.from = from;
      return this;
  }

  public getTo(): number {
      return this.to;
  }

  public setTo(to: number): Page {
      this.to = to;
      return this;
  }




}
