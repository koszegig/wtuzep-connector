export class Itembase {
  public checked: boolean;
  public name: string;
  public display_name: string;

  constructor(name: string, checked: boolean, display_name: string = null) {
    this.checked = checked;
    this.name = name;
    this.display_name = display_name;
  }
}
