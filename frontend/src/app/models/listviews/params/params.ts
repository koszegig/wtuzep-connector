import {Domelem} from '../mods/domelem';
import {Apiparams} from '../apiparams';
import {Buttondef} from '../../buttondef';
import {RootClass} from '../../RootClass';
import {Columns} from '../../datatable/columns';
import {Column} from '../../datatable/column';
import param from './param.json';
export class Params extends RootClass {

  private _domelem: Domelem;
  private _apiparams: Apiparams;
  private _buttons: Array<Buttondef>;
  private _createButton: Buttondef;
  private _params: any;
  private _columns: Columns;

  constructor(
    _params: any,
    _columns: Array<any>,
  ) {
    super();
    this._apiparams = new Apiparams();
    this._params = _params;
    this._columns = new Columns(_columns);
    this._buttons = [];
    this.addButtons(param.buttons);
    if (this.params)
    this.initialDomelem(this._params.domelems);
    this.addCreateButton();
  }

  /**
   * get class
   * @param {Array<string>} elems
   */
	public getClass(key: string) {
		return this.getPropVal(this._params.classes, key , '');
	}

    /**
     * Getter domelem
     * @return {Domelem}
     */
	public get domelem(): Domelem {
		return this._domelem;
	}

    /**
     * Setter domelem
     * @param {Domelem} value
     */
	public set domelem(value: Domelem) {
		this._domelem = value;
  }

    /**
     * Getter domelem
     * @return {Columns}
     */
	public get columns(): Columns {
		return this._columns;
	}

    /**
     * Setter domelem
     * @param {Columns} value
     */
	public set columns(value: Columns) {
		this._columns = value;
	}

  /**
   * Setter domelem
   * @param {Array<string>} elems
   */
	public initialDomelem(elems: Array<string>) {
		this._domelem = new Domelem(elems);
	}

  /**
   * get text
   * @param {Array<string>} elems
   */
	public getText(key: string) {
		return this._params.textKeys[key];
	}

  /**
   * Getter params
   * @return {Apiparams}
   */
	public get apiparams(): Apiparams {
		return this._apiparams;
	}

  /**
   * Setter params
   * @param {Apiparams} value
   */
	public set apiparams(value: Apiparams) {
		this._apiparams = value;
	}

  /**
   * Getter buttons
   * @return {Array<Buttondef>}
   */
	public get buttons(): Array<Buttondef> {
		return this._buttons;
	}



  /**
   * Getter createButton
   * @return {Buttondef}
   */
	public get createButton(): Buttondef {
		return this._createButton;
	}

  /**
   * Setter createButton
   * @param {Buttondef} value
   */
	public set createButton(value: Buttondef) {
		this._createButton = value;
	}


    /**
     * Getter params
     * @return {any}
     */
	public get params(): any {
		return this._params;
	}

    /**
     * Setter params
     * @param {any} value
     */
	public set params(value: any) {
		this._params = value;
	}


   /**
   * Add button
   * @param {string} type
   * @param {string} action
   * @param {string} modprop
   * @param {string} rowprop
   * @param {any} rowval
   */

	public addbutton(
    type: string = '',
    action: string = '',
    modprop: string = '',
    rowprop: string = '',
    rowval: any = null,
    swal: any = null): void {
		this.buttons.push(new Buttondef(type, action, modprop, rowprop, rowval, '', 'none', swal));
  }

  /**
   * Add buttons
   * @param {Array<any>} buttons
   */
  public addButtons(buttons: Array<any>): void {
    const that = this;
    this._.each(buttons, function (button) {
        const {type, action, modprop, rowprop, rowval, swal} = button;
        that.addbutton(type, action, modprop, rowprop, rowval, swal);
    });
  }

   /**
   * add createbutton
    * @param {string} text
   */

  addCreateButton(): void {
    const text = this.getText('addButton');
    const {type, action, modprop, rowprop, rowval, swal} = param.addbutton;
    this._createButton = new Buttondef(type, action, modprop, rowprop, rowval, text, 'prefix', swal);
  }

  public pushColumns(columns: Array<Column>){
    this.columns.pushColumns(columns);
  }
}
