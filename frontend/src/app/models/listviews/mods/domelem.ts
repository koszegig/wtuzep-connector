import { RootClass} from '../../RootClass';

export class Domelem extends RootClass{

    private _elems: Array<string>;

    constructor(elems: Array<string>) {
        super();
        this._elems = elems;
    }

    public isDisplay(elem: string){
        return this._.contains(this._elems, elem);
    }

    public unset(elem: string){
      this.logging(this._elems, 'this._elems', 'Domelem.unset');
      this._elems = this._.without(this._elems, elem);
      this.logging(this._elems, 'this._elems', 'Domelem.unset');
    }

}
