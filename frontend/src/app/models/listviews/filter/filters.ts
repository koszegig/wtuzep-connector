import {RootClass} from '../../RootClass';
import {Filter} from './filter';

export class Filters extends RootClass {


  private _elems: Array<Filter>;


  constructor(
  ) {
      super();
      this._elems = [];
  }

  public push(
    name: string,
    fields: Array<string>,
    value: string,
    filtertype: string = 'text',
    operator: string = 'AND',
    foperator: string = 'AND'){
    this._elems.push(new Filter(name, fields, value, filtertype, operator, foperator));
  }

  public indexOfByName(name: string){
    return this.ExtUnderscore.deepIndexOf(this._elems, {'name': name});
  }

    /**
   * Getter elems
   * @return {Array<Filter>}
   */
  public get elems(): Array<Filter> {
    return this._elems;
  }

  /**
   * Setter elems
   * @param {Array<Filter>} value
   */
  public set elems(value: Array<Filter>) {
    this._elems = value;
  }

}
