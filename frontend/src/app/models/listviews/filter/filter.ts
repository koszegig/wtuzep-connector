import {RootClass} from '../../RootClass';

export class Filter extends RootClass {

  private _name: string;
  private _fields: Array<string>;
  private _value: string;
  private _filtertype: string;
  private _operator: string;
  private _foperator: string;


    constructor(
        name: string,
        fields: Array<string>,
        value: string,
        filtertype: string = 'text',
        operator: string = 'AND',
        foperator: string = 'AND'
    ) {
        super();
        this._name = name;
        this._fields = fields;
        this._value = value;
        this._filtertype = filtertype;
        this._operator = operator;
        this._operator = foperator;
    }


  /**
   * Getter name
   * @return {string}
   */
	public get name(): string {
		return this._name;
	}

  /**
   * Setter name
   * @param {string} value
   */
	public set name(value: string) {
		this._name = value;
	}


      /**
     * Getter fields
     * @return {Array<string>}
     */
	public get fields(): Array<string> {
		return this._fields;
	}

    /**
     * Getter value
     * @return {string}
     */
	public get value(): string {
		return this._value;
	}

    /**
     * Getter filtertype
     * @return {string}
     */
	public get filtertype(): string {
		return this._filtertype;
	}

    /**
     * Getter operator
     * @return {string}
     */
	public get operator(): string {
		return this._operator;
	}

    /**
     * Getter foperator
     * @return {string}
     */
	public get foperator(): string {
		return this._foperator;
	}

    /**
     * Setter fields
     * @param {Array<string>} value
     */
	public set fields(value: Array<string>) {
		this._fields = value;
	}

    /**
     * Setter value
     * @param {string} value
     */
	public set value(value: string) {
		this._value = value;
	}

    /**
     * Setter filtertype
     * @param {string} value
     */
	public set filtertype(value: string) {
		this._filtertype = value;
	}

    /**
     * Setter operator
     * @param {string} value
     */
	public set operator(value: string) {
		this._operator = value;
	}

    /**
     * Setter foperator
     * @param {string} value
     */
	public set foperator(value: string) {
		this._foperator = value;
	}

}
