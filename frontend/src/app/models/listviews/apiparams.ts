import {Page} from '../page';
//import {Column} from '../column';
import {RootClass} from '../RootClass';
import { URLSearchParams } from '@angular/http';

export class Apiparams extends RootClass{
  public filter: any;
  public cfilter: any;
  public includes: any;
  public appends: any;
  public page: any;

  constructor(
    ) {
      super();
      this.filter = {};
      this.cfilter = [];
      this.page = new Page();
      this.includes = [];
      this.appends = [];
    }

 /* setFilter(columns: Array<Column>){
    const self = this;
    this.logging(columns, 'columns', 'setFilter');
    this._.each(columns, function(item: any){
          if (item.filterable){
            const custom = self.__.get(item, 'headercell.custom', self.__.get(item, 'custom', false));
            const key = self.__.get(item, 'headercell.filterfield', self.__.get(item, 'sf_name', false));
            let value = self.__.get(item, 'headercell.filter', self.__.get(item, 'filter', false));
            const type =  self.__.get(item, 'headercell.type', 'text');
            if (type == 'date')
              value = self.initialDate(value).toString();
            if (custom){
              self.cfilter[key] = value;
            }
            else
              self.filter[key] = value;
          }
    });
  }*/

  setPage(data: any){
    this.page.size = data.per_page * 1;
    //The total number of elements
    this.page.totalElements = data.total;
    //The total number of pages
    this.page.totalPages = data.last_page;
    //The current page number
    this.page.pageNumber = data.current_page * 1 - 1;
    this.page.from = data.from;
    this.page.to = data.to;
  }

  generateParams(){
    let params: URLSearchParams = new URLSearchParams();
    if (!this._.isEmpty(this.filter)) params = this.convertFilters(params);
    if (!this._.isEmpty(this.page)) params = this.convertPage(params);
    if (!this._.isEmpty(this.includes)) params = this.convertInclude(params);
    if (!this._.isEmpty(this.appends)) params = this.convertAppends(params);
    if (!this._.isEmpty(this.cfilter)) params = this.convertCFilters(params);
    return params;
  }

  convertFilters(params){
    return this.convertFilter(this.filter, params, 'filter');
  }

  convertCFilters(params){
      const cfilter = this.cfilter.join();
      params.append('cfilter', cfilter);
      return params;
  }

  convertFilter(filter: any, params: any, prefix: string){
    this._.map(filter, function(value, key){
      params.append(prefix + '[' + key + ']', value);
    });
    return params;
  }

  convertPage(params){
    params.append('page[number]', (this.page.pageNumber * 1 + 1));
    return params;
  }

  convertInclude(params){
    const inc = this.includes.join();
    params.append('include', inc);
    return params;
  }

  convertAppends(params){
    const _appends = this.appends.join();
    params.append('append', _appends);
    return params;
  }

  reset_filter(){
    const self = this;
    this._.each(this.filter, function(value, key){
      self.filter[key] = '';
    });
  }

  reset_page(){
    this.page.pageNumber = 0;
  }

}
