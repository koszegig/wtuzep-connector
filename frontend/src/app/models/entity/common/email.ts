export class Email {
	id: string;
    email: string;
    primary: boolean;
}
