export class Contact {
  id: string;
	firstname: string;
	middlename: string;
  lastname: string;
  company_id: string;
  position_id: string;
  organization_id: string;
}
