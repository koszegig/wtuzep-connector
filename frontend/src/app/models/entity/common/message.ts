export class Message {
    id: string;
    parent_id: string;
    sender_user_id: string;
    receive_user_id: string;
    related: string;
    related_id: string;
    body: string;

}
