<?php


namespace App\CustomProcessors;


    use App\Task;
    use Monolog\Handler\RotatingFileHandler;
    use Monolog\Processor\ProcessorInterface;

class PhpVersionProcessor implements ProcessorInterface {
    /**
     * @return array The processed record
     */
    public function __invoke(array $record) {
        if (function_exists('posix_getpwuid')) {
            $processUser = posix_getpwuid( posix_geteuid() );
            $processName= $processUser[ 'name' ];
        } else {
            $processName = 'windows';
        }
        $sapi = php_sapi_name();
        $record['extra']['process'] =   "$sapi-$processName";
        $record['extra']['php_version'] = phpversion();
        $record['extra']['Task'] =   Task::getTaskType(getmypid());
        $record['channel'] =   Task::getTaskType(getmypid());
        return $record;
    }
}
