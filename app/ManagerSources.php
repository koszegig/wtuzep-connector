<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

class ManagerSources  extends BaseConnectorModel
{
    //
    protected $tablename='manager_sources';
    /**
     * @var string
     */
    public $table="manager_sources";

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'manager_sources';
    }

    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("erp_id", "varchar", true,'RaktarKod', 'sku',false, null),
            new Field("cod", "varchar", true,null, null,false, null),
            new Field("name", "varchar", true,'RaktarMegnevezes', null,false, null),
            new Field("priority", "varchar", true,null, null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
        ]);
    }
}
