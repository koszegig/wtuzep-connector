<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

class Category extends BaseConnectorModel
{
    protected $tablename='categories';

    protected $guarded = [];

    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'categories';
    }
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("erp_id", "varchar", true,'CategoryID', null,false, null, null,  50, true),
            new Field("name", "Varchar", true,'CategoryName', null,false, null, null,  60, true),
            new Field('erp_partent_id', "varchar", true,'ParentId', null,false, null, null,  50, true),
            new Field('partent_id', "varchar", true,null, null,false, null),
            new Field('position', "varchar", true,null, null,false, null),
            new Field('level', "varchar", true,'Level', null,false, null, null,  50, true),
            new Field('status', "int4", true,null, null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('category_magento_id', "varchar", true,null, null,false, null),
        ]);
    }
}
