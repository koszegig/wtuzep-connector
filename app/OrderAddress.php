<?php


namespace App;
use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;

class OrderAddress extends BaseConnectorModel
{
    /**
     * @var string
     */
    public $table="order_addresses";
    protected $tablename='order_addresses';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'order_addresses';
    }
    public function setFields(){
        $this->fields = collect([

            new Field('id', "int4", true,null, null,true, null),
            new Field('order_id', "Varchar", true,'order_id', null,false, null),
            new Field('address_type', "Varchar", true,'address_type', null,false, null),
            new Field('company', "Varchar", true,'company', null,false, null),
            new Field('country_id', "Varchar", true,'country_id', null,false, null),
            new Field('email', "Varchar", true,'email', null,false, null),
            new Field('entity_id', "Varchar", true,'entity_id', null,false, null),
            new Field('firstname', "Varchar", true,'firstname', null,false, null),
            new Field('lastname', "Varchar", true,'lastname', null,false, null),
            new Field('middlename', "Varchar", true,null, null,false, null),
            new Field('parent_id', "Varchar", true,'parent_id', null,false, null),
            new Field('postcode', "Varchar", true,'postcode', null,false, null),
            new Field('prefix', "Varchar", true,null, null,false, null),
            new Field('street', "Varchar", true,'street', null,false, 'street'),
            new Field('region', "Varchar", true,'region', null,false, null),
            new Field('region_code', "Varchar", true,'region_code', null,false, null),
            new Field('region_id', "Varchar", true,'region_id', null,false, null),
            new Field('suffix', "Varchar", true,null, null,false, null),
            new Field('telephone', "Varchar", true,'telephone', null,false, null),
            new Field('city', "Varchar", true,'city', null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),

        ]);

    }
    public function street ($row) {
        return implode(" ",$row['street']);
    }
}
