<?php


namespace App\Routing;
use \Dingo\Api\Routing\ResourceRegistrar as OriginalRegistrar;


class ExtendedRouter extends OriginalRegistrar
{

    protected $resourceDefaults = array( 'index', 'store', 'show', 'update', 'destroy','listForDn','list');




    /**
     * Add the dropdown list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceListForDn($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/list/dropdown';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'listForDn', $options));
    }


    /**
     * Add the list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceList($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/list';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'list', $options));
    }
}
