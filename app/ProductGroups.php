<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

class ProductGroups extends BaseConnectorModel
{
    //
    protected $tablename='productgroups';
    /**
     * @var string
     */
    public $table="productgroups";

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'productgroups';
    }

    /* public function pictures()
     {
         return $this->hasMany('App\Picture');
     }

     public function attributevalues()
     {
         return $this->belongsToMany('App\AttributeValue');
     }
 */

    public function setFields(){
        $this->fields = [
            new Field("id", "int4", true,null, null,true, null),
            new Field("erp_id", "varchar", true,'CikkCsoportKod', null,false, null),
            new Field("Name", "varchar", true,'CikkCsoportMegnevezes', null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
            new Field('error', "varchar", true,null, null,false, null),
        ];
    }
    public function getGroupTree(){
        $tree =[
            'Mobiltelefon'=>'Mobiltelefon',
            'Okoseszköz'=>'Okoseszköz',
            'Tablet'=>'Tablet',
            'Tartozék'=>['Adatkábel',
                                'Akkumulátor',
                                'Autós tartó',
                                'Autós töltő',
                                'Billentyűzet',
                                'Bluetooth fülhallgató',
                                'Bluetooth hangszóró',
                                'Ceruza',
                                'Egyéb',
                                'Fólia',
                                'Fülhallgató',
                                'Hálózati töltő',
                                'Kamera',
                                'Külső akkumulátor',
                                'Memóriakártya',
                                'Okoseszköz',
                                'SSD',
                                'Szíj',
                                'Tok',
                                'Vezeték nélküli töltő'
                            ],
            'Technikai cikkek'=>'Engedmény'
        ];
        return $tree;
    }
}
