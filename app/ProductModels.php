<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModels extends Model
{
    //

    public $table="product_models";
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

}
