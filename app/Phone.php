<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model{

    protected $tablename='phones';
    public $primarykey="id";

    protected $guarded = [];
}

