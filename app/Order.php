<?php

namespace App;

use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;

class Order extends BaseConnectorModel
{
    /**
     * @var string
     */
    public $table="orders_head";
    protected $tablename='orders_head';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'orders_head';
    }
    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function setFields(){
        $this->fields = collect([

            new Field('id', "int4", true,null, null,true, null),
            new Field('order_id', "Varchar", true,'entity_id', null,false, null),
        new Field('status', "Varchar", true,'status', null,false, null),
        new Field('state', "Varchar", true,'state', null,false, null),
        new Field('coupon_code', "Varchar", true,'coupon_code', null,false, null),
        new Field('protect_code', "Varchar", true,'protect_code', null,false, null),
        new Field('shipping_description', "Varchar", true,'shipping_description', null,false, null),
        new Field('is_virtual', "Varchar", true,'is_virtual', null,false, null),
        new Field('store_id', "Varchar", true,'store_id', null,false, null),
        new Field('customer_id', "Varchar", true,'customer_id', null,false, null),
        new Field('base_discount_amount', "Varchar", true,'base_discount_amount', null,false, null),
        new Field('base_discount_canceled', "Varchar", true,'base_discount_canceled', null,false, null),
        new Field('base_discount_invoiced', "Varchar", true,'base_discount_invoiced', null,false, null),
        new Field('base_discount_refunded', "Varchar", true,'base_discount_refunded', null,false, null),
        new Field('base_grand_total', "Varchar", true,'base_grand_total', null,false, null),
        new Field('base_shipping_amount', "Varchar", true,'base_shipping_amount', null,false, null),
        new Field('base_shipping_refunded', "Varchar", true,'base_shipping_refunded', null,false, null),
        new Field('base_shipping_tax_amount', "Varchar", true,'base_shipping_tax_amount', null,false, null),
        new Field('base_shipping_tax_refunded', "Varchar", true,'base_shipping_tax_refunded', null,false, null),
        new Field('base_subtotal', "Varchar", true,'base_subtotal', null,false, null),
        new Field('base_subtotal_canceled', "Varchar", true,'base_subtotal_canceled', null,false, null),
        new Field('base_subtotal_invoiced', "Varchar", true,'base_subtotal_invoiced', null,false, null),
        new Field('base_subtotal_refunded', "Varchar", true,'base_subtotal_refunded', null,false, null),
        new Field('base_tax_amount', "Varchar", true,'base_tax_amount', null,false, null),
        new Field('base_tax_canceled', "Varchar", true,'base_tax_canceled', null,false, null),
        new Field('base_tax_invoiced', "Varchar", true,'base_tax_invoiced', null,false, null),
        new Field('base_tax_refunded', "Varchar", true,'base_tax_refunded', null,false, null),
        new Field('base_to_global_rate', "Varchar", true,'base_to_global_rate', null,false, null),
        new Field('base_to_order_rate', "Varchar", true,'base_to_order_rate', null,false, null),
        new Field('base_total_canceled', "Varchar", true,'base_total_canceled', null,false, null),
        new Field('base_total_invoiced', "Varchar", true,'base_total_invoiced', null,false, null),
        new Field('base_total_invoiced_cost', "Varchar", true,'base_total_invoiced_cost', null,false, null),
        new Field('base_total_offline_refunded', "Varchar", true,'base_total_offline_refunded', null,false, null),
        new Field('base_total_online_refunded', "Varchar", true,'base_total_online_refunded', null,false, null),
        new Field('base_total_paid', "Varchar", true,'base_total_paid', null,false, null),
        new Field('base_total_qty_ordered', "Varchar", true,'base_total_qty_ordered', null,false, null),
        new Field('base_total_refunded', "Varchar", true,'base_total_refunded', null,false, null),
        new Field('discount_amount', "Varchar", true,'discount_amount', null,false, null),
        new Field('discount_canceled', "Varchar", true,'discount_canceled', null,false, null),
        new Field('discount_invoiced', "Varchar", true,'discount_invoiced', null,false, null),
        new Field('discount_refunded', "Varchar", true,'discount_refunded', null,false, null),
        new Field('grand_total', "Varchar", true,'grand_total', null,false, null),
        new Field('shipping_amount', "Varchar", true,'shipping_amount', null,false, null),
        new Field('shipping_canceled', "Varchar", true,'shipping_canceled', null,false, null),
        new Field('shipping_invoiced', "Varchar", true,'shipping_invoiced', null,false, null),
        new Field('shipping_refunded', "Varchar", true,'shipping_refunded', null,false, null),
        new Field('shipping_tax_amount', "Varchar", true,'shipping_tax_amount', null,false, null),
        new Field('shipping_tax_refunded', "Varchar", true,'shipping_tax_refunded', null,false, null),
        new Field('store_to_base_rate', "Varchar", true,'store_to_base_rate', null,false, null),
        new Field('store_to_order_rate', "Varchar", true,'store_to_order_rate', null,false, null),
        new Field('subtotal', "Varchar", true,'subtotal', null,false, null),
        new Field('subtotal_canceled', "Varchar", true,'subtotal_canceled', null,false, null),
        new Field('subtotal_invoiced', "Varchar", true,'subtotal_invoiced', null,false, null),
        new Field('subtotal_refunded', "Varchar", true,'subtotal_refunded', null,false, null),
        new Field('tax_amount', "Varchar", true,'tax_amount', null,false, null),
        new Field('tax_canceled', "Varchar", true,'tax_canceled', null,false, null),
        new Field('tax_invoiced', "Varchar", true,'tax_invoiced', null,false, null),
        new Field('tax_refunded', "Varchar", true,'tax_refunded', null,false, null),
        new Field('total_canceled', "Varchar", true,'total_canceled', null,false, null),
        //new Field('total_invoiced', "Varchar", true,'coupon_code', null,false, null), $magentoOrdertotal_invoiced ?? '';
        new Field('total_offline_refunded', "Varchar", true,'total_offline_refunded', null,false, null),
        new Field('total_online_refunded', "Varchar", true,'total_online_refunded', null,false, null),
        new Field('total_paid', "Varchar", true,'total_paid', null,false, null),
        new Field('total_qty_ordered', "Varchar", true,'total_qty_ordered', null,false, null),
        new Field('total_refunded', "Varchar", true,'total_refunded', null,false, null),
        new Field('can_ship_partially', "Varchar", true,'can_ship_partially', null,false, null),
        new Field('can_ship_partially_item', "Varchar", true,'can_ship_partially_item', null,false, null),
        new Field('customer_is_guest', "Varchar", true,'customer_is_guest', null,false, null),
        new Field('customer_note_notify', "Varchar", true,'customer_note_notify', null,false, null),
        new Field('billing_address_id', "Varchar", true,'billing_address_id', null,false, null),
        new Field('customer_group_id', "Varchar", true,'customer_group_id', null,false, null),
        new Field('edit_increment', "Varchar", true,'edit_increment', null,false, null),
        new Field('email_sent', "Varchar", true,'email_sent', null,false, null),
        new Field('send_email', "Varchar", true,'send_email', null,false, null),
        //new Field('forced_shipment_with_invoice', "Varchar", true,'coupon_code', null,false, null),$magentoOrderforced_shipment_with_invoice ?? '';
        new Field('payment_auth_expiration', "Varchar", true,'payment_auth_expiration', null,false, null),
        new Field('quote_address_id', "Varchar", true,'quote_address_id', null,false, null),
        new Field('quote_id', "Varchar", true,'quote_id', null,false, null),
        new Field('shipping_address_id', "Varchar", true,'shipping_address_id', null,false, null),
        new Field('adjustment_negative', "Varchar", true,'adjustment_negative', null,false, null),
        new Field('adjustment_positive', "Varchar", true,'adjustment_positive', null,false, null),
        new Field('base_adjustment_negative', "Varchar", true,'base_adjustment_negative', null,false, null),
        new Field('base_adjustment_positive', "Varchar", true,'base_adjustment_positive', null,false, null),
        new Field('base_shipping_discount_amount', "Varchar", true,'base_shipping_discount_amount', null,false, null),
        new Field('base_subtotal_incl_tax', "Varchar", true,'base_subtotal_incl_tax', null,false, null),
        new Field('base_total_due', "Varchar", true,'base_total_due', null,false, null),
        new Field('payment_authorization_amount', "Varchar", true,'payment_authorization_amount', null,false, null),
        new Field('shipping_discount_amount', "Varchar", true,'shipping_discount_amount', null,false, null),
        new Field('subtotal_incl_tax', "Varchar", true,'subtotal_incl_tax', null,false, null),
        new Field('total_due', "Varchar", true,'total_due', null,false, null),
        new Field('weight', "Varchar", true,'weight', null,false, null),
        new Field('customer_dob', "Varchar", true,'customer_dob', null,false, null),
        new Field('increment_id', "Varchar", true,'increment_id', null,false, null),
        new Field('base_currency_code', "Varchar", true,'base_currency_code', null,false, null),
        new Field('customer_email', "Varchar", true,'customer_email', null,false, null),
        new Field('customer_firstname', "Varchar", true,'customer_firstname', null,false, null),
        new Field('customer_lastname', "Varchar", true,'customer_lastname', null,false, null),
        new Field('customer_middlename', "Varchar", true,'customer_middlename', null,false, null),
        new Field('customer_prefix', "Varchar", true,'customer_prefix', null,false, null),
        new Field('customer_suffix', "Varchar", true,'customer_suffix', null,false, null),
        new Field('customer_taxvat', "Varchar", true,'customer_taxvat', null,false, null),
        new Field('discount_description', "Varchar", true,'discount_description', null,false, null),
        new Field('ext_customer_id', "Varchar", true,'ext_customer_id', null,false, null),
        new Field('ext_order_id', "Varchar", true,'ext_order_id', null,false, null),
        new Field('global_currency_code', "Varchar", true,'global_currency_code', null,false, null),
        new Field('hold_before_state', "Varchar", true,'hold_before_state', null,false, null),
        new Field('hold_before_status', "Varchar", true,'hold_before_status', null,false, null),
        new Field('order_currency_code', "Varchar", true,'order_currency_code', null,false, null),
        new Field('original_increment_id', "Varchar", true,'original_increment_id', null,false, null),
        new Field('relation_child_id', "Varchar", true,'relation_child_id', null,false, null),
        new Field('relation_child_real_id', "Varchar", true,'relation_child_real_id', null,false, null),
        new Field('relation_parent_id', "Varchar", true,'relation_parent_id', null,false, null),
        new Field('relation_parent_real_id', "Varchar", true,'relation_parent_real_id', null,false, null),
        new Field('remote_ip', "Varchar", true,'remote_ip', null,false, null),
        new Field('shipping_method', "Varchar", true,'shipping_method', null,false, null),
        new Field('store_currency_code', "Varchar", true,'store_currency_code', null,false, null),
        new Field('x_forwarded_for', "Varchar", true,'x_forwarded_for', null,false, null),
        new Field('customer_note', "Varchar", true,'customer_note', null,false, null),
        new Field('total_item_count', "Varchar", true,'total_item_count', null,false, null),
        new Field('customer_gender', "Varchar", true,'customer_gender', null,false, null),
        new Field('discount_tax_compensation_amount', "Varchar", true,'discount_tax_compensation_amount', null,false, null),
        new Field('base_discount_tax_compensation_amount', "Varchar", true,'base_discount_tax_compensation_amount', null,false, null),
        new Field('shipping_discount_tax_compensation_amount', "Varchar", true,'shipping_discount_tax_compensation_amount', null,false, null),
        new Field('base_shipping_discount_tax_compensation_amnt', "Varchar", true,'base_shipping_discount_tax_compensation_amnt', null,false, null),
        new Field('discount_tax_compensation_invoiced', "Varchar", true,'discount_tax_compensation_invoiced', null,false, null),
        new Field('base_discount_tax_compensation_invoiced', "Varchar", true,'base_discount_tax_compensation_invoiced', null,false, null),
        new Field('discount_tax_compensation_refunded', "Varchar", true,'discount_tax_compensation_refunded', null,false, null),
        new Field('base_discount_tax_compensation_refunded', "Varchar", true,'base_discount_tax_compensation_refunded', null,false, null),
        new Field('shipping_incl_tax', "Varchar", true,'shipping_incl_tax', null,false, null),
        new Field('base_shipping_incl_tax', "Varchar", true,'base_shipping_incl_tax', null,false, null),
        new Field('coupon_rule_name', "Varchar", true,'coupon_rule_name', null,false, null),
        new Field('paypal_ipn_customer_notified', "Varchar", true,'paypal_ipn_customer_notified', null,false, null),
        new Field('order_date', "Varchar", true,'created_at', null,false, null),
        new Field('created_at', "Date", true,null, null,false, null),
        new Field('updated_at', "Date", true,null, null,false, null),
        new Field('synced', "bool", true,null, null,false, null),

        ]);
    }
}
