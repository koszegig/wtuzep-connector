<?php

namespace App;

use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;

class articlenumbers extends  BaseConnectorModel
{
    //
    /**
     * @var string
     */
    protected $tablename = 'articlenumbers';
    /**
     * @var string
     */
    public $table = "articlenumbers";

    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var bool
     */
    public $szinkron = true;

    /**
     *
     */
    protected function setStoredProcedure()
    {
        $this->storedProcedure = env('DB_PREFIX', '') . 'articlenumbers';
    }

    /* public function pictures()
     {
         return $this->hasMany('App\Picture');
     }

 */

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("id", "int4", true, null, null, true, null, null, null, false),
            new Field("mertekegysegerpkod", "Varchar", true, "mertekegysegerpkod", 'sku', false, null, null, 50, true),
            new Field("megnevezes", "Varchar", true, "megnevezes", null, false, null, null, 191, true),
            new Field("rovidnev", "Varchar", true, "rovidnev", null, false, null, null, 191, true),
            new Field('created_at', "Date", true, null, null, false, null, null, null, false),
            new Field('updated_at', "Date", true, null, null, false, null, null, null, false),
            new Field('synced', "bool", true, null, null, false, null, null, null, false),
            new Field('syncedorder', "Varchar", true, null, null, false, null, null, null, false),
            new Field('magento_id', "Varchar", true, null, null, false, null, null, null, false),
            new Field('active', "Bool", true, null, 'active', false, null, null, null, true),
        ]);
    }
}
