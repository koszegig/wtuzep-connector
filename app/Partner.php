<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $guarded = [];

    public function phones()
    {
        return $this->belongsToMany('App\Phone');
    }

    public function emails()
    {
        return $this->belongsToMany('App\Email');
    }
}
