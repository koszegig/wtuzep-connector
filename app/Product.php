<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

/**
 * Class Product
 * @package App
 */
class Product extends BaseConnectorModel
{
    //
    /**
     * @var string
     */
    protected $tablename='products';
    /**
     * @var string
     */
    public $table="products";

    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var bool
     */
    public $szinkron = true;

    /**
     *
     */
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'products';
    }

   /* public function pictures()
    {
        return $this->hasMany('App\Picture');
    }

*/
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributevalues()
    {
        return $this->belongsToMany('App\AttributeValue');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null, null,  null, false),
            new Field("cikkszam", "Varchar", true,"PRODUCTID", 'sku',false, null, null,  50, true),
            new Field("cikknev", "Varchar", true,"PRODUCTNAME", null,false, null, null,  191, true),
            new Field("mertekegyseg", "Varchar", true,"UNITID", null,false, null, null,  191, true),
            new Field("beszmertekegyseg", "Varchar", true,"PURCHASINGUNITID", null,false, null, null,  191, true),
            new Field("afa", "Varchar", true,"SALESVAT", 'name',false, null, null,  191, true),
            new Field("bonthato", "Varchar", true,"ISDIVISIBLE", null,false, null, null,  null, true),
            new Field("kiszereles", "Varchar", true,"PACKAGING", 'description',false, null, null,  80, true),
            new Field("megjegyzes", "Varchar", true,"PRODUCTCOMMENT", null,false, null, null,  null, true),
            new Field('created_at', "Date", true,null, null,false, null, null,  null, false),
            new Field('updated_at', "Date", true,null, null,false, null, null,  null, false),
            new Field('synced', "bool", true,null, null,false, null, null,  null, false),
            new Field('syncedorder', "Varchar", true,null, null,false, null, null,  null, false),
            new Field('magento_id', "Varchar", true,null, null,false, null, null,  null, false),
            new Field('enabled', "Bool", true,null, 'enabled',false, null, null,  null, true),
            new Field('active', "Bool", true,null, 'active',false, null, null,  null, true),
        ]);
    }
}
