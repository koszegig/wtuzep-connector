<?php

namespace App;

use App\Libraries\Field;
use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;

class AttributeValue extends BaseConnectorModel
{
    protected $tablename='attribute_values';
    /**
     * @var string
     */
    public $table="attribute_values";

    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'attribute_values';
    }
    protected $guarded = [];

    public function products()
    {
        //return $this->hasMany('App\Product');
        return $this->belongsToMany('App\Product');
    }
    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("attribute_id", "varchar", true,null, null,false, null),
            new Field("erp_id", "varchar", true,null, null,false, null),
            new Field("value", "varchar", true,null, null,false, null),
            new Field("label", "varchar", true,null, null,false, null),
            new Field("type", "varchar", true,null, null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
            new Field("garancia", "select", true,'Garancia', null,false, null),
            new Field("kifuto", "select", false,'Kifuto', null,false, null),
            new Field("sim", "select", false,'SIM', null,false, null),
            new Field("ram", "text", false,'RAM', null,false, null),
            new Field('rom', "select", false,'ROM', null,false, null),
            new Field('memoriabovithetoseg', "select", false,'MemoriaBovithetoseg', null,false, null),
            new Field('kijelzomeret', "text", false,'KijelzoMeret', null,false, null),
            new Field('kijelzofelbontas', "text", false,'KijelzoFelbontas', null,false, null),
            new Field('hatlapikamerafelbontas', "text", false,'HatlapiKameraFelbontas', null,false, null),
            new Field('elolapikamerafelbontas', "text", false,'ElolapiKameraFelbontas', null,false, null),
            new Field('processzormag', "select", false,'ProcesszorMag', null,false, null),
            new Field('operaciosrendszer', "select", false,'OperaciosRendszer', null,false, null),
            new Field('akkumeret', "text", false,'AkkuMeret', null,false, null),
            new Field('toltocsatlakozo', "select", false,'ToltoCsatlakozo', null,false, null),
            new Field('vezeteknelkultoltheto', "select", false,'VezetekNelkulToltheto', null,false, null),
            new Field('dobozbruttosuly', "text", false,'DobozBruttoSuly', null,false, null),
            new Field("szin", "select", true,'Szin', null,false, null),

        ]);
    }
}
