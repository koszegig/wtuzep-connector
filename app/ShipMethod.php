<?php

namespace App;

use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;

class ShipMethod  extends BaseConnectorModel
{
    //
    protected $tablename='shipmethods';
    /**
     * @var string
     */
    public $table="shipmethods";

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'shipmethods';
    }

    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field('erp_id', "Varchar", true,'SzallitasiModKod', null,false, null),
            new Field('erp_name', "Varchar", true,'SzallitasiModMegnevezes', null,false, null),
            new Field('magento_id', "Varchar", true,null, null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
        ]);
    }
}
