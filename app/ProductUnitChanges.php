<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

/**
 * Class Product
 * @package App
 */
class ProductUnitChanges extends BaseConnectorModel
{
    //
    /**
     * @var string
     */
    protected $tablename = 'productunitchanges';
    /**
     * @var string
     */
    public $table = "productunitchanges";

    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var bool
     */
    public $szinkron = true;

    /**
     *
     */
    protected function setStoredProcedure()
    {
        $this->storedProcedure = env('DB_PREFIX', '') . 'productunitchanges';
    }



    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("id", "int4", true, null, null, true, null, null, null, false),
            new Field("product_id", "int4", true, null, null, false, null, null, 50, true),
            new Field("unit_id", "int4", true, null, null, false, null, null, 191, true),
            new Field("productid", "Varchar", true, "productid", null, false, null, null, 191, true),
            new Field("unitid", "Varchar", true, "unitid", null, false, null, null, 191, true),
            new Field("Exchange", "Varchar", true, "Exchange", 'name', false, null, null, 191, true),
            new Field('created_at', "Date", true, null, null, false, null, null, null, false),
            new Field('updated_at', "Date", true, null, null, false, null, null, null, false),
            new Field('synced', "bool", true, null, null, false, null, null, null, false),
            new Field('syncedorder', "Varchar", true, null, null, false, null, null, null, false),
        ]);
    }
}

