<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

class StocksQty extends BaseConnectorModel
{

    protected $tablename='stocksqty';
    /**
     * @var string
     */
    public $table="stocksqty";

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'stocksqty';
    }

    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("ProductId", "Varchar", true,null, null,false, null),
            new Field("cikkszam", "Varchar", true,'ProductId', 'sku',false, null),
            new Field("StockId", "Varchar", true,'StorageId', null,false, null),
            new Field("source_code", "Varchar", true,null, 'source_code',false, null),
            new Field("StockName", "Varchar", true,null, null,false, null),
            new Field("FreeStock", "Varchar", true,null, null,false, null),
            new Field("ReservedStock", "Varchar", true,null, null,false, null),
            new Field("TotalStock", "Varchar", true,'StockQuantity', 'quantity',false, null),
            new Field('IncommingQuantity', "Varchar", true,null, null,false, null),
            new Field('IncommingStockDate', "Varchar", true,null, null,false, null),
            new Field('magento_id', "Varchar", true,null, null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
            new Field('synced', "Bool", true,null, null,false, null),
        ]);
    }
}
