<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use App\User;

trait HasUsers
{
    /**
     * A model may have multiple emails.
     */

    public function users()
    {
        $class = \MyClass::getClass(get_called_class());
        return $this->belongsToMany('App\User', 'model_has_users', 'model_id', 'user_id')->wherePivot('model', $class)->withPivot('type');
    }

    protected function getUser($userID): User
    {
        return app(User::class)->find($userID);
    }

    /**
     * Assign the given prItem to the model.
     *
     * @param array $prItem
     *
     * @return $this
     */
    public function addProjectUser($pUser)
    {
        $class = \MyClass::getClass(get_called_class());
        $user = $this->getUser($pUser['id']);
        $this->users()->attach($user->id, ['type' => $pUser['type'], 'model' => $class]);

        return $this;
    }

    /**
     * Assign multiple prItem to the model.
     *
     * @param array $prItems
     *
     * @return $this
     */
    public function addProjectUsers($pUsers)
    {
        foreach ($pUsers as $pUser) {
            $this->addProjectUser($pUser);
        }

        return $this;
    }

    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncProjectUsers($pUsers, $type = null)
    {
        if (is_null($type))
            $this->users()->detach();
        else
            $this->users()->wherePivot('type', '=', $type)->detach();
        return $this->addProjectUsers($pUsers);

        // return $this->addQuotationProductGroupItems($prItems);
    }

    public function getProjectUsers(): Collection
    {
        return $this->projectusers->pluck('name');
    }

    public function getProjectCoordinator()
    {
        $user = $this->users()->where('type', 'coordinator')->first();
        if (empty($user)) return $user;
        return $user;
    }

    public function getProjectManager()
    {
        $user = $this->users()->where('type', 'manager')->first();
        if (empty($user)) return $user;
        return $user;
    }

    /* public function getProjectTag()
    {
        $user = $this->users()->where('type','tag')->get();
        if(empty($user)) return $user;
        return $user;
    }*/

    public function getOwner()
    {
        $user = $this->users()->where('type', 'owner')->first();
        if (empty($user)) return $user;
        return $user;
    }



    public function getProjectTag()
    {
        $user = $this->users()->where('type', 'tag')->get();
        return $user;
    }

    public function getOwners()
    {
        $user = $this->users()->where('type', 'owner')->get();
        return $user;
    }

    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncProjectCoordinator($pUser)
    {
        $pUsers = [['id' => $pUser, 'type' => 'coordinator']];
        return $this->syncProjectUsers($pUsers, 'coordinator');
    }
    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncProjectManager($pUser)
    {
        $pUsers = [['id' => $pUser, 'type' => 'projectmanager']];
        return $this->syncProjectUsers($pUsers, 'projectmanager');
    }

    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncTag($pUsers)
    {
        $_pUsers = [];
        foreach ($pUsers as $user)
            array_push($_pUsers, ['id' => $user['id'], 'type' => 'tag']);
        return $this->syncProjectUsers($_pUsers, 'tag');
    }

    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncOwner($pUser)
    {
        $pUsers = [['id' => $pUser, 'type' => 'owner']];
        return $this->syncProjectUsers($pUsers, 'owner');
    }



    /**
     * Remove all current prItems and set the given ones.
     *
     * @param array|string $prItems
     *
     * @return $this
     */
    public function syncCreater($pUser)
    {
        $pUsers = [['id' => $pUser, 'type' => 'creater']];
        return $this->syncProjectUsers($pUsers, 'creater');
    }
    /**
    * Remove all current prItems and set the given ones.
    *
    * @param array|string $prItems
    *
    * @return $this
    */
   public function syncOwners($pUsers)
   {
       $_pUsers = [];
       foreach ($pUsers as $user)
           array_push($_pUsers, ['id' => $user['id'], 'type' => 'owner']);
       return $this->syncProjectUsers($_pUsers, 'owner');
   }
}
