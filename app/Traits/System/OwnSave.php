<?php

namespace App\Traits\System;

trait OwnSave
{
    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         */
        static::creating(function ($model) {
            if (!is_array($model->primaryKey) &&  empty($model->{$model->getKeyName()}))
                $model->{$model->getKeyName()} = (string) \MyHash::genUUID();
            if (in_array('created_at', $model->dates))
                $model->created_by = \MyUser::getUserID();
            if (in_array('updated_at', $model->dates))
                $model->updated_by = \MyUser::getUserID();
        });

        static::updating(function ($model) {
            if (in_array('updated_at', $model->dates))
                $model->updated_by = \MyUser::getUserID();
        });
    }
}
