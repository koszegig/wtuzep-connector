<?php

namespace App\Traits\System;

trait OwnCast
{

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                return new BaseCollection($this->fromJson($value));
            case 'date':
                return $this->asDate($value);
            case 'datetime':
            case 'custom_datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimestamp($value);
            case 'image':
                return $this->castImage($value);
            default:
                return $value;
        }
    }

    /**
     * The answer that belong to the question.
     */
    protected function castImage($value)
    {
        // return null;
        if (empty($value) || is_null($value)) {
            return null;
        }
        if (is_string($value)) {
            return $value;
        }
        $image = \MyString::byteToString($value);
        return $image;
    }
}
