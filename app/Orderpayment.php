<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;

class Orderpayment  extends BaseConnectorModel
{
    /**
     * @var string
     */
    public $table="order_payment";
    protected $tablename='order_payment';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'order_payment';
    }
    public function setFields(){
        $this->fields = collect([

            new Field('id', "int4", true,null, null,true, null),
            new Field('order_id', "Varchar", true,'order_id', null,false, null),
            new Field('paymentkey', "Varchar", true,'key', null,false, null),
            new Field('paymentvalue', "Varchar", true,'value', null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),

        ]);

    }
    public function street ($row) {
        return implode(" ",$row['street']);
    }
}
