<?php

namespace App;

use \App\Contracts\UserGroup as UserGroupContract;
use \App\Exceptions\UserGroupAlreadyExists;
use \App\Exceptions\UserGroupDoesNotExist;
use \App\Traits\HasRoles;

class UserGroup extends BaseModel implements UserGroupContract
{

    use HasRoles;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_groups';

    protected $guard_name = 'web';

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        array_push($this->fillable, 'name', 'active');
        parent::__construct($attributes);
        array_push($this->appends, 'display_name');
    }



    public static function create(array $attributes = [])
    {
        if (static::where('name', $attributes['name'])->first()) {
            throw UserGroupAlreadyExists::create($attributes['name']);
        }

        return static::query()->create($attributes);
    }

    /**
     * Find a usergroup by its name.
     *
     * @param string $name
     *
     * @return \App\Contracts\UserGroup|\App\Models\UserGroup
     *
     * @throws \App\Exceptions\UserGroupDoesNotExist
     */
    public static function findByName(string $name): UserGroupContract
    {
        $usergroup = static::where('name', $name)->first();

        if (! $usergroup) {
            throw UserGroupDoesNotExist::named($name);
        }

        return $usergroup;
    }

     /**
      * Find a usergroup by its id.
      *
      * @param string $id
      *
      * @return \App\Contracts\UserGroup|\App\Models\UserGroup
      *
      * @throws \App\Exceptions\UserGroupDoesNotExist
      */

    public static function findById(string $id): UserGroupContract
    {

        $usergroup = static::find('id', $id);

        if (! $usergroup) {
            throw UserGroupDoesNotExist::withId($id);
        }

        return $usergroup;
    }

    /**
     * Find or create usergroup by its name (and optionally guardName).
     *
     * @param string      $name
     * @param string|null $guardName
     *
     * @return \Spatie\Permission\Contracts\Role
     */
    public static function findOrCreate(string $name): UserGroupContract
    {
        $usergroup = static::where('name', $name)->first();

        if (! $usergroup) {
            return static::create(['name' => $name]);
        }

        return $usergroup;
    }



    public function getDisplayNameAttribute()
    {
        return __("usergroup.{$this->name}");
    }
}
