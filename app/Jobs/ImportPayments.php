<?php


namespace App\Jobs;
use App\Aion\Helpers\Arrayhelpers;
use App\payment;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\SAPTables\ActFizetesiMod;
use DB;
use Log;


class ImportPayments  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var array
     */
    protected $argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var int
     */
    protected $limitCount=0;


    /**
     * @var array
     */
    protected $payment =[];
    /**
     * @var
     */
    protected $paymentClass;
    /**
     * @var
     */
    protected $paymentClassFields;
    /**
     * ImportPayments constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->argumentum =$_argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handlepayments();
    }


    /**
     *
     */
    public function handlepayments()
    {
        Task::Start('ImportPayments');
        $this->paymentClass =  new payment();
        $this->paymentClassFields = $this->paymentClass->getFields();
        Task::updateLastTicket('ImportPayments', 000);
        $payments = ActFizetesiMod::all()->toArray();
        Arrayhelpers::toStringToEcho('Starting ImportPayments');
        Task::updateLastTicket('ImportPayments', 10);

        foreach ($payments as $row) {
            Task::updateLastTicket('ImportPayments', 30);
            //Arrayhelpers::toStringToAll($row, 'ImportPayments SAP ActRaktarKeszlet  row', __FILE__, __METHOD__, __LINE__);
            $this->payment = [];
            $this->filledpaymentFields($row);
                $sqlpayment = payment::UpdateOrCreate(['erp_id' => $this->payment['erp_id']], $this->payment);
                if ($sqlpayment->wasChanged()) {
                    Log::debug("Marking paayment dirty.");
                    $sqlpayment->synced = 0;
                    $sqlpayment->save();
                }
                Task::updateLastTicket('ImportPayments', 40);
                $this->limitCount++;
            if( ($this->limitCount>$this->argumentum['limit'] && $this->argumentum['limit'] !=0)  || Task::isStopped('ImportPayments') ) {
                Task::Stop('ImportPayments',$this->newCount,$this->refreshCount);
                break;
            }

        }

        Task::Stop('ImportPayments',$this->newCount,$this->refreshCount);
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledpaymentFields($row){
        Arrayhelpers::toStringToAll($row, 'ImportPayments filledStockQtyFields source ', __FILE__, __METHOD__, __LINE__);
        foreach($this->paymentClassFields  as $field){
            if (!empty($field->getImportFieldName())) {
                $this->payment[$field->getName()] = $row[$field->getImportFieldName()];
            }
        }
        Task::updateLastTicket('ImportPayments', 39);
    }
}
