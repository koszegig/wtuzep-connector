<?php

namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use App\Picture;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;
use App\Email;
use App\Phone;
use App\Partner;
use App\Contact;
use App\ProductModels;
use App\StocksQty;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/**
 * Class ExportPartners
 * @package App\Jobs
 */
class TruncateProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->truncateProducts();
    }

    /**
     *
     */
    public function truncateProducts()
    {
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();

        Log::info("Truncating all products in magento.");

        $response = $magento->get("/rest/V1/products/?searchCriteria[page_size]=200000");

        if (is_object($response) && property_exists($response, "items")) {
            if (is_array($response->items)) {
                foreach ($response->items as $item) {
                    $magento_id = $item->id;
                    $sku = $item->sku;
                    Log::info("Deleting {$sku} ({$magento_id})");
                    $response = $magento->delete("/rest/V1/products/".urlencode($sku), ["id" => $magento_id]);
                    dump($response);
                    if ($response) {
                        $product = Product::where("product_sku", $sku)->first();
                        if ($product) {
                            $product->magento_id = 0;
                            $product->synced = 0;
                            $product->save();
                            Picture::where("product_id", $product->id)->update(['synced' => 0, 'magento_id' => 0]);
                            ProductModels::where("product_id", $product->id)->update(['synced' => 0]);
                            StocksQty::where("product_id", $product->id)->update(['synced' => 0, 'magento_id' => 0]);
                        }

                    }
                }
            }
        }
    }

}
