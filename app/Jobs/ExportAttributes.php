<?php

namespace App\Jobs;

use App\Product;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use App\Aion\Helpers\ArrayHelpers;
use App\Attribute;
use App\AttributeValue;
use App\Aion\Connect\MagentoRestApi;

use Log;

/**
 * Class ExportAttributes
 * @package App\Jobs
 */
class ExportAttributes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var int
     */
    protected $limitCount=0;

    /**
     * ExportAttributes constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->exportAttributes();
    }

    /**
     *
     */
    public function exportAttributes()
    {
        Log:info("Exporting attributes");
        Task::Start('ExportAttributes');
        /*dd( Attribute::whereIn('id', function($query){
           $query->select('attribute_id')
               ->from(with(new AttributeValue)->getTable())
               ->where('synced','0');
       })->get());*/
        Attribute::whereIn('id', function($query){
        $query->select('attribute_id')
            ->from(with(new AttributeValue)->getTable())
            ->where('synced','0');
           })->update(['synced' => 0]);
       /* dd( Attribute::whereIn('id', function($query){
            $query->select('attribute_id')
                ->from(with(new AttributeValue)->getTable())
                ->where('synced','0');
        })->get());*/
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();


        // build existing attribute codes in set to assign unassigned attributes to attribute set
        $attribute_codes_in_set = [];
        $resp = $magento->get("/rest/V1/products/attribute-sets/4/attributes");
        foreach ($resp as $attr) {
            $attribute_codes_in_set[] = $attr->attribute_code;
        }

        //Arrayhelpers::toStringToLog($resp, 'exportAttributes  attributes get export - $resp');
        //Arrayhelpers::toStringToLog($attribute_codes_in_set, 'exportAttributes  attributes get export - $attribute_codes_in_set');
//        $resp = $magento->get("/rest/V1/products/attribute-sets/groups/list?searchCriteria[filterGroups][0][filters][0][field]=attribute_set_id&searchCriteria[filterGroups][0][filters][0][value]=4");
//        var_dump($resp);die;

        // igazabol az attribute value-kat kell nezni, hogy azok synced-ek-e select
        $attributes = Attribute::where('type','select')->where('synced','0')->orderby('id')->get();
        foreach ($attributes as $attribute) {
            if($attribute->type !='select'){
                continue;
            }
//            if ( $attribute->code != "it55web02") continue;

            Log::info(" => $attribute->name");
            Log::info("\tid: $attribute->id");
            Log::info("\tcode: $attribute->code");

            $options = [];
            foreach ( $attribute->values as $value ) {
                //echo ":: {$value->erp_id} :: " . $value->label ."\n";
                Arrayhelpers::toStringToEcho(":: {$value->erp_id} :: " . $value->label.':: ' . $value->label );
                if(empty($value->value)) {
                    continue;
                }
                $options[] = [ "label" => $value->value
                //    "value" => $value->id // nem szabad mert elhasal
                ];
            }
            /*if (isset($options[0])){
                $options[0]['is_default'] =true;
            }*/
            $ret = $magento->Get("/rest/V1/products/attributes/". $attribute->code);

            $attribute_already_exists = false;
            if ( property_exists($ret, 'default_frontend_label') ) {
                $attribute_already_exists = true;
            }


            // https://devdocs.magento.com/swagger/#/catalogProductAttributeRepositoryV1/catalogProductAttributeRepositoryV1SavePost
            $post = [
                "attribute" => [
                   "is_wysiwyg_enabled" => false,
                   "is_html_allowed_on_front"=> false,
                   "used_for_sort_by"=> false,
                   "is_filterable"=> true,
                   "is_filterable_in_search"=> true,
                   "is_used_in_grid"=> true,
                   "is_visible_in_grid"=> false,
                   "is_filterable_in_grid"=> true,
                   "position"=> 0,
                   "apply_to"=> [],
                   "is_searchable"=> "1",
                   "is_visible_in_advanced_search"=> "1",
                   "is_comparable"=> "1",
                   "is_used_for_promo_rules"=> "0",
                   "is_visible_on_front"=> "1",
                   "used_in_product_listing"=> "1",
                   "is_visible"=> true,
                   "scope" => "global",
                   "attribute_code" => $attribute->code,
                   "frontend_input" => "select",
                   "entity_type_id" => "4",
                   "is_required" => false,
                  // "options" => $options,
                   "is_user_defined" => true,
                   "default_frontend_label" => $attribute->name,
                   "frontend_labels" => null,
                   "backend_type" => "int",
                   "source_model" => "Magento%5C%5CEav%5C%5CModel%5C%5CEntity%5C%5CAttribute%5C%5CSource%5C%5CTable",
                   "default_value" => "",
                   "is_unique" => "0"
                ]
            ];

            if ( $attribute_already_exists == false ) {
                // uj attributumot hozunk letre
                Arrayhelpers::toStringToAll($post, 'exportAttributes post attributes - $post');
                $ret = $magento->Post("/rest/V1/products/attributes", $post);
                if ( is_object($ret) &&  property_exists($ret, "message")) {
                    Arrayhelpers::toStringToAll($ret->message, 'exportAttributes post attributes - $ret error');
                } else {
                    //print_r($ret);
                    // Arrayhelpers::toString($ret, 'exportAttributes post attributes - $ret');
                    Arrayhelpers::toStringToAll($ret, 'exportAttributes post attributes - $ret');
                    if (is_object($ret) && property_exists($ret, "attribute_id")) {
                        $attribute->magento_id = $ret->attribute_id;
                    }
                    $attribute->synced = 1;
                    $attribute->save();
                    foreach ($options as $option) {
                        $postOption = [
                            "option" => $option
                        ];
                        $isReadyOption = false;
                        $respGetOptions = $magento->get("/rest/V1/products/attributes/" . $attribute->code ."/options");
                        foreach ($respGetOptions as $magento_attribute_option) {
                            if ($option['label'] == $magento_attribute_option->label) {
                                $isReadyOption = true;
                            }
                        }
                        if (!$isReadyOption) {
                            Arrayhelpers::toStringToAll($postOption, 'exportAttributes update update - $postOption');
                            $respUpdateOption = $magento->post("/rest/V1/products/attributes/" . $attribute->code . "/options", $postOption);
                            Arrayhelpers::toStringToAll($respUpdateOption, 'exportAttributes update update - $respUpdateOption');
                        }
                    }

                }

            } else {
                //echo "Attributum already exists\n";
                Arrayhelpers::toStringToEcho("Attributum already exists" );

                if (is_object($ret) && property_exists($ret, "attribute_id")) {
                    $attribute->magento_id = $ret->attribute_id;
                    $attribute->synced = 1;
                    $attribute->save();

                }
                $respGetOptions = $magento->get("/rest/V1/products/attributes/" . $attribute->code ."/options");
                if($this->options['deleteattributtebvalues']) {
                     foreach ($respGetOptions as $delete_option) {
                         $respUpdateOption = $magento->Delete("/rest/V1/products/attributes/" . $attribute->code ."/options/".$delete_option->value,[]);
                     }
                    $respGetOptions = $magento->get("/rest/V1/products/attributes/" . $attribute->code ."/options");
                }
                    foreach ($options as $option) {
                        $postOption = [
                            "option" => $option
                        ];
                        $isReadyOption = false;
                        foreach ($respGetOptions as $magento_attribute_option) {
                            if ( $option['label'] == $magento_attribute_option->label ) {
                                $isReadyOption = true;
                            }
                        }
                        if (!$isReadyOption) {
                            Arrayhelpers::toStringToAll($postOption, 'exportAttributes update update - $postOption');
                            $respUpdateOption = $magento->post("/rest/V1/products/attributes/" . $attribute->code ."/options",$postOption);
                            Arrayhelpers::toStringToAll($respUpdateOption, 'exportAttributes update update - $respUpdateOption');
                        }

                 /*   $ret = $magento->Delete("/rest/V1/products/attributes/". $attribute->code, $post);
                   // unset($attribute_codes_in_set['$attribute->code']);
                    Arrayhelpers::toStringToAll($post, 'exportAttributes update post attributes - $post');
                    $retput = $magento->Post("/rest/V1/products/attributes", $post);
                    Arrayhelpers::toStringToAll($retput, 'exportAttributes update post attributes - $ret');*/

                }

                // $ret = $magento->Put("/rest/V1/products/attributes/".$attribute->code, $post);
                // echo "/rest/V1/products/attributes/".$attribute->code;
                // elobb toroljuk es ujra hozza adjuk...
                //$ret = $magento->Delete("/rest/V1/products/attributes/". $attribute->code, $post);
                //$ret = $magento->Post("/rest/V1/products/attributes", $post);

            }

            $resp = $magento->get("/rest/V1/products/attributes/" . $attribute->code ."/options");

            if ( is_array($resp) ) {
                foreach ($resp as $magento_attribute_option) {
                    if ($magento_attribute_option->value == '') {
                        continue;
                    }

                    $magento_id = $magento_attribute_option->value;
                    foreach ( $attribute->values as $value ) {
                        if ( $value->value == $magento_attribute_option->label ) {
                            echo "\t id:". $value->id . " label:" . $value->label . " magento_id:" . $magento_id ."\n";
                            $value->magento_id = $magento_id;
                            $value->synced=1;
                            $value->save();
                        }
                        Arrayhelpers::toStringToEcho($value->value ,'$value->value');
                        Arrayhelpers::toStringToEcho($magento_attribute_option->label ,'$magento_attribute_option->label');

                    }
                  //  dd($attribute->values);
                }
            }

            if ( ! in_array($attribute->code, $attribute_codes_in_set )) {
                Log::info("ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                $post = [
                    "attributeSetId" => 4,
                    "attributeGroupId" => 23, //
                    "attributeCode" => $attribute->code,
                    "sortOrder" => 0
                ];
                //print_r($post);
               // Arrayhelpers::toString($post, "ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                Arrayhelpers::toStringToLog($post, "ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                $resp = $magento->post("/rest/V1/products/attribute-sets/attributes", $post);

            } else {
                Log::info("ATTRIBUTESET Attribute {$attribute->code} already in the attribute set");


            }
            $this->limitCount++;;
            if( ($this->limitCount>$this->argumentum['limit'] && $this->argumentum['limit'] !=0)  || Task::isStopped('ExportAttributes') ) {
                break;
            }
            AttributeValue::where('attribute_id',$attribute->id)->update(['synced' => 1]);
        } // attributes
        // igazabol az attribute value-kat kell nezni, hogy azok simple-ek-e select
        $attributes = Attribute::where('type','text')->where('synced','0')->get();

        foreach ($attributes as $attribute) {
//            if ( $attribute->code != "it55web02") continue;
            if($attribute->type !='text'){
                continue;
            }
            Log::info(" => $attribute->name");
            Log::info("\tid: $attribute->id");
            Log::info("\tcode: $attribute->code");

            $options = [];
            foreach ( $attribute->values as $value ) {
                //echo ":: {$value->erp_id} :: " . $value->label ."\n";
                Arrayhelpers::toStringToEcho(":: {$value->erp_id} :: " . $value->label.':: ' . $value->label );
                $options[] = [ "label" => $value->value,
                    //    "value" => $value->id // nem szabad mert elhasal
                ];
            }
            $ret = $magento->Get("/rest/V1/products/attributes/". $attribute->code);
            $attribute_already_exists = false;
            if ( property_exists($ret, 'default_frontend_label') ) {
                $attribute_already_exists = true;
            }


            // https://devdocs.magento.com/swagger/#/catalogProductAttributeRepositoryV1/catalogProductAttributeRepositoryV1SavePost
            $post = [
                "attribute" => [
                    "is_wysiwyg_enabled" => false,
                    "is_html_allowed_on_front"=> false,
                    "used_for_sort_by"=> false,
                    "is_filterable"=> true,
                    "is_filterable_in_search"=> true,
                    "is_used_in_grid"=> true,
                    "is_visible_in_grid"=> false,
                    "is_filterable_in_grid"=> true,
                    "position"=> 0,
                    "apply_to"=> [],
                    "is_searchable"=> "1",
                    "is_visible_in_advanced_search"=> "1",
                    "is_comparable"=> "1",
                    "is_used_for_promo_rules"=> "0",
                    "is_visible_on_front"=> "1",
                    "used_in_product_listing"=> "1",
                    "is_visible"=> true,
                    "scope" => "global",
                    "attribute_code" => $attribute->code,
                    "frontend_input" => "text",
                    "entity_type_id" => "4",
                    "is_required" => false,
                  //  "options" => $options,
                    "is_user_defined" => true,
                    "default_frontend_label" => $attribute->name,
                    "frontend_labels" => null,
                    "backend_type" => "text",
                    "source_model" => "Magento%5C%5CEav%5C%5CModel%5C%5CEntity%5C%5CAttribute%5C%5CSource%5C%5CTable",
                    "default_value" => "",
                    "is_unique" => "0"
                ]
            ];

            if ( $attribute_already_exists == false ) {
                // uj attributumot hozunk letre
                Arrayhelpers::toStringToAll($post, 'exportAttributes post attributes - $post');
                $response = $magento->Post("/rest/V1/products/attributes", $post);
                //print_r($ret);
                // Arrayhelpers::toString($ret, 'exportAttributes post attributes - $ret');
                Arrayhelpers::toStringToAll($response, 'exportAttributes post attributes - $ret');
                if ( is_object($response) &&  property_exists($response, "message")) {
                    $attribute->synced = 2;
                } else{
                    $attribute->magento_id = $response->attribute_id;
                    $attribute->synced = 1;
                }

                $attribute->save();

            }else{
                if (is_object($ret) && property_exists($ret, "attribute_id")) {
                    $attribute->magento_id = $ret->attribute_id;
                    $attribute->synced = 1;
                    $attribute->save();

                }
            }

            $resp = $magento->get("/rest/V1/products/attributes/" . $attribute->code ."/options");

            if ( is_array($resp) ) {
                foreach ($resp as $magento_attribute_option) {
                    if ($magento_attribute_option->value == '') {
                        continue;
                    }

                    $magento_id = $magento_attribute_option->value;
                    foreach ( $attribute->values as $value ) {
                        if ( $value->value == $magento_attribute_option->label ) {
                            echo "\t id:". $value->id . " label:" . $value->label . " magento_id:" . $magento_id ."\n";
                            $value->magento_id = $magento_id;
                            $value->synced=1;
                            $value->save();
                        }
                        Arrayhelpers::toStringToEcho($value->value ,'$value->value');
                        Arrayhelpers::toStringToEcho($magento_attribute_option->label ,'$magento_attribute_option->label');

                    }
                    //  dd($attribute->values);
                }
            }

            if ( ! in_array($attribute->code, $attribute_codes_in_set )) {
                Log::info("ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                $post = [
                    "attributeSetId" => 4,
                    "attributeGroupId" => 23, //
                    "attributeCode" => $attribute->code,
                    "sortOrder" => 0
                ];
                //print_r($post);
                // Arrayhelpers::toString($post, "ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                Arrayhelpers::toStringToLog($post, "ATTRIBUTESET Attribute {$attribute->code} is not in default attribute set");
                $resp = $magento->post("/rest/V1/products/attribute-sets/attributes", $post);

            } else {
                Log::info("ATTRIBUTESET Attribute {$attribute->code} already in the attribute set");


            }
            $this->limitCount++;;
            if( ($this->limitCount>$this->argumentum['limit'] && $this->argumentum['limit'] !=0)  || Task::isStopped('ExportAttributes') ) {
                break;
            }
            AttributeValue::where('attribute_id',$attribute->id)->update(['synced' => 1]);
        } // attributes simple (text)

        Task::Stop('ExportAttributes',$this->newCount,$this->refreshCount);

    }
}
