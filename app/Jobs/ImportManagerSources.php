<?php


namespace App\Jobs;
use App\Aion\Helpers\Arrayhelpers;
use App\Aion\Library\FileManager;
use App\ManagerSources;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\SAPTables\ActRaktar;
use DB;
use Log;


class ImportManagerSources implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    protected $_argumentum;
    protected $options;
    protected $limitCount=0;


    /**
     * @var array
     */
    protected $managerSources =[];
    /**
     * @var
     */
    protected $managerSourcesClass;
    /**
     * @var
     */
    protected $managerSourcesCClassFields;
    protected $source_code = [ '.1_Fo' => 'kozpont',
                                '.9_SES' => '',
                                'Ark_fo' => 'arkad-budapest',
                                'Deb_fo' => 'debrecen',
                                'Mis_fo' => 'miskolc',
                                'Sze_fo' => 'szeged',
                                'Wes_fo' => 'westend-budapest',
                            ];

    /**
     * ImportStocksqtys constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->_argumentum =$_argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleStocksqty();
    }


    /**
     *
     */
    public function handleStocksqty()
    {
        Task::Start('ImportManagerSources');
        $this->managerSourcesClass =  new ManagerSources();
        $this->managerSourcesClassFields = $this->managerSourcesClass->getFields();
        Task::updateLastTicket('ImportManagerSources', 000);

        Arrayhelpers::toStringToEcho('Starting ImportManagerSources');
        Task::updateLastTicket('ImportManagerSources', 10);
        $managerSources = ActRaktar::whereIn('Raktarkod', ['.1_Fo', '.9_SES', 'Ark_fo', 'Deb_fo', 'Mis_fo', 'Sze_fo', 'Wes_fo'])->get()->toArray();

        foreach ($managerSources as $row) {
            Task::updateLastTicket('ImportManagerSources', 30);
            Arrayhelpers::toStringToAll($row, 'ImportManagerSources SAP ActRaktar  row', __FILE__, __METHOD__, __LINE__);
            $this->managerSources = [];
            $this->filledmanagerSourcesFields($row);
                Arrayhelpers::toStringToEcho($row["RaktarKod"], ' ManagerSources ');
                $related = [];
                Arrayhelpers::toStringToAll($this->managerSources, 'ImportStocksqtys connector managerSources', __FILE__, __METHOD__, __LINE__);
                $sqlManagerSources = ManagerSources::UpdateOrCreate(['erp_id' => $this->managerSources['erp_id']], $this->managerSources);
                if ($sqlManagerSources->wasChanged()) {
                    Log::debug("Marking ManagerSources dirty.");
                    $sqlManagerSources->synced = 0;
                    $sqlManagerSources->save();
                }
                Task::updateLastTicket('ImportManagerSources', 40);
                $this->limitCount++;
            if( ($this->limitCount>$this->_argumentum['limit'] && $this->_argumentum['limit'] !=0)  || Task::isStopped('ImportStocksqtys') ) {
                Task::Stop('ImportManagerSources',$this->newCount,$this->refreshCount);
                break;
            }

        }

        Task::Stop('ImportStocksqtys',$this->newCount,$this->refreshCount);
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledmanagerSourcesFields($row){
        Arrayhelpers::toStringToAll($row, 'ImportManagerSources filledmanagerSourcesFields source ', __FILE__, __METHOD__, __LINE__);
        foreach($this->managerSourcesClassFields  as $field){
            if (!empty($field->getImportFieldName())) {
                $this->managerSources[$field->getName()] = $row[$field->getImportFieldName()];
            }
        }
        if (isset($this->source_code[$this->managerSources['erp_id']])) {
            $this->managerSources['cod'] = $this->source_code[$this->managerSources['erp_id']];
        }

        Task::updateLastTicket('ImportManagerSources', 39);
    }
}
