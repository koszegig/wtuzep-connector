<?php


namespace App\Jobs;
use App\Aion\Helpers\ArrayHelpers;
use App\Aion\Connect\MagentoRestApi;
use App\Order;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Customers;
use App\Aion\Library\FileManager;
use \App\Aion\Helpers\Filehelpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class ExportNewCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->export();
    }


    /**
     *
     */
    public function export()
    {
        Task::Start('ExportNewCustomer');
        Task::updateLastTicket('ExportNewCustomer', 000);
        Log::info("export new  get " );
        $dataCustomers = Customers::where("synced", 0)->get();
        //Arrayhelpers::toStringToLog($dataCustomers,'$dataCustomers - ',__FILE__, __METHOD__, __LINE__);
        $doc = new \DomDocument('1.0',"UTF-8");
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        // create root node
        $root = $doc->createElement('Items');
        $root = $doc->appendChild($root);
        // process one row at a time

        foreach ($dataCustomers as $Item) {
            Arrayhelpers::toStringToEcho("name $Item->name",' NewCustomer ');
            $occ = $doc->createElement('Item');
            $occ = $root->appendChild($occ);
            $child = $doc->createElement('name');
            $child = $occ->appendChild($child);
            $value = $doc->createTextNode($Item->name);
            $value = $child->appendChild($value);
            $child = $doc->createElement('email');
            $child = $occ->appendChild($child);
            $value = $doc->createTextNode($Item->email);
            $value = $child->appendChild($value);
            $childrens['billing'] =$doc->createElement('Billingaddress');
            $childrens['billingoccitem'] = $occ->appendChild($childrens['billing'] );
            $child = $doc->createElement('billing_zip_code');
            $child = $childrens['billingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->billing_zip_code);
            $value = $child->appendChild($value);
            $child = $doc->createElement('billing_city');
            $child = $childrens['billingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->billing_city);
            $value = $child->appendChild($value);
            $child = $doc->createElement('billing_street');
            $child = $childrens['billingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->billing_street);
            $value = $child->appendChild($value);
            $child = $doc->createElement('billing_number');
            $child = $childrens['billingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->billing_number);
            $value = $child->appendChild($value);
            $childrens['shipping'] =$doc->createElement('Shippingaddress');
            $childrens['shippingoccitem'] = $occ->appendChild($childrens['shipping'] );
            $child = $doc->createElement('shipping_zip_code');
            $child = $childrens['shippingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->shipping_zip_code);
            $value = $child->appendChild($value);
            $child = $doc->createElement('shipping_city');
            $child = $childrens['shippingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->shipping_city);
            $value = $child->appendChild($value);
            $child = $doc->createElement('shipping_street');
            $child = $childrens['shippingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->shipping_street);
            $value = $child->appendChild($value);
            $child = $doc->createElement('shipping_number');
            $child = $childrens['shippingoccitem']->appendChild($child);
            $value = $doc->createTextNode($Item->shipping_number);
            $value = $child->appendChild($value);
            $Item->synced = 1;
            $Item->save();
        }

        $xmlstring = $doc->saveXML() ;
        Filehelpers::writetofile(env('EXPORTDIR', 'from/'),'new_user_'.now()->format('YmdHis').'.xml',$xmlstring);
        $filemanager = new  FileManager();
        $filemanager->sendNewCustomerFile();
        Task::updateLastTicket('ExportNewCustomer', 400);
        Task::Stop('ExportNewCustomer',$this->newCount,$this->refreshCount);
    }


}
