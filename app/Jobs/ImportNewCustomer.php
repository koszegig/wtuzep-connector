<?php

namespace App\Jobs;

use App\Aion\Helpers\ArrayHelpers;
use App\Aion\Connect\MagentoRestApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Customers;
use App\Partner;
use App\Task;
use App\Contact;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class ImportNewCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->import();
    }


    /**
     *
     */
    public function import()
    {
        Task::Start('ImportNewCustomer');
        Task::updateLastTicket('ImportNewCustomer', 000);
        Log::info("export new  get ");

        $magento = new MagentoRestApi();
        $magento->getAuthToken();
        $response = $magento->get('/rest/V1/customers/search?searchCriteria[sortOrders][0][field]=email&searchCriteria[sortOrders][0][direction]=asc');
        $result = ['isSuccess' => true,
            'message' => '',
            'data' => '',
            'path' => ''

        ];
        if (!$response->items) {
            $result['message'] = 'Not Custom !';
            echo $result['message'];
            Log::info($result['message']);
            Task::Stop('ImportNewCustomer',$this->newCount,$this->refreshCount,500);
            return;
        }
        $partners = $response->items;
        $XMLDatas = [];
        foreach ($partners as $row) {
            Task::updateLastTicket('ImportNewCustomer', 010);
                Task::updateLastTicket('ImportNewCustomer', 020);
                Arrayhelpers::toStringToEcho(trim($row->firstname) . ' ' . trim($row->lastname), ' - USer');
                $p = [];
                $p['magento_id'] = $row->id;
                $p['name'] = trim($row->firstname) . ' ' . trim($row->lastname);
                $p["email"] = trim($row->email);
                if (property_exists($row, "taxvat") ) {
                    $p["vat_number"] = $row->taxvat;
                }
                foreach ($row->addresses as $address) {
                    if ($address->default_billing ?? 0) {
                        $addressBilling["city"] = trim($address->city ?? '');
                        $addressBilling["street"] = trim($address->street[0] ?? '');
                        $addressBilling["zip_code"] = trim($address->postcode ?? '');
                        $addressBilling["number"] = trim($row->hazszam ?? '');
                        $addressBilling["email"] = trim($row->email);
                    }
                    if ($address->default_shipping ?? 0) {
                        $addressShipping["city"] = trim($address->city ?? '');
                        $addressShipping["street"] = trim($address->street[0] ?? '');
                        $addressShipping["zip_code"] = trim($address->postcode ?? '');
                        $addressShipping["number"] = trim($row->hazszam ?? '');
                        $addressShipping["email"] = trim($row->email);
                    }

                }
                Task::updateLastTicket('ImportNewCustomer', 030);
                if (!isset($addressBilling["city"])) {
                    $addressBilling = $addressShipping;
                }
                Task::updateLastTicket('ImportNewCustomer', 040);
                if (isset($addressBilling["city"])) {
                    $p["billing_city"] = $addressBilling["city"];
                    $p["billing_street"] = $addressBilling["street"];
                    $p["billing_zip_code"] = $addressBilling["zip_code"];
                    $p["billing_number"] = $addressBilling["number"];
                }
                Task::updateLastTicket('ImportNewCustomer', 050);
                if (isset($addressBilling["city"])) {
                    $p["shipping_city"] = $addressShipping["city"];
                    $p["shipping_street"] = $addressShipping["street"];
                    $p["shipping_zip_code"] = $addressShipping["zip_code"];
                    $p["shipping_number"] = $addressShipping["number"];
                }
                Task::updateLastTicket('ImportNewCustomer', 060);
                $newCustomers = Customers::UpdateOrCreate(['email' => $p['email']], $p);
                if ( $newCustomers->wasChanged() ) {
                    $this->refreshCount++;
                    Arrayhelpers::toStringToAll("Marking product dirty.");
                    $newCustomers->synced = 0;
                    $newCustomers->save();
                } else{
                  $this->newCount++;
                }

            Task::updateLastTicket('ImportNewCustomer', 070);
        }
        Task::Stop('ImportNewCustomer',$this->newCount,$this->refreshCount);
    }


}
