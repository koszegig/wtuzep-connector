<?php

namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use App\Attribute;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Product;
use App\AttributeValue;
use App\Category;
use App\Aion\Helpers\ArrayHelpers;
use App\Task;


use Illuminate\Support\Facades\Log;

/**
 * Class ExportProducts
 * @package App\Jobs
 */
class ExportProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     */
    const  SKU_VALIDATOR = '/^[A-Z0-9]+$/';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var array
     */
    protected $_argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var array
     */
    protected $configurableoptions = [];
    /**
     * @var int
     */
    protected $limitCount=0;
    /**
     * @var array
     */
    protected $product =[];
    /**
     * @var
     */
    protected $productClass;
    /**
     * @var
     */
    protected $productClassFields;
    /**
     * @var int
     */
    protected $i= 0;
    /**
     * ExportProducts constructor.
     * @param $_argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->_argumentum =$_argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->export();
    }


    /**
     * @return bool
     */
    public function export(){

        Task::Start('ExportProducts');
        Arrayhelpers::toStringToEcho(  'Starting Exporting products');

        $this->productClass =  new Product();
        $this->productClassFields = $this->productClass->getFields();

        $products = Product::where("synced", 0)->orderBy('syncedorder', 'DESC')->get();

         Log::info("Exporting products");

        if ( ! $products ) {
            Log::info("No products to be synced.");
            return true;
        }

        $magento =  new MagentoRestApi();
        $magento->getAuthToken();
        $magento_taxs = $magento->get("/rest/V1/taxClasses/search?searchCriteria[page_size]=200000");
        $magentoTaxRates = $magento->get("/rest/V1/taxRates/search?searchCriteria[page_size]=200000");
        $magentoTaxRules = $magento->get("/rest/V1/taxRules/search?searchCriteria[page_size]=200000");
        $tax_class_ids = [];
        $taxRateids =[];
        if (  is_object($magentoTaxRates) && property_exists($magentoTaxRates, "message")) {

        } else{
            foreach ($magentoTaxRates->items as $magentoTaxRate) {
                $taxRateids[$magentoTaxRate->id] = $magentoTaxRate->rate;
            }

            $taxRuleids =[];

            foreach ($magentoTaxRules->items as $magentoTaxRule) {
                $product_tax_class_id = 0;
                if (isset($magentoTaxRule->product_tax_class_ids[0])){
                    $product_tax_class_id = $magentoTaxRule->product_tax_class_ids[0];
                }
                foreach ($magentoTaxRule->tax_rate_ids as $taxRateId) {

                    if (isset($taxRateids[$taxRateId])){
                        $taxRateids[$magentoTaxRule->id] =$taxRateids[$taxRateId];
                        $tax_class_ids[$taxRateids[$taxRateId]] = $product_tax_class_id;
                    }
                }

            }
        }

        //dd($tax_class_ids);
        if ($this->options['all'] || $this->options['onlysimple']){
            $products = Product::where("synced", 0)->where("type_id", 'simple')->orderBy('syncedorder', 'DESC')->get();
             $this->simpleProducts($products, $magento);
        }
        if ($this->options['all'] || $this->options['onlyvirtual']) {
            $products = Product::where("synced", 0)->where("type_id", 'virtual')->orderBy('syncedorder', 'DESC')->get();
            $this->virtualProducts($products, $magento);
        }
         if ($this->options['all'] || $this->options['onlyconfigurable']) {
             $products = Product::where("synced", 0)->where("type_id", 'configurable')->orderBy('syncedorder', 'DESC')->get();
             $this->configurableProducts($products, $magento);
         }
        $_count = count($products);
        $this->i=0;

        Task::Stop('ExportProducts',$this->newCount,$this->refreshCount);

        Arrayhelpers::toStringToLog("ExportProducts product catalog Stop");

    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledProductFields($row){
        Arrayhelpers::toStringToAll($row['cikkszam'], 'ExportProducts filledProductFields source ', __FILE__, __METHOD__, __LINE__);
        foreach($this->productClassFields as $field){
            if (!empty($field->getExportFieldName())) {
                $this->product[$field->getExportFieldName()] = $row[$field->getName()];
            }
        }
        $this->product['error'] = '';
        Task::updateLastTicket('ExportProducts', 39);
    }

    /**
     * @param $products
     * @param $magento
     */
    public function simpleProducts($products, $magento){
        foreach ($products as $product) {
            Task::updateLastTicket('ExportProducts', 10);
            $this->i++;
            $this->product = [];
            $this->filledProductFields($product->toArray());


            $magento_product = $magento->get("/rest/V1/products/".urlencode($product->sku));
            Arrayhelpers::toStringToEcho( "ExportProducts simpleProducts Get Product {$product->sku} ");
            Arrayhelpers::toStringToLog( "ExportProducts simpleProducts Get Product {$product->sku} ");
            $url_key = "";
            $magento_id = false;

            if ( is_object($magento_product) && property_exists($magento_product, "id") ) {
                $magento_id = $magento_product->id;
                $url_key = $magento->get_attribute_value_from_product($magento_product, 'url_key');
                Arrayhelpers::toStringToLog("ExportProducts simpleProducts Product {$product->sku} found in magento: {$magento_product->id} with url_key: {$url_key}");
            }
            Arrayhelpers::toStringToEcho( "ExportProducts simpleProducts Get Product {$product->sku}  Catogri");
            Arrayhelpers::toStringToLog( "ExportProducts simpleProducts Get Product {$product->sku}  Catogri");
            $category_magento_ids = [];
            $category_links = [];
            $position = 0;
            foreach ($product->categories as $category) {
                Task::updateLastTicket('ExportProducts', 120);
                if ( $category->category_magento_id ) {
                    if ( ! in_array($category->category_magento_id, $category_magento_ids) ) {
                        $category_magento_ids[] = $category->category_magento_id;
                        $category_links[] = [
                            "position" => 0,
                            "category_id" => $category->category_magento_id
                        ];
                        $position++;
                    }
                }
            }
            // FIXME: kategoria linket nem veszi ki
            Arrayhelpers::toStringToEcho( "ExportProducts simpleProducts Get Product {$product->sku}  post valogat");
            $post = [
                "product"=> [
                    "sku"       =>    $this->product['sku'],
                    "name"      =>    $this->product['name'],
                    "attribute_set_id"=> 4,
                    //"price"     =>   $product->listprice/10000,
                    "price"     =>   $this->product['price'],
                    "status"    => 1,
                    "visibility"=> 4,
                    "type_id"   => 'simple',
                    "weight" =>  0,
                    //"type_id"   => 'configurable',

                    "extension_attributes" => [
                        "website_ids" => [ 1 ],

                        "stock_item" => [
                            // "qty" => $product->total_qty,
                            "qty" => $this->product['qty'],
                            "is_in_stock" => 1,
                            //"quantity_in_stock" => true,
                            "use_config_manage_stock" => true,
                            //"backorders" => 1,
                        ],
                        "category_links" => $category_links
                    ],
                    "custom_attributes" => [
                        [
                            "attribute_code" => "description",
                            "value" =>  $this->product['description']
                        ],
                    ]
                ]
            ];
            Arrayhelpers::toStringToEcho( "ExportProducts simpleProducts Get Product {$this->product['sku']}  multiple_skus");
          /*  if (  $this->product['active'] == 0 ) {
                $post["product"]["visibility"] = 3;
            }

            if (  $this->product['price'] == 0 ) {
                $post["product"]["visibility"] = 3;
            }
            if (  $this->product['price'] == 1 ) {
                $post["product"]["visibility"] = 3;
            }
            if (  $this->product['enabled'] == 0 ) {
                $post["product"]["status"] = 2;
            }*/

            $_count = count($product->attributevalues);
            Arrayhelpers::toStringToEcho( "ExportProducts simpleProducts Get Product {$this->product['sku']}  attributevalues {$_count} ");
            //Arrayhelpers::toStringToLog($product->attributevalues, ' ExportProducts $product->attributevalues  count {$_count}' ,__FILE__, __METHOD__, __LINE__ );
            $i =0;
            foreach ( $product->attributevalues as $attribute ) {
                $i ++;
                /// Arrayhelpers::toStringToAll( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                Task::updateLastTicket('ExportProducts', 100);
                //Arrayhelpers::toStringToEcho( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                // if ($attribute->attribute->synced) {
                if ($attribute->type == 'text') {
                   // if($attribute->attribute->code != 'MPN') {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->value
                    ];
                   /* } else {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => $attribute->attribute->code,
                            "value" => str_replace('-','',$attribute->value)
                        ];
                    }*/

                } else {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->magento_id
                    ];

                }

                // Arrayhelpers::toStringToLog($post["product"]["custom_attributes"], ' ExportProducts export Product - $post[product][custom_attributes');
                // }
            }
            Arrayhelpers::toStringToEcho( "Put/Post  simpleProducts Product {$product->sku}  $magento_id");
            Arrayhelpers::toLog( "Put/Post simpleProducts Product {$product->sku}  $magento_id");
            Task::updateLastTicket('ExportProducts', 110);
            if ( $magento_id || $product->magento_id ) {
                Log::info("ExportProducts simpleProducts Updating product");
                $this->refreshCount++;
                Arrayhelpers::toStringToAll($post, 'ExportProducts export Update product- $post',__FILE__, __METHOD__, __LINE__ );
                $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                Arrayhelpers::toStringToAll($response, 'ExportProducts export Update  $response',__FILE__, __METHOD__, __LINE__ );
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'. time()
                        ];
                    }
                    Task::updateLastTicket('ExportProducts', 40);
                    Arrayhelpers::toStringToLog("ExportProducts simpleProducts Resending with new urlkey");
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("ExportProducts simpleProducts New urlkey is {$new_url_key}");
                }

                if (  is_object($response) && property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                }
            } else {
                Arrayhelpers::toStringToLog("ExportProducts Adding new product to magento");

                $existing_attribute_codes = [];
                //   Arrayhelpers::toStringToAll($post, 'export post new product- $post',__FILE__, __METHOD__, __LINE__ );
                $this->newCount++;
                Task::updateLastTicket('ExportProducts', 50);
                $response = $magento->post("/rest/V1/products", $post);
                //Arrayhelpers::toStringToAll($response, 'export post new product- $response',__FILE__, __METHOD__, __LINE__ );
                $url_key = $magento->createUrlKey($this->product['name'], []);
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'.  time()
                        ];
                    }
                    //Log::info("Resending with new urlkey");
                    Arrayhelpers::toStringToLog("ExportProducts simpleProducts Resending with new urlkey" ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                    Arrayhelpers::toStringToAll($post, 'ExportProducts  simpleProductsexport update new product- $post',__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    //Arrayhelpers::toStringToAll( $response, " Product Put ");

                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("New urlkey is {$new_url_key}");
                }

                if ( is_object($response) &&  property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                    $product->error = $response->message;
                    $product->synced = 2;
                    $product->save();
                    continue;
                }
                foreach ($response->custom_attributes as $existing_attributes) {
                    $existing_attribute_codes[] = trim($existing_attributes->attribute_code);
                }

                $missing = 0;
                foreach ( $post["product"]["custom_attributes"] as $product_attributes) {
                    if ( ! in_array($product_attributes['attribute_code'], $existing_attribute_codes) ) {
                        $missing = 1;
                    }
                }
                if ( $missing ) {
                    Log::info("Resending product to properly assign attributes");
                    Arrayhelpers::toStringToLog($post,'ExportProducts  Resending product to properly assign attributes $post - ' ,__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->post("/rest/V1/products", $post);
                }

            } // insert

            if ( is_object($response) && property_exists($response,"id") && $response->id > 0) {
                Log::info("Processed ". $response->sku . " product_id: " . $product->id . " magento_id: ". $response->id);

                Arrayhelpers::toStringToLog($response ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                $product->magento_id = $response->id;
                $product->error ='';
                $product->synced = 1;
                $product->save();
            } else {
                //Log::error("Error during push. sku: ". $product->sku );
                Arrayhelpers::toErrorLog("Error during push. sku: ". $product->sku );

                $product->synced = 2;
                $product->save();
            }
            //Arrayhelpers::toStringToAll("{$_count}/{$i}",'ExportProducts');
            Task::updateLastTicket('ExportProducts', 400);
            $this->limitCount++;
            if( ($this->limitCount>$this->_argumentum['limit'] && $this->_argumentum['limit'] !=0)  || Task::isStopped('ExportProducts') ) {
                Task::Stop('ExportProducts',$this->newCount,$this->refreshCount);
                break;
            }
        }

    }

    /**
     * @param $products
     * @param $magento
     */
    public function virtualProducts($products, $magento){
        foreach ($products as $product) {
            Task::updateLastTicket('ExportProducts', 10);
            $this->i++;
            $this->product = [];
            $this->filledProductFields($product->toArray());


            $magento_product = $magento->get("/rest/V1/products/".urlencode($product->sku));
            Arrayhelpers::toStringToEcho( "ExportProducts virtualProducts Get Product {$product->sku} ");
            Arrayhelpers::toStringToLog( "ExportProducts virtualProducts Get Product {$product->sku} ");
            $url_key = "";
            $magento_id = false;

            if ( is_object($magento_product) && property_exists($magento_product, "id") ) {
                $magento_id = $magento_product->id;
                $url_key = $magento->get_attribute_value_from_product($magento_product, 'url_key');
                Arrayhelpers::toStringToLog("ExportProducts virtualProducts Product {$product->sku} found in magento: {$magento_product->id} with url_key: {$url_key}");
            }
            Arrayhelpers::toStringToEcho( "ExportProducts virtualProducts Get Product {$product->sku}  Catogri");
            Arrayhelpers::toStringToLog( "ExportProducts virtualProducts Get Product {$product->sku}  Catogri");
            $category_magento_ids = [];
            $category_links = [];
            $position = 0;
            foreach ($product->categories as $category) {
                Task::updateLastTicket('ExportProducts', 120);
                if ( $category->category_magento_id ) {
                    if ( ! in_array($category->category_magento_id, $category_magento_ids) ) {
                        $category_magento_ids[] = $category->category_magento_id;
                        $category_links[] = [
                            "position" => 0,
                            "category_id" => $category->category_magento_id
                        ];
                        $position++;
                    }
                }
            }
            // FIXME: kategoria linket nem veszi ki
            Arrayhelpers::toStringToEcho( "ExportProducts virtualProducts Get Product {$product->sku}  post valogat");
            $post = [
                "product"=> [
                    "sku"       =>    $this->product['sku'],
                    "name"      =>    $this->product['name'],
                    "attribute_set_id"=> 4,
                    //"price"     =>   $product->listprice/10000,
                    "price"     =>   $this->product['price'],
                    "status"    => 1,
                    "visibility"=> 1,
                    "weight" =>  0,
                    "type_id"   => 'virtual',
                    //"type_id"   => 'configurable',

                    "extension_attributes" => [
                        "website_ids" => [ 1 ],

                        "stock_item" => [
                            // "qty" => $product->total_qty,
                            "qty" => $this->product['qty'],
                            "is_in_stock" => 1,
                            //"quantity_in_stock" => true,
                            "use_config_manage_stock" => true,
                            //"backorders" => 1,
                        ],
                        "category_links" => $category_links
                    ],
                    "custom_attributes" => [
                        [
                            "attribute_code" => "description",
                            "value" =>  $this->product['description']
                        ],
//                        [
//                            "attribute_code" => "tax_class_id",
//                            "value" => $product->tax_class_id
//                        ],
                    ]
                ]
            ];
            Arrayhelpers::toStringToEcho( "ExportProducts virtual Products Get Product {$this->product['sku']}  multiple_skus");
            /*if (  $this->product['active'] == 0 ) {
                $post["product"]["visibility"] = 3;
            }

            if (  $this->product['price'] == 0 ) {
                $post["product"]["visibility"] = 3;
            }
            if (  $this->product['price'] == 1 ) {
                $post["product"]["visibility"] = 3;
            }
            if (  $this->product['enabled'] == 0 ) {
                $post["product"]["status"] = 2;
            }*/
            $_count = count($product->attributevalues);
            Arrayhelpers::toStringToEcho( "ExportProducts virtualProducts Get Product {$this->product['sku']}  attributevalues {$_count} ");
            //Arrayhelpers::toStringToLog($product->attributevalues, ' ExportProducts $product->attributevalues  count {$_count}' ,__FILE__, __METHOD__, __LINE__ );
            $i =0;
            foreach ( $product->attributevalues as $attribute ) {
                $i ++;
                /// Arrayhelpers::toStringToAll( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                Task::updateLastTicket('ExportProducts', 100);
                //Arrayhelpers::toStringToEcho( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                // if ($attribute->attribute->synced) {
                if ($attribute->type == 'text') {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->value
                    ];
                } else {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->magento_id
                    ];

                }


                // }
            }
            Arrayhelpers::toStringToLog($post["product"]["custom_attributes"], ' ExportProducts export Product - $post[product][custom_attributes');
            Arrayhelpers::toStringToEcho( "Put/Post virtualProducts Product {$product->sku}  $magento_id");
            Arrayhelpers::toLog( "Put/Post virtualProducts Product {$product->sku}  $magento_id");
            Task::updateLastTicket('ExportProducts', 110);

            if ( $magento_id || $product->magento_id ) {
                Log::info("ExportProducts Updating product");
                $this->refreshCount++;
                Arrayhelpers::toStringToAll($post, 'ExportProducts export Update product- $post',__FILE__, __METHOD__, __LINE__ );
                $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                Arrayhelpers::toStringToAll($response, 'ExportProducts export Update  $response',__FILE__, __METHOD__, __LINE__ );
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'. time()
                        ];
                    }
                    Task::updateLastTicket('ExportProducts', 40);
                    Arrayhelpers::toStringToLog("ExportProducts virtualProducts Resending with new urlkey");
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("ExportProducts New urlkey is {$new_url_key}");
                }

                if (  is_object($response) && property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                }
            } else {
                Arrayhelpers::toStringToLog("ExportProducts Adding new product to magento");

                $existing_attribute_codes = [];
                //   Arrayhelpers::toStringToAll($post, 'export post new product- $post',__FILE__, __METHOD__, __LINE__ );
                $this->newCount++;
                Task::updateLastTicket('ExportProducts', 50);
                $response = $magento->post("/rest/V1/products", $post);
                Arrayhelpers::toStringToAll($response, 'export post new product- $response',__FILE__, __METHOD__, __LINE__ );
                $url_key = $magento->createUrlKey($this->product['name'], []);
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'.  time()
                        ];
                    }
                    //Log::info("Resending with new urlkey");
                    Arrayhelpers::toStringToLog("ExportProducts virtualProducts Resending with new urlkey" ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                    Arrayhelpers::toStringToAll($post, 'ExportProducts virtualProducts export update new product- $post',__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    //Arrayhelpers::toStringToAll( $response, " Product Put ");

                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("New urlkey is {$new_url_key}");
                }

                if ( is_object($response) &&  property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                    $product->error = $response->message;
                    $product->synced = 2;
                    $product->save();
                    continue;
                }
                foreach ($response->custom_attributes as $existing_attributes) {
                    $existing_attribute_codes[] = trim($existing_attributes->attribute_code);
                }

                $missing = 0;
                foreach ( $post["product"]["custom_attributes"] as $product_attributes) {
                    if ( ! in_array($product_attributes['attribute_code'], $existing_attribute_codes) ) {
                        $missing = 1;
                    }
                }
                if ( $missing ) {
                    Log::info("Resending product to properly assign attributes");
                    Arrayhelpers::toStringToLog($post,'ExportProducts virtualProducts  Resending product to properly assign attributes $post - ' ,__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->post("/rest/V1/products", $post);
                }

            } // insert

            if ( is_object($response) && property_exists($response,"id") && $response->id > 0) {
                Log::info("Processed ". $response->sku . " product_id: " . $product->id . " magento_id: ". $response->id);

                Arrayhelpers::toStringToLog($response ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                $product->magento_id = $response->id;
                $product->error ='';
                $product->synced = 1;
                $product->save();
            } else {
                //Log::error("Error during push. sku: ". $product->sku );
                Arrayhelpers::toErrorLog("Error during push. sku: ". $product->sku );

                $product->synced = 2;
                $product->save();
            }
            //Arrayhelpers::toStringToAll("{$_count}/{$i}",'ExportProducts');
            Task::updateLastTicket('ExportProducts', 400);
            $this->limitCount++;
            if( ($this->limitCount>$this->_argumentum['limit'] && $this->_argumentum['limit'] !=0)  || Task::isStopped('ExportProducts') ) {
                Task::Stop('ExportProducts',$this->newCount,$this->refreshCount);
                break;
            }
        }
    }

    /**
     * @param $products
     * @param $magento
     */
    public function configurableProducts($products, $magento){
        foreach ($products as $product) {
            Task::updateLastTicket('ExportProducts', 10);
            $this->i++;
            $this->product = [];
            $this->filledProductFields($product->toArray());


            $magento_product = $magento->get("/rest/V1/products/".urlencode($product->sku));
            Arrayhelpers::toStringToEcho( "ExportProducts configurable Get Product {$product->sku} ");
            Arrayhelpers::toStringToLog( "ExportProducts configurable Get Product {$product->sku} ");
            $url_key = "";
            $magento_id = false;

            if ( is_object($magento_product) && property_exists($magento_product, "id") ) {
                $magento_id = $magento_product->id;
                $url_key = $magento->get_attribute_value_from_product($magento_product, 'url_key');
                Arrayhelpers::toStringToLog("ExportProducts configurable Product {$product->sku} found in magento: {$magento_product->id} with url_key: {$url_key}");
            }
            Arrayhelpers::toStringToEcho( "ExportProducts configurable Get Product {$product->sku}  Catogri");
            Arrayhelpers::toStringToLog( "ExportProducts configurable Get Product {$product->sku}  Catogri");
            $category_magento_ids = [];
            $category_links = [];
            $position = 0;
            foreach ($product->categories as $category) {
                Task::updateLastTicket('ExportProducts', 120);
                if ( $category->category_magento_id ) {
                    if ( ! in_array($category->category_magento_id, $category_magento_ids) ) {
                        $category_magento_ids[] = $category->category_magento_id;
                        $category_links[] = [
                            "position" => 0,
                            "category_id" => $category->category_magento_id
                        ];
                        $position++;
                    }
                }
            }
            // FIXME: kategoria linket nem veszi ki
            $chieldproducts = Product::where("parent_id", $product['id'])->where("type_id", 'virtual')->orderBy('syncedorder', 'DESC')->get();
            $price= 0;
            if ($chieldproducts) {
                foreach ($chieldproducts as $chieldproduct) {
                    if ($price == 0) {
                        $price=$chieldproduct->price;
                        //dd();
                    }
                    //  $post["product"]["extension_attributes"]["configurable_product_links"][] = $chieldproduct['magento_id'];
                }
            }
            Arrayhelpers::toStringToEcho( "ExportProducts configurable Get Product {$product->sku}  post valogat");
            $post = [
                "product"=> [
                    "sku"       =>    $this->product['sku'],
                    "name"      =>    $this->product['name'],
                    "attribute_set_id"=> 4,
                    //"price"     =>   $product->listprice/10000,
                    //"price"     =>   $this->product['price'],
                    "price"     =>   $price,
                    "status"    => 1,
                    "weight" =>  0,
                    "visibility"=> 4,
                    "type_id"   => 'configurable',

                    "extension_attributes" => [
                        "website_ids" => [ 1 ],

                        "stock_item" => [
                            // "qty" => $product->total_qty,
                            "qty" => 1,
                            "is_in_stock" => 1,
                            //"quantity_in_stock" => true,
                            "use_config_manage_stock" => true,
                            //"backorders" => 1,
                        ],
                        "category_links" => $category_links,
                        "configurable_product_links" => [],
                        "configurable_product_options" => [],

                    ],
                    "custom_attributes" => [
                        [
                            "attribute_code" => "description",
                            "value" =>  $this->product['description']
                        ],
//                        [
//                            "attribute_code" => "tax_class_id",
//                            "value" => $product->tax_class_id
//                        ],
                    ]
                ]
            ];
            $post["product"]["extension_attributes"]["configurable_product_links"] = [];
            $post['product']['custom_attributes'][] = [
                "attribute_code" => "sw_featured",
                "value" =>  1
            ];

            /*$multipleSKUsError ='';
            if ( $product->multiple_skus ) {


                if ($this->isValidmultiple_skus($product->multiple_skus)){
                    $post['product']['custom_attributes'][] = [
                        "attribute_code" => "multiple_skus",
                        "value" =>  $product->multiple_skus
                    ];
                } else{
                    $multipleSKUsError ="Product {$product->sku}  multiple_skus hibás {$product->multiple_skus} ";
                    $product->error =$multipleSKUsError;
                  Arrayhelpers::toStringToAll( " $multipleSKUsError",'ExportProducts');

                }
            }*/

            //  Arrayhelpers::toStringToLog($post, 'export - $post');
//            dump($post);


            $_count = count($product->attributevalues);
            Arrayhelpers::toStringToEcho( "ExportProducts configurable Get Product {$this->product['sku']}  attributevalues {$_count} ");
            //Arrayhelpers::toStringToLog($product->attributevalues, ' ExportProducts $product->attributevalues  count {$_count}' ,__FILE__, __METHOD__, __LINE__ );
            $i =0;
            foreach ( $product->attributevalues as $attribute ) {
                $i ++;
                /// Arrayhelpers::toStringToAll( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                Task::updateLastTicket('ExportProducts', 100);
                //Arrayhelpers::toStringToEcho( "ExportProducts Get Product {$product->sku}  attributevalues {$_count}/{$i} ");
                // if ($attribute->attribute->synced) {
                if ($attribute->type == 'text') {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->value
                    ];
                } else {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute->attribute->code,
                        "value" => $attribute->magento_id
                    ];

                }

                // Arrayhelpers::toStringToLog($post["product"]["custom_attributes"], ' ExportProducts export Product - $post[product][custom_attributes');
                // }
            }

            $post = $this->configuraleProductsAttribute($chieldproducts,$post);
            Arrayhelpers::toStringToEcho( "Put/Post configurable Product {$product->sku}  $magento_id");
            Arrayhelpers::toLog( "Put/Post Product {$product->sku}  $magento_id");
            Task::updateLastTicket('ExportProducts', 110);
            if ( $magento_id || $product->magento_id ) {
                Log::info("ExportProducts Updating product");
                $this->refreshCount++;
                //Arrayhelpers::toStringToAll($post, 'ExportProducts export Update product- $post',__FILE__, __METHOD__, __LINE__ );
                $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                Arrayhelpers::toStringToAll($response, 'ExportProducts export Update  $response',__FILE__, __METHOD__, __LINE__ );
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'. time()
                        ];
                    }
                    Task::updateLastTicket('ExportProducts', 40);
                    Arrayhelpers::toStringToLog("ExportProducts configurable Resending with new urlkey");
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("ExportProducts configurable New urlkey is {$new_url_key}");
                }

                if (  is_object($response) && property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                }
                if ($chieldproducts) {
                    $this->chieldproductsProducts($chieldproducts,$post,$magento);
                }
            } else {
                Arrayhelpers::toStringToLog("ExportProducts Adding new product to magento");

                $existing_attribute_codes = [];
                //   Arrayhelpers::toStringToAll($post, 'export post new product- $post',__FILE__, __METHOD__, __LINE__ );
                $this->newCount++;
                Task::updateLastTicket('ExportProducts', 50);
                $response = $magento->post("/rest/V1/products", $post);
                Arrayhelpers::toStringToAll($response, 'export post new product- $response',__FILE__, __METHOD__, __LINE__ );
                $url_key = $magento->createUrlKey($this->product['name'], []);
                if ( is_object($response) &&  property_exists($response, "message")) {
                    Log::info($response->message);
                    if ( $response->message == "URL key for specified store already exists." ) {
                        $post["product"]["custom_attributes"][] = [
                            "attribute_code" => "url_key",
                            "value" => $url_key . '-'.  time()
                        ];
                    }
                    //Log::info("Resending with new urlkey");
                    Arrayhelpers::toStringToLog("ExportProducts Resending with new urlkey" ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                    Arrayhelpers::toStringToAll($post, 'ExportProducts export update new product- $post',__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->put("/rest/V1/products/" . urlencode($this->product['sku']), $post);
                    Arrayhelpers::toStringToAll( $response, " Product Put ");

                    $new_url_key = $magento->get_attribute_value_from_product($response, 'url_key');
                    Arrayhelpers::toStringToLog("New urlkey is {$new_url_key}");
                }

                if ( is_object($response) &&  property_exists($response, "message")) {
                    Arrayhelpers::toErrorLog($response->message);
                    //$product->error = $response->message;
                    $product->synced = 2;
                    $product->save();
                    continue;
                }
                foreach ($response->custom_attributes as $existing_attributes) {
                    $existing_attribute_codes[] = trim($existing_attributes->attribute_code);
                }

                $missing = 0;
                foreach ( $post["product"]["custom_attributes"] as $product_attributes) {
                    if ( ! in_array($product_attributes['attribute_code'], $existing_attribute_codes) ) {
                        $missing = 1;
                    }
                }
                if ( $missing ) {
                    Log::info("Resending product to properly assign attributes");
                    Arrayhelpers::toStringToLog($post,'ExportProducts  Resending product to properly assign attributes $post - ' ,__FILE__, __METHOD__, __LINE__ );
                    $response = $magento->post("/rest/V1/products", $post);
                }
                if ($chieldproducts) {
                    $this->chieldproductsProducts($chieldproducts,$post,$magento);
                }


            } // insert

            if ( is_object($response) && property_exists($response,"id") && $response->id > 0) {
                Log::info("Processed ". $response->sku . " product_id: " . $product->id . " magento_id: ". $response->id);

                Arrayhelpers::toStringToLog($response ,'ExportProducts ',__FILE__, __METHOD__, __LINE__ );
                $product->magento_id = $response->id;
                $product->error ='';
                $product->synced = 1;
                $product->save();
            } else {
                //Log::error("Error during push. sku: ". $product->sku );
                Arrayhelpers::toErrorLog("Error during push. sku: ". $product->sku );

                $product->synced = 2;
                $product->save();
            }
            //Arrayhelpers::toStringToAll("{$_count}/{$i}",'ExportProducts');
            Task::updateLastTicket('ExportProducts', 400);
            $this->limitCount++;
            if( ($this->limitCount>$this->_argumentum['limit'] && $this->_argumentum['limit'] !=0)  || Task::isStopped('ExportProducts') ) {
                Task::Stop('ExportProducts',$this->newCount,$this->refreshCount);
                break;
            }
        }
    }

    /**
     * @param $chieldproducts
     * @param $post
     * @param $magento
     * @return mixed
     */
    public function chieldproductsProducts($chieldproducts, $post, $magento){
        $configurable_product_options_values = [];
        $this->configurableoptions = [];
        $valiuesIndex = [];
        $skues = [];
        foreach ($chieldproducts as $chieldproduct) {
            $skues[$chieldproduct['cikkszam']] = ['childSku' => $chieldproduct['cikkszam']];
            foreach ( $chieldproduct->attributevalues as $attribute ) {
                if ($attribute['type'] =='select') {
                    if (!empty($attribute['magento_id'])) {
                        $configurable_product_options_values[$attribute['attribute_id']][$attribute['magento_id']] = ["label" => $attribute['label'],
                            "value" => $attribute['value'] ]  ;
                        $valiuesIndex [$attribute['attribute_id']][] = ["value_index" => $attribute['magento_id']];
                    }
                }
                Task::updateLastTicket('ExportProducts', 100);
            }
          //  $post["product"]["extension_attributes"]["configurable_product_links"][] = $chieldproduct['magento_id'];
        }
        $position = 0;
        $index = 0;


        foreach ($configurable_product_options_values as $attribute_id =>  $configurable_product_options_value) {
            if (count($configurable_product_options_value) > 1) {
                $this->configurableoptions = $this->setemptyconfigurableoptions();
                $attributes = Attribute::where('id',$attribute_id)->first();
                    $valiueIndex[] = ['value_index' => $index];
                        $this->configurableoptions->option->attribute_id = $attributes->magento_id;
                        $this->configurableoptions->option->label = $attributes->name;
                        $this->configurableoptions->option->position =$position;
                        $this->configurableoptions->option->values = $valiueIndex;

                Arrayhelpers::toStringToAll( $this->configurableoptions, " configurable-products post ");
                $response = $magento->post("/rest/V1/configurable-products/" . urlencode($this->product['sku']).'/options', $this->configurableoptions);
                /*$configurableoptionsProduct = $this->setemptyProductconfigurableoptions();
                $configurableoptionsProduct->id = $response;

                $configurableoptionsProduct->attribute_id = $attributes->magento_id;
                $configurableoptionsProduct->label = $attributes->name;;
                $configurableoptionsProduct->position = $position;;
                $configurableoptionsProduct->values = $valiuesIndex [$attribute_id];;
                $post["product"]["extension_attributes"]["configurable_product_options"][] = $configurableoptionsProduct;*/
                $index++;
                $position++;
            }
        }
        foreach ($skues as $sku => $item){
            dump(urlencode($this->product['sku']).'/child');
            $response = $magento->post("/rest/V1/configurable-products/" . urlencode($this->product['sku']).'/child', $item);
            dump($response);
        }

       return $post;
    }

    /**
     * configurale termék atributuma
     * @param $chieldproducts
     * @param $post
     * @return mixed
     */
    public function configuraleProductsAttribute($chieldproducts, $post){
        $configurable_product_attribute_values = [];
        foreach ($chieldproducts as $chieldproduct) {
            foreach ( $chieldproduct->attributevalues as $attribute ) {
                        $configurable_product_attribute_values[$attribute['attribute_id']][$attribute['magento_id']] = ["label" => $attribute['label'],
                            "code" => $attribute->attribute->code,
                            "value" => $attribute['value'],
                            "magento_id" => $attribute['magento_id'],
                            "type" => $attribute['type']]  ;

            }
            //  $post["product"]["extension_attributes"]["configurable_product_links"][] = $chieldproduct['magento_id'];
        }
        foreach ($configurable_product_attribute_values as $attribute_id =>  $configurable_product_attribute_value) {
            if (count($configurable_product_attribute_value) == 1) {
                $product_attribute_values[] = array_values($configurable_product_attribute_value);
                $attribute = array_values($configurable_product_attribute_value);
                if ($attribute[0]['type'] == 'text') {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute[0]['code'],
                        "value" => $attribute[0]['value']
                    ];
                } else {
                    $post["product"]["custom_attributes"][] = [
                        "attribute_code" => $attribute[0]['code'],
                        "value" => $attribute[0]['magento_id']
                    ];

                }
            }
        }
        return $post;
    }

    /**
     * @return mixed
     */
    public function setemptyconfigurableoptions()
    {
                return json_decode('{
            "option": {
                "attribute_id": "",
                "label": "",
                "position": 0,
                "is_use_default": true,
                "values": []
            }
        }');
    }

    /**
     * @return mixed
     */
    public function setemptyProductconfigurableoptions(){
        return json_decode(' {
        "id": "",
        "attribute_id": "",
        "label": "",
        "position": 0,
        "values": [        ]
    }');
    }
}

