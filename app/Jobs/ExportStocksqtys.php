<?php


namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\StocksQty;
use App\Product;
use App\Aion\Helpers\ArrayHelpers;
use Illuminate\Support\Facades\Log;
use App\Task;

class ExportStocksqtys implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $stockQtyClass;
    /**
     * @var
     */
    protected $stockQtyClassFields;
    /**
     * @var array
     */
    protected $stockQty =[];
    protected $sourceitems =[];
    /**
     * @var
     */

    protected $productPutNumber =0;
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->export();
    }


    /**
     * @return bool
     */
    public function export()
    {
        Task::Start('ExportStocksqtys');
        $this->stockQtyClass =  new StocksQty();
        $this->stockQtyClassFields = $this->stockQtyClass->getFields();
        Arrayhelpers::toStringToEcho('Starting Exporting StocksQty');
        $stocksQTYs = StocksQty::where("stocksqty.synced", 0)->where('products.magento_id', '<>', 0)
            ->leftJoin('products', 'stocksqty.productid', '=', 'products.id')
            ->orderBy('.stocksqty.productid')->select('stocksqty.*')->get();

        Log::info("Exporting Exporting StocksQty");
        if (count($stocksQTYs) == 0) {
            Task::Stop('ExportStocksqtys',$this->newCount,$this->refreshCount,500);
            Log::info("No StocksQty to be synced.");
            return true;
        }

        $magento = new MagentoRestApi();
        $magento->getAuthToken();
        Task::updateLastTicket('ExportStocksqtys', 10);
        foreach ($stocksQTYs as $stocksQTY) {
            $this->filledstocksQTYFields($stocksQTY);
            if ( $this->stockQty['source_code'] != '' ) {
            $this->sourceitems[] = $this->stockQty;
            }
        }
        foreach ( array_chunk($this->sourceitems, 100) as $item) {
            $pos =[];
            //$pos['sourceItems']=$this->sourceitems;
            $pos['sourceItems']=$item;
            $response = $magento->post("rest/all/V1/inventory/source-items", $pos);
            Task::updateLastTicket('ExportProducts', 100);
            if ( is_object($response) &&  property_exists($response, "message")) {
                Arrayhelpers::toErrorLog($response->message, 'ExportStocksqtys error ', __FILE__, __METHOD__, __LINE__);
            } else {

                $Skus = [];
                foreach ($item as $qty) {
                    $Skus[$qty['sku']] =$qty['sku'];
                }
                StocksQty::whereIn('sku',array_values($Skus))->update(['synced' => 1]);
                Arrayhelpers::toStringToAll(array_values($Skus), 'ExportStocksqtys Update ', __FILE__, __METHOD__, __LINE__);
            }
        }

        $this->productPutNumber++;
        Task::updateLastTicket('ExportProducts', 400);

        Task::Stop('ExportStocksqtys',$this->newCount,$this->refreshCount);
        Log::info("Export StocksQty end.");
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledstocksQTYFields($row){

        $stockQty = [];
        $this->stockQty =[];
        foreach($this->stockQtyClassFields as $field){
            if (!empty($field->getExportFieldName())) {
                $stockQty[$field->getExportFieldName()] = $row[$field->getName()];
            }
        }
        $stockQty['status'] = 1;
        Arrayhelpers::toStringToAll($stockQty['sku'], 'ExportStocksqtys filledstocksQTYFields source ', __FILE__, __METHOD__, __LINE__);
        $this->stockQty=$stockQty;
        Task::updateLastTicket('ExportStocksqtys', 39);
    }
    public function setsourceitems()
    {
        return json_decode('{
  "sourceItems": [
    { "sku": "15001171",
      "source_code": "westend-budapest",
      "quantity": 3,
      "status": 1,
      "extension_attributes": {}
    }
  ]
}');
    }
}
