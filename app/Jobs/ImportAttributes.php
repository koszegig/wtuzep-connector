<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;
use App\Task;
use Log;
use DB;
use App\Attribute;
use App\AttributeValue;
use App\Models\Vectory\CCProductProperty;
use App\Models\Vectory\CCPropertyAssignments;


use App\Product;

/**
 * Class ImportAttributes
 * @package App\Jobs
 */
class ImportAttributes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    protected $argumentum;
    protected $options;
    /**
     * @var
     */
    protected $limitCount=0;

    protected $attribute =[];

    protected $attributeClass;
    protected $attributeClassFields;

    protected $attributeValues =[];
    protected $attributeValueClass;
    protected $attributeValueClassFields;

    /**
     * ExportAttributes constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum = [], $options = [])
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Task::Start('ImportAttributes');
        $this->attributeClass =  new Attribute();
        $this->attributeClassFields = $this->attributeClass->getFields();


        $this->attributeValueClass =  new AttributeValue();
        $this->attributeValueClassFields = $this->attributeValueClass->getFields();


        Task::updateLastTicket('ImportAttributes', 10);
        $CCProductProperty = new CCProductProperty();
        $attributes = $CCProductProperty->getFull();
        dump($attributes);
        foreach ($attributes as $row) {
        Log::info("Fetching attributes");
            $this->attribute = [];
            $this->attribute =  $this->attributeClass->filledImportFields($row);
            $sqlAttribute = Attribute::firstOrCreate(['code' => $this->attribute['code'] ], $this->attribute);
        }
        $CCPropertyAssignments = new CCPropertyAssignments;
        $attributeValues = $CCPropertyAssignments->getFull();
        dd($attributeValues);
        foreach ($attributeValues as $row) {
            $this->attributeValues = $this->attributeValueClass->filledImportFields($row);
            AttributeValue::UpdateOrCreate(['type' => $this->attributeValues['type'], 'erp_id' => $this->attributeValues['erp_id'], 'attribute_id' => $this->attributeValues['attribute_id']], $this->attributeValues);
        }
        Task::Stop('ImportAttributes',$this->newCount,$this->refreshCount);
    }



    }
