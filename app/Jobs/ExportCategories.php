<?php

namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use App\Picture;
use App\Product;
use App\Category;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Aion\Helpers\ArrayHelpers;
use Log;


/**
 * Class ExportCategories
 * @package App\Jobs
 */
class ExportCategories implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var int
     */
    protected $limitCount=0;
    /**
     * @var array
     */
    public $urlkeys = array();
    /**
     * @var array
     */
    public $existing_categories = array();

    /**
     * @var string
     */
    protected $task; // <-- Here
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($argumentum,$options,$task = "")
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->task) {
            case "media":
                $this->set_display_settings();
                $this->set_display_settings_from_sub_products();
                break;
            case "from_products":
                $this->set_display_settings_from_sub_products();
                break;
            case "from_csv":
                $this->set_display_settings();
                break;
            case "disable_empty_categories":
                $this->disable_empty_categories();
                break;
            default:
                $this->export();
        }

    }

    /**
     * Collect category ids from existing tree
     * @param $categories
     * @return array
     */
    public function walk_categories($categories) {
        foreach ($categories  as $category) {
            $this->existing_categories[] = $category->id;
            if ( property_exists($category,"children_data") ) {
                $this->walk_categories($category->children_data);
            }
        }
        return $this->existing_categories;
    }

    /**
     *
     */
    public function set_display_settings_from_sub_products()
    {
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();


        Log::info("Fetching existing categories and collecting URL Keys");

        $existing_categories = $magento->get("/rest/V1/categories");
        $category_ids = $this->walk_categories($existing_categories->children_data);

        foreach ($category_ids as $category_id) {
            $image = false;
            $category = $magento->get("/rest/V1/categories/" . $category_id);
            if ($category) {
                foreach ($category->custom_attributes as $index => $attribute) {
                    if ($attribute->attribute_code == 'image') {
                        $image = $attribute->value;
                    }
                }
                if ($image == false) {
                    Log::info("Fetching products under magento category id {$category_id}");

                    $products = $magento->get("/rest/V1/categories/" . $category_id . "/products");
                    foreach ($products as $product) {
                        $product = Product::where("sku", $product->sku)->get()->first();
                        if ( $product ) {
                            $picture = Picture::where("type","PHOTO")->where("sku", $product->product_sku)->get()->first();
                            if ( $picture ) {
                                // POST

                                Log::info("Updating category image for {$category_id}", ["sku"=>$product->product_sku, "path", $picture->path]);

                                $file = $picture->name;
                                try {
                                    $bs = Storage::disk('pictures')->get($picture->path);
                                    $dst = env("MAGENTO_MEDIA_DIR")."/catalog/category/$file";
                                    file_put_contents($dst, $bs);

                                } catch (\Exception $e ) {
                                    Log::info($e);
                                    continue;
                                }

                                $post = [
                                    "category" => [
                                        "id" => $category_id,
                                        'custom_attributes' => [
                                            [ "attribute_code" => "image", "value" => $file ]
                                        ]
                                    ]
                                ];
                                $resp = $magento->Put("/rest/V1/categories/" .  $category_id, $post);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     *
     */
    public function disable_empty_categories()
    {
        Task::Start('ExportCategoriesemptyCetagories ');
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();

        Arrayhelpers::toLog("Fetching existing categories and collecting URL Keys",__FILE__, __METHOD__, __LINE__);
        //Log::info("Fetching existing categories and collecting URL Keys");

        $existing_categories = $magento->get("/rest/V1/categories");

        $category_ids = $this->walk_categories($existing_categories->children_data);
        Arrayhelpers::toLog("Done. Looping categories.",__FILE__, __METHOD__, __LINE__);
        foreach ($category_ids as $category_id) {
            $image = false;
            Task::updateLastTicket('ExportCategoriesemptyCetagories', 10);
            $category = $magento->get("/rest/V1/categories/" . $category_id);
            if ($category) {
                Task::updateLastTicket('ExportCategoriesemptyCetagories', 20);
//                Log::info("Fetching products under magento category id {$category_id}");
                $products = $magento->get("/rest/V1/categories/" . $category_id . "/products");
                $count=0;
                if ( ! is_iterable($products) ) {
                    continue;
                }
                foreach ($products as $product) {
                    // FIXME: check product status. Do not count disabled products.
                    Task::updateLastTicket('ExportCategoriesemptyCetagories', 25);
                    $count++;
                }
                if ( $count == 0 ) {
//                    Log::info("Disabling category {$category_id}");
                    $is_active = false;
                } else {
//                    Log::info("Enabling category {$category_id}");
                    $is_active = true;
                }
                Task::updateLastTicket('ExportCategoriesemptyCetagories', 30);
                if ( $is_active != $category->is_active) {
                    Arrayhelpers::toStringToAll( ["is_active" => $is_active], "Setting category status {$category_id}" ,__FILE__, __METHOD__, __LINE__);
                    //Log::info("Setting category status {$category_id}", ["is_active" => $is_active] );
                    $post = [
                        "category" => [
                            "id" => $category_id,
                            "is_active" => $is_active
                        ]
                    ];
                    $resp = $magento->Put("/rest/V1/categories/" .  $category_id, $post);
                } else {
//                    Log::info("Category status already set {$category_id}", ["is_active" => $is_active] );
                }
            }
        }
        Task::Stop('ExportCategoriesemptyCetagories',$this->newCount,$this->refreshCount);
    }


    /**
     *
     */
    public function set_display_settings() {

        $magento =  new MagentoRestApi();
        $magento->getAuthToken();

        // oda kell masolni majd a kepet ($file)!
        // cp ../hafner/pub/media/catalog/category/10_1573_0_F-00491.jpg ../hafner/pub/media/catalog/category/F-00023.jpg

        $csv = file_get_contents("./docs/kategoria.csv");
        $csv = explode("\n", $csv);
        foreach ($csv as $record) {
            $columns = explode(";", $record);

            if (count($columns) > 20 ) {
                $description = $columns[18];
                $file = $columns[20];


//                dump($file);

                if ( $file == "-" || $file == "") {
                    continue;
                }
                $fallback = 0;
                $magento_category_id = -1;
                $picture = Picture::where("path", "like", "%".$file."%")->get()->first();
                if ( ! $picture ) {
                    // megprobaljuk nevre is
                    $file = $columns[21]; // ez a symbol
                    $picture = Picture::where("path", "like", "%".$file."%")->get()->first();
                    $fallback = "picture_symbol";
                }
                if ( $picture ) {
                    $product = Product::where("id",$picture->product_id)->get()->first();
                    if ( $product ) {
                        $category = $product->categories()->first();

                        if ( ! $category ) {
                            $category_texts = [$columns[2] , $columns[4] , $columns[6] ];
                            foreach ($category_texts as $category_text) {
                                if ( trim($category_text) != "-" ) {
                                    $category = Category::where("name", $category_text)->get()->first();
                                    $fallback = "category_name";
                                }
                            }
                        }



                        if ( $category ) {
                            Log::info("COPY $file", [ "fallback" => $fallback ]);
//                            continue;
                            $magento_category = $magento->get("/rest/V1/categories/". $category->category_magento_id);


                            try {
                                $bs = Storage::disk('pictures')->get($picture->path);
                                $dst = env("MAGENTO_MEDIA_DIR")."/catalog/category/$file";
                                file_put_contents($dst, $bs);

                            } catch (\Exception $e ) {
                                Log::info($e);

                                continue;
                            }


                            $post = [
                                "category" => [
                                    "id" => $category->category_magento_id,
                                    'custom_attributes' => [
                                        [ "attribute_code" => "description", "value" => $description ],
                                        [ "attribute_code" => "image", "value" => $file ]
                                    ]
                                ]
                            ];
                            $resp = $magento->Put("/rest/V1/categories/" .  $category->category_magento_id, $post);
                            Arrayhelpers::toStringToAll($resp);

                        } else {
                            Log::Error("Category not found", ["product_id" => $product->id, "sku" => $product->sku, "category_texts" => $category_texts]);
                        }
                    } else {
                        Log::Error("Product not found");
                    }

                } else {
                    Log::Error("Picture not found", [ "file" => $file]);
                }


            }

            Arrayhelpers::toStringToLog($columns);
        }
    }

    /**
     *
     */
    public function export()
    {
        Task::Start('ExportCategories');
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();

//        Log::info("Fetching existing categories and collecting URL Keys");
//
//        $existing_categories = $magento->get("/rest/V1/categories");
//        $category_ids = $this->walk_categories($existing_categories->children_data);
//
//        foreach ($category_ids as $category_id) {
//            $category = $magento->get("/rest/V1/categories/" . $category_id);
//            if ($category) {
//                foreach ($category->custom_attributes as $index => $attribute) {
//                    if ( $attribute->attribute_code == 'url_key') {
//                        $this->urlkeys[] = $attribute->value;
//                    }
//                }
//            }
//        }
//        var_dump($this->urlkeys);
        Log::info("Pushing categories.");
        Task::updateLastTicket('ExportCategories', 10);
        $categories = Category::where("synced",0)->get();
        foreach ($categories as $category) {

            Task::updateLastTicket('ExportCategories', 20);

            if ( $category->parent_id == 0 ) {

                $magento_parent_id = 2;
            } else {
                $parent_category = Category::Find($category->parent_id);
                $magento_parent_id = $parent_category->category_magento_id;
            }
            if ( $magento_parent_id ) {

                if ( $category->category_magento_id ) {
                    Log::info("Updating category" , [ $category->category_magento_id ]);
                    Arrayhelpers::toStringToAll("Updating category");
                    $url_key = $magento->createUrlKey($category->name, []) . "-" . $category->id;

                    $post= [
                        "category" => [
//                            "id" =>  $category->category_magento_id,
                            "name" => $category->name,
                            "parent_id" => $magento_parent_id,
                            //"isActive" => $category->status ? true : false,
                            "isActive" => true,
                            "include_in_menu" => true,
                            "custom_attributes" => [
                                ["attribute_code" => "url_key",
                                    "value" => $url_key]
                            ]
                        ],
                        "saveOptions" => true
                    ];
                    Arrayhelpers::toStringToAll($post);
                    Task::updateLastTicket('ExportCategories', 30);
                    $resp = $magento->Put("/rest/V1/categories/" .  $category->category_magento_id, $post);
                    Arrayhelpers::toStringToLog($resp);
                   // if ( is_object($resp) && property_exists($resp, "id") ) {
                        $category->synced = 1;
                        $category->save();
                    //}
                } else {
                    Log::info("Creating new category" , [ $category->name ]);
                    Arrayhelpers::toStringToAll("category category");
//                    $url_key = $magento->createUrlKey($category->name, $this->urlkeys);
                    $url_key = $magento->createUrlKey($category->name, []) . "-" . $category->id;

                    $post= [
                        "category" => [
                            "name" => $category->name,
                            "parent_id" => $magento_parent_id,
                            //"isActive" =>  $category->status ? true : false,
                            "isActive" =>   true,
                            "include_in_menu" => true,
                            "custom_attributes" => [
                                     ["attribute_code" => "url_key",
                                     "value" => $url_key]
                            ]
                        ],
                        "saveOptions" => true
                    ];
                    Arrayhelpers::toStringToAll($post);


                    if ( $category->category_magento_id ) {
                        $resp = $magento->Put("/rest/V1/categories/" .  $category->category_magento_id, $post);
                    }else {
                        $resp = $magento->Post("/rest/V1/categories", $post);
                    }
                    Task::updateLastTicket('ExportCategories', 50);
                    // ellenorzom, hogy sikerul-e? ha nem miert? ha url-key miatt? akkor goto $post, url_key append $valami++
                    // vagy azt csinalom, hogy leszedem az osszes kategoriat, megnezem az url-key, epitek belole dictionarity, ha benne mar eleve figyelek, hoogy uniq legyen.

                    if ( property_exists($resp, "message") && property_exists($resp, "parameters") ) {
                        if ( is_array($resp->parameters) ) {
                            foreach ($resp->parameters as $message) {
                                if ( trim($message) == "URL key for specified store already exists." ) {
                                    Log::info("Resending category with randomized urlkey");

                                    $url_key = $magento->createUrlKey($category->name, []) . "-" . time();
                                    Log::info($url_key);

                                    $post= [
                                        "category" => [
                                            "name" => $category->name,
                                            "parent_id" => $magento_parent_id,
                                            "isActive" => true,
                                            "include_in_menu" => true,
                                            "custom_attributes" => [
                                                     ["attribute_code" => "url_key",
                                                     "value" => $url_key]
                                            ]
                                        ],
                                        "saveOptions" => true
                                    ];
                                    print_r($post);
                                    $resp = $magento->Post("/rest/V1/categories", $post);

                                }
                            }
                        }
                    }

                    Arrayhelpers::toStringToAll($resp);
                    if ( property_exists($resp, "id") ) {
                        $category->category_magento_id = $resp->id;
                        $category->url_key = $url_key;
                        $category->synced = 1;
                        $category->save();
                    }
                    Task::updateLastTicket('ExportCategories', 100);
                }
            }
            $this->limitCount++;
            if( ($this->limitCount>$this->argumentum['limit'] && $this->argumentum['limit'] !=0)  || Task::isStopped('ExportCategories') ) {
                Task::Stop('ExportCategories',$this->newCount,$this->refreshCount);
                break;
            }
        }
        if ($this->options['product']) {
            $products = Product::where("synced", 1)->get();
            $_count = count($products);
            $i=0;
            foreach ($products as $product) {
                $i++;
                $category_magento_ids = [];
                $category_links = [];
                $position = 0;
                foreach ($product->categories as $category) {
                    if ($category->category_magento_id) {
                        if (!in_array($category->category_magento_id, $category_magento_ids)) {
                            $category_magento_ids[] = $category->category_magento_id;
                            $category_links[] = [
                                "position" => 0,
                                "category_id" => $category->category_magento_id
                            ];
                            $position++;
                        }
                    }
                }
                $post = [
                    "product" => [
                        "sku" => $product->sku,
                        "extension_attributes" => [
                            "category_links" => $category_links
                        ],
                    ]
                ];
                if ($product->magento_id) {
                    $response = $magento->put("/rest/V1/products/" . urlencode($product->sku), $post);
                    //Arrayhelpers::toStringToAll($response);
                }
                Arrayhelpers::toStringToAll("{$_count}/{$i}");
            }
        }
        Task::Stop('ExportCategories',$this->newCount,$this->refreshCount);
    }

}
