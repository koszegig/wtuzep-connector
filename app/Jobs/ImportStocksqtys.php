<?php


namespace App\Jobs;
use App\Aion\Helpers\Arrayhelpers;
use App\Aion\Library\FileManager;
use App\ManagerSources;
use App\StocksQty;
use App\Models\Vectory\CCStock;
use App\Product;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\SAPTables\ActRaktarKeszlet;
use DB;
use Log;


/**
 * Class ImportStocksqtys
 * @package App\Jobs
 */
class ImportStocksqtys  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var array
     */
    protected $argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var int
     */
    protected $limitCount=0;


    /**
     * @var array
     */
    protected $stockQty =[];
    /**
     * @var array
     */
    protected $managersources =[];
    /**
     * @var
     */
    protected $stockQtyClass;
    /**
     * @var
     */
    protected $stockQtyClassFields;
    /**
     * ImportStocksqtys constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->argumentum =$_argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleStocksqty();
    }


    /**
     *
     */
    public function handleStocksqty()
    {
        Task::Start('ImportStocksqtys');
        $this->stockQtyClass =  new StocksQty();
        $this->stockQtyClassFields = $this->stockQtyClass->getFields();
        Task::updateLastTicket('ImportStocksqtys', 000);
        /*$SqlManagersources = ManagerSources::all()->toArray();
        $this->managersources =[];
        foreach ($SqlManagersources as $SqlManagersource){
            $this->managersources[$SqlManagersource['erp_id']] =$SqlManagersource['cod'];
        }*/
        Arrayhelpers::toStringToEcho('Starting ImportStocksqtys');
        Task::updateLastTicket('ImportStocksqtys', 10);
        $Stocka = ['3335'=>'Dabas áruház',
                   '3304' => 'DH áruház',
                   '3376' => 'Érdi raktár',
                   '25' => 'Pázmány úti raktár',
            ];

        $CCStock = new CCStock();
        $StockQtys = $CCStock->getFull();


        foreach ($StockQtys as $row) {
            Task::updateLastTicket('ImportStocksqtys', 30);
            //Arrayhelpers::toStringToAll($row, 'ImportStocksqtys SAP ActRaktarKeszlet  row', __FILE__, __METHOD__, __LINE__);
            $this->stockQty = [];
            Arrayhelpers::toStringToEcho('S');
            $this->stockQty =  $this->stockQtyClass->filledImportFields($row);
            $product = product::where("cikkszam", $this->stockQty['cikkszam'])->first();
            if ($product) {
                $this->stockQty['ProductId'] = $product->id;
            }
            $this->stockQty['StockName'] =  $Stocka[$this->stockQty['StockId']];
            Arrayhelpers::toStringToEcho('S2');
            if (isset($this->stockQty['ProductId'])) {
                $sku = $this->stockQty['cikkszam'];

                Arrayhelpers::toStringToEcho("$sku", ' Product ');
                Arrayhelpers::toStringToEcho($this->stockQty['StockId'] , ' Product StockId ');
                $related = [];
                Arrayhelpers::toStringToAll($this->stockQty, 'ImportStocksqtys connector stockQty', __FILE__, __METHOD__, __LINE__);
                $sqlStocksQty = StocksQty::UpdateOrCreate(['StockId' => $this->stockQty['StockId'], 'cikkszam' => $sku], $this->stockQty);
                if ($sqlStocksQty->wasChanged()) {
                    Log::debug("Marking product dirty.");
                    $sqlStocksQty->synced = 0;
                    $sqlStocksQty->save();
                }
                Task::updateLastTicket('ImportStocksqtys', 40);
                $this->limitCount++;
            }
            if( ($this->limitCount>$this->argumentum['limit'] && $this->argumentum['limit'] !=0)  || Task::isStopped('ImportStocksqtys') ) {
                Task::Stop('ImportStocksqtys',$this->newCount,$this->refreshCount);
                $result['next'] = false;
                break;
            }

        }

        Task::Stop('ImportStocksqtys',$this->newCount,$this->refreshCount);
    }

}
