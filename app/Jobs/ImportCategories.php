<?php

namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use App\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;

use App\Models\Vectory\CCProductCategory;
use App\Task;
use Log;
use DB;
use App\Category;
use App\ProductGroups;


/**
 * Class ImportCategories
 * @package App\Jobs
 */
class ImportCategories implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $limitCount=0;
    /**
     * @var array
     */
    protected $argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var array
     */
    protected $category =[];
    /**
     * @var
     */
    protected $categoriClass;
    /**
     * @var
     */
    protected $categoriClassFields;


    /**
     * ImportCategories constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->argumentum =$_argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Task::Start('ImportCategories');
        Log::info("ImportCategories catalog");
        $this->categoriClass =  new Category();
        $this->categoriClassFields = $this->categoriClass->getFields();
        Task::updateLastTicket('ImportCategories', 10);
        $ccproductCategory = new CCProductCategory();
        $Categorys = $ccproductCategory->getFull();
        foreach ($Categorys as $row) {
            //  Arrayhelpers::toStringToLog($row);

            $this->category = [];
            $this->category = $this->categoriClass->filledImportFields($row);
                        $this->newCount++;
                        $this->category['status'] = 1;
                        $erp_id = $this->category['erp_id'];
                        Task::updateLastTicket('ImportCategories', 30);
                        $sqlCategory = Category::UpdateOrCreate(['erp_id' => $erp_id],$this->category );
        }
        ;

        Task::Stop('ImportCategories',$this->newCount,$this->refreshCount);

        Log::info("Importing product catalog Stop");
    }
}
