<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use Log;
use DB;
use \App\Aion\Helpers\Filehelpers;
use App\Aion\Helpers\ArrayHelpers;
use App\Order;
use App\OrderItem;
use App\OrderAddress;
use App\Customers;
use App\Orderpayment;
use App\Task;
use App\Models\SAPTables\Actdochead;
use App\Models\SAPTables\ActdocLine;
use Illuminate\Support\Arr;
use function GuzzleHttp\Promise\all;

class ExportOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var array
     */
    protected $orderHead =[];
    /**
     * @var
     */
    protected $orderClass;
    /**
     * @var
     */
    protected $orderClassFields;
    /**
     * @var array
     */
    protected $orderParentItems =[];
    /**
     * @var array
     */
    protected $orderChildItems =[];
    /**
     * @var array
     */
    protected $orderItem =[];
    /**
     * @var
     */
    protected $ordeItemClass;
    /**
     * @var
     */
    protected $orderItemClassFields;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * Insert or Update orders in ERP
     * @var bool
     */
    public $commit = true;

    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->exportOrders();

    }

    /**
     *
     */
    public function exportOrders()
    {
        Task::Start('ExportOrders');
        Task::updateLastTicket('ExportOrders', 000);
        $dataOrders = Order::where("synced", 0)->get();
        $this->orderClass =  new Actdochead();
        $this->orderClassFields = $this->orderClass->getFields();
        $this->ordeItemClass =  new ActdocLine();
        $this->orderItemClassFields = $this->ordeItemClass->getFields();


        $i = 0;

        foreach ($dataOrders as $magento_order) {
            $this->orderHead = [];
            $order_id = $magento_order->order_id;
            $magento_order->synced = 1;
            $magento_order->save();

            $OrderAddress = OrderAddress::where("order_id", $order_id)->get();
            $magento_order->address =$OrderAddress;
            $Orderpayment = Orderpayment::where("order_id", $order_id)->get();
            $magento_order->payment = $Orderpayment;
            $this->orderHead = $this->orderClass->filledImportFields($magento_order->toArray());
            \AionArray::toStringToEcho($this->orderHead['Code'],' ExportOrders - Orderhead ',__FILE__, __METHOD__, __LINE__);
            Actdochead::UpdateOrCreate(['Code' => $this->orderHead['Code']], $this->orderHead);
            //$datarder_items = OrderItem::where("order_id", $order_id)->get();
            $datarder_items = OrderItem::where("order_id", $order_id)->where('product_type','configurable')->whereNull('parent_item_id')->get();
            $this->orderParentItems = [];
                $line_number = 0;
                foreach ($datarder_items as $datarderitem) {
                    $this->orderItem = [];
                  //  \AionArray::toStringToLog($datarderitem,'$datarderitem - ',__FILE__, __METHOD__, __LINE__);
                    //$this->filledOrderItemFields($datarderitem->toArray());
                    $this->orderItem = $this->ordeItemClass->filledImportFields($datarderitem->toArray());
                    $this->orderItem['U_LineNum'] = $line_number;
                    $this->orderItem['U_Price'] = floatval($this->orderItem['U_Price']);
                    $this->orderItem['U_Quantity'] = floatval($this->orderItem['U_Quantity']);
                    $this->orderItem['U_LineTotal'] = floatval($this->orderItem['U_LineTotal']);
                    $this->orderItem['item_id'] = $datarderitem['item_id'];
                    $this->orderItem['parent_item_id'] = $datarderitem['parent_item_id'];
                    $this->orderItem['product_type'] = $datarderitem['product_type'];
                    $line_number ++;
                    \AionArray::toStringToEcho($this->orderItem['U_LineNum'],' ExportOrders - OrderLine ',__FILE__, __METHOD__, __LINE__);
                    $this->orderParentItems[$this->orderItem['item_id']] = $this->orderItem;

                    /*ActdocLine::select("SET ANSI_WARNINGS  OFF;");
                    ActdocLine::UpdateOrCreate(['U_HeadCode' => $this->orderItem['U_HeadCode'],'U_LineNum' => $this->orderItem['U_LineNum']], $this->orderItem);
                    ActdocLine::select("SET ANSI_WARNINGS  ON;");
                    $datarderitem->synced = 1;
                    $datarderitem->save();*/
                }
            $datarder_items = OrderItem::where("order_id", $order_id)->where('product_type','virtual')->whereNotNull('parent_item_id')->get();
            $this->orderChildItems  = [];
            $line_number = 0;
            foreach ($datarder_items as $datarderitem) {
                $this->orderItem = [];
                //  \AionArray::toStringToLog($datarderitem,'$datarderitem - ',__FILE__, __METHOD__, __LINE__);
                //$this->filledOrderItemFields($datarderitem->toArray());
                $this->orderItem = $this->ordeItemClass->filledImportFields($datarderitem->toArray());
                $this->orderItem['U_LineNum'] = $line_number;
                $this->orderItem['U_Price'] = floatval($this->orderItem['U_Price']);
                $this->orderItem['U_Quantity'] = floatval($this->orderItem['U_Quantity']);
                $this->orderItem['U_LineTotal'] = floatval($this->orderItem['U_LineTotal']);
                $this->orderItem['item_id'] = $datarderitem['item_id'];
                $this->orderItem['parent_item_id'] = $datarderitem['parent_item_id'];
                $this->orderItem['product_type'] = $datarderitem['product_type'];
                $line_number ++;
                \AionArray::toStringToEcho($this->orderItem['U_LineNum'],' ExportOrders - OrderLine ',__FILE__, __METHOD__, __LINE__);
                $this->orderChildItems[$this->orderItem['item_id']] = $this->orderItem;

                /*ActdocLine::select("SET ANSI_WARNINGS  OFF;");
                ActdocLine::UpdateOrCreate(['U_HeadCode' => $this->orderItem['U_HeadCode'],'U_LineNum' => $this->orderItem['U_LineNum']], $this->orderItem);
                ActdocLine::select("SET ANSI_WARNINGS  ON;");
                $datarderitem->synced = 1;
                $datarderitem->save();*/
            }
            $datarder_items = OrderItem::where("order_id", $order_id)->where('product_type','simple')->get();
            $line_number = 0;
            foreach ($datarder_items as $datarderitem) {
                $this->orderItem = [];
                //  \AionArray::toStringToLog($datarderitem,'$datarderitem - ',__FILE__, __METHOD__, __LINE__);
                //$this->filledOrderItemFields($datarderitem->toArray());
                $this->orderItem = $this->ordeItemClass->filledImportFields($datarderitem->toArray());
                $this->orderItem['U_LineNum'] = $line_number;
                $this->orderItem['U_Price'] = floatval($this->orderItem['U_Price']);
                $this->orderItem['U_Quantity'] = floatval($this->orderItem['U_Quantity']);
                $this->orderItem['U_LineTotal'] = floatval($this->orderItem['U_LineTotal']);
                $this->orderItem['item_id'] = $datarderitem['item_id'];
                $this->orderItem['parent_item_id'] = $datarderitem['parent_item_id'];
                $this->orderItem['product_type'] = $datarderitem['product_type'];

                $line_number ++;
                \AionArray::toStringToEcho($this->orderItem['U_LineNum'],' ExportOrders - OrderLine ',__FILE__, __METHOD__, __LINE__);
                $this->orderChildItems[$this->orderItem['item_id']] = $this->orderItem;


            }
            $line_number = 0;
            foreach ($this->orderChildItems as $datarderitemKey => $datarderitem) {
                $this->orderItem = [];
                $this->orderItem['U_LineNum'] = $line_number;
                $line_number ++;
                $this->orderItem['Code'] = $datarderitem['Code'];
                $this->orderItem['Name'] = $datarderitem['Name'];
                $this->orderItem['U_HeadCode'] = $datarderitem['U_HeadCode'];
                $this->orderItem['U_ItemCode'] = $datarderitem['U_ItemCode'];
                $U_Quantity = $datarderitem['U_Quantity'];
                $U_Price  = $datarderitem['U_Price'];
                $U_LineTotal = $datarderitem['U_LineTotal'];
                if (isset($this->orderParentItems[$datarderitem['parent_item_id']])) {
                        if ($this->orderParentItems[$datarderitem['parent_item_id']]['U_Quantity'] > $U_Quantity) {
                            $U_Quantity = $this->orderParentItems[$datarderitem['parent_item_id']]['U_Quantity'];
                        }
                        if ($this->orderParentItems[$datarderitem['parent_item_id']]['U_LineTotal'] >$U_LineTotal) {
                            $U_LineTotal = $this->orderParentItems[$datarderitem['parent_item_id']]['U_LineTotal'];
                        }
                        if ($this->orderParentItems[$datarderitem['parent_item_id']]['U_Price'] >$U_Price) {
                            $U_Price = $this->orderParentItems[$datarderitem['parent_item_id']]['U_Price'];
                        }
                }
                $this->orderItem['U_Quantity'] = $U_Quantity;
                $this->orderItem['U_Price'] = $U_Price;
                $this->orderItem['U_LineTotal'] = $U_LineTotal;
                ActdocLine::select("SET ANSI_WARNINGS  OFF;");
               ActdocLine::UpdateOrCreate(['U_HeadCode' => $this->orderItem['U_HeadCode'],'U_LineNum' => $this->orderItem['U_LineNum']], $this->orderItem);
               ActdocLine::select("SET ANSI_WARNINGS  ON;");
            }


            }
        Task::Stop('ExportOrders',$this->newCount,$this->refreshCount);
    }

}
