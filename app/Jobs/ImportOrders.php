<?php

namespace App\Jobs;

use App\Aion\Connect\MagentoRestApi;
use App\Aion\Helpers\Arrayhelpers;
use App\Orderpayment;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \App\Aion\Library\CompareOrder;
use App\Order;
use App\OrderItem;
use App\Customers;
use App\OrderAddress;
use App\Task;
use Illuminate\Support\Arr;
use Log;

/**
 * Class ImportOrders
 * @package App\Jobs
 */
class ImportOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * @var array
     */
    protected $orderHead =[];
    /**
     * @var
     */
    protected $orderClass;
    /**
     * @var
     */
    protected $orderClassFields;
    /**
     * @var array
     */
    protected $orderItem =[];
    /**
     * @var
     */
    protected $orderItemClass;
    /**
     * @var
     */
    protected $orderItemClassFields;
    /**
     * @var array
     */
    protected $customer =[];
    /**
     * @var
     */
    protected $customerClass;
    /**
     * @var
     */
    protected $customerClassFields;
    /**
     * @var array
     */
    protected $OrderAddress =[];

    /**
     * @var
     */
    protected $OrderAddressClass;
    /**
     * @var
     */
    protected $OrderAddressClassFields;

    /**
     * @var array
     */
    protected $Orderpayment =[];

    /**
     * @var
     */
    protected $OrderpaymentClass;
    /**
     * @var
     */
    protected $OrderpaymentClassFields;


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->importCustomer();
        $this->import();

    }

    /**
     *
     */
    public function import()
    {
        Task::Start('ImportOrders');
        Task::updateLastTicket('ImportOrders', 000);
        Arrayhelpers::toStringToEcho(  'Import orders');
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();
        Task::updateLastTicket('ImportOrders', 10);
        $magento_orders = $magento->get("/rest/V1/orders?searchCriteria=all");

        Task::updateLastTicket('ImportOrders', 20);
        $this->orderClass =  new Order();
        $this->orderClassFields = $this->orderClass->getFields();

        $this->orderItemClass =  new OrderItem();
        $this->orderItemClassFields = $this->orderItemClass->getFields();

        $this->OrderAddress = [];
        $this->OrderAddressClass = new OrderAddress();
        $this->OrderAddressClassFields = $this->OrderAddressClass->getFields();

        $this->OrderpaymentClass = new Orderpayment();
        $this->OrderpaymentClassFields = $this->OrderpaymentClass->getFields();


        // Arrayhelpers::toStringToLog($magento_orders,'$magento_orders - Magento order',__FILE__, __METHOD__, __LINE__ );
        foreach ($magento_orders->items as $magento_order) {
            //dd(@json_decode(json_encode($magento_order), true));
            $this->orderHead = [];
            $order_id = $magento_order->entity_id;
            $this->filledOrderHeadFields($magento_order);
            Task::updateLastTicket('ImportOrders', 30);
            if ( property_exists($magento_order, "customer_id") ) {
                $magento_customer_id = $magento_order->customer_id;
            } else {
                Log::error("No customer_id found in magento order. Customer maybe deleted. Skipping.. ");
                $magento_customer_id =  -1;
            }
            Arrayhelpers::toStringToEcho("Processing #{$order_id} order with customer: {$magento_customer_id} ");
            Log::info("Processing #{$order_id} order with customer: {$magento_customer_id} ");

            $partner = Customers::where("magento_id", $magento_customer_id)->first();
            Task::updateLastTicket('ImportOrders', 40);

            // FIXME: shipment ????
            $shipment = $magento->get("/rest/V1/shipments?searchCriteria[filter_groups][0][filters][0][field]=order_id&searchCriteria[filter_groups][0][filters][0][value]=" . $magento_order->entity_id);
            Arrayhelpers::toStringToLog($magento_order,'$magento_order - Magento order',__FILE__, __METHOD__, __LINE__ );
            ///$compareorder = new CompareOrder ();
            $this->orderHead['order_id'] = $magento_order->entity_id;
            ///$this->orderHead['partner_id'] = $partner_id;


            // FIXME: delivery_instrucftions
            // FIXME: shipping_method
            // FIXME: payment_method


            if ($shipment->total_count > 0) {
                if(count($shipment->items[0]->tracks)>0) {
                    $o['tracking_code'] = $shipment->items[0]->tracks[0]->track_number;
                }
            }
            Task::updateLastTicket('ImportOrders', 60);
            $this->newCount++;
            \AionArray::toStringToEcho($order_id,' ImportOrders - Order ',__FILE__, __METHOD__, __LINE__);

            $order = Order::UpdateOrCreate(['order_id' => $order_id],$this->orderHead);

            if ( $order->wasChanged() ) {
                $this->refreshCount++;
                $this->newCount--;
                Log::debug("Marking order dirty.");
                $order->synced = 0;
                $order->save();
            } else {
                Log::debug("Order $order_id is unchanged");
            }

            ///$order_item = OrderItem::Where( "order_id", $order_id)->delete();
            $this->OrderAddress = [];
            if ( is_object($magento_order) && property_exists($magento_order,"billing_address") ) {
                $this->filledAdressFields($magento_order->billing_address);
                $this->OrderAddress['order_id'] = $order_id;
                OrderAddress::UpdateOrCreate(['order_id' => $order_id , 'address_type' => $this->OrderAddress['address_type']],$this->OrderAddress);
           }
            $this->OrderAddress = [];
            if ( is_object($magento_order) && property_exists($magento_order,"shipping_address") ) {
                $this->filledAdressFields($magento_order->shipping_address);
                $this->OrderAddress['order_id'] = $order_id;
                OrderAddress::UpdateOrCreate(['order_id' => $order_id , 'address_type' => $this->OrderAddress['address_type']],$this->OrderAddress);
            }
            $this->OrderAddress = [];
            if ( is_object($magento_order) && property_exists($magento_order,"extension_attributes") ) {
                if ( is_object($magento_order->extension_attributes) && property_exists($magento_order->extension_attributes,"payment_additional_info") ) {
                    foreach ($magento_order->extension_attributes->payment_additional_info as $item) {
                        $this->Orderpayment = [];
                        $this->filledOrderpaymentFields($item);
                        $this->Orderpayment['order_id'] = $order_id;
                        Orderpayment::UpdateOrCreate(['order_id' => $order_id, 'paymentkey' => $this->Orderpayment['paymentkey']], $this->Orderpayment);
                    }
                }
        }


            foreach ($magento_order->items as $item) {
                $this->orderItem = [];
                $product = Product::where("cikkszam",$item->sku)->first();
                if ( $product ) {
                    $product_id = $product->id;
                    Log::info("Product id found for {$item->sku}", [ "product_id" => $product_id ]);

                } else {
                    $product_id = 0;
                    Log::Error("No product id found for {$item->sku}");
                }
//                dump($item);
                // order_head_id lesz a kulcs az order_head -re nem az order_id
                $i = [];
                Arrayhelpers::toStringToLog($item,'$item - Magento order iem',__FILE__, __METHOD__, __LINE__ );
                $this->filledOrderItemFields($item);
                //$i = $compareorder->compareImportItem($item);
                $this->orderItem['order_id'] = $order_id;
                $this->orderItem['product_id'] = $product_id;
                $this->orderItem['product_sku'] = $item->sku;
                $this->orderItem['price'] =  $item->price;
                $this->orderItem['quantity'] = $item->qty_ordered;
               // dump($this->orderItem);
                    Task::updateLastTicket('ImportOrders', 70);
                //if ($this->orderItem['product_type'] !='virtual') {
                   $order_item = OrderItem::UpdateOrCreate(['order_id' => $order_id , 'item_id' => $item->item_id],$this->orderItem);
               // }

                //Create( $i);
            }
        }
        Task::updateLastTicket('ImportOrders', 400);
        Task::Stop('ImportOrders',$this->newCount,$this->refreshCount);

    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledOrderHeadFields($row){
        foreach($this->orderClassFields as $field){
            //Task::updateLastTicket('ImportProducts', 34);
            if (!empty($field->getImportFieldName())) {
                //dd($field->getImportFieldName());
                $arrow = @json_decode(json_encode($row), true);
                $this->orderHead[$field->getName()] = Arr::get($arrow, $field->getImportFieldName()) ;
            }
            //Task::updateLastTicket('ImportProducts', 36);
        }
        Task::updateLastTicket('ImportOrders', 39);
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledOrderItemFields($row){

        foreach($this->orderItemClassFields as $field){
            //Task::updateLastTicket('ImportProducts', 34);
            if (!empty($field->getImportFieldName())) {
                //dd($field->getImportFieldName());
                $arrow = @json_decode(json_encode($row), true);
                $this->orderItem[$field->getName()] = Arr::get($arrow, $field->getImportFieldName()) ;
            }
            //Task::updateLastTicket('ImportProducts', 36);
        }
        Task::updateLastTicket('ImportOrders', 39);
    }

    /**
     *
     */
    public function importCustomer()
    {
        Log::info("export new  get ");
        Task::Start('ImportNewCustomer');
        $magento = new MagentoRestApi();
        $magento->getAuthToken();
        $response = $magento->get('/rest/V1/customers/search?searchCriteria[sortOrders][0][field]=email&searchCriteria[sortOrders][0][direction]=asc');
        $partners = $response->items;
        $this->customerClass =  new Customers();
        $this->customerClassFields = $this->customerClass->getFields();
        foreach ($partners as $row) {
                Arrayhelpers::toStringToAll($row, ' ImportNewCustomer - New USer',__FILE__, __METHOD__, __LINE__);
                $this->customer = [];
                $this->filledCustomerFields($row);
                Arrayhelpers::toStringToAll($this->customer, ' ImportNewCustomer - New USer',__FILE__, __METHOD__, __LINE__);
                $sqlcustomer = Customers::UpdateOrCreate(['email' => $this->customer['email']], $this->customer);
                 if ( $sqlcustomer->wasChanged() ) {
                     Log::debug("Marking order dirty.");
                     Arrayhelpers::toStringToEcho("Marking order dirty.");
                     $sqlcustomer->synced = 0;
                     $sqlcustomer->save();
                 }
        }
        Task::Stop('ImportNewCustomer',0,0);
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledCustomerFields($row){

        foreach($this->customerClassFields as $field){
            if (!empty($field->getImportFieldName())) {
                $arrow = @json_decode(json_encode($row), true);
                $this->customer[$field->getName()] = Arr::get($arrow, $field->getImportFieldName()) ;
            }
        }
        $addressBilling =[];
        $addressShipping =[];
        $this->customer['name'] = trim($row->firstname) . ' ' . trim($row->lastname);
        foreach ($row->addresses as $address) {
            if ($address->default_billing ?? 0) {
                $addressBilling["city"] = trim($address->city ?? '');
                $addressBilling["street"] = trim($address->street[0] ?? '');
                $addressBilling["zip_code"] = trim($address->postcode ?? '');
                $addressBilling["number"] = trim($address->number ?? '');
                $addressBilling["email"] = trim($row->email);
            }
            if ($address->default_shipping ?? 0) {
                $addressShipping["city"] = trim($address->city ?? '');
                $addressShipping["street"] = trim($address->street[0] ?? '');
                $addressShipping["zip_code"] = trim($address->postcode ?? '');
                $addressShipping["number"] = trim($address->number ?? '');
                $addressShipping["email"] = trim($row->email);
            }

        }
        if (!isset($addressBilling["city"])) {
            $addressBilling = $addressShipping;
        }
        if (isset($addressBilling["city"])) {
            $this->customer["billing_city"] = $addressBilling["city"];
            $this->customer["billing_street"] = $addressBilling["street"];
            $this->customer["billing_zip_code"] = $addressBilling["zip_code"];
            $this->customer["billing_number"] = $addressBilling["number"];
        }
        if (isset($addressBilling["city"])) {
            $this->customer["shipping_city"] = $addressShipping["city"];
            $this->customer["shipping_street"] = $addressShipping["street"];
            $this->customer["shipping_zip_code"] = $addressShipping["zip_code"];
            $this->customer["shipping_number"] = $addressShipping["number"];
        }
        Task::updateLastTicket('ImportNewCustomer', 39);
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledAdressFields($row)
    {
        foreach ($this->OrderAddressClassFields as $field) {
            if (!empty($field->getImportFieldName())) {
                $arrow = @json_decode(json_encode($row), true);
                if (!empty($field->getCast())) {
                    $cast = $field->getCast();
                    $this->OrderAddress[$field->getName()] = $this->OrderAddressClass->$cast($arrow);
                } else {
                    $this->OrderAddress[$field->getName()] = Arr::get($arrow, $field->getImportFieldName());
                }
            }
        }
    }
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledOrderpaymentFields($row)
    {
        foreach ($this->OrderpaymentClassFields as $field) {
            if (!empty($field->getImportFieldName())) {
                $arrow = @json_decode(json_encode($row), true);
                if (!empty($field->getCast())) {
                    $cast = $field->getCast();
                    $this->Orderpayment[$field->getName()] = $this->OrderpaymentClass->$cast($arrow);
                } else {
                    $this->Orderpayment[$field->getName()] = Arr::get($arrow, $field->getImportFieldName());
                }
            }
        }
    }

}
