<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Library\XMLManager;
use App\Aion\Library\FileManager;
use App\Aion\Helpers\ArrayHelpers;
use App\AttributeValue;

use App\Product;
use App\Models\Vectory\CCProductImagePaths;

use App\Picture;
use App\Task;
use App\CustomLog;
use DB;
use Log;


class ImportProductPictures implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */

    protected $productPutNumber =0;
    protected $picture =[];
    protected $pictureClass;
    protected $pictureClassFields;


    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProductPictures();
    }


    /**
     *
     */
    public function handleProductPictures()
    {
        Task::Start('ImportProductPictures');
        $this->pictureClass =  new Picture();
        $this->pictureClassFields = $this->pictureClass->getFields();

        $ccproductImagePaths = new CCProductImagePaths();
        Task::updateLastTicket('ImportProductReplacement', 20);

        $productpictures = $ccproductImagePaths->getFull();

        Task::updateLastTicket('ImportProductPictures', 000);



        Log::info("Fetching products");

        foreach ($productpictures as $row) {
            Task::updateLastTicket('ImportProductPictures', 30);
            Arrayhelpers::toStringToAll($row, 'ImportProductPictures SAP actcikk row', __FILE__, __METHOD__, __LINE__);
            $this->picture = [];
            Arrayhelpers::toStringToEcho('S');
            $this->picture =$this->pictureClass->filledImportFields($row);

            Arrayhelpers::toStringToEcho('S2');
            $sku = $row['PRODUCTID'];

            Arrayhelpers::toStringToEcho("$sku",' Product ');
            $sqlProduct = Product::where('cikkszam',$sku)->get()->first();
            if ($sqlProduct) {
                $this->picture["product_id"] = $sqlProduct->id;
                if (!empty($this->picture["product_id"])) {
                      Arrayhelpers::toStringToAll($this->picture['path'], 'ImportProductPictures $row->picture.', __FILE__, __METHOD__, __LINE__);
                      $pic = Picture::firstOrCreate($this->picture);
                }
                $this->productPutNumber++;
            }
            if ($this->productPutNumber>$this->argumentum['limit'] && $this->argumentum['limit'] !=0) {
                break;
            }
            Task::updateLastTicket('ImportProductPictures', 40);
        }

        //} while ($result['isSuccess']);
        Task::Stop('ImportProductPictures',$this->newCount,$this->refreshCount);
    }


}

