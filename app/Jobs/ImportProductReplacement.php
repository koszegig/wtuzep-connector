<?php


namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;

use App\Product;
use App\ProductReplace;
use App\Models\Vectory\CCProductReplacement;


use App\Task;
use App\CustomLog;
use DB;
use Log;
use function GuzzleHttp\Promise\all;

class ImportProductReplacement  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount = 0;
    /**
     * @var
     */
    protected $refreshCount = 0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */

    protected $PutNumber = 0;
    /**
     * @var array
     */
    protected $ProductReplace = [];
    /**
     * @var
     */
    protected $ProductReplaceClass;
    /**
     * @var
     */
    protected $ProductReplaceClassFields;


    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum = $argumentum;
        $this->options = $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProducts();
    }


    /**
     *
     */
    public function handleProducts()
    {
        Task::Start('ImportProductReplacement');
        $this->ProductReplaceClass = new ProductReplace();
        $this->ProductReplaceClassFields = $this->ProductReplaceClass->getFields();
        Task::updateLastTicket('ImportProductReplacement', 000);

        Arrayhelpers::toStringToEcho('Starting ImportProductReplacement');
        Task::updateLastTicket('ImportProductReplacement', 10);
        $ccproductReplacement = new CCProductReplacement();
        Task::updateLastTicket('ImportProductReplacement', 20);

        $productReplacements = $ccproductReplacement->getFull();
        Log::info("foreach ProductReplacement");
        foreach ($productReplacements as $row) {
            Task::updateLastTicket('ImportProductReplacement', 30);
            $this->ProductReplace = [];
            $this->ProductReplace = $this->ProductReplaceClass->filledImportFields($row);

            $product_id = $this->ProductReplace['ProductId'];
            $ReplacementProductId = $this->ProductReplace['ReplacementProductId'];
            Arrayhelpers::toStringToEcho("$product_id", ' ImportProductReplacement ');

            $sqlProductReplace = ProductReplace::UpdateOrCreate(['ProductId' => $product_id,'ReplacementProductId' => $ReplacementProductId], $this->ProductReplace);
            $this->PutNumber++;
            if ($this->PutNumber > $this->argumentum['limit'] && $this->argumentum['limit'] != 0) {
                break;
            }
            Task::updateLastTicket('ImportProductReplacement', 40);
        }

        //} while ($result['isSuccess']);
        Task::Stop('ImportProductReplacement', $this->newCount, $this->refreshCount);
    }

}
