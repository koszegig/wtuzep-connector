<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;

use App\Product;
use App\Models\Vectory\CCProducts;

use App\Category;
use App\Task;
use App\CustomLog;
use DB;
use Log;
use function GuzzleHttp\Promise\all;

/**
 * Class ImportProducts
 * @package App\Jobs
 */
class ImportProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */

    protected $productPutNumber =0;
    /**
     * @var array
     */
    protected $product =[];
    /**
     * @var
     */
    protected $productClass;
    /**
     * @var
     */
    protected $productClassFields;


    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProducts();
    }


    /**
     *
     */
    public function handleProducts()
    {
        Task::Start('ImportProducts');

        $this->productClass =  new Product();
        $this->productClassFields = $this->productClass->getFields();
        Task::updateLastTicket('ImportProducts', 000);

        Arrayhelpers::toStringToEcho('Starting ImportProducts');
        Task::updateLastTicket('ImportProducts', 10);
        $cikktorzs = new CCProducts();

        Task::updateLastTicket('ImportProducts', 20);
        Log::info("Fetching products");

        $products = $cikktorzs->getFull();
        Log::info("foreach products");
        foreach ($products as $row) {
            Task::updateLastTicket('ImportProducts', 30);
           // Arrayhelpers::toStringToAll($row, 'ImportProducts SAP actcikk row', __FILE__, __METHOD__, __LINE__);
            $this->product = [];

            $this->product = $this->productClass->filledImportFields($row);
            $sku = $this->product['cikkszam'];

            Arrayhelpers::toStringToEcho("$sku",' Product ');
            $sqlProduct = Product::UpdateOrCreate(['cikkszam' => $sku],$this->product);
            $this->productPutNumber++;
            if ($this->productPutNumber>$this->argumentum['limit'] && $this->argumentum['limit'] !=0) {
                break;
            }
            Task::updateLastTicket('ImportProducts', 40);
        }

        //} while ($result['isSuccess']);
        Task::Stop('ImportProducts',$this->newCount,$this->refreshCount);
    }

}
