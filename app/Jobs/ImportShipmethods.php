<?php

namespace App\Jobs;

use App\Aion\Helpers\Arrayhelpers;
use App\ShipMethod;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\SAPTables\ActSzallitasiMod;
use DB;
use Log;


class ImportShipmethods implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount = 0;
    /**
     * @var
     */
    protected $refreshCount = 0;
    /**
     * @var array
     */
    protected $argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var int
     */
    protected $limitCount = 0;


    /**
     * @var array
     */
    protected $shipmethod = [];
    /**
     * @var
     */
    protected $shipmethodClass;

    /**
     * ImportShipmethods constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->argumentum = $_argumentum;
        $this->options = $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleshipmethods();
    }


    /**
     *
     */
    public function handleshipmethods()
    {
        Task::Start('ImportShipmethods');
        $this->hipmethodClass = new ShipMethod();
        Task::updateLastTicket('ImportShipmethods', 000);
        $shipments = ActSzallitasiMod::all()->toArray();
        Arrayhelpers::toStringToEcho('Starting ImportShipmethods');
        Task::updateLastTicket('ImportShipmethods', 10);

        foreach ($shipments as $row) {
            Task::updateLastTicket('ImportShipmethods', 30);
            //Arrayhelpers::toStringToAll($row, 'ImportShipmethods SAP ActRaktarKeszlet  row', __FILE__, __METHOD__, __LINE__);
            $this->shipmethod =   $this->orderItem = $this->hipmethodClass->filledImportFields($row);;
            $sqlshipmethod = ShipMethod::UpdateOrCreate(['erp_id' => $this->shipmethod['erp_id']], $this->shipmethod);
            if ($sqlshipmethod->wasChanged()) {
                Log::debug("Marking paayment dirty.");
                $sqlshipmethod->synced = 0;
                $sqlshipmethod->save();
            }
            Task::updateLastTicket('ImportShipmethods', 40);
            $this->limitCount++;
            if (($this->limitCount > $this->argumentum['limit'] && $this->argumentum['limit'] != 0) || Task::isStopped('ImportShipmethods')) {
                Task::Stop('ImportShipmethods', $this->newCount, $this->refreshCount);
                break;
            }

        }

        Task::Stop('ImportShipmethods', $this->newCount, $this->refreshCount);
    }


}

