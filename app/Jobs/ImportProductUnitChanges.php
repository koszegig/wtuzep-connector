<?php


namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;

use App\Product;
use App\ProductUnitChanges;
use App\Units;
use App\Models\Vectory\CCProductUnitChanges;


use App\Task;
use App\CustomLog;
use DB;
use Log;
use function GuzzleHttp\Promise\all;

/**
 * Class ImportProducts
 * @package App\Jobs
 */
class ImportProductUnitChanges implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount = 0;
    /**
     * @var
     */
    protected $refreshCount = 0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */

    protected $PutNumber = 0;
    /**
     * @var array
     */
    protected $ProductUnitChange = [];
    /**
     * @var
     */
    protected $ProductUnitChangeClass;
    /**
     * @var
     */
    protected $ProductUnitChangeClassFields;


    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum = $argumentum;
        $this->options = $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProducts();
    }


    /**
     *
     */
    public function handleProducts()
    {
        Task::Start('ImportProductUnitChanges');
        $this->ProductUnitChangeClass = new ProductUnitChanges();
        $this->ProductUnitChangeClassFields = $this->ProductUnitChangeClass->getFields();
        Task::updateLastTicket('ImportProductUnitChanges', 000);

        Arrayhelpers::toStringToEcho('Starting ImportProducts');
        Task::updateLastTicket('ImportProductUnitChanges', 10);
        $ccproductunitchanges = new CCProductUnitChanges();
        Task::updateLastTicket('ImportProductUnitChanges', 20);
        Log::info("Fetching products");
        $products = Product::all()->toArray();
        $productIds = [];
        foreach ($products as $item){
            $productIds[$item['cikkszam']] =$item['id'];
        }

        $units = Units::all()->toArray();

        $unitsIds = [];
        foreach ($units as $item){
            $unitsIds[$item['erp_id']] =$item['id'];
        }

        $productunitchanges = $ccproductunitchanges->getFull();
        Log::info("foreach products");
        foreach ($productunitchanges as $row) {
            Task::updateLastTicket('ImportProductUnitChanges', 30);
            // Arrayhelpers::toStringToAll($row, 'ImportProducts SAP actcikk row', __FILE__, __METHOD__, __LINE__);
            $this->ProductUnitChange = [];
            $this->ProductUnitChange = $this->ProductUnitChangeClass->filledImportFields($row);
            $this->ProductUnitChange['product_id'] = $productIds[$this->ProductUnitChange['productid']] ?? 0;
            $this->ProductUnitChange['unit_id'] = $unitsIds[$this->ProductUnitChange['unitid']] ?? 0;
            $product_id = $this->ProductUnitChange['product_id'];
            $unit_id = $this->ProductUnitChange['unit_id'];
            Arrayhelpers::toStringToEcho("$product_id", ' productunitchanges ');

            $sqlProduct = ProductUnitChanges::UpdateOrCreate(['product_id' => $product_id,'unit_id' => $unit_id], $this->ProductUnitChange);
            $this->PutNumber++;
            if ($this->PutNumber > $this->argumentum['limit'] && $this->argumentum['limit'] != 0) {
                break;
            }
            Task::updateLastTicket('ImportProductUnitChanges', 40);
        }

        //} while ($result['isSuccess']);
        Task::Stop('ImportProductUnitChanges', $this->newCount, $this->refreshCount);
    }

}
