<?php


namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Aion\Helpers\ArrayHelpers;

use App\RetailPrice;
use App\Models\Vectory\CCListPrices;
use App\Task;
use DB;
use Log;

/**
 * Class ImportProductPricies
 * @package App\Jobs
 */
class ImportProductPricies implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    protected $argumentum;
    protected $options;
    protected $limitCount=0;
    protected $RetailPrice =[];
    protected $RetailPriceClass;
    protected $RetailPriceClassFields;

    /**
     * ImportProductPricies constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum = [], $options = [])
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProductPricies();
    }


    /**
     *
     */
    public function handleProductPricies()
    {
        Task::Start('ImportProductPricies');
        $this->RetailPriceClass =  new RetailPrice();
        $this->RetailPriceClassFields = $this->RetailPriceClass->getFields();

        $CCListPrices = new CCListPrices();
        $ListPrices = $CCListPrices->getFull();
        dd($ListPrices);
        Task::updateLastTicket('ImportProductPricies', 000);

        Arrayhelpers::toStringToEcho('Starting ImportProductPricies');
        Task::updateLastTicket('ImportProductPricies', 10);
        foreach ($ListPrices as $row) {
            Task::updateLastTicket('ImportProductPricies', 20);
            $this->RetailPrice =  $this->RetailPriceClass->filledImportFields($row);
            RetailPrice::UpdateOrCreate(['sku' => $this->RetailPrice['sku']], $this->RetailPrice);
            Task::updateLastTicket('ImportProductPricies', 40);
        }

        Task::Stop('ImportProductPricies',$this->newCount,$this->refreshCount);
    }

}
