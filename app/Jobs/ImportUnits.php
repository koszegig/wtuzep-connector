<?php


namespace App\Jobs;


use App\Aion\Helpers\Arrayhelpers;
use App\Models\Vectory\CCUnits;
use App\Units;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\CustomLog;
use Log;

class ImportUnits implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var
     */
    protected $argumentum;
    /**
     * @var
     */
    protected $options;
    /**
     * @var
     */

    protected $putNumber =0;
    /**
     * @var array
     */
    protected $unit =[];
    /**
     * @var
     */
    protected $unitClass;
    /**
     * @var
     */
    protected $unitClassFields;


    /**
     * ExportProductModels constructor.
     * @param $argumentum
     * @param $options
     */
    public function __construct($argumentum, $options)
    {
        $this->argumentum =$argumentum;
        $this->options =$options;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->handleProducts();
    }


    /**
     *
     */
    public function handleProducts()
    {
        Task::Start('ImportUnits');
        $this->unitClass =  new Units();
        $this->unitClassFields = $this->unitClass->getFields();
        Task::updateLastTicket('ImportUnits', 000);
        \AionArray::toStringToEcho('Starting ImportProducts');
        Task::updateLastTicket('ImportUnits', 10);
        $ccunits = new CCUnits();

        Task::updateLastTicket('ImportUnits', 20);
        Log::info("Fetching products");
        $units = $ccunits->getFull();

        Log::info("foreach products");
        foreach ($units as $row) {
            Task::updateLastTicket('ImportUnits', 30);
            // Arrayhelpers::toStringToAll($row, 'ImportUnitsimplements SAP actcikk row', __FILE__, __METHOD__, __LINE__);
            $this->unit = [];
            $this->unit = $this->unitClass->filledImportFields($row);
            $erp_id = $this->unit['erp_id'];
            \AionArray::toStringToEcho("$erp_id",' Units ');
            $sqlUnits = Units::UpdateOrCreate(['erp_id' => $erp_id],$this->unit);
            $this->putNumber++;
            if ($this->putNumber>$this->argumentum['limit'] && $this->argumentum['limit'] !=0) {
                break;
            }
            Task::updateLastTicket('ImportUnits', 40);
        }

        //} while ($result['isSuccess']);
        Task::Stop('ImportUnits',$this->newCount,$this->refreshCount);
    }

}

