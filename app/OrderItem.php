<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;

class OrderItem extends BaseConnectorModel
{

    /**
     * @var string
     */
    public $table="order_items";
    protected $tablename='order_items';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'order_items';
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function setFields(){
        $this->fields = collect([

            new Field('id', "int4", true,null, null,true, null),
            new Field('item_id', "Varchar", true,'item_id', null,false, null),
            new Field('order_id', "Varchar", true,'order_id', null,false, null),
            new Field('parent_item_id', "Varchar", true,'parent_item_id', null,false, null),
            new Field('quote_item_id', "Varchar", true,'quote_item_id', null,false, null),
            new Field('store_id', "Varchar", true,'store_id', null,false, null),
            new Field('product_id', "Varchar", true,'product_id', null,false, null),
            new Field('product_type', "Varchar", true,'product_type', null,false, null),
            new Field('product_sku', "Varchar", true,null, null,false, null),
            new Field('product_options', "Varchar", true,'product_options', null,false, null),
            new Field('weight', "Varchar", true,'weight', null,false, null),
            new Field('is_virtual', "Varchar", true,'is_virtual', null,false, null),
            new Field('sku', "Varchar", true,'sku', null,false, null),
            new Field('name', "Varchar", true,'name', null,false, null),
            new Field('description', "Varchar", true,'description', null,false, null),
            new Field('applied_rule_ids', "Varchar", true,'applied_rule_ids', null,false, null),
            new Field('additional_data', "Varchar", true,'additional_data', null,false, null),
            new Field('is_qty_decimal', "Varchar", true,'is_qty_decimal', null,false, null),
            new Field('no_discount', "Varchar", true,'no_discount', null,false, null),
            new Field('qty_backordered', "Varchar", true,'qty_backordered', null,false, null),
            new Field('qty_canceled', "Varchar", true,'qty_canceled', null,false, null),
            new Field('qty_invoiced', "Varchar", true,'qty_invoiced', null,false, null),
            new Field('qty_ordered', "Varchar", true,'qty_ordered', null,false, null),
            new Field('qty_refunded', "Varchar", true,'qty_refunded', null,false, null),
            new Field('qty_shipped', "Varchar", true,'qty_shipped', null,false, null),
            new Field('base_cost', "Varchar", true,'base_cost', null,false, null),
            new Field('price', "Varchar", true,'price', null,false, null),
            new Field('base_price', "Varchar", true,'base_price', null,false, null),
            new Field('original_price', "Varchar", true,'original_price', null,false, null),
            new Field('base_original_price', "Varchar", true,'base_original_price', null,false, null),
            new Field('tax_percent', "Varchar", true,'tax_percent', null,false, null),
            new Field('tax_amount', "Varchar", true,'tax_amount', null,false, null),
            new Field('base_tax_amount', "Varchar", true,'base_tax_amount', null,false, null),
            new Field('tax_invoiced', "Varchar", true,'tax_invoiced', null,false, null),
            new Field('base_tax_invoiced', "Varchar", true,'base_tax_invoiced', null,false, null),
            new Field('discount_percent', "Varchar", true,'discount_percent', null,false, null),
            new Field('discount_amount', "Varchar", true,'discount_amount', null,false, null),
            new Field('base_discount_amount', "Varchar", true,'base_discount_amount', null,false, null),
            new Field('discount_invoiced', "Varchar", true,'discount_invoiced', null,false, null),
            new Field('base_discount_invoiced', "Varchar", true,'base_discount_invoiced', null,false, null),
            new Field('amount_refunded', "Varchar", true,'amount_refunded', null,false, null),
            new Field('base_amount_refunded', "Varchar", true,'base_amount_refunded', null,false, null),
            new Field('row_total', "Varchar", true,'row_total', null,false, null),
            new Field('base_row_total', "Varchar", true,'base_row_total', null,false, null),
            new Field('row_invoiced', "Varchar", true,'row_invoiced', null,false, null),
            new Field('base_row_invoiced', "Varchar", true,'base_row_invoiced', null,false, null),
            new Field('row_weight', "Varchar", true,'row_weight', null,false, null),
            new Field('base_tax_before_discount', "Varchar", true,'base_tax_before_discount', null,false, null),
            new Field('tax_before_discount', "Varchar", true,'tax_before_discount', null,false, null),
            new Field('ext_order_item_id', "Varchar", true,'ext_order_item_id', null,false, null),
            new Field('locked_do_invoice', "Varchar", true,'locked_do_invoice', null,false, null),
            new Field('locked_do_ship', "Varchar", true,'locked_do_ship', null,false, null),
            new Field('price_incl_tax', "Varchar", true,'price_incl_tax', null,false, null),
            new Field('base_price_incl_tax', "Varchar", true,'base_price_incl_tax', null,false, null),
            new Field('row_total_incl_tax', "Varchar", true,'row_total_incl_tax', null,false, null),
            new Field('base_row_total_incl_tax', "Varchar", true,'base_row_total_incl_tax', null,false, null),
            new Field('discount_tax_compensation_amount', "Varchar", true,'discount_tax_compensation_amount', null,false, null),
            new Field('base_discount_tax_compensation_amount', "Varchar", true,'base_discount_tax_compensation_amount', null,false, null),
            new Field('discount_tax_compensation_invoiced', "Varchar", true,'discount_tax_compensation_invoiced', null,false, null),
            new Field('base_discount_tax_compensation_invoiced', "Varchar", true,'base_discount_tax_compensation_invoiced', null,false, null),
            new Field('discount_tax_compensation_refunded', "Varchar", true,'discount_tax_compensation_refunded', null,false, null),
            new Field('base_discount_tax_compensation_refunded', "Varchar", true,'base_discount_tax_compensation_refunded', null,false, null),
            new Field('tax_canceled', "Varchar", true,'tax_canceled', null,false, null),
            new Field('discount_tax_compensation_canceled', "Varchar", true,'discount_tax_compensation_canceled', null,false, null),
            new Field('tax_refunded', "Varchar", true,'tax_refunded', null,false, null),
            new Field('base_tax_refunded', "Varchar", true,'base_tax_refunded', null,false, null),
            new Field('discount_refunded', "Varchar", true,'discount_refunded', null,false, null),
            new Field('base_discount_refunded', "Varchar", true,'base_discount_refunded', null,false, null),
            new Field('free_shipping', "Varchar", true,'free_shipping', null,false, null),
            new Field('gift_message_id', "Varchar", true,'gift_message_id', null,false, null),
            new Field('gift_message_available', "Varchar", true,'gift_message_available', null,false, null),
            new Field('weee_tax_applied', "Varchar", true,'weee_tax_applied', null,false, null),
            new Field('weee_tax_applied_amount', "Varchar", true,'weee_tax_applied_amount', null,false, null),
            new Field('weee_tax_applied_row_amount', "Varchar", true,'weee_tax_applied_row_amount', null,false, null),
            new Field('weee_tax_disposition', "Varchar", true,'weee_tax_disposition', null,false, null),
            new Field('weee_tax_row_disposition', "Varchar", true,'weee_tax_row_disposition', null,false, null),
            new Field('base_weee_tax_applied_amount', "Varchar", true,'base_weee_tax_applied_amount', null,false, null),
            new Field('base_weee_tax_applied_row_amnt', "Varchar", true,'base_weee_tax_applied_row_amnt', null,false, null),
            new Field('base_weee_tax_disposition', "Varchar", true,'base_weee_tax_disposition', null,false, null),
            new Field('base_weee_tax_row_disposition', "Varchar", true,'base_weee_tax_row_disposition', null,false, null),
            new Field('quantity', "Varchar", true,null, null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),

        ]);
    }
}
