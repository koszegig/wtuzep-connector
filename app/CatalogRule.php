<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogRule extends Model
{
    protected $tablename='catalog_rules';
    public $primarykey="id";
    public $timestamps=false;
}
