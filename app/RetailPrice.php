<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;

class RetailPrice extends BaseConnectorModel
{
    protected $tablename='retail_price';
    /**
     * @var string
     */
    public $table="retail_price";

    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'retail_price';
    }

    protected $guarded = [];

    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null, null,  50, true),
            new Field("product_id", "varchar", true,null, null,false, null, null,  80, true),
            new Field("sku", "varchar", true,'ProductId', null,false, 'code', null,  80, true),
            new Field("magento_id", "varchar", true,null, null,false, null, null,  80, true),
            new Field("erp_id", "varchar", true,'ProductId', null,false, null, null,  80, true),
            new Field("tier_price_website", "varchar", true,null, null,false, null, null,  80, true),
            new Field("tier_price_customer_group", "varchar", true,null, null,false, null, null,  80, true),
            new Field("tier_price_qty", "varchar", true,null, null,false, null, null,  80, true),
            new Field("tier_price", "varchar", true,'Price', null,false, null, null,  80, true),
            new Field("tier_price_value_type", "varchar", true,null, null,false, null, null,  80, true),
            new Field("fromdate", "varchar", true,'FromDate', null,false, null, null,  80, true),
            new Field('created_at', "timestamp", true,null, null,false, null, null,  80, false),
            new Field('updated_at', "timestamp", true,null, null,false, null, null,  80, false),
            new Field('synced', "bool", true,null, null,false, null, null,  80, false),
        ]);
    }
}
