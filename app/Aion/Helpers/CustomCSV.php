<?php


namespace App\Aion\Helpers;
use \App\Aion\Helpers\ArrayHelpers;

class CustomCSV
{

    public  $heading	= true;
    public  $delimiter	= ',';
    public  $enclosure	= '"';
    public	$escape		= "\\";
    public  $encode		= false;
    public  $eol		= null;

    private $keys		= [];
    public  $data		= [];
    public  $no			= 0;

    public function auto ($file) {

        $this->no = 0;
        if (is_null($this->eol)) {

            $this->fopenParser($file);

        }
        else {

            $this->splitParser($file);

        }

    }

    private function fopenParser ($file) {

        if (file_exists($file)) {
            $handle = fopen($file, "r");
            if ($handle) {

                while ($data = fgetcsv($handle, 10000, $this->delimiter, $this->enclosure)) {

//					if ($this->no == 1408) print_r($data);
                    $this->processLine($data);

                }

                fclose($handle);
            }
        } else {
            echo 'Not file'.$file;
        }

    }
    private function splitParser ($file) {

        $file = file_get_contents($file);

        switch ($this->eol) {
            case "CR":		$re = '/\r/';		break;
            case "LF":		$re = '/[^\r]\n/';	break;
            case "CRLF":	$re = '/\r\n/';		break;
            default:		$re = '/\r\n/';		break;
        }

        $lines = preg_split($re, $file) ?? [];
        foreach ($lines as $lineNo => $line) {

            $this->processLine($line);

        }

    }

    private function processLine ($data) {

        $skip = false;
        if (!is_array($data)) {
            $data = str_getcsv($data, $this->delimiter, $this->enclosure, $this->escape);
        }

        if (empty($this->keys)) {
            if ($this->heading) {
                $this->keys = array_values($data);
                $skip = true;
            }
            else {
                $this->keys = range(1, count($data));
            }
        }

        $emptyData		= array_fill(0, count($this->keys), '');
        $limitedData	= array_slice($data, 0, count($this->keys));
        $data			= array_replace($emptyData, $limitedData);
        $data			= array_combine($this->keys, $data);

        if ($skip) return;

        $this->data [] = $data;
        $this->no++;

    }


}
