<?php

namespace App\Aion\Helpers;

/**
 * Class Arrayhelpers
 * @package App\Aion\Helpers
 */
class Arrayhelpers
{

    /**
     * @param $array
     */
    public static function toString($array, $title = '')
    {
        if (env('APP_DEBUG','true')) {
            echo($title.'<pre>');
            //print_r($array);
            dump($array);
            echo('</pre>');
        }
    }

    /**
     * @param $array
     * @param string $title
     * @param string $filename __FILE__,
     * @param string $methodName __METHOD__,
     * @param int $line __LINE__
     */
    public static function toLog($array = '',
                                         $filename 	= '',
                                         $methodName	= '',
                                         $line		= '',
                                         $task = '')
    {
        if (env('APP_DEBUG','true')) {
            //\Log::debug($array);
            \Log::stack(['database'])->notice(print_r($array, true). " {$filename} {$methodName} {$line} lines:\n\t");
            //\Log::debug($title . '---->');
            //\Log::debug(print_r($array, true));
            \Log::debug(print_r($array, true). " {$filename} {$methodName} {$line} lines:\n\t");
        }
    }
    /**
     * @param $array
     * @param string $title
     * @param string $filename __FILE__,
     * @param string $methodName __METHOD__,
     * @param int $line __LINE__
     */
    public static function toErrorLog($array = '',
                                 $filename 	= '',
                                 $methodName	= '',
                                 $line		= '',
                                      $task = '')
    {
        if (env('APP_DEBUG','true')) {
            //\Log::debug($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            \Log::stack(['database'])->error(print_r($array, true). " {$filename} {$methodName} {$line} lines:\n\t");
            //\Log::debug($title . '---->');
            \Log::debug(print_r($array, true));
            \Log::debug('<----' . $array . " {$filename} {$methodName} {$line} lines:\n\t");
        }
    }
    /**
     * @param $array
     * @param string $title
     * @param string $filename __FILE__,
     * @param string $methodName __METHOD__,
     * @param int $line __LINE__
     */
    public static function toStringToLog($array, $title = '',
                                         $filename 	= '',
                                         $methodName	= '',
                                         $line		= '',
                                         $task = '')
    {
        if (env('APP_DEBUG','true')) {
            //\Log::debug($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            \Log::stack(['database'])->notice($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            /*\Log::debug($title . '---->');
            \Log::debug(print_r($array, true));*/
            \Log::debug(print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
        }
    }
    /**
     * @param $array
     * @param string $title
     * @param string $filename __FILE__,
     * @param string $methodName __METHOD__,
     * @param int $line __LINE__
     */
    public static function toStringToAll($array, $title = '',
                                         $filename 	= '',
                                         $methodName	= '',
                                         $line		= '',
                                         $task = '')
    {
        if (env('APP_DEBUG','true')) {
            echo($title . '<pre>');
            dump($array);
            echo('</pre>');
            //\Log::debug($title . '---->');
            //\Log::debug($title . '---->',print_r($array, true));
            //\Log::debug($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            \Log::stack(['database'])->notice($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            \Log::debug(print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
        }
    }


    /**
     * @param $msgString
     * @param string $filename
     * @param string $methodName
     * @param string $line
     */
    public static function toStringToEcho($msgString,
                                          $filename 	= '',
                                          $methodName	= '',
                                          $line		= '')
        {
            echo $msgString." {$filename} {$methodName} {$line}  \n\t";
        }
    /**
     * @param $array
     * @param string $title
     * @param string $filename __FILE__,
     * @param string $methodName __METHOD__,
     * @param int $line __LINE__
     */
    public static function toStringToJson($array, $title = '',
                                          $filename 	= '',
                                          $methodName	= '',
                                          $line		= '')
    {
        if (env('APP_DEBUG','true')) {
            echo($title . '<pre>');
            dump(json_encode($array));
            echo('</pre>');
           /* \Log::debug($title . '---->');
            \Log::debug(print_r(json_encode($array), true));
            \Log::debug('<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");*/
            //\Log::debug($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
            \Log::stack(['database'])->notice($title .'---->'.print_r($array, true).'<----' . $title . " {$filename} {$methodName} {$line} lines:\n\t");
        }
    }
    /**
     * @param $prop
     * @param $propVal
     * @param $objectArray
     * @return bool|mixed
     */
    public static function finObjectByProp($prop, $propVal, $objectArray)
    {
        foreach($objectArray as $object){
            if ($propVal == $object->{$prop}) { return $object;
            }
        }
        return false;
    }

    /**
     * @param $key
     * @param $val
     * @param $Array
     * @return bool|mixed
     */
    public static function finElementyByElement($key, $val, $Array)
    {
        foreach($Array as $element){
            if ($val == $element[$key]) { return $element;
            }
        }
        return false;
    }

    /**
     * @param $props
     * @param $objectArray
     * @return bool|mixed
     */
    public static function findElementBypElements($props, $objectArray)
    {
        foreach($objectArray as $object) {
            $equel = 0;
            foreach ($props as $prop => $propVal) {
                if ($propVal != $object[$prop]) { continue(2);
                } else { $equel++;
                }
            }
            if($equel == count($props)) { return $object;
            }
        }
        return false;
    }

    /**
     * @param $key
     * @param $array
     * @param bool $default
     * @return bool|mixed
     */
    public static  function element($key, $array, $default = false)
    {
        return (is_array($array) && array_key_exists($key, $array)) ? $array[$key] : $default;
    }

    /**
     * @param $key
     * @param $val
     * @param $array
     * @param bool $forceNull
     */
    public static  function addElement($key, $val, &$array, $forceNull = false)
    {
        if(!$forceNull && !is_null($val)) { $data[$key] = $val;
        }
    }

    /**
     * @param $keys
     * @param $array
     * @param bool $default
     * @return array
     */
    public static function elements($keys, $array, $default = false)
    {
        $return = [];
        is_array($keys) or $keys = [$keys];

        foreach ($keys as $key) {
            $return[$key] = self::element($key, $array, $default);
        }

        return $return;
    }

    /**
     * @param $array
     * @param $elem
     */
    public static function unset(&$array, $elem)
    {
        if (($key = array_search($elem, $array)) !== false) {
            unset($array[$key]);
        }
    }

    /**
     * @param $array
     * @param $elems
     */
    public static function multiunset(&$array, $elems)
    {
        is_array($elems) or $elems = [$elems];
        foreach($elems as $elem) {
            self::unset($array, $elem);
        }
    }

    /**
     * @param array $arr
     * @return bool
     */
    public static function isAssoc(array $arr)
    {
        if (array() === $arr) { return false;
        }
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * @param $array
     * @return array
     */
    public static function assocToArray($array)
    {
        $arr = [];
        foreach($array as $key => $value){
            if($value == null || $value == '') { continue;
            }
            $arr[] = [$key, $value];
        }
        return $arr;
    }
}
