<?php

namespace App\Aion\Helpers;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class Hashhelpers
{

    public static function reqSignhash($requestID, $timestamp, $signing_key, $base64 = null)
    {
        $hash = $requestID . $timestamp['cleaned'] . $signing_key;
        if (!is_null($base64))
            $hash .= self::reqSignCRC32($base64);
        return strtoupper(hash('sha512', $hash));
    }

    public static function reqSignCRC32($base64)
    {
        $hash = "";
        foreach ($base64 as $value) {
            $hash .= self::crc32($value);
        }
        return $hash;
    }

    public static function crc32($string)
    {
        return sprintf("%u", crc32($string));
    }

    public static function decryptToken($token, $user = null)
    {
        if (is_null($user))
            $user = property('n_user', \MyUser::getCurrentUser());
        $key = base64_decode($user->exchange_key); //die;
        return self::aes128Decrypt($token, $key);
    }

    public static function aes128Decrypt($string, $key)
    {
        return openssl_decrypt($string, 'AES-128-ECB', $key);
    }

    public static function genRequestID()
    {
        $uuid4 = Uuid::uuid4();
        $maxlen = 20;
        $misslen = $maxlen;
        $numbers = self::getmiddle($misslen);
        $uuid = \MyString::clean3($uuid4->toString());
        $uuidpref = substr($uuid, 0, $numbers[0]);
        $uuidsubf = substr($uuid, $numbers[1] * -1);
        return $uuidpref . $uuidsubf;
    }

    private static function getmiddle($number)
    {
        $middle = $number / 2;
        $low = floor($middle);
        $up = ceil($middle);
        return [$low, $up, $low + $up, $number];
    }

    /**
     * Get a new version 4 (random) UUID.
     *
     * @return \Rhumsaa\Uuid\Uuid
     */
    public static function genUUID()
    {
        $uuid4 = Uuid::uuid4();
        return $uuid4->toString();
    }
}
