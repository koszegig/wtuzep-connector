<?php

namespace App\Aion\Helpers;
use \App\Aion\Helpers\ArrayHelpers;
use Input;
use Artisan;
use Log;
use Illuminate\Support\Arr;
use App\Exceptions\FileNotExits;
class Filehelpers {

    public static function writetofile($dir,$filename,$data){
		self::createDir(storage_path($dir));
	    \File::put(storage_path("{$dir}/{$filename}"), $data);
  	}

    public static function createDir($dir){
    	if (file_exists ( $dir )) return;
     	mkdir ($dir,0775,true);
  	}
    public static function moveFile($file,$destination)

    {
        if(\File::exists($file)){

            \Storage::copy($file,$destination);

        }else{

            dd('File does not exists.');

        }

    }
  	public static function readFromfile($path){
		if (!file_exists ( $path )){
            Log::error($path.' File Not found');
            return '';
        }
		return \File::get($path);
  	}
    public static function diferentfiles($sourceFiles,$destFiles){
        $srcFiles = [];
        if (count($sourceFiles) > 0 ) {
            foreach ($sourceFiles as $_file) {
                $parts = pathinfo($_file);
                $srcFiles [$parts['filename']] = $_file;
            }
        }
        $dstFiles = [];
        if (count($destFiles) > 0 ) {
            foreach ($destFiles as $_file) {
                $parts = pathinfo($_file);
                $dstFiles [$parts['filename']] = $_file;
            }
        }
        $result = array_diff_key($dstFiles, $srcFiles);
        /*Arrayhelpers::toStringToLog($dstFiles,'$dstFiles - k',__FILE__, __METHOD__, __LINE__ );
        Arrayhelpers::toStringToLog($srcFiles,'$srcFiles - k',__FILE__, __METHOD__, __LINE__ );
        Arrayhelpers::toStringToLog($result,'$result - k',__FILE__, __METHOD__, __LINE__ );*/


/*        if ((count($srcFiles) > 0 ) && (count($dstFiles) == 0 ) ) {
            $result = $srcFiles;
        }*/
        ksort($result);
      /*  Arrayhelpers::toStringToLog($srcFiles,'$dstFiles - kimenő',__FILE__, __METHOD__, __LINE__ );
        Arrayhelpers::toStringToLog($srcFiles,'$srcFiles - forrás',__FILE__, __METHOD__, __LINE__ );
        Arrayhelpers::toStringToLog($result,'$result - kimenő',__FILE__, __METHOD__, __LINE__ );*/
        /*foreach ($result as $_file) {
            Arrayhelpers::toStringToLog(Arr::last(explode("_", $parts['filename'])), 'ddsdesult - k', __FILE__, __METHOD__, __LINE__);
        }
        Arrayhelpers::toStringToLog($result,'$result - k',__FILE__, __METHOD__, __LINE__ );*/
        return $result;
    }
    public static function sort_files($sourceFiles){
        $result = [];
        if (count($sourceFiles) > 0 ) {
            foreach ($sourceFiles as $_file) {
                $parts = pathinfo($_file);
                $result [Arr::last(explode("_", $parts['filename']))] = $_file;
            }
        }
        ksort($result);
        return $result;
    }
}
