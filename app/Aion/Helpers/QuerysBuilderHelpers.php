<?php

namespace App\Aion\Helpers;

class QueryBuilderhelpers
{

  public static function generateWhere()
  {
    $where = 'WHERE 1=1';
    $filters =  array_get($_GET, 'filter', []);
    if (empty($filters)) {
      return $where;
    }
    foreach ($filters as $key => $value) {
      if (empty($value) || $value == '' || is_null($value)) {
        continue;
      }

      $value = str_replace(' ', '%%', $value);
      $where .= " AND LOWER({$key}) LIKE LOWER('%{$value}%')";
    }
    return $where;
  }

  public static function generateCfilter()
  {
    $filters = array_get($_GET, 'cfilter', []);
  //  \MyArray::toStringToLog($filters);
    $cfilter = explode(',', $filters);
  //  \MyArray::toStringToLog($cfilter);
    return $cfilter;
  }
}
