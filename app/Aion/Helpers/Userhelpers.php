<?php

namespace  App\Aion\Helpers;

use Input;
use JWTAuth;
use Auth;
use App\User;

class Userhelpers
{

  public static function getme($_user = null)
  {
    //$user = Auth::user();
    $user = self::getCurrentUser();
    if (empty($user)) {
      $user = $_user;
    }
    $token = JWTAuth::fromUser($user);
    // $user->only('id','firstname','lastname','email');
    // $user->getAllPermissions();
    // $user->getRoleNames();
    list($abilities, $userRole, $userGroups) = self::generateAbilities($user);

    //$user->getEmailAddresses();
   // $user->getPhoneNumbers();
    $user = $user->only('id', 'name', 'firstname', 'lastname');
    //\MyArray::toStringToLog($user);
    return compact('user', 'token', 'abilities', 'userRole', 'userGroups');
  }

  public static function getmeapp($_user = null)
  {
    //$user = Auth::user();
    $user = self::getCurrentUser();
    if (empty($user)) {
      $user = $_user;
    }
    $token = JWTAuth::fromUser($user);
    $user = $user->only('id', 'firstname', 'lastname');
    return compact('user', 'token');
  }

  private static function generateAbilities($user)
  {
    $user->getAllPermissions();
    $roles = $user->getRoleNames()->toArray();
    $groups = $user->getGroupNames()->toArray();
    $abilities = [];
    self::generateRoles($user->roles, $abilities, $roles);

    foreach ($user->groups as $group) {
      $group->getAllPermissions();
      $roles = array_merge($roles, $group->getRoleNames()->toArray());
      self::generateRoles($group->roles, $abilities, $roles);
    }
    return [$abilities, $roles, $groups];
  }

  private static function generateRoles($_roles, &$abilities, &$roles)
  {
    foreach ($_roles as $role) {
      $abilities[$role->name] = [];
      foreach ($role->permissions as $permission) {
        $abilities[$role->name][] = $permission->slug;
      }
    }
  }

  public static function getCurrentUser()
  {
    $user = Auth::user();
    // \MyArray::toStringToLog($user->toArray());
    return $user;
  }

  public static function changeCurgrade($gradeID)
  {
    $user = self::getCurrentUser();
    $user->setGrades($gradeID);
    return $user;
  }

  public static function isDeveloper()
  {
    $current = \MyUser::getCurrentUser();
    $slugs = array_pluck($current->roles, 'slug');
    return in_array('admin.developer', $slugs);
  }

  public static function refreshToken()
  {
    $refreshed = JWTAuth::refresh(JWTAuth::getToken());
    $user = JWTAuth::setToken($refreshed)->authenticate();
    return $refreshed;
  }



  public static function refreshTokenFromUser($user)
  {
    $token = \JWTAuth::fromUser($user);
    $refreshed = JWTAuth::refresh($token);
    $user = JWTAuth::setToken($refreshed)->authenticate();
    return $refreshed;
  }

  public static function updatePwToken($empty = false)
  {
    $data =  [
      'password_reset_token' => null,
      'password_reset_token_expired' => null
    ];
    if (!$empty) {
      $data['password_reset_token'] = str_random(60);
      $data['password_reset_token_expired'] = \MyDate::generatePasswordResetTokenExpired();
    }
    return $data;
  }

  public static function getCurrentUserOrg()
  {
    $user = self::getCurrentUser();
    return ($user->isSuperadmin()) ? 'all' : $user->organization_id;
  }

  public static function getUserID()
  {
    $user = self::getCurrentUser();
    if (is_null($user)) {
      $user = self::selectSystemUser();
    }
    return $user->id;
  }

  public static function selectSystemUser()
  {
    return User::where('system', true)->first();
  }
}
