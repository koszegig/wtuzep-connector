<?php

namespace App\Aion\Connect;

use GuzzleHttp\Client;
use Log;

class MagentoModuleApi {
    private $token = "";

    public function __construct()
    {
        ini_set('memory_limit', '1024M');

        $this->token = env("AION_CONNECT_MAGENTO_API_TOKEN", "1234" );
    }

    public function debug($msg, $level = "INFO") {
        echo $msg ."\n";
        Log::info($msg);
    }

    public function createJson($method, $collection) {
        return array(
            "token" => $this->token,
            "request" => $method,
            "collection" => $collection
        );
    }
    public function encodeJson($array) {
        return json_encode($array,JSON_PRETTY_PRINT );
    }

    public function writeJson($filename, $content) {
        file_put_contents("/tmp/$filename.json", $content);
    }

    public function postJson($method, $array) {
        $client = new Client();
        $url = env("AION_CONNECT_MAGENTO_API_BASE_URL" ) . "/" . $method;

        $this->debug("Posting to $url");
        echo "\n\n======\n\n ". json_encode($array) ."\n\n======\n\n";

        $response = $client->post($url, [
            \GuzzleHttp\RequestOptions::JSON => $array
        ]);

        $status = $response->getStatusCode();

        if ( $status == 200) {
            $data = json_decode($response->getBody(), true);
            // file_put_contents("/tmp/resp.json", $response->getBody());

            if ( is_array($data) && array_key_exists("STATUS", $data) ) {
                $this->debug("Post results: ". $data['STATUS']);
                return $data;
            }  else {
                $this->debug("Post results is unknown");
                return false;
            }
        } else {
            $this->debug("ERROR Response code: $status");
            return false;
        }

    }
}
?>

