<?php

namespace App\Aion\Connect;

use App\Product;
use Illuminate\Support\Facades\DB;

class SyncStat
{

    public $object_types = ['product'];
    public $tables = ["products", "pictures", "categories", "partners", "orders_head"];

    function getSyncStats()
    {
        $rows = [];
        foreach ($this->tables as $table ) {
            $s = array();

            $row=[];
            $s['total'] = DB::table($table)->select(DB::raw('count(*) as total'))->get()->toArray();

            $s['sync_stat'] = DB::table($table)
                ->select('synced', DB::raw('count(*) as total'))
                ->groupBy('synced')
                ->orderBy("synced")
                ->get()->toArray();

            // pivot
            $row["table"] = $table;
            $row["total"] = ($s['total'][0]->total);

            $status = [];
            foreach ($s['sync_stat'] as $r) {
            //    $row["synced_". $r->synced] = $r->total;
                $status[$r->synced] = $r->total;
            }

            foreach ([0,1,2,3,4,5,6,7,8,9] as $syncstatus) {
                $key = 'synced_'. $syncstatus;

                if ( array_key_exists($syncstatus, $status)) {
                    $value = $status[$syncstatus];
                } else {
                    $value = 0;
                }
                if ( $syncstatus > 1 ) {
                    @$row['errors'] += $value;
                } else {
                    @$row[$key] = $value;
                }
            }

            $rows[]=$row;
        }
        return $rows;
    }

    public function getStats() {
        return json_decode(json_encode($this->getSyncStats()), True);
    }


}

?>
