<?php


namespace App\Aion\Library;
use App\Aion\Library\FTPManager;
use Illuminate\Support\Facades\Storage;
use \App\Aion\Helpers\ArrayHelpers;
use \App\Aion\Helpers\Filehelpers;
use Illuminate\Support\Arr;
use Log;


/**
 * Class FileManager
 * @package App\Aion\Library
 */
class FileManager
{
    /**
     * @param string $filesFilter
     * @return array
     */
    public function getNewProductFile($filesFilter= ''){
        if ($filesFilter =='') {
            $filesFilter  = env('IMPORTPRODUCTFILTER', 'cikk');
        }
        $result = [ 'isSuccess' => true,
            'message' => '',
            'data' => '',
            'path' => ''

        ];
        $ftpmanager = new FTPManager();
        $ftpmanager->downloadCSVFiles(env('IMPORTPRODUCTPATH', ''),env('IMPORTPRODUCTREMOTEPATH', ''),$filesFilter);
        $sourcepath = env('IMPORTPRODUCTPATH', 'product/');
        $destpath = env('IMPORTPRODUCTPATH', 'product/').env('IMPORTREADYDIR', 'ready/');
        Filehelpers::createDir(storage_path($destpath));
        $destFiles = Storage::disk('csv')->files($destpath);
        if ($filesFilter !='') {
            $filesFilterDestc = $destpath . $filesFilter;
            $fitlered_files = array_filter($destFiles, function($str) use ($filesFilterDestc){
                return strpos($str, $filesFilterDestc) === 0;
            });
            $destFiles =  $fitlered_files;
        }
        //Arrayhelpers::toStringToLog($destFiles,'newProductFile - $destFiles');
        $sourceFiles = Storage::disk('csv')->files($sourcepath);

        if ($filesFilter !='') {
            $filesFilterSRC = $sourcepath . $filesFilter;
            $fitlered_files = array_filter($sourceFiles, function($str) use ($filesFilterSRC){
                return strpos($str, $filesFilterSRC) === 0;
            });
            $sourceFiles =  $fitlered_files;
        }
        //Arrayhelpers::toStringToLog($sourceFiles,'newProductFile - $sourceFiles');
        //$diferentFiles = Filehelpers::diferentfiles($sourceFiles,$destFiles);
        $diferentFiles = Filehelpers::sort_files($sourceFiles);
        $_path = Arr::first($diferentFiles);
        //Arrayhelpers::toStringToLog($result,'newProductFile - result');
        if ($_path == '') {
            $result['isSuccess'] = false;
            return $result;
        }
        $path = storage_path($_path);
        $CSVmanage =  new CSVManager();
        $_result = $CSVmanage->convertCSV($path);
        if ($_result['isSuccess']) {
            $result['data'] = $_result['data'];
            $result['path'] = $path;
            return $result;
        } else {
            echo $_result['message'];
            $result['isSuccess'] = $_result;
            return $result;
        }
    }

    /**
     *
     */
    public function sendNewProductFile(){
        $ftpmanager = new FTPManager();
        $ftpmanager->uploadXMLFiles(env('IMPORTPRODUCTPATH', '').env('IMPORTREADYDIR', 'ready/'),env('IMPORTPRODUCTREMOTEPATH', '').env('FTP_READYDIR', 'ready/'));
    }
    /**
     * @param $path
     */
    public function sendDeleteProductFFile($path){
        $ftpmanager = new FTPManager();
        $ftpmanager->deleteXMLFiles($path);
    }
    /**
     *
     */
    public function sendNewOrderFile(){
        $ftpmanager = new FTPManager();
        $ftpmanager->uploadCSVFiles(env('EXPORTDIR', 'from/'),env('EXPORTREMOTEDIR', ''));
    }
    /**
     *
     */
    public function sendNewPartnerFile(){
        $ftpmanager = new FTPManager();
        $ftpmanager->uploadCSVFiles(env('EXPORTDIR', 'from/'),env('EXPORTREMOTEDIR', ''));
    }


}
