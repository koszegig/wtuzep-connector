<?php


namespace App\Aion\Library;


/**
 * Class CompareOrder
 * @package App\Aion\Library
 */
class CompareOrder
{/**
 * @param $magentoOrder
 * @return array
 */
    public function compareImportHeader($magentoOrder)
    {   $data = [];
        $data['order_id'] = $magentoOrder->entity_id;
        $data['status'] = $magentoOrder->status ?? '';
        $data['state'] = $magentoOrder->state ?? '';
        $data['coupon_code'] = $magentoOrder->coupon_code ?? '';
        $data['protect_code'] = $magentoOrder->protect_code ?? '';
        $data['shipping_description'] = $magentoOrder->shipping_description ?? '';
        $data['is_virtual'] = $magentoOrder->is_virtual ?? 0;
        $data['store_id'] = $magentoOrder->store_id ?? 0;
        $data['customer_id'] = $magentoOrder->customer_id ?? 0;
        $data['base_discount_amount'] = $magentoOrder->base_discount_amount ?? 0;
        $data['base_discount_canceled'] = $magentoOrder->base_discount_canceled ?? '';
        $data['base_discount_invoiced'] = $magentoOrder->base_discount_invoiced ?? '';
        $data['base_discount_refunded'] = $magentoOrder->base_discount_refunded ?? '';
        $data['base_grand_total'] = $magentoOrder->base_grand_total ?? '';
        $data['base_shipping_amount'] = $magentoOrder->base_shipping_amount ?? 0;
//          $data['base_shipping_canceled'] = $magentoOrder->base_shipping_canceled ?? '';
//          $data['base_shipping_invoiced'] = $magentoOrder->base_shipping_invoiced ?? '';
        $data['base_shipping_refunded'] = $magentoOrder->base_shipping_refunded ?? '';
        $data['base_shipping_tax_amount'] = $magentoOrder->base_shipping_tax_amount ?? 0;
        $data['base_shipping_tax_refunded'] = $magentoOrder->base_shipping_tax_refunded ?? 0;
        $data['base_subtotal'] = $magentoOrder->base_subtotal ?? '';
        $data['base_subtotal_canceled'] = $magentoOrder->base_subtotal_canceled ?? '';
        $data['base_subtotal_invoiced'] = $magentoOrder->base_subtotal_invoiced ?? '';
        $data['base_subtotal_refunded'] = $magentoOrder->base_subtotal_refunded ?? '';
        $data['base_tax_amount'] = $magentoOrder->base_tax_amount ?? '';
        $data['base_tax_canceled'] = $magentoOrder->base_tax_canceled ?? '';
        $data['base_tax_invoiced'] = $magentoOrder->base_tax_invoiced ?? '';
        $data['base_tax_refunded'] = $magentoOrder->base_tax_refunded ?? '';
        $data['base_to_global_rate'] = $magentoOrder->base_to_global_rate ?? '';
        $data['base_to_order_rate'] = $magentoOrder->base_to_order_rate ?? '';
        $data['base_total_canceled'] = $magentoOrder->base_total_canceled ?? '';
        $data['base_total_invoiced'] = $magentoOrder->base_total_invoiced ?? '';
        $data['base_total_invoiced_cost'] = $magentoOrder->base_total_invoiced_cost ?? '';
        $data['base_total_offline_refunded'] = $magentoOrder->base_total_offline_refunded ?? '';
        $data['base_total_online_refunded'] = $magentoOrder->base_total_online_refunded ?? '';
        $data['base_total_paid'] = $magentoOrder->base_total_paid ?? '';
        $data['base_total_qty_ordered'] = $magentoOrder->base_total_qty_ordered ?? '';
        $data['base_total_refunded'] = $magentoOrder->base_total_refunded ?? '';
        $data['discount_amount'] = $magentoOrder->discount_amount ?? '';
        $data['discount_canceled'] = $magentoOrder->discount_canceled ?? '';
        $data['discount_invoiced'] = $magentoOrder->discount_invoiced ?? '';
        $data['discount_refunded'] = $magentoOrder->discount_refunded ?? '';
        $data['grand_total'] = $magentoOrder->grand_total ?? '';
        $data['shipping_amount'] = $magentoOrder->shipping_amount ?? '';
        $data['shipping_canceled'] = $magentoOrder->shipping_canceled  ?? '';
        $data['shipping_invoiced'] = $magentoOrder->shipping_invoiced ?? '';
        $data['shipping_refunded'] = $magentoOrder->shipping_refunded ?? '';
        $data['shipping_tax_amount'] = $magentoOrder->shipping_tax_amount ?? '';
        $data['shipping_tax_refunded'] = $magentoOrder->shipping_tax_refunded ?? '';
        $data['store_to_base_rate'] = $magentoOrder->store_to_base_rate ?? '';
        $data['store_to_order_rate'] = $magentoOrder->store_to_order_rate ?? '';
        $data['subtotal'] = $magentoOrder->subtotal ?? '';
        $data['subtotal_canceled'] = $magentoOrder->subtotal_canceled ?? '';
        $data['subtotal_invoiced'] = $magentoOrder->subtotal_invoiced ?? '';
        $data['subtotal_refunded'] = $magentoOrder->subtotal_refunded ?? '';
        $data['tax_amount'] = $magentoOrder->tax_amount ?? '';
        $data['tax_canceled'] = $magentoOrder->tax_canceled ?? '';
        $data['tax_invoiced'] = $magentoOrder->tax_invoiced ?? '';
        $data['tax_refunded'] = $magentoOrder->tax_refunded ?? '';
        $data['total_canceled'] = $magentoOrder->total_canceled ?? '';
        $data['total_invoiced'] = $magentoOrdertotal_invoiced ?? '';
        $data['total_offline_refunded'] = $magentoOrder->total_offline_refunded ?? '';
        $data['total_online_refunded'] = $magentoOrder->total_online_refunded ?? '';
        $data['total_paid'] = $magentoOrder->total_paid ?? '';
        $data['total_qty_ordered'] = $magentoOrder->total_qty_ordered ?? '';
        $data['total_refunded'] = $magentoOrder->total_refunded ?? '';
        $data['can_ship_partially'] = $magentoOrder->can_ship_partially ?? '';
        $data['can_ship_partially_item'] = $magentoOrder->can_ship_partially_item ?? '';
        $data['customer_is_guest'] = $magentoOrder->customer_is_guest ?? '';
        $data['customer_note_notify'] = $magentoOrder->customer_note_notify ?? '';
        $data['billing_address_id'] = $magentoOrder->billing_address_id ?? '';
        $data['customer_group_id'] = $magentoOrder->customer_group_id ?? '';
        $data['edit_increment'] = $magentoOrder->edit_increment ?? '';
        $data['email_sent'] = $magentoOrder->email_sent ?? '';
        $data['send_email'] = $magentoOrder->send_email ?? '';
        $data['forced_shipment_with_invoice'] = $magentoOrderforced_shipment_with_invoice ?? '';
        $data['payment_auth_expiration'] = $magentoOrder->payment_auth_expiration ?? '';
        $data['quote_address_id'] = $magentoOrder->quote_address_id ?? '';
        $data['quote_id'] = $magentoOrder->quote_id ?? '';
        $data['shipping_address_id'] = $magentoOrder->shipping_address_id ?? '';
        $data['adjustment_negative'] = $magentoOrder->adjustment_negative ?? '';
        $data['adjustment_positive'] = $magentoOrder->adjustment_positive ?? '';
        $data['base_adjustment_negative'] = $magentoOrder->base_adjustment_negative ?? '';
        $data['base_adjustment_positive'] = $magentoOrder->base_adjustment_positive ?? '';
        $data['base_shipping_discount_amount'] = $magentoOrder->base_shipping_discount_amount ?? '';
        $data['base_subtotal_incl_tax'] = $magentoOrder->base_subtotal_incl_tax ?? '';
        $data['base_total_due'] = $magentoOrder->base_total_due ?? '';
        $data['payment_authorization_amount'] = $magentoOrder->payment_authorization_amount ?? '';
        $data['shipping_discount_amount'] = $magentoOrder->shipping_discount_amount ?? '';
        $data['subtotal_incl_tax'] = $magentoOrder->subtotal_incl_tax ?? '';
        $data['total_due'] = $magentoOrder->total_due ?? '';
        $data['weight'] = $magentoOrder->weight ?? '';
        $data['customer_dob'] = $magentoOrder->customer_dob ?? '';
        $data['increment_id'] = $magentoOrder->increment_id ?? '';
        $data['base_currency_code'] = $magentoOrder->base_currency_code ?? '';
        $data['customer_email'] = $magentoOrder->customer_email ?? '';
        $data['customer_firstname'] = $magentoOrder->customer_firstname ?? '';
        $data['customer_lastname'] = $magentoOrder->customer_lastname ?? '';
        $data['customer_middlename'] = $magentoOrder->customer_middlename ?? '';
        $data['customer_prefix'] = $magentoOrder->customer_prefix ?? '';
        $data['customer_suffix'] = $magentoOrder->customer_suffix ?? '';
        $data['customer_taxvat'] = $magentoOrder->customer_taxvat ?? '';
        $data['discount_description'] = $magentoOrder->discount_description ?? '';
        $data['ext_customer_id'] = $magentoOrder->ext_customer_id ?? '';
        $data['ext_order_id'] = $magentoOrder->ext_order_id ?? '';
        $data['global_currency_code'] = $magentoOrder->global_currency_code ?? '';
        $data['hold_before_state'] = $magentoOrder->hold_before_state ?? '';
        $data['hold_before_status'] = $magentoOrder->hold_before_status ?? '';
        $data['order_currency_code'] = $magentoOrder->order_currency_code ?? '';
        $data['original_increment_id'] = $magentoOrder->original_increment_id ?? '';
        $data['relation_child_id'] = $magentoOrder->relation_child_id ?? '';
        $data['relation_child_real_id'] = $magentoOrder->relation_child_real_id ?? '';
        $data['relation_parent_id'] = $magentoOrder->relation_parent_id ?? '';
        $data['relation_parent_real_id'] = $magentoOrder->relation_parent_real_id ?? '';
        $data['remote_ip'] = $magentoOrder->remote_ip ?? '';
        $data['shipping_method'] = $magentoOrder->shipping_method ?? '';
        $data['store_currency_code'] = $magentoOrder->store_currency_code ?? '';
        $data['x_forwarded_for'] = $magentoOrder->x_forwarded_for ?? '';
        $data['customer_note'] = $magentoOrder->customer_note ?? '';
        $data['total_item_count'] = $magentoOrder->total_item_count ?? '';
        $data['customer_gender'] = $magentoOrder->customer_gender ?? '';
        $data['discount_tax_compensation_amount'] = $magentoOrder->discount_tax_compensation_amount ?? '';
        $data['base_discount_tax_compensation_amount'] = $magentoOrder->base_discount_tax_compensation_amount ?? '';
        $data['shipping_discount_tax_compensation_amount'] = $magentoOrder->shipping_discount_tax_compensation_amount ?? '';
        $data['base_shipping_discount_tax_compensation_amnt'] = $magentoOrder->base_shipping_discount_tax_compensation_amnt ?? '';
        $data['discount_tax_compensation_invoiced'] = $magentoOrder->discount_tax_compensation_invoiced ?? '';
        $data['base_discount_tax_compensation_invoiced'] = $magentoOrder->base_discount_tax_compensation_invoiced ?? '';
        $data['discount_tax_compensation_refunded'] = $magentoOrder->discount_tax_compensation_refunded ?? '';
        $data['base_discount_tax_compensation_refunded'] = $magentoOrder->base_discount_tax_compensation_refunded ?? '';
        $data['shipping_incl_tax'] = $magentoOrder->shipping_incl_tax ?? '';
        $data['base_shipping_incl_tax'] = $magentoOrder->base_shipping_incl_tax ?? '';
        $data['coupon_rule_name'] = $magentoOrder->coupon_rule_name ?? '';
        $data['paypal_ipn_customer_notified'] = $magentoOrder->paypal_ipn_customer_notified ?? '';
        return $data;
    }
    /**
     * @param $magentoOrder
     * @return array
     */
    public function compareExportHeader($magentoOrder)
    {   $data = [];
        $data['vat_number'] = $magentoOrder->vat_number ?? 0;
        return $data;
    }
    /**
     * @param $magentoOrder
     * @return array
     */
    public function compareExportCustomer($customer)
    {   $data = [];
        $data['ugyfel_nev'] = $customer["name"] ?? '';
        $data['ugyfel_kod'] =  3;
        $data['Vevo_szallito'] =  'v';
        $data['Iranyitoszam'] = $customer["billing_zip_code"] ?? '';
        $data['Telepulés'] = $customer["billing_city"] ?? '';
        $data['Utca_hazszam'] = $customer["shipping_street"] ?? '';

        $data['Kozterulet'] =
        $data['Kozterulet_jellege'] =
        $data['Hazszam'] ='';
        $data['Epulet'] ='';
        $data['Lepcsohaz'] ='';
        $data['Emelet'] ='';
        $data['Ajto'] ='';
        $data['Orszag'] ='';
        $data['Telefon'] ='';
        $data['Fax_Skype'] ='';
        $data['E_mail'] = $customer["emailr"] ?? '';
        $data['Weboldal'] ='';
        $data['Fizetesi_mod'] ='';
        $data['Fizetesi_hatarido'] = '';
        $data['Adoszam'] = $customer["vat_number"] ?? '';
        $data['EU_adoszam'] = '';
        $data['Bankszamlaszam'] = '';
        $data['IBAN'] = '';
        $data['Swift'] = '';
        $data['Ugyintezo_nev'] = '';
        $data['Ugyintezo_telefon'] = '';
        $data['Ugyintezo_e_mail'] = '';
        $data['Megjegyzes'] = '';
        $data['Engedmeny_Felar'] = '';
        $data['Arkategoria_kod'] = '';
        return $data;
    }

    /**
     * @param $OrderItem
     * @return array
     */
    public function compareExportItem($OrderItem){
        $data = [];
        $data['cikkszám'] = $OrderItem->sku ?? '';
        $data['Megnevezés'] = $OrderItem->name ?? '';
        $data['mennyiség'] = $OrderItem->qty_ordered ?? '';
        $data['netto_lista_eladásiár'] = $OrderItem->price ?? '';
        return $data;
    }

    /**
     * @param $OrderItem
     * @return array
     */
    public function compareImportItem($OrderItem){
        $data = [];
        $data['item_id'] = $OrderItem->item_id ?? 0;
        $data['order_id'] = $OrderItem->order_id ?? '';
        $data['parent_item_id'] = $OrderItem->parent_item_id ?? '';
        $data['quote_item_id'] = $OrderItem->quote_item_id ?? '';
        $data['store_id'] = $OrderItem->store_id ?? '';
        $data['product_id'] = $OrderItem->product_id ?? '';
        $data['product_type'] = $OrderItem->product_type?? '';
        $data['product_options'] = $OrderItem->product_options ?? '';
        $data['weight'] = $OrderItem->weight ?? '';
        $data['is_virtual'] = $OrderItem->is_virtual ?? '';
        $data['sku'] = $OrderItem->sku ?? '';
        $data['name'] = $OrderItem->name ?? '';
        $data['description'] = $OrderItem->description ?? '';
        $data['applied_rule_ids'] = $OrderItem->applied_rule_ids ?? '';
        $data['additional_data'] = $OrderItem->additional_data ?? '';
        $data['is_qty_decimal'] = $OrderItem->is_qty_decimal ?? '';
        $data['no_discount'] = $OrderItem->no_discount ?? '';
        $data['qty_backordered'] = $OrderItem->qty_backordered ?? '';
        $data['qty_canceled'] = $OrderItem->qty_canceled ?? '';
        $data['qty_invoiced'] = $OrderItem->qty_invoiced ?? '';
        $data['qty_ordered'] = $OrderItem->qty_ordered ?? '';
        $data['qty_refunded'] = $OrderItem->qty_refunded ?? '';
        $data['qty_shipped'] = $OrderItem->qty_shipped ?? '';
        $data['base_cost'] = $OrderItem->base_cost ?? '';
        $data['price'] = $OrderItem->price ?? '';
        $data['base_price'] = $OrderItem->base_price ?? '';
        $data['original_price'] = $OrderItem->original_price ?? '';
        $data['base_original_price'] = $OrderItem->base_original_price ?? '';
        $data['tax_percent'] = $OrderItem->tax_percent ?? '';
        $data['tax_amount'] = $OrderItem->tax_amount ?? '';
        $data['base_tax_amount'] = $OrderItem->base_tax_amount ?? '';
        $data['tax_invoiced'] = $OrderItem->tax_invoiced ?? '';
        $data['base_tax_invoiced'] = $OrderItem->base_tax_invoiced ?? '';
        $data['discount_percent'] = $OrderItem->discount_percent ?? '';
        $data['discount_amount'] = $OrderItem->discount_amount ?? '';
        $data['base_discount_amount'] = $OrderItem->base_discount_amount ?? '';
        $data['discount_invoiced'] = $OrderItem->discount_invoiced ?? '';
        $data['base_discount_invoiced'] = $OrderItem->base_discount_invoiced ?? '';
        $data['amount_refunded'] = $OrderItem->amount_refunded ?? '';
        $data['base_amount_refunded'] = $OrderItem->base_amount_refunded;
        $data['row_total'] = $OrderItem->row_total ?? '';
        $data['base_row_total'] = $OrderItem->base_row_total ?? '';
        $data['row_invoiced'] = $OrderItem->row_invoiced ?? '';
        $data['base_row_invoiced'] = $OrderItem->base_row_invoiced ?? '';
        $data['row_weight'] = $OrderItem->row_weight ?? '';
        $data['base_tax_before_discount'] = $OrderItem->row_weight ?? '';
        $data['tax_before_discount'] = $OrderItem->tax_before_discount ?? '';
        $data['ext_order_item_id'] = $OrderItem->ext_order_item_id ?? '';
        $data['locked_do_invoice'] = $OrderItem->locked_do_invoice ?? '';
        $data['locked_do_ship'] = $OrderItem->locked_do_ship ?? '';
        $data['price_incl_tax'] = $OrderItem->price_incl_tax ?? '';
        $data['base_price_incl_tax'] = $OrderItem->base_price_incl_tax ?? '';
        $data['row_total_incl_tax'] = $OrderItem->row_total_incl_tax ?? '';
        $data['base_row_total_incl_tax'] = $OrderItem->base_row_total_incl_tax ?? '';
        $data['discount_tax_compensation_amount'] = $OrderItem->discount_tax_compensation_amount ?? '';
        $data['base_discount_tax_compensation_amount'] = $OrderItem->base_discount_tax_compensation_amount ?? '';
        $data['discount_tax_compensation_invoiced'] = $OrderItem->discount_tax_compensation_invoiced ?? '';
        $data['base_discount_tax_compensation_invoiced'] = $OrderItem->base_discount_tax_compensation_invoiced ?? '';
        $data['discount_tax_compensation_refunded'] = $OrderItem->discount_tax_compensation_refunded ?? '';
        $data['base_discount_tax_compensation_refunded'] = $OrderItem->base_discount_tax_compensation_refunded ?? '';
        $data['tax_canceled'] = $OrderItem->tax_canceled ?? '';
        $data['discount_tax_compensation_canceled'] = $OrderItem->discount_tax_compensation_canceled ?? '';
        $data['tax_refunded'] = $OrderItem->tax_refunded ?? '';
        $data['base_tax_refunded'] = $OrderItem->base_tax_refunded ?? '';
        $data['discount_refunded'] = $OrderItem->discount_refunded ?? '';
        $data['base_discount_refunded'] = $OrderItem->base_discount_refunded ?? '';
        $data['free_shipping'] = $OrderItem->free_shipping ?? '';
        $data['gift_message_id'] = $OrderItem->gift_message_id ?? '';
        $data['gift_message_available'] = $OrderItem->gift_message_available ?? '';
        $data['weee_tax_applied'] = $OrderItem->weee_tax_applied ?? '';
        $data['weee_tax_applied_amount'] = $OrderItem->weee_tax_applied_amount ?? '';
        $data['weee_tax_applied_row_amount'] = $OrderItem->weee_tax_applied_row_amount ?? '';
        $data['weee_tax_disposition'] = $OrderItem->weee_tax_disposition ?? '';
        $data['weee_tax_row_disposition'] = $OrderItem->weee_tax_row_disposition ?? '';
        $data['base_weee_tax_applied_amount'] = $OrderItem->base_weee_tax_applied_amount ?? '';
        $data['base_weee_tax_applied_row_amnt'] = $OrderItem->base_weee_tax_applied_row_amnt ?? '';
        $data['base_weee_tax_disposition'] = $OrderItem->base_weee_tax_disposition ?? '';
        $data['base_weee_tax_row_disposition'] = $OrderItem->base_weee_tax_row_disposition ?? '';
        return $data;
    }
}
