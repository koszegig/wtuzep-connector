<?php


namespace App\Aion\Library;
use App\Aion\Helpers\ArrayHelpers;
use App\Aion\Helpers\CustomCSV;

class CSVManager
{
    /**
     * @var array
     */
    const DELIMITERS = [",", "\t", ";", "|", ":"];

    /**
     * @param string $fileID
     * @param array $settings
     * @example $settings ['delimiter'	 => string,
     *					  'enclosure'	 => string,
     *					  'skipFirstRow' => boolean]
     * @return array
     */
    public function convertCSV($path,$settings = ['skipFirstRow' => 0,
        'delimiter' =>';',
        'enclosure' => '"'
        ]
    ){
        $result = [ 'isSuccess' => true,
            'message' => '',
            'data' => []

        ];

        $csv = new CustomCSV();
        $csv->heading		= (intval($settings['skipFirstRow'] ?? 0) !== 0);
        $csv->delimiter		= $settings['delimiter'];
        $csv->enclosure		= $settings['enclosure'];
        if (isset($settings ['escape'])) {
            $csv->escape		= $settings ['escape'];
        }
        $csv->encode		= true;
        if(isset($settings['eol']) && !empty($settings['eol'])){
            $csv->eol		= $settings['eol'];
        }
        $csv->heading =false;
        $csv->auto($path);


        //Arrayhelpers::toStringToLog($item,'$item - xmlmanager',__FILE__, __METHOD__, __LINE__);
        //dd($csv->data[0]);
        $firstRow = array_shift($csv->data);
        //$csv->data = array_combine($firstRow, $csv->data);

        foreach ($firstRow as &$value) {
            $value = preg_replace('# #', '_', $value);
            $value = preg_replace('#\(#', '_', $value);
            $value = preg_replace('#\)#', '_', $value);
            $value = preg_replace('#%#', '_', $value);
        }

        foreach ($csv->data as &$_value) {
            $object = new \stdClass();
            $item = (array_combine($firstRow, $_value));
            foreach ($item as $key => $value) {
                $object->$key = $value;
            }
            $result['data']  [] = $object;
        }
        return $result;

    }
    /**
     * @param array | boolean (false) $array
     * @param array $repeatFields
     * @param string | boolean (false) $pref
     * @default $array (boolean) false
     * @default $repeatFields []
     * @default $pref (boolean) false
     * @return array
     */
    public function normalizeArray($array = false, $repeatFields = [], $pref = false){
        $result = [];
        if(is_array($pref) || is_object($pref)) $pref = '';

        if(is_array($array)){
            foreach($array as $key => $value){
                if(in_array($key, $repeatFields) || empty(count($repeatFields))){
                    if(is_array($value)){
                        $result = array_merge($result, $this->normalizeArray($value, $repeatFields, $pref.$key.'/'));
                    }
                    else{
                        $result[$pref.$key] = $value;
                    }
                }
            }
        }

        return $result;
    }
    /**
     * @param array $orders
     * @param array $repeatFields
     * @param array $result
     * @default $orders (boolean) false
     * @default $repeatFields []
     * @default $result []
     * @return array
     */
    public static function convertArrayToRows($order, $repeatFields = [], $result = [], $isBaseRow = true){
        $Oneresult = [];
        $baseRow = [];
        $firstBaseRow = [];
        $arraysRow = [];
        $maxCount = 0;
        foreach($order as $key => $value){ //Note Szétszedi array és nem array mezőkre
            if(is_array($value) && (count($value) > 1)){
                $baseRow[$key] = '';
                $arraysRow[$key] = $value;
                if($maxCount < count($value)) $maxCount = count($value);
            }
            else{
                if(is_array($value)){
                    if(is_array($value[0])){
                        $baseRow[$key] = implode("/", $value[0]);
                    }
                    else{
                        $baseRow[$key] = $value[0];
                    }
                }
                else{
                    $baseRow[$key] = $value;
                }
            }
        }
        $emptyRow = array_fill_keys(array_keys($baseRow),'');
        foreach($repeatFields as $item ){
            foreach($baseRow as $key => &$value){
                if(strtolower($key) == $item) $emptyRow[$key] = $value;
            }
        }



    }


}
