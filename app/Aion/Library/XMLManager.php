<?php
namespace App\Aion\Library;
use Log;
use \App\Aion\Helpers\Filehelpers;
use App\Aion\Helpers\ArrayHelpers;

/**
 * Class XMLManager
 * @package App\Aion\Library
 */
class XMLManager
{

    /**
     * @param $content
     * @param string $version
     * @param string $encoding
     * @return array|bool
     */
    public function validate($content, $version = '1.0', $encoding = 'utf-8'){
            if(trim($content) == '') return false;

            libxml_use_internal_errors(true);
            libxml_disable_entity_loader(false);

            $doc = new \DOMDocument($version, $encoding);
            $doc->loadXML($content, LIBXML_PARSEHUGE);

            $errors = libxml_get_errors();
            libxml_clear_errors();
            if(!empty($errors)) return $errors;

            return true;
    }


    /**
     * @param $path
     * @param string $content
     * @param string $keyFieldName
     * @return array|string
     */
    public function convertXML($content, $itamPath = 'Item', $keyFieldName = ''){
       $result = [ 'isSuccess' => true,
                   'message' => '',
                   'data' => []

                  ];

        ///$content = Filehelpers::readFromfile($path);
        //Arrayhelpers::toStringToLog($path,'$path - xmlmanager',__FILE__, __METHOD__, __LINE__ );
        if( $content == ''){
            Log::error('Not valid XML.');
            $result['isSuccess']  = false;
            $result['message']  = $path.'  File Not found';
            return $result;
        }

              $validateResult =$this->validate($content);
             if( $validateResult !== true){
                 Log::error('Not valid XML.');
                 $result['isSuccess']  = false;
                 $result['message']  = 'Not valid XML.';
                 return $result;
             }
             else{
                 if($content){
                     $xml = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_PARSEHUGE);
                     if(!$xml)  {
                         Log::error('XML is empty.');
                         $result['isSuccess']  = false;
                         $result['message']  = 'XML is empty.';
                         return $result;

                     };
//                     $data = json_decode(json_encode((array)$xml, JSON_UNESCAPED_UNICODE), true);
                     $array = json_decode(json_encode((array)$xml, JSON_UNESCAPED_UNICODE), true);
                 }
                 $data = [];
                 //Log::debug('keyFieldName:'.$keyFieldName);
                 //Log::debug('$itamPath:'.$itamPath);
                 //Arrayhelpers::toStringToLog($array,'ImportAttributes - $array---- xmlmanager');
                 //Arrayhelpers::toStringToLog($array[$itamPath],'$array[$itamPath] ',__FILE__, __METHOD__, __LINE__);
                 //Arrayhelpers::toStringToLog(count($array[$itamPath]),'count($array[$itamPath]) ',__FILE__, __METHOD__, __LINE__);
                 if (isset($array[$itamPath][0])) {
                     foreach ($array[$itamPath] as $item)
                     {
                         $object = new \stdClass();
                         //Arrayhelpers::toStringToLog($item,'$item - xmlmanager',__FILE__, __METHOD__, __LINE__);
                         foreach ($item as $key => $value) {
                             $object->$key = $value;
                         }
                         if ( $keyFieldName == '') {
                            $data [] = $object;
                         } else {
                            $data [$item [$keyFieldName]] = $object;
                         }
                     }
                 }
                 if (!isset($array[$itamPath][0])) {
                     $object = new \stdClass();
                     ///Arrayhelpers::toStringToLog($item,'$item - xmlmanager',__FILE__, __METHOD__, __LINE__);
                     foreach ($array[$itamPath] as $key => $value) {
                         $object->$key = $value;
                     }
                     if ( $keyFieldName == '') {
                         $data [] = $object;
                     } else {
                         $data [$array [$keyFieldName]] = $object;
                     }
                 }
                 ksort($data);
                 $result['data']  =$data;
                 //Arrayhelpers::toStringToLog($data,'$data - xmlmanager',__FILE__, __METHOD__, __LINE__);
             }

             return $result;
    }

    /**
     * @param $array
     * @param null $rootElement
     * @param null $xml
     * @return \SimpleXMLElement|null
     */
    function arrayTOXML($array, $rootElement = null, $xml = null) {
        $_xml = $xml;

        // If there is no Root Element then insert root
        if ($_xml === null) {
            $_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<Items/>');
        }

        // Visit all key value pair
        foreach ($array as $key => $v) {

            // If there is nested array then
            if (is_array($v)) {
                if( is_numeric($key) ){
                    $key = 'Item'; //dealing with <0/>..<n/> issues

                }
                // Call function for nested array
                //$this->arrayTOXML($v, $key, $_xml->addChild($key));
            }

            else {

                // Simply add child element.
                $_xml->addChild($key, $v);
            }
        }

        return $_xml;
        //return $_xml->asXML();
    }
    public static function convertFloat($value, $default = 0)
    {
        $value = str_replace(',', '.', $value);
        return is_numeric($value) ? $value : $default;
    }

    public static function ConvertToDateTimeStringFromUTC($datetimestring, $default = "")
    {
        if ($datetimestring == '') return $default;
        $txt = substr($datetimestring, 0, 19);
        $txt[10] = ' ';
        return $txt;
    }

    public static function GetDefaultValue($node)
    {
        if ($node == null)
        {
         //   Log::Add('Hiányzó xml node');
            return;
        }

        if ($node->count() > 1)
        {
            foreach ($node as $item)
            {
                if ($item->attributes()->IsDefault == 1)
                {
                    return (string) $item;
                }
            }
        }
        else
        {
            return (string) $node;
        }
    }

    public static function GetNodes($node, $tagname)
    {
        if (!isset($node->$tagname))
        {
           // Log::Add('Hiányzó xml node ' . $tagname);
            return array();
        }

        $result = array();

        if ($node->$tagname->count() > 1)
        {
            foreach ($node->$tagname as $item)
            {
                $result[] = $item;
            }
        }
        else
        {
            $result[] = $node->$tagname;
        }
        return $result;
    }

}
