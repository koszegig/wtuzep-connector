<?php


namespace App\Aion\Library;
use App\Exports\ExportXLS;
use Maatwebsite\Excel\Facades\Excel;


class XLSManager
{
    public function arrayTOXLS($data = [], $dir, $filename)

    {
        //dd(storage_path("{$dir}{$filename}"));
        if (count($data)>0) {
            $result =  (new ExportXLS($data))->store("{$dir}{$filename}"); //using FromQuery
            //\AionFile::moveFile(storage_path("app/{$dir}{$filename}"),storage_path("{$dir}/"));
            return $result;
        }
    }
}
