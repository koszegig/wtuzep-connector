<?php


namespace App\Aion\Library;
use Illuminate\Support\Facades\Storage;
use \App\Aion\Library\XMLManager;
use \App\Aion\Helpers\ArrayHelpers;
use \App\Aion\Helpers\Filehelpers;


class FTPManager
{
    public function downloadCSVFiles($storedpath,$remotePath,$filesFilter= ''){
        ///$_remoteFiles = Storage::disk('ftp')->files($remotePath);
        $result = Storage::disk('ftp')->files($remotePath);


        Arrayhelpers::toStringToAll($result,'ftp $resultFiles');
        //Arrayhelpers::toStringToLog($filesFilter,'$filesFilter - $destFiles');
        if ($filesFilter !='') {
            $filesFilterSRC = $remotePath.$filesFilter;
            $fitlered_files = array_filter($result, function($str) use ($filesFilterSRC){
                return strpos($str, $filesFilterSRC) === 0;
            });
            $result = [];
            foreach ($fitlered_files as $_file) {
                $parts = pathinfo($_file);
                $result [$parts['filename']] = $_file;
            }
        }
        /*$_localFiles = Storage::disk('csv')->files($storedpath);
        if ($filesFilter !='') {
            $filesFilterLocal = $storedpath.$filesFilter;
            $fitlered_files = array_filter($_localFiles, function($str) use ($filesFilterLocal){
                return strpos($str, $filesFilterLocal) === 0;
            });
            $_localFiles =  $fitlered_files;
        }*/
        //Arrayhelpers::toStringToLog($_localFiles, '$_localFiles',__FILE__, __METHOD__, __LINE__ );
        //Arrayhelpers::toStringToLog($_remoteFiles, '$_remoteFiles',__FILE__, __METHOD__, __LINE__ );
        //$result=Filehelpers::diferentfiles($_localFiles,$_remoteFiles);


        //Arrayhelpers::toStringToLog($_remoteFiles);
        // filter the ones that match the filename.*
        //Arrayhelpers::toStringToLog($result,'reuslt');
        //Arrayhelpers::toStringToLog($result,'result');
        foreach ($result  as  $filename => $remoteFile) {
            $content = Storage::disk('ftp')->get($remoteFile);
            Filehelpers::writetofile($storedpath,$filename.'.csv',$content);
            Arrayhelpers::toStringToEcho($filename,'Ftp download  - file');
        }
    }
    public function uploadCSVFiles($storedpath,$remotePath){
        $_remoteFiles = Storage::disk('ftp')->files($remotePath);
        $_localFiles = Storage::disk('csv')->files($storedpath);

        $result=Filehelpers::diferentfiles($_remoteFiles, $_localFiles);
        //Arrayhelpers::toStringToLog($result);
        //Arrayhelpers::toStringToLog($result,'result');
        foreach ($result  as  $filename => $sourceFile) {
            $path = storage_path($sourceFile);
            $parts = pathinfo($path);
            $content = Filehelpers::readFromfile($path);
            Storage::disk('ftp')->put($remotePath.$filename.'.'.$parts['extension'], $content);
            Arrayhelpers::toStringToEcho($filename,'Ftp upload  - file');
        }
    }
    public function deleteCSVFiles($path){
        Storage::disk('ftp')->delete($path);
        Arrayhelpers::toStringToEcho($path,'Ftp delete  - file');

    }
}
