<?php

namespace App\Libraries\Database;

class Select
{
  private $select;

  public function __construct($select = null)
  {
    $this->select = $select;
  }

  /**
   * Get the value of select
   */
  public function getSelect()
  {
    return $this->select;
  }

  /**
   * Set the value of select
   *
   * @return  self
   */
  public function setSelect($select)
  {
    $this->select = $select;

    return $this;
  }

  public function build(){

      if ($this->select == '*') {
          return "SELECT * ";
      } else {
          if (is_array($this->select)) {
              return "SELECT " ."`". implode("`,`", $this->select)."`";
          }else {
                return "SELECT " . join(",`", $this->select."`"). " ";
          }
      }
  }
}
