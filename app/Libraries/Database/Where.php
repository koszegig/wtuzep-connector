<?php

namespace App\Libraries\Database;
use App\Aion\Helpers\ArrayHelpers;

class Where
{
  private $field;
  private $value;
  private $sub;
  private $op;
  private $lop;
  private $lower;
  private $quoted;


  public function __construct($field = null, $value = null, $op = null, $lop = null, $lower = false, $quoted = false)
  {
    $this->field = $field;
    $this->value = $value;
    $this->sub = [];
    $this->op = $op;
    $this->lop = $lop;
    $this->lower = $lower;
    $this->quoted = $quoted;
  }

  /**
   * Get the value of field
   */
  public function getField()
  {
    return $this->field;
  }

  /**
   * Set the value of field
   *
   * @return  self
   */
  public function setField($field)
  {
    $this->field = $field;

    return $this;
  }

  /**
   * Get the value of value
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * Set the value of value
   *
   * @return  self
   */
  public function setValue($value)
  {
    $this->value = $value;

    return $this;
  }

  /**
   * Get the value of sub
   */
  public function getSub()
  {
    return $this->sub;
  }

  /**
   * Set the value of sub
   *
   * @return  self
   */
  public function setSub($sub)
  {
    $this->sub = $sub;

    return $this;
  }

  /**
   * Get the value of op
   */
  public function getOp()
  {
    return $this->op;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setOp($op)
  {
    $this->op = $op;

    return $this;
  }

  /**
   * Get the value of op
   */
  public function getLop()
  {
    return $this->lop;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setLop($lop)
  {
    $this->lop = $lop;

    return $this;
  }



  /**
   * Get the value of op
   */
  public function getLower()
  {
    return $this->lower;
  }

  /**
   * Set the value of op
   *
   * @return  self
   */
  public function setLower($lower)
  {
    $this->lower = $lower;

    return $this;
  }

  public function build(){
      //\AionArray::toStringToLog($this->sub,'build  Where 2 ',__FILE__, __METHOD__, __LINE__ );
    if(!empty($this->sub)) return $this->buildSub();
    if($this->op == 'between')
        return " $this->field $this->op $this->value[0] AND $this->value[1]";
    $where = "";
      // \AionArray::toStringToLog($where,'$where  Where 2 ',__FILE__, __METHOD__, __LINE__ );
    $where .= $this->_buildCondition();
      //\AionArray::toStringToLog($where,'$where  Where 2 ',__FILE__, __METHOD__, __LINE__ );
    if($this->lop != null) $where .= " {$this->lop} ";
    return $where;
  }

  private function _buildCondition(){
    $where = $this->buildCondition('field');
    $where.= " $this->op ";
    $condval = $this->buildCondition('value');
    $where.= $condval;
    return $where;
  }

  private function buildCondition($_prop){
      //\AionArray::toStringToLog($_prop,'buildCondition  Where 2 ',__FILE__, __METHOD__, __LINE__ );
    $prop = $this->$_prop;
      //\AionArray::toStringToLog($prop,'$prop  buildCondition Where 2 ',__FILE__, __METHOD__, __LINE__ );
    if($_prop == 'value'){
      $prop = $this->setPSign($prop);
      $prop = $this->setQuoted($prop);
    }
      if($_prop == 'field' && $prop != '1'){
          $prop = "`".$prop ."`";
      }
    return ($this->lower) ? "LOWER(".$prop.")" : $prop;
  }

  private function setQuoted($prop){
    return ($this->quoted) ? "'".$prop."'" : $prop;
  }

  private function setPSign($prop){
    return (in_array($this->op, ['like', 'not like'])) ? "%".str_replace(' ','%%', $prop)."%" : $prop;
  }

  private function buildSub(){
    $where = "( ";
    foreach($this->sub as $index => $sub){
        if($index == (count($this->sub)-1)) $sub->setLop(null);
        $where .= $sub->build();
    }
    $where .= ") {$this->lop} ";
      //\AionArray::toStringToLog($where,'buildSub  Where 2 ',__FILE__, __METHOD__, __LINE__ );
    return $where;
  }

  public function pushSub($field, $value, $op, $lop, $lower, $quoted){
      array_push($this->sub, new self($field, $value, $op, $lop, $lower, $quoted));
      return $this;
  }
}
