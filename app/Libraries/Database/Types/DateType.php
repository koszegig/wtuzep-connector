<?php

namespace App\Libraries\Database\Types;

class DateType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = [ '=', '<', '>', '<=', '>=', '<>', 'between'];
    $this->quote = true;
    $this->lower = false;
  }
}
