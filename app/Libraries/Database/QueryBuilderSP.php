<?php

namespace App\Libraries\Database;

use App\Libraries\Database\Where;
use App\Libraries\Database\From;
use App\Libraries\Database\Select;
use App\Aion\Helpers\ArrayHelpers;

class QueryBuilderSP
{
  private $query;
  private $condition;
  private $model;
  private $filters;
  private $paginate;
  private $select;
  private $condstring;
  private $from;
  private $params;

  public $types = [
    'uuid', 'Varchar', 'Bool', 'Timestamp', 'Int4', 'Text','Date'
  ];

  public function __construct($model, $request, $paginate, $select, $params)
  {
    $this->model = new $model();
    $this->paginate = $paginate;
    $this->params = $params;
    $this->select = new Select($select);
    $this->condition = [];
    $this->buildlop = true;
    $this->from = new From($this->model);
    $this->initialFilters($request);
    $this->processFilters();
    array_push($this->condition, new where('1', '1', '='));
  }

  public function paginate($size = 25){
    $this->paginate = $size;
    return $this;
  }

  private function initialFilters($request){

    $this->filters =  isset($request->filters) ? $request->filters : [];
     // \AionArray::toStringToLog($this->filters,'initialFilters 1 ',__FILE__, __METHOD__, __LINE__ );
  }

  private function processFilters(){
    if(empty($this->filters)) return;
    foreach($this->filters as $filter){
      $this->processFilter($filter);
    }
  }

  private function processFilter($filter){
    $this->where($filter);
  }

  private function where($filter){
      \AionArray::toStringToLog($filter,'where 1 ',__FILE__, __METHOD__, __LINE__ );
      if (is_array($filter['field'])) {
          if (count($filter['field']) == 1) {
              $tempfilterfield = $filter['field'];
              $filter['field'] =  $tempfilterfield[0]['field'];
              $filter['op'] =  $tempfilterfield[0]['op'];
          }
      }
      \AionArray::toStringToLog($filter,'where 1 ',__FILE__, __METHOD__, __LINE__ );
    $where = (is_array($filter['field'])) ? $this->subwhere($filter) : $this->__where($filter);
    array_push($this->condition, $where);
  }

  private function  subwhere($filter){
    $where = new Where(null, null, null, $filter['lop']);
    foreach($filter['field'] as $field){
        //\AionArray::toStringToLog($field,'$field subwhere ',__FILE__, __METHOD__, __LINE__ );
      $type = $this->getType($field['field'],$filter);
        //\AionArray::toStringToLog($type,'$type subwhere ',__FILE__, __METHOD__, __LINE__ );
      if(!$this->isValidFilter($field, $type)) continue;

      $where->pushSub($field['field'], $filter['value'], $field['op'], $field['lop'], $type->isLower(), $type->quoted());
        // \AionArray::toStringToLog($where,'$where subwhere ',__FILE__, __METHOD__, __LINE__ );
    }
    return $where;
  }

  private function getType($_field, $filter){
    $field = $this->field($_field)->first();
      //\AionArray::toStringToLog($filter,'$filter getType ',__FILE__, __METHOD__, __LINE__ );
    $type = ($field == null) ? $filter['fieldtype'] : $field->getType();
      //\AionArray::toStringToLog($type,'$type getType ',__FILE__, __METHOD__, __LINE__ );
    return $this->initialTypeClass($type);
  }

  private function isValidFilter($filter, $type){
    return ($type !== false && $type->isValidOperator($filter['op']));
  }

  private function __where($filter){
      //\AionArray::toStringToLog($filter,'__where 1 ',__FILE__, __METHOD__, __LINE__ );
    $type = $this->getType($filter['field'],$filter);
      if(!$this->isValidFilter($filter, $type)) {
    //      \AionArray::toStringToLog($type,'Nem nyó ',__FILE__, __METHOD__, __LINE__ );
    //      \AionArray::toStringToLog($filter['field'],'Nem nyó ',__FILE__, __METHOD__, __LINE__ );
      }
    if(!$this->isValidFilter($filter, $type)) return;
    return new Where($filter['field'], $filter['value'], $filter['op'], $filter['lop'], $type->isLower(), $type->quoted());
  }

  private function field($_field){
     return $this->model->getFields()->filter(function ($field) use ($_field) {
      return $field->getName() == $_field;
    });
  }

  private function initialTypeClass($type){
    if(!$this->isValidType($type)) return false;
    $__type = 'App\Libraries\Database\Types\\'.ucfirst($type).'Type';
    return new $__type();
  }


  public function get(){
    $this->build();
     //\AionArray::toStringToLog($this->query,'get 1 ',__FILE__, __METHOD__, __LINE__ );
     //\AionArray::toStringToLog($this->condstring,'get 1 ',__FILE__, __METHOD__, __LINE__ );
    $dmdata = \DB::select($this->query.$this->condstring, $this->generateparams());
    $dmdata = $this->model::hydrate($dmdata);
    if(!is_null($this->paginate)){
      $dmdata = \MyPage::paginateWithoutKey($dmdata, $this->paginate);
    }
    return $dmdata;
  }

  private function generateparams(){
    $array = [env('DB_PREFIX', null)];
      //\AionArray::toStringToLog($array,'generateparams 1 ',__FILE__, __METHOD__, __LINE__ );
    if(!empty($this->params)) $array = array_merge($array, $this->params);
    array_push($array, $this->condstring);
    return $array;
  }


  private function build(){
      //\AionArray::toStringToLog('----','build Start ',__FILE__, __METHOD__, __LINE__ );
    $this->query = $this->select->build();
    $this->query .= $this->from->build();
      //\AionArray::toStringToLog($this->query,'build 1 ',__FILE__, __METHOD__, __LINE__ );

    $this->condstring = $this->buildCond();
    //  \AionArray::toStringToLog($this->condstring,' $this->condstring build 2 ',__FILE__, __METHOD__, __LINE__ );
    //\AionArray::toStringToLog($this->query,'$this->query build 2 ',__FILE__, __METHOD__, __LINE__ );

  }

  private function buildCond(){
    //\AionArray::toStringToLog($this->condition,'buildCond 1 ',__FILE__, __METHOD__, __LINE__ );
    $where = 'where ';
    foreach($this->condition as $condition){
       //   \AionArray::toStringToLog($condition,'buildCond 2 ',__FILE__, __METHOD__, __LINE__ );
      $where.= $condition->build();
     //   \AionArray::toStringToLog($where,'$where buildCond 2 ',__FILE__, __METHOD__, __LINE__ );
    }
    return $where;
  }

  private function isValidType($type){
    return in_array($type, $this->types);
  }
}
