<?php

namespace App\Libraries;

/**
 * Class Field
 * @package App\Libraries
 */
class Field
{
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $fillable;
    /**
     * @var
     */
    private $primary;
    /**
     * @var
     */
    private $type;
    /**
     * @var
     */
    private $cast;
    /**
     * @var
     */
    private $fieldLength;
    /**
     * @var
     */
    private $importFieldName;
    /**
     * @var
     */
    private $exportFieldName;
    /**
     * @var mixed|null
     */
    private $defaultvalue;
    /**
    * @var
    */
    private $fieldSynced;

    /**
     * Field constructor.
     * @param $name
     * @param $type
     * @param $fillable
     * @param $importFieldName
     * @param $exportFieldName
     * @param $primary
     * @param $cast
     * @param null $defaultvalue
     */
    public function __construct($name, $type, $fillable, $importFieldName, $exportFieldName, $primary, $cast, $defaultvalue = null, $fieldLength = null,$fieldSynced =true)
  {
    $this->name = $name;
    $this->fillable = $fillable;
    $this->primary = $primary;
    $this->type = $type;
    $this->cast = $cast;
    $this->importFieldName = $importFieldName;
    $this->exportFieldName = $exportFieldName;
    $this->defaultvalue = $defaultvalue;
    $this->fieldLength = $fieldLength;
    $this->fieldSynced = $fieldSynced;
  }

    /**
     * @return mixed
     */
    public function getName() {
    return $this->name;
  }

    /**
     * @param $name
     */
    public function setName($name) {
      $this->name = $name;
  }

    /**
     * @return mixed
     */
    public function getFillable() {
      return $this->fillable;
  }

    /**
     * @param $fillable
     */
    public function setFillable($fillable) {
      $this->fillable = $fillable;
  }

    /**
     * @return mixed
     */
    public function getPrimary() {
      return $this->primary;
  }

    /**
     * @param $primary
     */
    public function setPrimary($primary) {
      $this->primary = $primary;
  }

    /**
     * @return mixed
     */
    public function getType() {
      return $this->type;
  }

    /**
     * @param $type
     */
    public function setType($type) {
      $this->type = $type;
  }

    /**
     * @return mixed
     */
    public function getImportFieldName() {
      return $this->importFieldName;
  }

    /**
     * @param $importFieldName
     */
    public function setImportFieldName($importFieldName) {
      $this->importFieldName = $importFieldName;
  }

    /**
     * @return mixed
     */
    public function getExportFieldName() {
      return $this->exportFieldName;
  }

    /**
     * @param $exportFieldName
     */
    public function setExportFieldName($exportFieldName) {
      $this->exportFieldName = $exportFieldName;
  }

    /**
     * @return bool
     */
    public function isPrimary() {
    return $this->primary == true;
  }

    /**
     * @return bool
     */
    public function isFillable() {
    return $this->fillable == true;
  }

    /**
     * @return mixed
     */
    public function getCast() {
    return $this->cast;
  }

    /**
     * @param $cast
     */
    public function setCast($cast) {
      $this->cast = $cast;
  }

    /**
     * @return mixed|null
     */
    public function getDefaultvalue() {
    return $this->defaultvalue;
    }

    /**
     * @param $defaultvalue
     */
    public function setDefaultvalue($defaultvalue) {
      $this->defaultvalue = $defaultvalue;
    }
    /**
     * @return mixed|null
     */
    public function getfieldLength() {
        return $this->fieldLength;
    }

    /**
     * @param $fieldLength
     */
    public function setfieldLength($fieldLength) {
        $this->fieldLength = $fieldLength;
    }
    /**
     * @return mixed|null
     */
    public function getfieldSynced() {
        return $this->fieldSynced;
    }

    /**
     * @param $fieldLength
     */
    public function setfieldSynced($fieldSynced) {
        $this->fieldSynced = $fieldSynced;
    }
    /**
     * @return bool
     */
    public function isSynced()
    {
        return $this->fieldSynced == true;
    }
}
