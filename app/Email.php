<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $tablename='emails';
    public $primarykey="id";

    protected $guarded = [];
}
