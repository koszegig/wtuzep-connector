<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;


class Attribute extends BaseConnectorModel
{
    //
    protected $tablename='attributes';
    /**
     * @var string
     */
    public $table="attributes";

    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'attributes';
    }

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany('App\Products');
    }

    public function values()
    {
        return $this->hasMany('App\AttributeValue');
    }
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null, null,  50, true),
            new Field("erp_id", "varchar", true,'PROPERTYID', null,false, null, null,  80, true),
            new Field("code", "varchar", true,'PROPERTYNAME', null,false, 'code', null,  191, true),
            new Field("name", "varchar", true,'PROPERTYNAME', null,false, null, null,  255, true),
            new Field("type", "varchar", true,null, null,false, null, 'select',  80, true),
            new Field('created_at', "timestamp", true,null, null,false, null, null,  80, false),
            new Field('updated_at', "timestamp", true,null, null,false, null, null,  80, false),
            new Field('synced', "bool", true,null, null,false, null, null,  80, false),
            new Field('magento_id', "varchar", true,null, null,false, null, null,  80, false),
        ]);
    }
    public function code ($row) {
        $code = $row['PROPERTYNAME'];
        $code =strtolower($code);
        /*$code =str_replace('é','e',$code);
        $code =str_replace('á','a',$code);
        $code=preg_replace('~[^A-Za-z0-9?.!]~','_',$code);*/
        return $code;
    }
}
