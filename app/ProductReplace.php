<?php

namespace App;

use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;

class ProductReplace  extends BaseConnectorModel
{
    //
    /**
     * @var string
     */
    protected $tablename = 'productreplace';
    /**
     * @var string
     */
    public $table = "productreplace";

    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var bool
     */
    public $szinkron = true;

    /**
     *
     */
    protected function setStoredProcedure()
    {
        $this->storedProcedure = env('DB_PREFIX', '') . 'productreplace';
    }



    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("id", "int4", true, null, null, true, null, null, null, false),
            new Field("ProductId", "int4", true, null, null, false, null, null, 50, true),
            new Field("ReplacementProductId", "int4", true, null, null, false, null, null, 191, true),
            new Field('created_at', "Date", true, null, null, false, null, null, null, false),
            new Field('updated_at', "Date", true, null, null, false, null, null, null, false),
            new Field('synced', "bool", true, null, null, false, null, null, null, false),
            new Field('syncedorder', "Varchar", true, null, null, false, null, null, null, false),
        ]);
    }
}

