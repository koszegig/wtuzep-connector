<?php

namespace App;

class Rolepermission extends BaseModel
{
    protected $table = 'permission_role';

    /* Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        array_push($this->fillable,'permission_id','role_id');
        parent::__construct($attributes);
    }
}
