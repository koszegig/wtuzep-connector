<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;

/**
 * Class Task
 * @package App
 */
class Task extends BaseConnectorModel
{
    /**
     * @var string
     */
    public $table="task";
    protected $tablename='task';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'task';
    }

    /**
     * @param $taskType
     * @return mixed
     */
    public static function start($taskType){
        $record = [
            'task_type' => $taskType,
            'start' => date('Y-m-d H:i:s'),
            'new' => 0,
            'stopme'	=> 0,
            'refresh' =>  0,
            'PID'			=> getmypid()
        ];
        //$task = Task::where("task_type", $taskType)->first();
        //dd($task);
        return  Task::UpdateOrCreate(['task_type' => $taskType],$record);
    }

    /**
     * @param     $taskType
     * @param int $new
     * @param int $refresh
     * @return mixed
     */
    public static function stop($taskType, $new = 0, $refresh =0, $statusCode = 400 ){
        if ($new<0) {
            $new =0;
        }
        $record = [
           'task_type' => $taskType,
           'stop' => date('Y-m-d H:i:s'),
           'new' => $new,
            'statusCode'	=> $statusCode,
           'refresh' =>  $refresh,
            'PID'			=> 0
            ];


        return  Task::UpdateOrCreate(['task_type' => $taskType],$record);
    }
    /**
     * @param string $taskType
     * @param integer $statusCode
     * @return string (numeric) | integer
     */
    public static function updateLastTicket($taskType, $statusCode=0){
        $record = [
            'lastTick'	=> date('Y-m-d H:i:s')
            , 'statusCode'	=> $statusCode
            , 'PID'			=> getmypid()
        ];
        return  Task::UpdateOrCreate(['task_type' => $taskType],$record);
    }

    /**
     * @param $taskType
     * @return mixed
     */
    public static function setStopped($taskType){
        $record = [
            'lastTick'	=> date('Y-m-d H:i:s')
            , 'statusCode'	=> 404
            , 'stopme'	=> 1
            , 'PID'		=> getmypid()
        ];
        return  Task::UpdateOrCreate(['task_type' => $taskType],$record);
    }
    /**
     * @param $taskType
     * @return mixed
     */
    public static function isStopped($taskType){
        $task = Task::where("task_type", $taskType)->first();
        return  $task->stopme === 1;
    }
    /**
     * @param $taskType
     * @return mixed
     */
    public static function getCron($taskType){
        $task = Task::where("task_type", $taskType)->first();
        if ($task) {
        return  $task->cron;
        } else{
            return env('CRON',"*/10 * * * *");
        }
    }
    /**
     * @param string $taskType
     * @param integer $statusCode
     * @return string (numeric) | integer
     */
    public static function getPID($taskType){
        $task = Task::where("task_type", $taskType)->first();

        return  $task->PID ?? 0;
    }

    /**
     * @param     $taskType
     * @param int $limit
     * @return int
     */
    public static function getTaskLimit($taskType, $limit = 10){
        $task = Task::where("task_type", $taskType)->first();
        $task_Limit = $task->limit ?? -1;
        if ($task_Limit == -1 ) {
            $task_Limit = $limit;
        }

        return  $task_Limit;
    }

    public static function getTaskType($PID){
        $task = Task::where("PID", $PID)->first();

        return  $task->task_type ?? 'local';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("id", "int4", true,null, null,true, null),
            new Field("task_type", "Varchar", true,null, null,false, null),
            new Field("start", "Date", true,null, null,false, null),
            new Field("lastTick", "Date", true,null, null,false, null),
            new Field("stop", "Date", true,null, null,false, null),
            new Field("new", "int4", true,null, null,false, null),
            new Field("refresh", "int4", true,null, null,false, null),
            new Field("PID", "int4", true,null, null,false, null),
            new Field("statusCode", "int4", true,null, null,false, null),
            new Field("stopme", "Bool", true,null, null,false, null),
            new Field("cron", "Varchar", true,null, null,false, null),
            new Field("limits", "int4", true,null, null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
        ]);
    }
}
