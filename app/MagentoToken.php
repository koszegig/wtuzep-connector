<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MagentoToken extends Model
{
    protected $guarded = [];
    public $table="magentotoken";
    /**
     * @param $taskType
     * @return mixed
     */
    public static function getToken($userName){
        $magentotoken = self::where("username", $userName)->where('until', '>', date('Y-m-d'))->first();
        if ($magentotoken) {
            return  $magentotoken->rp_token;
        } else{
            return '';
        }
    }
}
