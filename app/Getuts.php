<?php

namespace App;

use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

/**
 * Class Getuts
 * @package App
 */
class Getuts extends BaseConnectorModel
{
    //

    //
    /**
     * @var string
     */
    protected $tablename='getuts';
    /**
     * @var string
     */
    public $table="getuts";


    /**
     * @var bool
     */
    public $szinkron = false;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     *
     */
    protected function setStoredProcedure(){
       $this->storedProcedure = env('DB_PREFIX', '').'getuts';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("id", "int4", true, null, null, true, null),
            new Field("resourcename", "varchar", true, null, null, false, null),
            new Field("full", "Int4", true, null, null, false, null),
            new Field("uts", "Int4", true, null, null, false, null),
            new Field("ready", "Int4", true, null, null, false, null),
            new Field('created_at', "timestamp", true, null, null, false, null),
            new Field('updated_at', "timestamp", true, null, null, false, null),
        ]);
    }

    /**
     * @param $resourcename
     * @param $full
     * @return mixed
     */
    public static function start($resourcename, $full){
        $maxUts = Getuts::where("resourcename", $resourcename)->where("full", $full)->max('uts');
        $maxUts = $maxUts +1;
        $record = [
            'resourcename' => $resourcename,
            'full' => $full,
            'uts' => $maxUts,
            'ready' =>  0
        ];
        return  Getuts::UpdateOrCreate(['resourcename' => $resourcename,'full'=>$full],$record);
    }
    /**
     * @param     $taskType
     * @param int $new
     * @param int $refresh
     * @return mixed
     */
    public static function stop($resourcename, $full){
        $maxUts = Getuts::where("resourcename", $resourcename)->where("full", $full)->max('uts');
        $record = [
            'resourcename' => $resourcename,
            'full' => $full,
            'uts' => $maxUts,
            'ready' =>  1
        ];
        return  Getuts::UpdateOrCreate(['resourcename' => $resourcename,'full'=>$full],$record);
    }
}
