<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $tablename='contacts';
    public $primarykey="id";

    protected $guarded = [];

}
