<?php

namespace App;

use App\Models\Base\BaseConnectorModel;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\Field;

class Picture extends BaseConnectorModel
{
    //
    //
    protected $tablename='pictures';
    /**
     * @var string
     */

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'pictures';
    }

    /*public function product()
    {
        return $this->belongsTo('App\Product');
    }*/
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("product_id", "varchar", true,null, null,false, null),
            new Field("sku", "varchar", true,'ProductId', null,false, null),
            new Field("name", "varchar", true,'ProductId', null,false, null),
            new Field("type", "varchar", true,null, null,false, null,'picture'),
            new Field("path", "varchar", true,'Path', null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
        ]);
    }
}
