<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\Field;
use App\Models\Base\BaseConnectorModel;

class Customers extends BaseConnectorModel
{
    /**
     * @var string
     */
    public $table="customers";
    protected $tablename='customers';
    /**
     * @var array
     */
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'customers';
    }
    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }
    public function setFields(){
        $this->fields = collect([
            new Field('id', "int4", true,null, null,true, null),
            new Field('magento_id', "Varchar", true,'id', null,false, null),
            new Field('vat_number', "Varchar", true,null, null,false, null),
            new Field('name', "Varchar", true,'name', null,false, null),
            new Field('billing_zip_code', "Varchar", true,null, null,false, null),
            new Field('email', "Varchar", true,'email', null,false, null),
            new Field('billing_city', "Varchar", true,null, null,false, null),
            new Field('billing_street', "Varchar", true,null, null,false, null),
            new Field('billing_number', "Varchar", true,null, null,false, null),
            new Field('shipping_zip_code', "Varchar", true,null, null,false, null),
            new Field('shipping_city', "Varchar", true,null, null,false, null),
            new Field('shipping_street', "Varchar", true,null, null,false, null),
            new Field('shipping_number', "Varchar", true,null, null,false, null),
            new Field('created_at', "Date", true,null, null,false, null),
            new Field('updated_at', "Date", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),

        ]);
    }
}
