<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;


class Units extends  BaseConnectorModel
{
    //
    /**
     * @var string
     */
    protected $tablename='units';
    /**
     * @var string
     */
    public $table="units";

    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var bool
     */
    public $szinkron = true;

    /**
     *
     */
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'units';
    }

    /* public function pictures()
     {
         return $this->hasMany('App\Picture');
     }

 */

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null, null,  null, false),
            new Field("erp_id", "Varchar", true,"unitid", 'sku',false, null, null,  50, false),
            new Field("megnevezes", "Varchar", true,"unitname", null,false, null, null,  191, true),
            new Field("rovidnev", "Varchar", true,"shortunitname", null,false, null, null,  191, true),
            new Field('created_at', "Date", true,null, null,false, null, null,  null, false),
            new Field('updated_at', "Date", true,null, null,false, null, null,  null, false),
            new Field('synced', "bool", true,null, null,false, null, null,  null, false),
            new Field('syncedorder', "Varchar", true,null, null,false, null, null,  null, false),
            new Field('magento_id', "Varchar", true,null, null,false, null, null,  null, false),
            new Field('active', "Bool", true,null, 'active',false, null, null,  null, true),
        ]);
    }
}
