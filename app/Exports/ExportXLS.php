<?php


namespace App\Exports;;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ExportXLS implements FromArray
{
    use Exportable, SerializesModels;
    private $data;

    public function __construct($datas)
    {
       // dd(array_keys($data[0]));
        $this->data = [];
        $this->data [] = array_keys($datas[0]);
        foreach ($datas as $data) {
            $this->data [] =  array_values($data);
        }
        //$this->data = $data;     //Inject data
    }

    public function array(): array // this was query() before
    {
        return $this->data;
    }

}
