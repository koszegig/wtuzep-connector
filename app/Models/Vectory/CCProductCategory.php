<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCProductCategory extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCProductCategory";
    protected $tablename = 'CCProductCategory';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCProductCategory';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("CategoryID", "int4", true, null, null, true, null),
            new Field("CategoryName", "varchar", true, null, null, false, null),
            new Field("ParentId", "varchar", true, null, null, false, null),
            new Field('Level', "timestamp", true, null, null, false, null),
        ]);
    }

}
