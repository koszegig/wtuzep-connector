<?php


namespace App\Models\Vectory;
use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class vectoryBuyerParty  extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "BuyerParty";
    protected $tablename = 'BuyerParty';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'BuyerParty';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("GLN", "int4", true, null, null, true, null),
            new Field("VectoryId", "varchar", true, null, null, false, null),
            new Field("TaxID", "varchar", true, null, null, false, null),
            new Field("Name", "varchar", true, null, null, false, null),
            new Field('Street', "varchar", true, null, null, false, null),
            new Field('PostalCode', "varchar", true, null, null, false, null),
            new Field('City', "timestamp", true, null, null, false, null),
            new Field('CountryCode', "varchar", true, null, null, false, null),
            new Field('ContactPerson', "varchar", true, null, null, false, null),
            new Field('ContactTel', "varchar", true, null, null, false, null),
        ]);
    }

}
