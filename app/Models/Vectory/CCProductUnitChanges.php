<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCProductUnitChanges  extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCProductUnitChanges";
    protected $tablename = 'CCProductUnitChanges';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCProductUnitChanges';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("productid", "int4", true, null, null, true, null),
            new Field("unitid", "varchar", true, null, null, false, null),
            new Field("exchange", "varchar", true, null, null, false, null),
        ]);
    }

}
