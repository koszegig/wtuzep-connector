<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCCustomers  extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCCustomers";
    protected $tablename = 'CCCustomers';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCCustomers';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("CustomerId", "varchar", true, null, null, true, null),
            new Field("CustomerName", "varchar", true, null, null, false, null),
            new Field("Email", "varchar", true, null, null, false, null),
            new Field("Phone", "varchar", true, null, null, false, null),
            new Field("TaxNumber", "varchar", true, null, null, false, null),
            new Field("CountryCode", "varchar", true, null, null, false, null),
            new Field("ZipCode", "varchar", true, null, null, false, null),
            new Field("City", "varchar", true, null, null, false, null),
            new Field("fizmod", "varchar", true, null, null, false, null),
            new Field("szallmod", "varchar", true, null, null, false, null),
            new Field("OuterId", "varchar", true, null, null, false, null),
        ]);
    }

}
