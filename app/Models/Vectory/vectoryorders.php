<?php
namespace App\Models\Vectory;
use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use App\Models\Vectory\vectoryOrderHeader;
use App\Order;
use Illuminate\Database\Eloquent\Model;

class vectoryorders  extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "order";
    protected $tablename = 'order';
    protected $guarded = [];
    protected $vectoryOrderHeader;
    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->vectoryOrderHeader =  new vectoryOrderHeader();
        parent::__construct($attributes);
    }
    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'order';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("BuyerOrderNumber", "int4", true, null, null, true, null),
            new Field("OrderReference", "varchar", true, null, null, false, null),
            new Field("megnevezes", "varchar", true, null, null, false, null),
            new Field("rovidnev", "varchar", true, null, null, false, null),
            new Field('created_at', "timestamp", true, null, null, false, null),
            new Field('updated_at', "timestamp", true, null, null, false, null),
        ]);
    }

}
