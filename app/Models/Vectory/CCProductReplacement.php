<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCProductReplacement extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCProductReplacement";
    protected $tablename = 'CCProductReplacement';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCProductReplacement';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "varchar", true, null, null, true, null),
            new Field("ReplacementProductId", "int4", true, null, null, false, null),
        ]);
    }

}
