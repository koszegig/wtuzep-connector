<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class vectoryOrderItem extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "Item";
    protected $tablename = 'Item';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'Item';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("GTIN", "varchar", true, null, null, false, null),
            new Field("BuyerItemID", "varchar", true, null, null, false, null),
            new Field("QuantityOrdered", "varchar", true, null, null, false, null),
            new Field("NameOfMeassure", "varchar", true, null, null, false, null),
            new Field("UnitOfMeassure", "varchar", true, null, null, false, null),
            new Field("ProductDescription", "varchar", true, null, null, false, null),
            new Field("UnitNetPrice", "varchar", true, null, null, false, null),
            new Field("UnitGrossPrice", "varchar", true, null, null, false, null),
            new Field("BuyerItemNum", "varchar", true, null, null, false, null),

        ]);
    }

}
