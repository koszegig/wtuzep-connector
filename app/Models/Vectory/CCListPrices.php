<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCListPrices extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCListPrices";
    protected $tablename = 'CCListPrices';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCListPrices';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "varchar", true, null, null, true, null),
            new Field("Price", "int4", true, null, null, false, null),
            new Field("FromDate", "varchar", true, null, null, false, null),
        ]);
    }

}
