<?php


namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class vectoryOrderHeader extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "OrderHeader";
    protected $tablename = 'OrderHeader';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'OrderHeader';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("BuyerOrderNumber", "int4", true, 'order_id', null, true, null),
            new Field("OrderReference", "varchar", true, 'order_id', null, false, null),
            new Field("OrderIssueDate", "timestamp", true, null, null, false, 'createdate'),
            new Field("OrderCurrencyCode", "varchar", true, 'order_currency_code', null, false, null),
            new Field('PaymentMethod', "varchar", true, null, null, false, null,'2'),//Kell kódtár
            new Field('DeliveryMethod', "varchar", true, null, null, false, null,'7'),//Kell kódtár
            new Field('OrderType', "timestamp", true, null, null, false, null,'A'),
            new Field('PurchasingInfo', "varchar", true, null, null, false, null,''),
            new Field('NetValue', "varchar", true, 'base_subtotal', null, false, null),
            new Field('GrossValue', "varchar", true, 'total_due', null, false,  'Brutto'),
            new Field('Storage', "varchar", true, null, null, false, null,'1'),
            new Field('PackageNumber', "varchar", true, null, null, false, null),//<!-- Csomagszám -->
            new Field('Status', "varchar", true, null, null, false, null,''),
            new Field('Shipper', "varchar", true, null, null, false, null),
            new Field('DateOfFinacialPerforming', "timestamp", true, null, null, false, null),
            new Field('ShipperDeliveryDate', "timestamp", true, null, null, false, null),
            new Field('PaymentStatus', "varchar", true, null, null, false, null),
            new Field('BankTranId', "varchar", true, null, null, false, null),
            new Field('CustomerGroup', "varchar", true, null, null, false, null),
            new Field('Administartor', "varchar", true, null, null, false, null),
        ]);
    }
    public function createdate ($row) {
        $d = new \DateTime();
        $createDate = $d->format('Y.m.d H:i:s');
        return $createDate;
        //return date("Y.m.d");
    }
    public function Brutto ($row) {
        $DocTotal =$row['total_due'];
        if (empty($row['total_due'])) {
            $DocTotal =$row['base_subtotal'];
        }
        return $DocTotal;
    }
}
