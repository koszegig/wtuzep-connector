<?php


namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class vectorySellerParty   extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "SellerParty";
    protected $tablename = 'SellerParty';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'SellerParty';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("GLN", "int4", true, null, null, true, null),
            new Field("Name", "varchar", true, null, null, false, null),
            new Field('Street', "varchar", true, null, null, false, null),
            new Field('PostalCode', "varchar", true, null, null, false, null),
            new Field('City', "timestamp", true, null, null, false, null),
            new Field('CountryCode', "varchar", true, null, null, false, null),
        ]);
    }

}
