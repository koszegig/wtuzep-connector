<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCStock extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCStock";
    protected $tablename = 'CCStock';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCStock';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "varchar", true, null, null, true, null),
            new Field("StorageId", "int4", true, null, null, false, null),
            new Field("StockQuantity", "varchar", true, null, null, false, null),
        ]);
    }

}
