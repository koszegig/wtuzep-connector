<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCPropertyAssignments extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCPropertyAssignments";
    protected $tablename = 'CCPropertyAssignments';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCPropertyAssignments';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "varchar", true, null, null, true, null),
            new Field("PropertyID", "int4", true, null, null, false, null),
            new Field("Value", "varchar", true, null, null, false, null),
        ]);
    }

}
