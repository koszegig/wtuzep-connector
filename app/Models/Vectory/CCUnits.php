<?php
namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCUnits extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCUnits";
    protected $tablename = 'CCUnits';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCUnits';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("unitid", "int4", true, null, null, true, null),
            new Field("unitname", "varchar", true, null, null, false, null),
            new Field("shortunitname", "varchar", true, null, null, false, null),
            new Field('created_at', "timestamp", true, null, null, false, null),
            new Field('updated_at', "timestamp", true, null, null, false, null),
        ]);
    }

}
