<?php

namespace App\Models\Vectory;

use App\Libraries\Field;
use App\Models\Base\BaseVectoryModel;
use Illuminate\Database\Eloquent\Model;

class CCProductImagePaths extends BaseVectoryModel
{
    /**
     * @var string
     */
    public $table = "CCProductImagePaths";
    protected $tablename = 'CCProductImagePaths';
    protected $guarded = [];

    protected function setStoredProcedure()
    {
        $this->storedProcedure = 'CCProductImagePaths';
    }

    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "varchar", true, null, null, true, null),
            new Field("Path", "varchar", true, null, null, false, null),
        ]);
    }

}
