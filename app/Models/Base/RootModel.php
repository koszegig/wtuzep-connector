<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
use App\Libraries\Field;
use \App\Traits\System\OwnBoot;
use \App\Traits\System\OwnCast;
use \App\Traits\System\HasCompositePrimaryKey;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class RootModel extends Model
{
    use OwnBoot, OwnCast, HasCompositePrimaryKey;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;
    /**
     * The connection associated with the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $szinkron = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

    protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $syncees = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * The fields that should be hidden for arrays.
     *
     * @var array
     */
    protected $fields = [];
    /**
     * The fields that should be hidden for arrays.
     *
     * @var array
     */
    public $isSynceed = false;
    /**
     * The cast that should be hidden for arrays.
     *
     * @var array
     */
    protected static function boot()
    {
        parent::boot();


        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
        static::creating(function ($model) {
        if (!is_array($model->primaryKey) &&  empty($model->{$model->getKeyName()}))
        $model->{$model->getKeyName()} = (string) \MyHash::genUUID();
        if (in_array('created_at', $model->dates))
        $model->created_by = \MyUser::getUserID();
        if (in_array('updated_at', $model->dates))
        $model->updated_by = \MyUser::getUserID();
        });
         */
        static::updating(function ($model) {

        if ($model->szinkron){
            $model->isSynceed = false;
            $dirty = $model->getDirty();

            foreach ($dirty as $field => $newdata)
            {
                if (in_array($field, $model->getSyncees())) {
                    $olddata = $model->getOriginal($field);
                    if ($olddata != $newdata)
                    {
                        $model->isSynceed = true;
                    }
                }
            }
            if ($model->isSynceed) {
             $model->synced = 0;
            }
        }
        });
    }

    protected $cast = [];

      /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->setFields();
        $this->initialTable();
        $this->processField();
        $this->initialHidden();
        parent::__construct($attributes);
        $this->initialAppends();
    }

    public function processField(){
      $this->primaryKey = [];

      foreach($this->fields as $field){
        $name = $field->getName();
        $this->setFillable($field, $name);
        $this->setSynceeable($field, $name);
        $this->setPrimaryKey($field, $name);
        $this->setCast($field, $name);
      }

      if(count($this->primaryKey) == 1)
      {
        $this->primaryKey = $this->primaryKey[0];
      }
    }

    /**
     * setFillable
     *
     * @param  Field $field
     * @param  string $name
     *
     * @return void
     */
    public function setFillable(Field $field, string $name){
      if($field->isFillable())
      {
        $this->fillable[] = $name;
      }
    }
    /**
     * setFillable
     *
     * @param  Field $field
     * @param  string $name
     *
     * @return void
     */
    public function setSynceeable(Field $field, string $name){
        if($field->isSynced())
        {
            $this->syncees[] = $name;
        }
    }
    /**
     * setFillable
     *
     * @param  Field $field
     * @param  string $name
     *
     * @return void
     */
    public function getSyncees(){

        return $this->syncees;
    }
    /**
     * setPrimaryKey
     *
     * @param  Field $field
     * @param  string $name
     *
     * @return void
     */
    public function setPrimaryKey(Field $field, string $name){
      if($field->isPrimary())
      {
        $this->primaryKey[] = $name;
      }
    }

    /**
     * setCast
     *
     * @param  Field $field
     * @param  string $name
     *
     * @return void
     */
    public function setCast(Field $field, string $name){
      $cast = $field->getCast();
      if($cast !== null)
      {
        $this->cast[$name] = $cast;
      }
    }

    /**
     * getFillable
     *
     * @return array
     */

    public function getFillable(){
      return $this->fillable;
    }

    public function initialTable(){}

    public function initialHidden(){}

    public function initialAppends(){}

    /**
     * Get the value of fields
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set the value of fields
     *
     * @return  self
     */
    public function setFields(){}
    /**
     * @param string $fieldValue
     * @return boolean
     */
    public function filledImportFields($row){
        $ImportItem = [];
        foreach($this->fields as $field){
            if (!empty($field->getDefaultvalue()) || $field->getDefaultvalue() =='0') {
                $ImportItem[$field->getName()] =$field->getDefaultvalue();
            }
            if (!empty($field->getImportFieldName()) ) {
                $arrow = @json_decode(json_encode($row), true);
                if (!empty($field->getCast())) {
                    $cast = $field->getCast();
                    $ImportItem[$field->getName()] = $this->$cast($arrow);
                } else {
                    $ImportItem[$field->getName()] = Arr::get($arrow, strtoupper($field->getImportFieldName()));
                }
            }
            if (!empty($field->getfieldLength())) {
                if (isset($ImportItem[$field->getName()])) {
                    $ImportItem[$field->getName()] = Str::substr($ImportItem[$field->getName()],0, $field->getfieldLength()); ;
                }
            }
        }
        return $ImportItem;
    }
}
