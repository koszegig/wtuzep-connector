<?php


namespace App\Models\Base;
use App\Models\Base\BaseModel;

class BaseConnectorModel  extends BaseModel
{
    /**
     * The connection associated with the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

}
