<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use danielme85\LaravelLogToDB\Models\LogToDbCreateObject;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;

class CustomLog extends BaseConnectorModel
{
    use LogToDbCreateObject;

    protected $table = 'log';
    //protected $connection = 'mysql';
    //
    protected $tablename='log';

    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'log';
    }
    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field("message", "varchar", true,null, null,false, null),
            new Field("channel", "varchar", true,null, null,false, null),
            new Field("level", "int4", true,null, null,false, null),
            new Field("level_name", "varchar", true,null, null,false, null),
            new Field("unix_time", "int4", true,null, null,false, null),
            new Field("datetime", "date", true,null, null,false, null),
            new Field("context", "varchar", true,null, null,false, null),
            new Field("extra", "varchar", true,null, null,false, null),
            new Field('created_at', "date", true,null, null,false, null),
            new Field('updated_at', "date", true,null, null,false, null),
        ]);
    }
}
