<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

class ImportProductPictures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:pictures
                                    {limit=0 : limit count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import product pictures from JDE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $options = $this->option();
        $_argumentum['limit'] = str_replace('limit=','',$_argumentum['limit']);
        \App\Jobs\ImportProductPictures::dispatch($_argumentum,$options);
    }
}
