<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportAttributes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:attributes
                                {limit=0 : limit count}
                                {--deleteattributtebvalues : description}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export attributes to Magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //return \App\Jobs\ExportAttributes::dispatch();
        $_argumentum = $this->argument();
        $options = $this->option();
        if (!isset($_argumentum['limit'])){
            $_argumentum['limit'] = 0;
        }
        $_argumentum['limit'] = str_replace('limit=','',$_argumentum['limit']);
        //
        \App\Jobs\ExportAttributes::dispatch($_argumentum,$options);
    }
}
