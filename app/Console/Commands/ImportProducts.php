<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:products
                            {limit=0 : limit Product count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products from XML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argumentum = $this->argument();
        $options = $this->option();
        \App\Jobs\ImportProducts::dispatch($argumentum,$options);
        //return \App\Jobs\ImportProducts::dispatch();
    }
}
