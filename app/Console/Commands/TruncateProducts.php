<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TruncateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:truncate:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate products from magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        //return \App\Jobs\TruncateProducts::dispatch();
        \App\Jobs\TruncateProducts::dispatch();
    }
}
