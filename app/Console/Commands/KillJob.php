<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;

class KillJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:kill:job
                                {tasktype=0 : limit count} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $tasktype = str_replace('tasktype=','',$_argumentum['tasktype']);

        if ($tasktype === '0') {
            $tasks = Task::all();
            foreach ($tasks as $task) {
               if ($task->PID !=0) {
                   Task::setStopped($task->task_type);
                   if (file_exists( "/proc/$task->PID" )){
                         echo $task->task_type . ' Kill Run '.$task->PID;
                         shell_exec("kill -9 $task->PID");
                       Task::stop($task->task_type,0,0,500);
                   } else{
                       Task::stop($task->task_type,0,0,500);
                   }
               }
            }

        } else {
            Task::setStopped($tasktype);
        }
    }
}
