<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CustomLog;

class logsCLear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Logs file clear';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Delete logs \n\t';
        CustomLog::truncate();
        exec('rm ' . storage_path('logs/*.log'));
    }
}
