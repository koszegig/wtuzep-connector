<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Aion\Connect\SyncStat;

class SyncStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $stats = new SyncStat;
        $s = $stats->getStats();

        $this->table(['table','total','pending','synced','errors'], $s);
    }
}
