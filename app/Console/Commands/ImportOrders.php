<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:orders
                            {limit=0 : limit order count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import orders from Magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $argumentum = $this->argument();
        $options = $this->option();
       \App\Jobs\ImportOrders::dispatch($argumentum,$options);
    }
}
