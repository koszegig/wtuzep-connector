<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:orders
                            {limit=0 : limit order count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export orders to magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     //   return \App\Jobs\ExportOrders::dispatch();
        $argumentum = $this->argument();
        $options = $this->option();
          \App\Jobs\ExportOrders::dispatch($argumentum,$options);
    }
}
