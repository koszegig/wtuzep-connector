<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:categories
                                {limit=0 : limit count}
                                {--product : description}
                                {--disable_empty_categories : description}
                                ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export categories to Magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $options = $this->option();
        $task = '';
        $_argumentum['limit'] = str_replace('limit=','',$_argumentum['limit']);
        if ($options['disable_empty_categories']) {
            $task = 'disable_empty_categories';
        }

         \App\Jobs\ExportCategories::dispatch($_argumentum,$options,$task);
    }
}
