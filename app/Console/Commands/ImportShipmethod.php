<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ImportShipmethods;

class ImportShipmethod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:shipmethod
                                {limit=0 : limit count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //return \App\Jobs\ImportCategories::dispatch();
        $_argumentum = $this->argument();
        $options = $this->option();
        $_argumentum['limit'] = str_replace('limit=','',$_argumentum['limit']);
        ImportShipmethods::dispatch($_argumentum,$options);
    }
}
