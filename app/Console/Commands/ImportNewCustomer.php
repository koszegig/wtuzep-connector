<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportNewCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:newcustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $argumentum = $this->argument();
        $options = $this->option();
        \App\Jobs\ImportNewCustomer::dispatch($argumentum,$options);
    }
}
