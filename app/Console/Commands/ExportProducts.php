<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:products
                            {limit=0 : limit Product count}
                            {--onlysimple : description}
                            {--onlyvirtual : description}
                            {--onlyconfigurable : description}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export products to Magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $options = $this->option();

        $options['all'] = false;
        if (!$options['onlysimple'] && !$options['onlyvirtual'] && !$options['onlyconfigurable'] ) {
            $options['all'] = true;
        }
         \App\Jobs\ExportProducts::dispatch($_argumentum,$options);
        //return \App\Jobs\ExportProducts::dispatch();
    }
}
