<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ExportStocksqtys;

class ExportStocksqty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:stocksqty
                            {limit=0 : limit Product count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export multi qty ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $argumentum = $this->argument();
        $options = $this->option();
        ExportStocksqtys::dispatch($argumentum,$options);
        //return \App\Jobs\ExportProducts::dispatch();
    }
}
