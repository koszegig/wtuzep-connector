<?php

namespace App\Console;

use App\Jobs\ExportProducts;
use App\Jobs\ExportStocksqtys;
use App\Jobs\ImportPayments;
use App\Jobs\ImportNewCustomer;
use App\Jobs\ImportOrders;
use App\Jobs\ImportProducts;
use App\Jobs\ImportAttributes;
use App\Jobs\ImportCategories;
use App\Jobs\ImportShipmethods;
use App\Jobs\ImportProductPricies;
use App\Jobs\ImportStocksqtys;
use App\Jobs\ImportProductPictures;
use App\Jobs\ExportOrders;
use App\Jobs\ExportCategories;
use App\Jobs\ExportAttributes;
use App\Jobs\ImportManagerSources;
use App\Jobs\ExportNewCustomer;
use App\Task;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\CustomLog;
use voku\helper\ASCII;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        if (env('CRONTAB', 0) ==1) {
            $schedule->command('logs:clear')->cron("*/40 * * * *");

//Import

            $argumentum['limit'] =env('LIMIT', 0);
            $options =[];
            $ImportjobLists=[
                ['job' =>new ImportAttributes($argumentum, $options),
                  'jobname' =>  'ImportAttributes'
                ],
                ['job' =>new ImportCategories($argumentum, $options),
                  'jobname' =>  'ImportCategories'
                ],
                ['job' =>new ImportProductPricies($argumentum, $options),
                    'jobname' =>  'ImportProductPricies'
                ],
                ['job' =>new ImportNewCustomer($argumentum, $options),
                    'jobname' =>  'ImportNewCustomer'
                ],
                ['job' =>new ImportPayments($argumentum, $options),
                  'jobname' =>  'ImportPayments'
                ],
                ['job' =>new ImportProductPictures($argumentum, $options),
                  'jobname' =>  'ImportProductPictures'
                ],
                ['job' =>new ImportProducts($argumentum, $options),
                  'jobname' =>  'ImportProducts'
                ],
                ['job' =>new ImportStocksqtys($argumentum, $options),
                  'jobname' =>  'ImportStocksqtys'
                ],
                ['job' =>new ImportShipmethods($argumentum, $options),
                  'jobname' =>  'ImportShipmethods'
                ],
                ['job' =>new ImportManagerSources($argumentum, $options),
                    'jobname' =>  'ImportManagerSources'
                ],
                ['job' =>new ImportOrders($argumentum, $options),
                    'jobname' =>  'ImportOrders'
                ]
            ];

            /*   foreach ($ImportjobLists as $jobList) {
                   $this->jobRuning($schedule,$jobList['job'],$jobList['jobname']);
               }*/
            $ImportCommandLists=[
                ['command' =>'connect:import:attributes ',
                    'jobname' =>  'ImportAttributes'
                ],
                ['command' =>'connect:import:categories',
                    'jobname' =>  'ImportCategories'
                ],
                ['command' =>'connect:import:productprices',
                    'jobname' =>  'ImportProductPricies'
                ],
                ['command' =>'connect:import:managersource',
                    'jobname' =>  'ImportManagerSources'
                ],
                ['command' => 'connect:import:shipmethod',
                    'jobname' =>  'ImportShipmethods'
                ],
                ['command' =>  'connect:import:newcustomer',
                    'jobname' =>  'ImportNewCustomer'
                ],
                ['command' =>'connect:import:products',
                    'jobname' =>  'ImportProducts'
                ],
                ['command' =>'connect:import:pictures',
                    'jobname' =>  'ImportProductPictures'
                ],
                ['command' =>'connect:import:stocksqty',
                    'jobname' =>  'ImportStocksqtys'
                ],
                ['command' =>'connect:import:payments',
                    'jobname' =>  'ImportPayments'
                ],
                ['command' =>'connect:import:orders',
                    'jobname' =>  'ImportOrders'
                ]
            ];
            foreach ($ImportCommandLists as $CommandList) {
                   $this->CommandRuning($schedule,$CommandList['command'],$CommandList['jobname']);
            }
            $ExportjobLists=[
                ['job' =>new ExportAttributes($argumentum, $options),
                    'jobname' =>  'ExportAttributes'
                ],
                ['job' =>new ExportCategories($argumentum, $options),
                    'jobname' =>  'ExportCategories'
                ],
                ['job' =>new ExportProducts($argumentum, $options),
                    'jobname' =>  'ExportProducts'
                ],
                ['job' =>new ExportStocksqtys($argumentum, $options),
                    'jobname' =>  'ExportStocksqtys'
                ],
                ['job' =>new ImportOrders($argumentum, $options),
                    'jobname' =>  'ExportOrders'
                ]

            ];

            /*   foreach ($ImportjobLists as $jobList) {
                   $this->jobRuning($schedule,$jobList['job'],$jobList['jobname']);
               }*/
            $ExportCommandLists=[
                ['command' =>'connect:export:attributes',
                    'jobname' =>  'ExportAttributes'
                ],
                ['command' =>'connect:export:categories',
                    'jobname' =>  'ExportCategories'
                ],
                ['command' =>'connect:export:products',
                    'jobname' =>  'ExportProducts'
                ],
                ['command' => 'connect:export:stocksqty',
                    'jobname' =>  'ExportStocksqtys'
                ],
                ['command' =>'connect:export:orders',
                    'jobname' =>  'ExportOrders'
                ]
            ];
            foreach ($ExportCommandLists as $CommandList) {
                $this->CommandRuning($schedule,$CommandList['command'],$CommandList['jobname']);
            }

        }
    }
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @param                                         $job
     * @param                                         $jobname
     */
    protected function jobRuning(Schedule $schedule, $job, $jobname)
    {

        echo Task::getCron($jobname).' \n\t';
        echo $job.' \n\t';
        if (Task::getPID($jobname) == 0 ) {
          $schedule->job($job)->cron(Task::getCron($jobname))->onOneServer()->runInBackground();
        } else{
            echo $jobname . ' is Run \n\t';
           // die;
            $pid = Task::getPID($jobname);
            if (file_exists( "/proc/$pid" )){
                echo $jobname . ' Kill Run '.$pid;
                shell_exec("kill -9 $pid");
            }
            Task::stop($jobname, 0, 0, 500);
           // $schedule->job($job)->cron(Task::getCron($jobname))->onOneServer()->runInBackground();
        }
    }

    /**
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @param                                         $command
     * @param                                         $jobname
     */
    protected function CommandRuning(Schedule $schedule, $command, $jobname)
    {
        $path = storage_path('logs').'/';
        $stopCron = Task::getStopCron($jobname);
        echo Task::getCron($jobname);
        echo $stopCron;
        echo $command.' \n\t';;
        $limit = env('LIMIT', 0);
        $limit =Task::getTaskLimit($jobname,$limit);
        $command = $command.' '.$limit;
        echo $command.' \n\t';;
        $stopcommand = 'connect:kill:job tasktype='.$jobname;
        if (Task::getPID($jobname) == 0 ) {
            $schedule->command($command)->cron(Task::getCron($jobname))->onOneServer()->runInBackground()->before(function () {
                echo ' start';
            })
                ->after(function () {
                    echo ' stop';;
                })->sendOutputTo($path.$jobname.'sch.log');;
            if($stopCron !='-') {
                $schedule->command($stopcommand)->cron($stopCron)->onOneServer()->runInBackground()->before(function () {
                    echo ' Kill start';
                })
                    ->after(function () {
                        echo ' stop';;
                    })->sendOutputTo($path.'StopKillsch.log');;
            }
        } else{
            echo $jobname . ' is Run \n\t';;
            $pid = Task::getPID($jobname);
            if (file_exists( "/proc/$pid" )){
                //  echo $jobname . ' Kill Run '.$pid;
                //  shell_exec("kill -9 $pid");
            } else{
                Task::stop($jobname,0,0,500);
            }
            /*  $schedule->command($command)->cron(Task::getCron($jobname))->onOneServer()->runInBackground()->before(function () {
                  echo ' start \n\t';;
              })
                  ->after(function () {
                      echo ' stop  \n\t';;;
                  })->sendOutputTo($path.$jobname.'sch.log');;*/
        }
    }
}
