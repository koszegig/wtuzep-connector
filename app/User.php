<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\HasGroups;
use Laravel\Passport\HasApiTokens;

class User extends BaseModel  implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    JWTSubject
{
    use Notifiable, Authenticatable, Authorizable, CanResetPassword, HasRoles, HasGroups;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->table = 'users';
        array_push($this->fillable, 'firstname', 'middlename', 'username', 'lastname', 'birth_date', 'password',  'active', 'system');
        array_push($this->hidden, 'password', 'remember_token');
        parent::__construct($attributes);
        //array_push($this->appends, 'primary_email', 'primary_phone', 'name', 'organizations');
    }
    /**
     * Check whether the user has the requested role.
     */
    public function getNameAttribute()
    {
        $name = $this->firstname;
        $name .= " ";
        if (!is_null($this->middlename) &&  !empty($this->middlename)) {
            $name .= $this->middlename;
            $name .= " ";
        }
        $name .= $this->lastname;
        return trim($name);
    }


    ///

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    ///
}
