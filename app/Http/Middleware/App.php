<?php

namespace App\Http\Middleware;

use Closure;

class App
{
  /**
   * Handle an incoming request.
   *
   * @return void
   */
  public function handle($request, Closure $next)
  {
    if (session()->has('locale')) {
      \App::setLocale(session('locale'));
    }

    return $next($request);
  }
}
