<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Libraries\Database\QueryBuilderSP;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $model;
    protected $select;

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model::destroy($id);
        return response()->success('success');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        return $this->model::find($id)->modifyActive(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function inactivate($id)
    {
        return $this->model::find($id)->modifyActive(false);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listForDn(Request $request)
    {
        $dmdata = $this->getDnData($request->all());
        $dmdata =  $dmdata->map(
            function ($item) {
                return $item->only($this->dncol);
            }
        );
        return response()->success(compact('dmdata'));
    }
    protected function getDnData($condition)
    {
        $this->getModelname();
        if(empty($condition))
            return $this->model::all();
        else
            $cond = \MyArray::assocToArray($condition);
        return $this->model::where($cond)->get();
    }
    public function getFields()
    {
        $model = new $this->model();
        return $model->getFields();
    }
    public function find($id)
    {
        return $this->model::find($id);
    }

    public function get($id){
        return $this->find($id);
    }
    protected function getModelname()
    {
        $model = explode('\\', $this->model);
        return array_last($model);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dmdata = $this->get($id);
        $dmdata = $this->postGet($dmdata);
        return response()->success($dmdata);
    }
    public function postGet($dmdata)
    {
        return $dmdata;
    }

    public function postStore($dmdata, Request $request)
    {
        return $this->show($dmdata->id);
    }

    public function postUpdate($dmdata, Request $reques)
    {
        return $this->postStore($dmdata, $reques);
    }

    public function preStore($data, Request $request)
    {
        return $data;
    }

    public function preUpdate($data, Request $request)
    {
        return $this->preStore($data, $request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validator($data);
        $data = $this->preStore($data, $request);
        $dmdata = $this->create($data);
        $dmdata = $this->postStore($dmdata, $request);
        return $dmdata;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $this->validator($data);
        $data = $this->preUpdate($data, $request);
        $dmdata = $this->edit($id, $data);
        $dmdata = $this->postUpdate($dmdata, $request);
        return $dmdata;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        $data = $this->getCreateData($data);
        return $this->model::create($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $data)
    {
        $data = $this->getUpdateData($data);
        $dmdata = $this->find($id);
        foreach ($data as $key => $value) {
            $dmdata->$key = $value;
        }
        $dmdata->save();
        return $dmdata;
    }



    protected function getDbData($_data)
    {
        $data = [];
        $model = new $this->model();
        foreach($model->getFillable() as $field){
            $value = Arr::get($_data, $field, null);
            if($value !== null){
                $data[$field] = $value;
            }
        }
        return $data;
    }

    protected function getCreateData($data)
    {
        return $this->getDbData($data);
    }

    protected function getUpdateData($data)
    {
        return $this->getDbData($data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return true;
    }

    protected function filter()
    {
        return QueryBuilder::for($this->model)->get();
    }

    protected function selectStoredProcedure($model, $request, $select, $paginate = null, $params = [])
    {
        if(!isset($request->filters)) $request->filters = [];
        $this->setFilters($request);
        $builder = new QueryBuilderSP($model, $request, $paginate, $select, $params);
        return $builder->get();
    }

    public function setFilters(Request &$request){
        return;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        //$select = '*';

        $dmdata = $this->selectStoredProcedure($this->model, $request, $this->select, $size);
        return response()->success($dmdata);
    }


}
