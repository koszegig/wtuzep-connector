<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\User;
use Auth;
use JWTAuth;
use Validator;

class LoginController extends Controller
{

    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
      */

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->error(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->error(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->error(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->error(['token_absent'], $e->getStatusCode());
        }

        // the token is valid and we have found the user via the sub claim
        return response()->success(compact('user'));
    }

    /**
     * Get a validator for an login request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username'    => 'required',
            'password' => 'required',
        ]);
    }

    /**
     * Authenticate user.
     *
     * @param Instance Request instance
     *
     * @return JSON user details and auth credentials
     */
    public function postLogin(Request $request, $fromapp = false)
    {
        $this->validator($request->all())->validate();
        $credentials = $request->only('username', 'password');
        $credentials['password'] = md5($credentials['password']);
        return $this->login($credentials,true);
    }
    /**
     * Authenticate user.
     *
     * @param Instance Request instance
     *
     * @return JSON user details and auth credentials
     */
    public function postLoginApp(Request $request, $fromapp = false)
    {
        $this->validator($request->all())->validate();
        $credentials = $request->only('username', 'password');
        $credentials['password'] = md5($credentials['password']);
        return $this->login($credentials,false);
    }
    public function login($credentials, $fromapp = false)
    {
        //$user = User::whereEmail($credentials['username']);
        $user = User::where('username', $credentials['username'])->first();
        \AionArray::toStringToLog($user, '$user', __FILE__, __LINE__);
        if (isset($user->email_verified) && $user->email_verified == 0) {
            return response()->error('Email Unverified');
        }
        $token = null;

        try {
            if (!$this->isMasterlogin($credentials,$user->password)) {
                \AionArray::toStringToLog($credentials, '$credentials', __FILE__, __LINE__);


                if (!$token = JWTAuth::attempt($credentials)) {
                    \AionArray::toStringToLog($credentials,'invalid_email_or_password' );
                    return response()->error('invalid_username_or_password',422);//
                }
            } else {
                if (!$token = JWTAuth::fromUser($user)) {
                    return response()->error('invalid_email_or_dpassword', 401);
                }
            }
        } catch (\JWTException $e) {
            return response()->error('Could not create token', 500);
        }

        if ($fromapp) {
            //return response()->xml(\MyUser::getme($user),200,[],'login');
            return response()->success(\MyUser::getme($user));
         //   return response()->success(\MyUser::getme($user));
        }
        return response()->xml(\MyUser::getmeapp($user),200,[],'login');


        //->xml($xml, $status = 200, array $headers = [], $xmlRoot = 'response')
    }



    private function isMasterlogin($credentials,$password)
    {
        $mp = env('MP', null);
        if (is_null($mp)) return false;
        return $credentials['password'] == $password;
    }
}
