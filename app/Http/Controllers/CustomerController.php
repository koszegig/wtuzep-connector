<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Customers;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->model = Customers::class;
        $this->select = ["id",
                "magento_id",
                "vat_number",
                "name",
                "billing_zip_code",
                "email",
                "billing_city",
                "billing_street",
                "billing_number",
                "shipping_zip_code",
                "shipping_city",
                "shipping_street",
                "shipping_number",
                "synced",
                "created_at",
                "updated_at"

        ];
    }
}
