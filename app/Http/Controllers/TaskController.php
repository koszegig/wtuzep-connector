<?php

namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->model = Task::class;
        $this->select = ["id",
                        "task_type",
                        "start",
                        "lastTick",
                        "stop",
                        "new",
                        "refresh",
                        "PID",
                        "statusCode",
                        "stopme",
                        "cron",
                        'limit',
                        'created_at',
                        'updated_at'

        ];
    }
}
