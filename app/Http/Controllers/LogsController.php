<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomLog;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->model = CustomLog::class;
        $this->select = ["id",
            "message",
            "channel",
            "level",
            "level_name",
            "unix_time",
            "datetime",
            "context",
            "extra",
            'created_at',
            'updated_at'
        ];
    }
}
