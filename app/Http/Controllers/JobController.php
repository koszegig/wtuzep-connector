<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ExportProducts;

class JobController extends Controller
{
    public function processQueue()
    {
        $argumentum['limit'] =10;
        $options =[];
        dispatch(new ExportProducts($argumentum, $options));
        echo 'Export product';
    }
}
