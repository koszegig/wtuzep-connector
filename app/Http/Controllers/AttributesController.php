<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class AttributesController extends Controller
{
    public function __construct()
    {
        $this->model = Attribute::class;
        $this->select = ["id",
            "code",
            "name",
            "type",
            "magento_id",
            'created_at',
            'updated_at',
            'synced'
        ];
    }
}
