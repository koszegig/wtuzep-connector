<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StocksQty;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StocksqtysController extends Controller
{
    public function __construct()
    {
        $this->model = StocksQty::class;
        $this->select = ["id",
            "ProductId",
            "sku",
            "StockId",
            "source_code",
            "TotalStock",
            "magento_id",
            'created_at',
            'updated_at',
            'synced'
        ];
    }
}
