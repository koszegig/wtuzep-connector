<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use XmlValidator\XmlValidator;
use Vyuldashev\XmlToArray\XmlToArray;
use \App\Aion\Library\XMLManager ;


class CategoriesController extends Controller
{
            public function __construct()
        {
            $this->model = Category::class;
            $this->select = ["id",
                        "erp_id",
                        "category_magento_id",
                        "name",
                        "position",
                        "status",
                        "synced",
                        "created_at",
                        "updated_at",
                        "erp_parent_id",
                        "parent_id",
                        "url_key",
                        "group_id",
                        "group_erp_id",


            ];
        }
}
