<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\CatalogRule;
use App\Category;
use App\Order;
use App\OrderItem;
use App\Partner;
use App\Product;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use XmlValidator\XmlValidator;
use Vyuldashev\XmlToArray\XmlToArray;
use \App\Aion\Library\XMLManager ;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->model = Product::class;
        $this->select = ["id",
            "cikkszam",
            "vonalkod",
            "gyartoicikkszam",
            "gyartommegnevezese",
            "termekmegnevezese",
            "garancia",
            "termekleirasa",
            "keplink",
            "szin",
            "cikkcsoportkod",
            "termekkategoriakod",
            "termekrogzitesdatuma",
            "kifuto",
            "doboztartalom",
            "sim",
            "ram",
            'rom',
            'memoriabovithetoseg',
            'kijelzomeret',
            'kijelzofelbontas',
            'hatlapikamerafelbontas',
            'elolapikamerafelbontas',
            'processzormag',
            'operaciosrendszer',
            'akkumeret',
            'toltocsatlakozo',
            'vezeteknelkultoltheto',
            'dobozbruttosuly',
            'kapcsolodotermekek',
            'kisker',
            'nagyker',
            'created_at',
            'updated_at',
            'synced',
            'syncedorder',
            'magento_id',
            'price',
            'total_qty',
            'parent_id',
            'type_id',
            'enabled',
            'active'

        ];
    }
}
