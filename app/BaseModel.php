<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\UserController;

class BaseModel extends Model
{
   use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         */
        static::creating(function ($model) {
            \AionArray::toStringToLog($model);
            if (!is_array($model->primaryKey) &&  empty($model->{$model->getKeyName()}))
                $model->{$model->getKeyName()} = (string)$model->generateNewId();
            if (in_array('created_at', $model->dates))
                $model->created_by = $model->getUserID();
            if (in_array('updated_at', $model->dates))
                $model->updated_by = $model->getUserID();
        });

        static::updating(function ($model) {
            if (in_array('updated_at', $model->dates))
                $model->updated_by = $model->getUserID();
        });
    }

    public function save(array $options = [])
    {
        if (!is_array($this->getKeyName())) {
            return parent::save($options);
        }

        // Fire Event for others to hook
        if ($this->fireModelEvent('saving') === false) return false;

        // Prepare query for inserting or updating
        $query = $this->newQueryWithoutScopes();

        // Perform Update
        if ($this->exists) {
            if (count($this->getDirty()) > 0) {
                // Fire Event for others to hook
                if ($this->fireModelEvent('updating') === false) {
                    return false;
                }

                // Touch the timestamps
                if ($this->timestamps) {
                    $this->updateTimestamps();
                }

                //
                // START FIX
                //


                // Convert primary key into an array if it's a single value
                $primary = (count($this->getKeyName()) > 1) ? $this->getKeyName() : [$this->getKeyName()];

                // Fetch the primary key(s) values before any changes
                $unique = array_intersect_key($this->original, array_flip($primary));

                // Fetch the primary key(s) values after any changes
                $unique = !empty($unique) ? $unique : array_intersect_key($this->getAttributes(), array_flip($primary));

                // Fetch the element of the array if the array contains only a single element
                //$unique = (count($unique) <> 1) ? $unique : reset($unique);

                // Apply SQL logic
                $query->where($unique);

                //
                // END FIX
                //

                // Update the records
                $query->update($this->getDirty());

                // Fire an event for hooking into
                $this->fireModelEvent('updated', false);
            }
        }
        // Insert
        else {
            // Fire an event for hooking into
            if ($this->fireModelEvent('creating') === false) return false;

            // Touch the timestamps
            if ($this->timestamps) {
                $this->updateTimestamps();
            }

            // Retrieve the attributes
            $attributes = $this->attributes;

            if ($this->incrementing && !is_array($this->getKeyName())) {
                $this->insertAndSetId($query, $attributes);
            } else {
                $query->insert($attributes);
            }

            // Set exists to true in case someone tries to update it during an event
            $this->exists = true;

            // Fire an event for hooking into
            $this->fireModelEvent('created', false);
        }

        // Fires an event
        $this->fireModelEvent('saved', false);

        // Sync
        $this->original = $this->attributes;

        // Touches all relations
        if (array_get($options, 'touch', true)) $this->touchOwners();

        return true;
    }

    /**
     * Get a new version 4 (random) UUID.
     *
     * @return \Rhumsaa\Uuid\Uuid
     */
    public static function generateNewId()
    {
        $uuid4 = Uuid::uuid4();
        return $uuid4->toString();
    }

    private static function getUserID()
    {
        return \MyUser::getUserID();
    }

    private static function getCurrentUser()
    {
        $UserController = new UserController();
        return $UserController->_getMe();
    }

    public function setAsActive()
    {
        return $this->modifyActive(true);
    }

    public function setAsInActive()
    {
        return $this->modifyActive(false);
    }

    public function modifyActive($_active)
    {
        $this->active = $_active;
        if ($this->save()) {
            return $this;
        }
        return false;
    }

    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }



    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeOrderbyname($query)
    {
        return $query->orderBy('name');
    }

    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->fillable, (array)$value));
    }

    public function getFields($fullType = true)
    {
        $fieldAndTypeList = [];
        $dbprefix = env('DB_PREFIX', null);
        foreach (\DB::select("SELECT COLUMN_NAME,DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_NAME = '$dbprefix" . "$this->table'") as $field) {
            $fieldAndTypeList[$field->column_name] = $field->data_type;
        }

        return $fieldAndTypeList;
    }

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                return new BaseCollection($this->fromJson($value));
            case 'date':
                return $this->asDate($value);
            case 'datetime':
            case 'custom_datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimestamp($value);
            case 'image':
                return $this->castImage($value);
            default:
                return $value;
        }
    }

    /**
     * The answer that belong to the question.
     */
    protected function castImage($value)
    {

    }
}
