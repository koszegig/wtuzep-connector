<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseConnectorModel;
use App\Libraries\Field;
use DB;

class payment  extends BaseConnectorModel
{
    //
    protected $tablename='payment';
    /**
     * @var string
     */
    public $table="payment";

    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = env('DB_PREFIX', '').'payment';
    }

    public function setFields(){
        $this->fields = collect( [
            new Field("id", "int4", true,null, null,true, null),
            new Field('paymentkey', "Varchar", true,null, null,false, null),
            new Field('paymentvalue', "Varchar", true,'FizetesiModMegnevezes', null,false, null),
            new Field('erp_id', "Varchar", true,'FizetesiModKod', null,false, null),
            new Field('created_at', "timestamp", true,null, null,false, null),
            new Field('updated_at', "timestamp", true,null, null,false, null),
            new Field('synced', "bool", true,null, null,false, null),
            new Field('magento_id', "varchar", true,null, null,false, null),
        ]);
    }
}
