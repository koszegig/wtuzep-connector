<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //             // sku erp_id order_qty reserve_qty qty shop_id\


    protected $fillable = [
        'erp_id',
        'shop_id',
        'sku',
        'order_qty',
        'reserve_qty',
        'qty',
        'synced'
    ];
}
