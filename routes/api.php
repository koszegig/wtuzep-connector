<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
            $api->group(['namespace' => 'Auth'], function ($api) {
                $api->group(['prefix' => 'auth'], function ($api) {
                    $api->post('/login', 'LoginController@postLogin');
                    $api->post('/login/app', 'LoginController@postLoginApp');
                });
                $api->group(['prefix' => 'user'], function ($api) {
                    $api->post('/registration', 'RegisterController@register');
                });
            });
            /*$api->group(['prefix' => 'host'], function ($api) {
                $api->post('/data', 'HostDataController@getHostData');
            });*/


        $api->resource('/task', 'TaskController');
        $api->resource('/product', 'ProductController');
        $api->resource('/stocksqty', 'StocksqtysController');
        $api->resource('/attributes', 'AttributesController');
        $api->resource('/log', 'LogsController');
        $api->resource('/categorie', 'CategoriesController');
        $api->resource('/order', 'OrderController');
        $api->resource('/customer', 'CustomerController');
    });
});
